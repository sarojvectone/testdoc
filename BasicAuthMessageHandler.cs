﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using unifiedringmyaccountwebapi;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;

namespace unifiedringmyaccountwebapi
{
    public class BasicAuthMessageHandler : DelegatingHandler
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        #region Declaration
        private const string BasicAuthResponseHeader = "WWW-Authenticate";
        private const string BasicAuthResponseHeaderValue = "Basic";
        string projectValue = string.Empty;
        string projectKey = string.Empty;
        private string GeneratedToken;
        string UrlKey = string.Empty;
        public IProvidePrincipal PrincipalProvider { get; set; }
        private string ProjectName;
        static bool IsNullOrEmpty(string[] myStringArray)
        {
            return myStringArray == null || myStringArray.Length < 1;
        }
        #endregion

        #region Code
        protected override System.Threading.Tasks.Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            Log.Info("-------------------------------------------------------");
            Log.Info("API - Request :" + request.RequestUri.Segments[request.RequestUri.Segments.Length -1].ToString());
            //Getting Authorization Info
            #region Authorization Info   
            string[] urlKeys = request.RequestUri.Segments;
            UrlKey = urlKeys[3].Trim('/');
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            AuthenticationHeaderValue authValue = request.Headers.Authorization;
            var Namespace = MethodBase.GetCurrentMethod().DeclaringType.Namespace;
            
            if (UrlKey == "Unifiedringmyaccountwebapiinserttokenmaster")
            {
                string jsonContent = request.Content.ReadAsStringAsync().Result;
                var data = (JObject)JsonConvert.DeserializeObject(jsonContent);
                if (data != null)
                projectValue = data["Proj_ID"].Value<string>();
            }
                

            if (authValue != null)
            {
                ProjectName = Base64Decode(authValue.Parameter);
                int len = ProjectName.Trim().Length;
                ProjectName = ProjectName.Substring(6, len - 10);
            }

            #region jsonContent UnusedCode
            //string jsonContent = request.Content.ReadAsStringAsync().Result;
            //if (!String.IsNullOrEmpty(jsonContent))
            //{
            //    Dictionary<object, object> getProjectID = JsonConvert.DeserializeObject<Dictionary<object, object>>(jsonContent);              
            //    foreach (KeyValuePair<object, object> projID in getProjectID)
            //    {
            //        projectKey = Convert.ToString(projID.Key);
            //        if (projectKey == "Proj_ID")
            //        {
            //            projectValue = Convert.ToString(projID.Value);
            //            break;
            //        }
            //    }
            //}
            #endregion
            #endregion
            //Authorization validation process
            #region Authorization
            if (authValue != null && !String.IsNullOrWhiteSpace(authValue.Parameter) && !IsNullOrEmpty(urlKeys) && UrlKey == "Unifiedringmyaccountwebapiinserttokenmaster")
            {
                try
                {
                        Credentials parsedCredentials = ParseAuthorizationHeader(authValue.Parameter, UrlKey, projectValue);
                        if (parsedCredentials != null)
                        {
                            Thread.CurrentPrincipal = PrincipalProvider.CreatePrincipal(parsedCredentials.Username, parsedCredentials.Password, parsedCredentials.UrlKey);
                        }   
                }
                catch (Exception ex)
                {

                }
            }
            else if (authValue == null && Namespace != null && UrlKey != "Unifiedringmyaccountwebapiinserttokenmaster")
            {
                try 
                {
                    UrlKey = aes_Convert(UrlKey);
                    Thread.CurrentPrincipal = PrincipalProvider.ReturnTokenStatus(Namespace, UrlKey);
                }
                catch (Exception ex)
                {
                
                }
            }
           

            
            return base.SendAsync(request, cancellationToken) 
                .ContinueWith(task =>
                {
                   string test = cancellationToken.ToString();
                    var response = task.Result; 
                    if (response.StatusCode == HttpStatusCode.Unauthorized
                        && !response.Headers.Contains(BasicAuthResponseHeader))
                    {
                        test = response.ReasonPhrase.ToString();
                        response.Headers.Add(BasicAuthResponseHeader
                                             , BasicAuthResponseHeaderValue);
                        Log.Info("API - Response :" + request.RequestUri.Segments[request.RequestUri.Segments.Length -1].ToString() + "-"+response.ReasonPhrase.ToString());
                    }
                    Log.Info("API - Response :" + request.RequestUri.Segments[request.RequestUri.Segments.Length -1].ToString()+"-"+response.ReasonPhrase.ToString());
                    Log.Info("-------------------------------------------------------");
                    return response;
                });
            #endregion
        }

        #region ParseAuthorization
        private Credentials ParseAuthorizationHeader(string authHeader, string UrlKey, string projectID)
        {

            Credentials objCredential = new Credentials();
            string authuser = authHeader.Substring(0, 15);
            string authpass = authHeader.Substring(15);
            string authkey = UrlKey;
            //auth
            authuser = aes_Convert(authuser);
            authpass = aes_Convert(authpass);
            authkey = projectID;

            if (!String.IsNullOrEmpty(authkey))
            {
                int firauthkey = Convert.ToInt16(authkey.Substring(0, 1));
                if (firauthkey == 1)
                {
                    //authkey = authkey.Substring(Math.Max(0, authkey.Length - 2));
                    authkey = authkey.Substring(4);
                }
                else if (firauthkey == 2)
                {
                    authkey = authkey.Substring(5);
                }
                var myList = new List<string>();
                myList.Add(authuser);
                myList.Add(authpass);
                myList.Add(authkey);

                string[] credentials = myList.ToArray();

                if (credentials.Length != 3 || string.IsNullOrEmpty(credentials[0])
                    || string.IsNullOrEmpty(credentials[1])) return null;
                objCredential.Username = credentials[0];
                objCredential.Password = credentials[1];
                objCredential.UrlKey = credentials[2];
            }
            return objCredential;
        }
        #endregion

        #region Convertion's
        //Convertion's
        #region aes convertion
        private string aes_Convert(string aes_Convert)
        {
            string original = aes_Convert;

            // Create a new instance of the Aes
            // class.  This generates a new key and initialization 
            // vector (IV).
            //string roundtrip = "";
            string encryptStr = string.Empty;

            using (Aes myAes = Aes.Create())
            {
                byte[] encrypted;

                byte[] bkey = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
                byte[] biv = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };

                // Encrypt the string to an array of bytes.
                //encrypted = encryptStringToBytes_AES(original, myAes.Key, myAes.IV);
                encrypted = encryptStringToBytes_AES(original, bkey, biv);

                // Decrypt the bytes to a string.
                //roundtrip = decryptStringFromBytes_AES(encrypted, myAes.Key, myAes.IV);

                StringBuilder print = new StringBuilder();


                for (int i = 0; i < encrypted.Length; i++)
                {
                    print.Append(encrypted[i].ToString());
                }


                //Store encrypted string
                //StringBuilder to string
                encryptStr = print.ToString();
            }

            return encryptStr;
        }
        #endregion
        #region Base64conversion
        //To encrypt from Base64
        #region To encrypt from Base64
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        #endregion

        //To decrypt from Base64
        #region To decrypt from Base64
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        #endregion
        #endregion



        //To encrypt AES
        #region AES Encryption
        static byte[] encryptStringToBytes_AES(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // Declare the streams used
            // to encrypt to an in memory
            // array of bytes.
            MemoryStream msEncrypt = null;
            CryptoStream csEncrypt = null;
            StreamWriter swEncrypt = null;

            // Declare the Aes object
            // used to encrypt the data.
            Aes aesAlg = null;

            // Declare the bytes used to hold the
            // encrypted data.
            //byte[] encrypted = null;

            try
            {
                // Create an Aes object
                // with the specified key and IV.
                aesAlg = Aes.Create();
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                msEncrypt = new MemoryStream();
                csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
                swEncrypt = new StreamWriter(csEncrypt);

                //Write all data to the stream.
                swEncrypt.Write(plainText);

            }
            finally
            {
                // Clean things up.

                // Close the streams.
                if (swEncrypt != null)
                    swEncrypt.Close();
                //if (csEncrypt != null)
                //    csEncrypt.Close();
                //if (msEncrypt != null)
                //    msEncrypt.Close();

                // Clear the Aes object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            // Return the encrypted bytes from the memory stream.
            return msEncrypt.ToArray();

        }
        #endregion

        //To Decrypt AES
        #region AES Decryption
        static string decryptStringFromBytes_AES(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // TDeclare the streams used
            // to decrypt to an in memory
            // array of bytes.
            MemoryStream msDecrypt = null;
            CryptoStream csDecrypt = null;
            StreamReader srDecrypt = null;

            // Declare the Aes object
            // used to decrypt the data.
            Aes aesAlg = null;

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            try
            {
                // Create an Aes object
                // with the specified key and IV.
                aesAlg = Aes.Create();
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                msDecrypt = new MemoryStream(cipherText);
                csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
                srDecrypt = new StreamReader(csDecrypt);

                // Read the decrypted bytes from the decrypting stream
                // and place them in a string.
                plaintext = srDecrypt.ReadToEnd();
            }
            finally
            {
                // Clean things up.

                // Close the streams.
                if (srDecrypt != null)
                    srDecrypt.Close();
                //if (csDecrypt != null)
                //    csDecrypt.Close();
                //if (msDecrypt != null)
                //    msDecrypt.Close();

                // Clear the Aes object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            return plaintext;

        }
        #endregion
        #endregion
        #endregion
    }
}