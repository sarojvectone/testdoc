﻿namespace unifiedringmyaccountwebapi
{
    public class Credentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string UrlKey { get; set; }
    }

    public class CTALoginkey
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class UspapireturntokenstatusInput
    {
        public string ProjectName { get; set; }
        public string tokenID { get; set; }
    }

    public class UspapireturntokenstatusOutput
    {
        public string Status { get; set; }
    }
}