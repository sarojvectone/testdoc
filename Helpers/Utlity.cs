﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Unifiedringmyaccountwebapi.Helper
{
    #region Common
    public class BICSErrorResponse
    {
        public string code { get; set; }
        public string description { get; set; }
        public string timestamp { get; set; }
    }
    public class CommonOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }
    #endregion
    #region GetInventory
    public class GetInventory
    {
        public int id = 0;
        public string number {get; set;}
        public string Prod_Source = "BICS";
    }
    
    public class WebsitecentralbicsgetinventoryOutput : CommonOutput
    {
        public List<GetInventory> lstGetInventory { get; set; }
    }


    #region TeselvModel
    public class WebsitecentralTELESERVOutput : CommonOutput
    {
        //public List<GetInventoryTELSERV> lstGetTELESERVNUMBERS { get; set; }
        public int id { get; set; }
        public string number { get; set; }
        public string Prod_Source = "TELSERV";
    }
    public class GetInventoryTELSERV
    {
        public int id { get; set; }
        public string number { get; set; }
        public string Prod_Source = "TELSERV";
    }  

    #endregion
    #region GetNumberDetails
    public class WebsitecentralbicsgetnumberdetailsInput
    {
        public string number { get; set; }
    }

    public class WebsitecentralbicsgetnumberdetailsOutput : CommonOutput
    {
        public GetNumberDetails getNumberDetails { get; set; }
    }

    public class GetNumberDetails
    {
        public string number { get; set; }
        public string product { get; set; }
        public string country { get; set; }
        public string location { get; set; }
        public string accessArea { get; set; }
        public string profile { get; set; }
        public string status { get; set; }
        public string addressReference { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public List<GetNumberDetailsrouting> routing { get; set; }
        public GetNumberDetailsnonUsageFees nonUsageFees { get; set; }
        public List<GetNumberDetailsusageFees> usageFees { get; set; }
    }

    public class GetNumberDetailsrouting
    {
        public string accessType { get; set; }
        public string accessNetwork { get; set; }
        public string crn { get; set; }
        public string pop { get; set; }
    }

    public class GetNumberDetailsnonUsageFees
    {
        public string setUpFee { get; set; }
        public string MonthlyFee { get; set; }
        public string currency { get; set; }
    }

    public class GetNumberDetailsusageFees
    {
        public string accessNetwork { get; set; }
        public string accessType { get; set; }
        public string tde { get; set; }
        public string callSetUpFee { get; set; }
        public string perMinuteFee { get; set; }
        public string currency { get; set; }
    }
    #endregion

    #region GetLocations
    public class WebsitecentralbicsgetlocationsOutput : CommonOutput
    {
        public List<GetLocations> lstLocations { get; set; }
    }

    public class GetLocations
    {
        public string location { get; set; }
        public string country { get; set; }
    }
    #endregion

    #region GetPricelistByProduct
    public class WebsitecentralbicsgetpricelistbyproductInput
    {
        public string product { get; set; }
    }

    public class WebsitecentralbicsgetpricelistbyproductOutput : CommonOutput
    {
        public List<GetPricelistByProduct> lstGetPricelistByProduct { get; set; }
    }

    public class NonUsageFees
    {
        public string setUpFee { get; set; }
        public string MonthlyFee { get; set; }
        public string currency { get; set; }
    }

    public class UsageFee
    {
        public object accessNetwork { get; set; }
        public string accessType { get; set; }
        public string tde { get; set; }
        public string callSetUpFee { get; set; }
        public string perMinuteFee { get; set; }
        public string currency { get; set; }
    }

    public class GetPricelistByProduct
    {
        public string product { get; set; }
        public string country { get; set; }
        public string accessArea { get; set; }
        public string profile { get; set; }
        public List<string> locations { get; set; }
        public NonUsageFees nonUsageFees { get; set; }
        public List<UsageFee> usageFees { get; set; }
    }
    #endregion

    #region UpdateAddressReference
    public class WebsitecentralbicsupdateaddressreferenceInput
    {
        public string number { get; set; }
        public string addressReference { get; set; }
    }

    public class WebsitecentralbicsupdateaddressreferenceOutput : CommonOutput
    {

    }
    #endregion

    #region GetAvailableNumbers
    public class GetAvailableNumbers
    {
        public string number { get; set; }
        public string product { get; set; }
        public string country { get; set; }
        public string location { get; set; }
        public string accessArea { get; set; }
        public string profile { get; set; }
        public string status { get; set; }
        public string addressReference { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
    }

    public class WebsitecentralbicsgetavailablenumbersInput
    {
        public string product { get; set; }
        public string country { get; set; }
        public string location { get; set; }
    }

    public class WebsitecentralbicsgetavailablenumbersOutput : CommonOutput
    {
        public List<GetAvailableNumbers> lstGetAvailableNumbers { get; set; }
    }
    #endregion

    #region OrderSingleNumber
    public class Routing
    {
        public string accessType { get; set; }
        public string accessNetwork { get; set; }
        public string crnType { get; set; }
        public string crnValue { get; set; }
        public string pop { get; set; }
    }

    public class OrderSingleNumberInput
    {
        public string product { get; set; }
        public string country { get; set; }
        public string location { get; set; }
        public string areaCode { get; set; }
        public int quantity { get; set; }
        public string addressReference { get; set; }
        public List<Routing> routing { get; set; }
    }
    public class OrderSingleNumberOutput :CommonOutput
    {
        public string number { get; set; }
        public string product { get; set; }
        public string country { get; set; }
        public string accessArea { get; set; }
        public string location { get; set; }
        public string areaCode { get; set; }
        public string orderId { get; set; }
        public string addressReference { get; set; }
        public List<Routing> routing { get; set; }
    }
    public class OrderItem
    {
        public string number { get; set; }
        public string location { get; set; }
        public string areaCode { get; set; }
        public string addressReference { get; set; }
        public List<Routing> routing { get; set; }
    }

    public class OrderItemOutput :CommonOutput
    {
        public string orderId { get; set; }
        public string status { get; set; }
        public string product { get; set; }
        public string country { get; set; }
        public string location { get; set; }
        public string areaCode { get; set; }
        public string accessArea { get; set; }
        public string profile { get; set; }
        public List<OrderItem> orderItems { get; set; }
    }
    # endregion

    public static class Utility
    {
        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        #region GetINumberListForBICS        
        public static List<WebsitecentralbicsgetinventoryOutput> GetInventory(string product, string country, string areaCode)
        {
            WebsitecentralbicsgetinventoryOutput output = new WebsitecentralbicsgetinventoryOutput();
            List<WebsitecentralbicsgetinventoryOutput> lstoutput = new List<WebsitecentralbicsgetinventoryOutput>();
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                string URL = string.Format(ConfigurationManager.AppSettings["BICSAPIENDPOINT"].ToString() + "availablenumbers?product={0}&country={1}&areaCode={2}", product, country, areaCode);

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.Headers.Add("Authorization", "Bearer " + ConfigurationManager.AppSettings["BICSAPIKEY"]);
                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Timeout = 180000;
                httpWebRequest.Method = "GET";

                using (HttpWebResponse httpResponce = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    using (Stream resStreams = httpResponce.GetResponseStream())
                    {
                        using (StreamReader readers = new StreamReader(resStreams))
                        {
                            string response = readers.ReadToEnd();
                            Log.Info("GetInventory : Response : " + response);
                            List<GetInventory> lstGetInventory = JsonConvert.DeserializeObject<List<GetInventory>>(response);
                            if (lstGetInventory != null && lstGetInventory.Count > 0)
                            {
                                output.errcode = 0;
                                output.errmsg = "Success";
                                output.lstGetInventory = lstGetInventory;
                            }
                            else
                            {
                                output.errcode = -1;
                                output.errmsg = "No records found!";
                            }
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                Log.Error(ex.Message);
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    try
                    {
                        HttpWebResponse err = ex.Response as HttpWebResponse;
                        if (err != null)
                        {
                            string errResponse = new StreamReader(err.GetResponseStream()).ReadToEnd();
                            Log.Info("Exception : Response : " + errResponse);
                            List<BICSErrorResponse> response = JsonConvert.DeserializeObject<List<BICSErrorResponse>>(errResponse);
                            if (response != null && response.Count > 0)
                            {
                                output.errcode = -1;
                                output.errmsg = response.ElementAt(0).code + ":" + response.ElementAt(0).description;
                            }
                        }
                    }
                    catch (Exception innerEx)
                    {
                        Log.Error(innerEx.Message);
                        output.errcode = -1;
                        output.errmsg = innerEx.Message;
                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            lstoutput.Add(output);
            return lstoutput;
        }
        #endregion
       
       // Added for telsv
        #region GetINumberListForTelesev
        public static List<WebsitecentralTELESERVOutput> GetInventoryTELSERVDetails(string country, string city)
        {
            WebsitecentralTELESERVOutput output = new WebsitecentralTELESERVOutput();
            List<WebsitecentralTELESERVOutput> lstoutput = new List<WebsitecentralTELESERVOutput>();
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                string URL = string.Format(ConfigurationManager.AppSettings["TELESEVAPIENDPOINT"].ToString() + "shop?apiKey=1B7F4B7B-652A-4D47-93EE-9B0172A13812&&country={0}&city={1}", country,city);
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.ContentType = "application/vnd.api+json";
                httpWebRequest.Method = "GET";
                using (HttpWebResponse httpResponce = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    using (Stream resStreams = httpResponce.GetResponseStream())
                    {
                        using (StreamReader readers = new StreamReader(resStreams))
                        {
                            string response = readers.ReadToEnd();
                            Log.Info("GetTelevResponseTelSERV : Response : " + response);
                            List<WebsitecentralTELESERVOutput> lstGetTELESERVNUMBERS = JsonConvert.DeserializeObject<List<WebsitecentralTELESERVOutput>>(response);
                            lstoutput = lstGetTELESERVNUMBERS;
                            //if (lstGetTELESERVNUMBERS != null && lstGetTELESERVNUMBERS.Count > 0)
                            //{
                            //    output.errcode = 0;
                            //    output.errmsg = "Success";
                            //    output.lstGetTELESERVNUMBERS = lstGetTELESERVNUMBERS[0].lstGetTELESERVNUMBERS;
                            //}
                            //else
                            //{
                            //    output.errcode = -1;
                            //    output.errmsg = "No records found!";
                            //}
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                Log.Error(ex.Message);
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    try
                    {
                        HttpWebResponse err = ex.Response as HttpWebResponse;
                        if (err != null)
                        {
                            string errResponse = new StreamReader(err.GetResponseStream()).ReadToEnd();
                            Log.Info("Exception : Response : " + errResponse);
                            List<BICSErrorResponse> response = JsonConvert.DeserializeObject<List<BICSErrorResponse>>(errResponse);
                            if (response != null && response.Count > 0)
                            {
                                output.errcode = -1;
                                output.errmsg = response.ElementAt(0).code + ":" + response.ElementAt(0).description;
                            }
                        }
                    }
                    catch (Exception innerEx)
                    {
                        Log.Error(innerEx.Message);
                        output.errcode = -1;
                        output.errmsg = innerEx.Message;
                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            //lstoutput.Add(output);
            return lstoutput;
        }
        #endregion
        //end  TElsev

        // Added for DIDWW
        #region GetINumberListForDIDWW           
            public class country
            {
                public List<included> data { get; set; }
            }
            public class city
            {
                public List<citylist> data { get; set; }
            }
            public class citylist
            {
                public string id { get; set; }
                public string type { get; set; }
                public cityAttributes attributes { get; set; }
                public relationshipCity relationships { get; set; }
            }
            public class relationshipCity
            {
                public relationshipLinks country { get; set; }
                public relationshipLinks region { get; set; }
            }
            public class cityAttributes
            {
                public string name { get; set; }
            }
            public class Numbers
            {
                public List<Numbersdata> data { get; set; }
            }
            public class Numbersdata
            {
                public NumberAttributes attributes { get; set; }
                public string id { get; set; }
                public NumberRelationship relationships { get; set; }
                public string type { get; set; }
            }
            public class NumberRelationship
            {
                public countries did_group { get; set; }
            }
            public class NumberAttributes
            {
                public string number { get; set; }
            }
            public class resultNumbers
            {
                public string Number { get; set; }
                public string id { get; set; } 
                //public string available_dids_id { get; set; }           
                public string Prod_Source = "DIDWW";
            }
            public class included
            {
                public includedAttributes attributes { get; set; }
                public string id { get; set; }
                public string type { get; set; }
            }
            public class includedAttributes
            {
                public string iso { get; set; }
                public string name { get; set; }
                public int prefix { get; set; }
            }

            public class countries
            {
                public countryData data { get; set; }
                public relationshipLinks links { get; set; }
            }
            public class countryData
            {
                public string id { get; set; }
                public string type { get; set; }
            }
            public class relationshipLinks
            {
                public string related { get; set; }
                public string self { get; set; }
            }

        public static List<resultNumbers> GetInventoryDIDWW(string InCountry, string InCity)
        {
            WebClient client = new WebClient();
            string apiUrl = "https://api.didww.com/v3";
            client.Headers["Content-type"] = "application/vnd.api+json";
            client.Headers["Api-Key"] = "xkp0gtusdra585c11pzskho347aubc4h";
            client.Encoding = Encoding.UTF8;
            string resultCountries = client.DownloadString(apiUrl + "/countries");
            country Response = JsonConvert.DeserializeObject<country>(resultCountries);
            string countryId = "";
            foreach (included i in Response.data)
            {
                if (i.attributes.name == InCountry)
                {
                    countryId = i.id;
                    break;
                }
            }
            string resultCity = client.DownloadString(apiUrl + "/cities");
            city cityResponse = JsonConvert.DeserializeObject<city>(resultCity);
            string cityId = "";
            foreach (citylist i in cityResponse.data)
            {
                if (i.attributes.name == InCity)
                {
                    cityId = i.id;
                    break;
                }
            }
            string resultNumbers = client.DownloadString(apiUrl + "/available_dids?filter[country.id]=" + countryId + "&filter[city.id]=" + cityId + "&&include=did_group.stock_keeping_units");
            Numbers NumbersResponse = JsonConvert.DeserializeObject<Numbers>(resultNumbers);
            List<resultNumbers> ListNum = new List<resultNumbers>();
            foreach (Numbersdata i in NumbersResponse.data)
            {
                resultNumbers RN = new resultNumbers();
                RN.Number = i.attributes.number;
                RN.id = i.id;
                ListNum.Add(RN);
            }

            return ListNum;
           
        }
        #endregion
    #endregion

        // END of DIDWW
        #region GetNumberDetails
        public static WebsitecentralbicsgetnumberdetailsOutput GetNumberDetails(string number)
        {
            WebsitecentralbicsgetnumberdetailsOutput output = new WebsitecentralbicsgetnumberdetailsOutput();
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                string URL = string.Format(ConfigurationManager.AppSettings["BICSAPIENDPOINT"].ToString() + "numbers/{0}", number);
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.Headers.Add("Authorization", "Bearer " + ConfigurationManager.AppSettings["BICSAPIKEY"]);
                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Timeout = 180000;
                httpWebRequest.Method = "GET";

                using (HttpWebResponse httpResponce = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    using (Stream resStreams = httpResponce.GetResponseStream())
                    {
                        using (StreamReader readers = new StreamReader(resStreams))
                        {
                            string response = readers.ReadToEnd();
                            Log.Info("GetNumberDetails : Response : " + response);
                            GetNumberDetails getNumberDetails = JsonConvert.DeserializeObject<GetNumberDetails>(response);
                            if (getNumberDetails != null)
                            {
                                output.errcode = 0;
                                output.errmsg = "Success";
                                output.getNumberDetails = getNumberDetails;
                            }
                            else
                            {
                                output.errcode = -1;
                                output.errmsg = "No records found!";
                            }
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                Log.Error(ex.Message);
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    try
                    {
                        HttpWebResponse err = ex.Response as HttpWebResponse;
                        if (err != null)
                        {
                            string errResponse = new StreamReader(err.GetResponseStream()).ReadToEnd();
                            Log.Info("Exception : Response : " + errResponse);
                            List<BICSErrorResponse> response = JsonConvert.DeserializeObject<List<BICSErrorResponse>>(errResponse);
                            if (response != null && response.Count > 0)
                            {
                                output.errcode = -1;
                                output.errmsg = response.ElementAt(0).code + ":" + response.ElementAt(0).description;
                            }
                        }
                    }
                    catch (Exception innerEx)
                    {
                        Log.Error(innerEx.Message);
                        output.errcode = -1;
                        output.errmsg = innerEx.Message;
                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return output;
        }
        #endregion

        #region GetLocations
        public static WebsitecentralbicsgetlocationsOutput GetLocations()
        {
            WebsitecentralbicsgetlocationsOutput output = new WebsitecentralbicsgetlocationsOutput();
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                string URL = ConfigurationManager.AppSettings["BICSAPIENDPOINT"].ToString() + "reference/locations";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.Headers.Add("Authorization", "Bearer " + ConfigurationManager.AppSettings["BICSAPIKEY"]);
                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Timeout = 180000;
                httpWebRequest.Method = "GET";

                using (HttpWebResponse httpResponce = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    using (Stream resStreams = httpResponce.GetResponseStream())
                    {
                        using (StreamReader readers = new StreamReader(resStreams))
                        {
                            string response = readers.ReadToEnd();
                            Log.Info("GetLocations : Response : " + response);
                            List<GetLocations> lstLocations = JsonConvert.DeserializeObject<List<GetLocations>>(response);
                            if (lstLocations != null && lstLocations.Count > 0)
                            {
                                output.errcode = 0;
                                output.errmsg = "Success";
                                output.lstLocations = lstLocations;
                            }
                            else
                            {
                                output.errcode = -1;
                                output.errmsg = "No records found!";
                            }
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                Log.Error(ex.Message);
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    try
                    {
                        HttpWebResponse err = ex.Response as HttpWebResponse;
                        if (err != null)
                        {
                            string errResponse = new StreamReader(err.GetResponseStream()).ReadToEnd();
                            Log.Info("Exception : Response : " + errResponse);
                            List<BICSErrorResponse> response = JsonConvert.DeserializeObject<List<BICSErrorResponse>>(errResponse);
                            if (response != null && response.Count > 0)
                            {
                                output.errcode = -1;
                                output.errmsg = response.ElementAt(0).code + ":" + response.ElementAt(0).description;
                            }
                        }
                    }
                    catch (Exception innerEx)
                    {
                        Log.Error(innerEx.Message);
                        output.errcode = -1;
                        output.errmsg = innerEx.Message;
                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return output;
        }
        #endregion

        #region GetPricelistByProduct
        public static WebsitecentralbicsgetpricelistbyproductOutput GetPricelistByProduct(string product)
        {
            WebsitecentralbicsgetpricelistbyproductOutput output = new WebsitecentralbicsgetpricelistbyproductOutput();
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                string URL = ConfigurationManager.AppSettings["BICSAPIENDPOINT"].ToString() + "pricelist/product/" + product;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.Headers.Add("Authorization", "Bearer " + ConfigurationManager.AppSettings["BICSAPIKEY"]);
                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Timeout = 180000;
                httpWebRequest.Method = "GET";

                using (HttpWebResponse httpResponce = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    using (Stream resStreams = httpResponce.GetResponseStream())
                    {
                        using (StreamReader readers = new StreamReader(resStreams))
                        {
                            string response = readers.ReadToEnd();
                            Log.Info("GetPricelistByProduct : Response : " + response);
                            List<GetPricelistByProduct> lstGetPricelistByProduct = JsonConvert.DeserializeObject<List<GetPricelistByProduct>>(response);
                            if (lstGetPricelistByProduct != null && lstGetPricelistByProduct.Count > 0)
                            {
                                output.errcode = 0;
                                output.errmsg = "Success";
                                output.lstGetPricelistByProduct = lstGetPricelistByProduct;
                            }
                            else
                            {
                                output.errcode = -1;
                                output.errmsg = "No records found!";
                            }
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                Log.Error(ex.Message);
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    try
                    {
                        HttpWebResponse err = ex.Response as HttpWebResponse;
                        if (err != null)
                        {
                            string errResponse = new StreamReader(err.GetResponseStream()).ReadToEnd();
                            Log.Info("Exception : Response : " + errResponse);
                            List<BICSErrorResponse> response = JsonConvert.DeserializeObject<List<BICSErrorResponse>>(errResponse);
                            if (response != null && response.Count > 0)
                            {
                                output.errcode = -1;
                                output.errmsg = response.ElementAt(0).code + ":" + response.ElementAt(0).description;
                            }
                        }
                    }
                    catch (Exception innerEx)
                    {
                        Log.Error(innerEx.Message);
                        output.errcode = -1;
                        output.errmsg = innerEx.Message;
                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return output;
        }
        #endregion

        #region UpdateAddressReference
        public static WebsitecentralbicsupdateaddressreferenceOutput UpdateAddressReference(string number, string addressReference)
        {
            WebsitecentralbicsupdateaddressreferenceOutput output = new WebsitecentralbicsupdateaddressreferenceOutput();
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                string URL = ConfigurationManager.AppSettings["BICSAPIENDPOINT"].ToString() + "numbers/addressReferences";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.Headers.Add("Authorization", "Bearer " + ConfigurationManager.AppSettings["BICSAPIKEY"]);
                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Timeout = 180000;
                httpWebRequest.Method = "POST";

                using (var streamWriters = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(new
                    {
                        number = number,
                        addressReference = addressReference
                    });
                    streamWriters.Write(json);
                }

                using (HttpWebResponse httpResponce = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    using (Stream resStreams = httpResponce.GetResponseStream())
                    {
                        using (StreamReader readers = new StreamReader(resStreams))
                        {
                            string response = readers.ReadToEnd();
                            Log.Info("UpdateAddressReference : Response : " + response);
                            output.errcode = 0;
                            output.errmsg = "Success";

                        }
                    }
                }
            }
            catch (WebException ex)
            {
                Log.Error(ex.Message);
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    try
                    {
                        HttpWebResponse err = ex.Response as HttpWebResponse;
                        if (err != null)
                        {
                            string errResponse = new StreamReader(err.GetResponseStream()).ReadToEnd();
                            Log.Info("Exception : Response : " + errResponse);
                            List<BICSErrorResponse> response = JsonConvert.DeserializeObject<List<BICSErrorResponse>>(errResponse);
                            if (response != null && response.Count > 0)
                            {
                                output.errcode = -1;
                                output.errmsg = response.ElementAt(0).code + ":" + response.ElementAt(0).description;
                            }
                        }
                    }
                    catch (Exception innerEx)
                    {
                        Log.Error(innerEx.Message);
                        output.errcode = -1;
                        output.errmsg = innerEx.Message;
                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return output;
        }
        #endregion

        #region GetAvailableNumbers
        public static WebsitecentralbicsgetavailablenumbersOutput GetAvailableNumbers(string product, string country, string location)
        {
            WebsitecentralbicsgetavailablenumbersOutput output = new WebsitecentralbicsgetavailablenumbersOutput();
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                string URL = string.Format(ConfigurationManager.AppSettings["BICSAPIENDPOINT"].ToString() + "availablenumbers?product={0}&country={1}&location={2}", product, country, location);
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.Headers.Add("Authorization", "Bearer " + ConfigurationManager.AppSettings["BICSAPIKEY"]);
                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Timeout = 180000;
                httpWebRequest.Method = "GET";

                using (HttpWebResponse httpResponce = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    using (Stream resStreams = httpResponce.GetResponseStream())
                    {
                        using (StreamReader readers = new StreamReader(resStreams))
                        {
                            string response = readers.ReadToEnd();
                            Log.Info("GetAvailableNumbers : Response : " + response);
                            List<GetAvailableNumbers> lstGetAvailableNumbers = JsonConvert.DeserializeObject<List<GetAvailableNumbers>>(response);
                            if (lstGetAvailableNumbers != null && lstGetAvailableNumbers.Count > 0)
                            {
                                output.errcode = 0;
                                output.errmsg = "Success";
                                output.lstGetAvailableNumbers = lstGetAvailableNumbers;
                            }
                            else
                            {
                                output.errcode = -1;
                                output.errmsg = "No records found!";
                            }
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                Log.Error(ex.Message);
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    try
                    {
                        HttpWebResponse err = ex.Response as HttpWebResponse;
                        if (err != null)
                        {
                            string errResponse = new StreamReader(err.GetResponseStream()).ReadToEnd();
                            Log.Info("Exception : Response : " + errResponse);
                            List<BICSErrorResponse> response = JsonConvert.DeserializeObject<List<BICSErrorResponse>>(errResponse);
                            if (response != null && response.Count > 0)
                            {
                                output.errcode = -1;
                                output.errmsg = response.ElementAt(0).code + ":" + response.ElementAt(0).description;
                            }
                        }
                    }
                    catch (Exception innerEx)
                    {
                        Log.Error(innerEx.Message);
                        output.errcode = -1;
                        output.errmsg = innerEx.Message;
                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return output;
        }
        #endregion

        #region Order Bullk numbers
        public static OrderItemOutput PostOrderBulkNumber(OrderSingleNumberInput objOrderSingleNumberInput)
        {
            OrderItemOutput output = new OrderItemOutput();
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                string URL = string.Format(ConfigurationManager.AppSettings["BICSAPIENDPOINT"].ToString()+"order/bulk");

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.Headers.Add("Authorization", "Bearer " + ConfigurationManager.AppSettings["BICSAPIKEY"]);
                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Timeout = 180000;
                httpWebRequest.Method = "POST";

                using (HttpWebResponse httpResponce = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    using (Stream resStreams = httpResponce.GetResponseStream())
                    {
                        using (StreamReader readers = new StreamReader(resStreams))
                        {
                            string response = readers.ReadToEnd();
                            Log.Info("OrderSingleNumberOutput : Response : " + response);
                            List<OrderItemOutput> lstGetAvailableNumbers = JsonConvert.DeserializeObject<List<OrderItemOutput>>(response);
                            if (lstGetAvailableNumbers != null && lstGetAvailableNumbers.Count > 0)
                            {
                                output.errcode = 0;
                                output.errmsg = "Success";
                                output.orderItems= lstGetAvailableNumbers[0].orderItems;
                            }
                            else
                            {
                                output.errcode = -1;
                                output.errmsg = "No records found!";
                            }
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                Log.Error(ex.Message);
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    try
                    {
                        HttpWebResponse err = ex.Response as HttpWebResponse;
                        if (err != null)
                        {
                            string errResponse = new StreamReader(err.GetResponseStream()).ReadToEnd();
                            Log.Info("Exception : Response : " + errResponse);
                            List<BICSErrorResponse> response = JsonConvert.DeserializeObject<List<BICSErrorResponse>>(errResponse);
                            if (response != null && response.Count > 0)
                            {
                                output.errcode = -1;
                                output.errmsg = response.ElementAt(0).code + ":" + response.ElementAt(0).description;
                            }
                        }
                    }
                    catch (Exception innerEx)
                    {
                        Log.Error(innerEx.Message);
                        output.errcode = -1;
                        output.errmsg = innerEx.Message;
                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return output;
        }
        #endregion

        #region Order Single number BICS 
        public static OrderSingleNumberOutput PostOrderSingleNumber(OrderSingleNumberInput objOrderSingleNumberInput)
        {
            OrderSingleNumberOutput output = new OrderSingleNumberOutput();
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                string URL = string.Format(ConfigurationManager.AppSettings["BICSAPIENDPOINT"].ToString() + "order");

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.Headers.Add("Authorization", "Bearer " + ConfigurationManager.AppSettings["BICSAPIKEY"]);
                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Timeout = 180000;
                httpWebRequest.Method = "POST";

                using (HttpWebResponse httpResponce = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    using (Stream resStreams = httpResponce.GetResponseStream())
                    {
                        using (StreamReader readers = new StreamReader(resStreams))
                        {
                            string response = readers.ReadToEnd();
                            Log.Info("OrderSingleNumberOutput : Response : " + response);
                            List<OrderSingleNumberOutput> lstGetAvailableNumbers = JsonConvert.DeserializeObject<List<OrderSingleNumberOutput>>(response);
                            if (lstGetAvailableNumbers != null && lstGetAvailableNumbers.Count > 0)
                            {
                                output.errcode = 0;
                                output.errmsg = "Success";
                                output.routing = lstGetAvailableNumbers[0].routing;
                            }
                            else
                            {
                                output.errcode = -1;
                                output.errmsg = "No records found!";
                            }
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                Log.Error(ex.Message);
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    try
                    {
                        HttpWebResponse err = ex.Response as HttpWebResponse;
                        if (err != null)
                        {
                            string errResponse = new StreamReader(err.GetResponseStream()).ReadToEnd();
                            Log.Info("Exception : Response : " + errResponse);
                            List<BICSErrorResponse> response = JsonConvert.DeserializeObject<List<BICSErrorResponse>>(errResponse);
                            if (response != null && response.Count > 0)
                            {
                                output.errcode = -1;
                                output.errmsg = response.ElementAt(0).code + ":" + response.ElementAt(0).description;
                            }
                        }
                    }
                    catch (Exception innerEx)
                    {
                        Log.Error(innerEx.Message);
                        output.errcode = -1;
                        output.errmsg = innerEx.Message;
                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return output;
        }
        #endregion

    }
}
