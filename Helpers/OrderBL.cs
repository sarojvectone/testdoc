﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using unifiedringmyaccountwebapi.Models;

namespace unifiedringmyaccountwebapi.Helpers
{
    public partial class OrderBL
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static int Get_Customer_DomainId_By(int CustomerId)
        {
            try
            {
                var result = new cOutMessage();
                string sql = "SELECT ISNULL (vpa.domain_id,-1) AS errcode,cast (vpa.domain_id as varchar) AS errmsg  FROM vbcp_pbx_account vpa \n"
                            + "WHERE EXISTS (SELECT * FROM vbcp_company vc WHERE vc.customer_id = {0} AND vpa.company_id = vc.company_id)";
                using (DataMapper _iMapper = new DataMapper())
                {
                    _iMapper.CatalogName = GlobalConfig.Catalog_Name;
                    result = _iMapper.MapDataToSingle<cOutMessage>(string.Format(sql, CustomerId));
                    if (result == null)
                        result = new cOutMessage() { ErrCode = -1, ErrMsg = "Null/Empty Reference" };
                }
                return result.ErrCode;
            }
            catch (Exception ex)
            {
                Log.Error("Get_Customer_DomainId_By Error : {0}", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public static DeskPhoneModel GetDeskphoneModel(int DomainId, string ExtensionId)
        {
            try
            {
                var result = new DeskPhoneModel();
                string sql = string.Format("crm_get_extension_detail {0},'{1}'", DomainId, ExtensionId);
                using (DataMapper _iMapper = new DataMapper())
                {
                    _iMapper.CatalogName = GlobalConfig.Catalog_Name;
                    result = _iMapper.MapDataToSingle<DeskPhoneModel>(sql);
                }
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("GetDeskphoneModel error : {0}", ex.Message);
                throw new Exception(ex.Message);
            }
        }

        public static MyAccountCompany GetCompanyDetailByDomain(int DomainId)
        {
            try
            {
                var result = new MyAccountCompany();
                using (DataMapper _iMapper = new DataMapper())
                {
                    _iMapper.CatalogName = GlobalConfig.Catalog_Name;
                    result = _iMapper.MapDataToSingle<MyAccountCompany>(string.Format("vt_account_get_company_detail_by_domain {0}", DomainId));
                }
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("GetCompanyDetailByDomain error : {0}", ex.Message);
                throw new Exception(ex.Message);
            }
        }






        public static string Aastra_Content(MyAccountCompany In, DeskPhoneModel Model, int customerID)
        {
            string SIPSTUNIP = string.Empty;
            string SIPSTUNPORT = string.Empty;
            string SIPNATPORT = string.Empty;
            string SIPRTPPORT = string.Empty;

            //Log.Debug("MyAccountCompany In : {0}", JsonConvert.SerializeObject(In));
            //Log.Debug("DeskPhoneModel Model : {0}", JsonConvert.SerializeObject(Model));
            //Log.Debug("customerID : {0}", customerID);

            try
            {
                //if (customerID == 1533)
                //{
                SIPSTUNIP = "0.0.0.0";
                SIPSTUNPORT = System.Configuration.ConfigurationManager.AppSettings["SIPSTUNPORT"].ToString();
                SIPNATPORT = "0";
                SIPRTPPORT = "0";
                //}
                //else
                //{
                //    SIPSTUNIP = System.Configuration.ConfigurationManager.AppSettings["SIPSTUNIP"].ToString();
                //    SIPSTUNPORT = System.Configuration.ConfigurationManager.AppSettings["SIPSTUNPORT"].ToString();
                //    SIPNATPORT = "51620";
                //    SIPRTPPORT = "51720";
                //}
                //"sip nat ip"
                //and "sip nat port"
                //#sip nat port
                //sip rtp port
                StringBuilder sb = new StringBuilder()
                            .AppendLine(string.Format("^sip auth name: {0}", Model.extension))
                            .AppendLine(string.Format("^sip password: {0}", Model.vm_password))
                            .AppendLine(string.Format("^sip user name: {0}", Model.extension))
                            .AppendLine(string.Format("^sip display name: \"{0}\"", Model.Effective_Caller_Id_Name))
                            .AppendLine(string.Format("^sip screen name: \"{0}\"", Model.extension))
                            .AppendLine(string.Format("^sip screen name 2: \"{0}\"", Model.Effective_Caller_Id_Name))
                            .AppendLine(string.Format("^sip line1 auth name: {0}", Model.extension))
                            .AppendLine(string.Format("^sip line1 password: {0}", Model.vm_password))
                            .AppendLine(string.Format("^sip line1 user name: {0}", Model.extension))
                            .AppendLine(string.Format("^sip line1 display name: \"{0}\"", Model.Effective_Caller_Id_Name))
                            .AppendLine(string.Format("^sip line1 screen name: \"{0}\"", Model.extension))
                            .AppendLine(string.Format("^sip line1 screen name 2: \"{0}\"", Model.Effective_Caller_Id_Name))
                            .AppendLine(string.Format("^sip proxy ip: {0}", In.domain_name))
                            .AppendLine(string.Format("^sip registrar ip: {0}", In.domain_name))
                            .AppendLine(string.Format("^sip send line: 1"))
                            .AppendLine(string.Format("^sip pai: 0"))
                            .AppendLine(string.Format("^sip customized codec: payload=8;ptime=30;silsupp=on"))  //Added for codec                                   
                            .AppendLine(string.Format("^https validate certificates: 1")) //Added for Certificate                          
                            .AppendLine(string.Format("^hostname: pbx.ext.{0}", Model.extension))
                            .AppendLine(string.Format("^admin password: {0}", Model.admin_password))
                            .AppendLine(string.Format("^sip stun ip: {0}", SIPSTUNIP))
                            .AppendLine(string.Format("^sip stun port: {0}", SIPSTUNPORT))
                            .AppendLine(string.Format("^sip nat port: {0}", SIPNATPORT))
                            .AppendLine(string.Format("^sip nat rtp port: {0}", SIPRTPPORT));

                return sb.ToString();
            }
            catch (Exception ex)
            {
                Log.Error("Aastra_Content error : {0}", ex.Message);
                throw;
            }

            //return sb.ToString();
        }

        public static string Aastra_Content1(MyAccountCompany In, DeskPhoneModel Model)
        {
            Log.Debug("MyAccountCompany In : {0}", JsonConvert.SerializeObject(In));
            Log.Debug("DeskPhoneModel Model : {0}", JsonConvert.SerializeObject(Model));
            //Log.Debug("customerID : {0}", customerID);

            try
            {
                StringBuilder sb = new StringBuilder()
                                .AppendLine(string.Format("^sip auth name: {0}", Convert.ToString(Model.extension) + ","))
                                .AppendLine(string.Format("^sip password: {0}", Convert.ToString(Model.vm_password) + ","))
                                .AppendLine(string.Format("^sip user name: {0}", Convert.ToString(Model.extension) + ","))
                                .AppendLine(string.Format("^sip display name: \"{0}\"", Convert.ToString(Model.Effective_Caller_Id_Name) + ","))
                                .AppendLine(string.Format("^sip screen name: \"{0}\"", Convert.ToString(Model.extension)))
                                .AppendLine(string.Format("^sip screen name 2: \"{0}\"", Convert.ToString(Model.Effective_Caller_Id_Name) + ","))
                                .AppendLine(string.Format("^sip line1 auth name: {0}", Convert.ToString(Model.extension) + ","))
                                .AppendLine(string.Format("^sip line1 password: {0}", Convert.ToString(Model.vm_password) + ","))
                                .AppendLine(string.Format("^sip line1 user name: {0}", Convert.ToString(Model.extension) + ","))
                                .AppendLine(string.Format("^sip line1 display name: \"{0}\"", Convert.ToString(Model.Effective_Caller_Id_Name) + ","))
                                .AppendLine(string.Format("^sip line1 screen name: \"{0}\"", Convert.ToString(Model.extension) + ","))
                                .AppendLine(string.Format("^sip line1 screen name 2: \"{0}\"", Convert.ToString(Model.Effective_Caller_Id_Name) + ","))
                                .AppendLine(string.Format("^sip proxy ip: {0}", Convert.ToString(In.domain_name + ",")))
                                .AppendLine(string.Format("^sip registrar ip: {0}", Convert.ToString(In.domain_name + ",")))
                                .AppendLine(string.Format("^sip send line: 1") + ",")
                                 .AppendLine(string.Format("^sip pai: 0") + ",")
                                .AppendLine(string.Format("^sip customized codec: payload=8;ptime=30;silsupp=on"))  //Added for codec                                   
                                 .AppendLine(string.Format("^https validate certificates: 1")) //Added for Certificate           
                                .AppendLine(string.Format("^hostname: pbx.ext.{0}", Convert.ToString(Model.extension) + ","))
                                .AppendLine(string.Format("^admin password: {0}", Convert.ToString(Model.admin_password) + ","))
                                .AppendLine(string.Format("^sip stun ip: {0}", System.Configuration.ConfigurationManager.AppSettings["SIPSTUNIP"]) + ",")
                                .AppendLine(string.Format("^sip stun port: {0}", System.Configuration.ConfigurationManager.AppSettings["SIPSTUNPORT"]) + ",");
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("[Failed Generate Config File Deskphone] " + ex.Message);
            }
        }

        public static string Aastra_Config_Content(MyAccountCompany In, DeskPhoneModel Model, int customerID)
        {
            try
            {
                StringBuilder sb = new StringBuilder()
                                .AppendLine("^directory 1:directory1.csv")
                                .AppendLine("^time reserved: -1")
                                .AppendLine("^time server1: 192.168.4.22")
                                .AppendLine("^time server2: 192.168.4.23")
                                .AppendLine("^time server3: 192.168.4.22")
                                .AppendLine("^time format: 1")
                                .AppendLine("^date format: 9")
                                .AppendLine("^time zone name: GB-London")
                                .AppendLine("^sip silence suppression: 1")
                                .AppendLine("^sip dial plan: \"911|900X|0XX+#|102X+#|*[13]|9011X+#|[38]XXX|*[65]X|*[78][456]XXXX|X[2-9]XX[2-9]XXXXXX|X1[2-9]XX[2-9]XXXXXX|XX+#\"")
                                .AppendLine("^sip intercom type: 2")
                                .AppendLine("^sip intercom prefix code: *75")
                                .AppendLine("^sip out-of-band dtmf: 0")
                                .AppendLine("^sip dtmf method: 0")
                                .AppendLine("^sip use basic codecs: 1")
                                .AppendLine("^auto resync mode: 3")
                                .AppendLine("^auto resync time: 01:00")
                                .AppendLine("^auto resync max delay: 30")
                                .AppendLine("^auto resync days: 0")
                                  .AppendLine("^sip pai: 0")
                                .AppendLine(string.Format("^admin password: {0}", Model.admin_password));
                //.AppendLine(string.Format("^admin password: {0}", "4619"));
                //   .AppendLine("^HTTP_SERVER:80")
                //    .AppendLine(string.Format("^HTTP_PATH:{0}", Model.HTTP_PATH));

                return sb.ToString();
            }
            catch (Exception ex)
            {
                Log.Error("Aastra_Config_Content error : {0}", ex.Message);
                throw;
            }
        }
        public static bool DeleteConfigAastrafile(string fullFileName, string fileContent, Deskphone_FTP_Info Info, int Customer_Id, string user)
        {
            
            try
            {
                FileInfo configfile = new FileInfo(fullFileName);
                WinSCPInvoker uploader = new WinSCPInvoker(string.Empty);
                return (uploader.DeleteFile(GlobalConfig.FTPDeskPhone, GlobalConfig.FTPDeskPhoneRemote + Info.Security_Id + "/", configfile.FullName, Info.Filename) == WinSCPInvoker.SFTPErrCode.Success);
            }
            catch (Exception ex)
            {
                Log.Error("DeleteConfigAastrafile error : {0}", ex.Message);
                return false;    
            }
        }
        public static bool GenerateDeskPhoneConfigAastra(string fullFileName, string fileContent, Deskphone_FTP_Info Info)
        {
            try
            {
                StreamWriter writer = new StreamWriter(fullFileName);
                writer.Write(fileContent);
                writer.Close();
                FileInfo configFile = new FileInfo(fullFileName);
                if (configFile.Exists)
                {
                    WinSCPInvoker uploader = new WinSCPInvoker(string.Empty);

                    return (uploader.Upload(GlobalConfig.FTPDeskPhone, GlobalConfig.FTPDeskPhoneRemote + Info.Security_Id + "/", configFile.FullName) == WinSCPInvoker.SFTPErrCode.Success);
                }
            }
            catch (Exception ex)
            {
                Log.Error("GenerateDeskPhoneConfigAastra error : {0}", ex.Message);
                return false;
            }
            return true;
        }

        public static bool GenerateDeskPhoneConfigAastrafile(string fullFileName, string fileContent, Deskphone_FTP_Info Info, int Customer_Id, string user)
        {

            try
            {
                StreamWriter writer = new StreamWriter(fullFileName);
                writer.Write(fileContent);
                writer.Close();
                FileInfo configfile = new FileInfo(fullFileName);
                var result = OrderBL.CreateAastraConfig(Customer_Id, user);
                if (result.ErrCode == 0)
                {
                    if (configfile.Exists)
                    {
                        WinSCPInvoker uploader = new WinSCPInvoker(string.Empty);
                        return (uploader.Upload(GlobalConfig.FTPDeskPhone, GlobalConfig.FTPDeskPhoneRemote + Info.Security_Id + "/", configfile.FullName) == WinSCPInvoker.SFTPErrCode.Success);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("GenerateDeskPhoneConfigAastrafile error : {0}", ex.Message);
                return false;
            }
            return true;
        }

        public static string Grandstream_Content(MyAccountCompany In, DeskPhoneModel Model, int customerID)
        {
            try
            {
                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Encoding = Encoding.UTF8;
                xmlSettings.Indent = true;
                xmlSettings.NewLineOnAttributes = false;
                xmlSettings.NewLineChars = "\r\n";
                xmlSettings.CloseOutput = true;

                StringBuilder strBuilder = new StringBuilder();
                string result = "";
                using (MemoryStream output = new MemoryStream())
                {
                    using (XmlWriter xml = XmlWriter.Create(output, xmlSettings))
                    {
                        // XmlWriter xml = XmlWriter.Create(strBuilder, xmlSettings);

                        xml.WriteStartDocument();
                        xml.WriteStartElement("gs_provision");
                        {
                            xml.WriteAttributeString("version", "1");
                            xml.WriteElementString("mac", Model.deskphone_mac.ToString().Trim());
                            xml.WriteStartElement("config");
                            {


                                xml.WriteAttributeString("version", "1");

                                xml.WriteElementString("P271", "1");
                                xml.WriteElementString("P270", Model.extension_name);
                                //xml.WriteElementString("P47", "5901.ur.mundio.com");
                                xml.WriteElementString("P47", Model.domain_name);
                               // xml.WriteElementString("P48", Model.domain_name);
                                xml.WriteElementString("P35", Model.extension);
                                xml.WriteElementString("P36", Model.extension);
                                xml.WriteElementString("P34", Model.vm_password);
                                xml.WriteElementString("P3", Model.extension_name); 
                                xml.WriteElementString("P57", "8");//Added for Codec
                                //Account-2 setting
                                xml.WriteElementString("P401", "1");
                                xml.WriteElementString("P417", Model.extension_name);
                                //xml.WriteElementString("P402", "5901.ur.mundio.com");
                                xml.WriteElementString("P402", Model.domain_name);
                                xml.WriteElementString("P404", Model.extension);
                                xml.WriteElementString("P405", Model.extension);
                                xml.WriteElementString("P406", Model.vm_password);
                                xml.WriteElementString("P407", Model.extension_name);
                            }
                            xml.WriteEndElement();
                        }
                        xml.WriteEndElement();
                        xml.Flush();
                        xml.Close();

                    }
                    result = Encoding.UTF8.GetString(output.ToArray());
                }

                return result;

            }
            catch (Exception ex)
            {
                Log.Error("Grandstream_Content error : {0}", ex.Message);
                return string.Empty;
            }
        }

        public static string SNOM_Content(MyAccountCompany In, DeskPhoneModel Model, int customerID)
        {
            try
            {

                StringBuilder sb = new StringBuilder()     
                .AppendLine(string.Format("<settings>"))
                .AppendLine(string.Format(@"<provisioning_order perm="""">pnp:stop redirection:stop dhcp:stop tr69:stop</provisioning_order>"))
                .AppendLine(string.Format(@"<phone-settings e=""2"">"))
                .AppendLine(string.Format(@"<language perm="""">{0}</language>", "English"))
                .AppendLine(string.Format(@"<http_user perm="""">{0}</http_user>","admin"))
                .AppendLine(string.Format(@"<http_pass perm="""">{0}</http_pass>", "22222"))
                .AppendLine(string.Format(@"<admin_mode_password perm="""">22222</admin_mode_password>"))
                .AppendLine(string.Format(@"<http_client_user perm="""">admin</http_client_user>"))
                .AppendLine(string.Format(@"<http_client_pass perm="""">22222</http_client_pass>"))
                .AppendLine(string.Format(@"<user_active idx=""1"" perm="""">on</user_active>"))
                .AppendLine(string.Format(@"<user_realname idx=""1"" perm="""">{0}</user_realname>",Model.Effective_Caller_Id_Name))
                .AppendLine(string.Format(@"<user_name idx=""1"" perm="""">{0}</user_name>", Model.extension))
                .AppendLine(string.Format(@"<user_host idx=""1"" perm="""">{0}</user_host>",Model.domain_name))
                .AppendLine(string.Format(@"<user_pname idx=""1"" perm="""">{0}</user_pname>", Model.extension_name))
                .AppendLine(string.Format(@"<user_pass idx=""1"" perm="""">{0}</user_pass>", Model.vm_password))
                .AppendLine(string.Format(@"<user_outbound idx=""1"" perm=""""></user_outbound>"))
                .AppendLine(string.Format(@"<user_was_registered idx=""1"" perm="""">true</user_was_registered>"))
                .AppendLine(string.Format(@"<hide_identity idx=""1"" perm="""">false</hide_identity>"))
                .AppendLine(string.Format(@"<codec_priority_list idx=""1"" perm=""RW"">pcma,pcmu,g729,telephone-event</codec_priority_list>"))
                .AppendLine(string.Format(@"<codec_priority_list idx=""2"" perm=""RW"">pcma,pcmu,g729,telephone-event</codec_priority_list>"))
                .AppendLine(string.Format(@"<use_contact_in_refer_to_hdr idx=""1"" perm="""">off</use_contact_in_refer_to_hdr>"))
                .AppendLine(string.Format(@"</phone-settings>"))
                .AppendLine(string.Format(@"<functionKeys e=""2"">"))
                .AppendLine(string.Format(@"<fkey idx=""0"" context=""active"" label="""" perm="""">line</fkey>"))
                .AppendLine(string.Format(@"<fkey idx=""1"" context=""active"" label="""" perm="""">line</fkey>"))
                .AppendLine(string.Format(@"</functionKeys>"))
                .AppendLine(string.Format(@"<tbook e='2'/>"))
                .AppendLine(string.Format(@"</settings>"));
                return sb.ToString();
            }
            catch (Exception ex)
            {
                Log.Error("SNOM_Content error : {0}", ex.Message);
                return string.Empty;
            }
        }

        public static string SNOM_MailContent(MyAccountCompany In, DeskPhoneModel Model)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
      
                return sb.ToString();
            }
            catch (Exception ex)
            {
                Log.Error("Grandstream_MailContent error : {0}", ex.Message);
                throw ex;
            }
        }

        public static string Grandstream_MailContent(MyAccountCompany In, DeskPhoneModel Model)
        {
            try
            {
                StringBuilder sb = new StringBuilder()
                                .AppendLine(string.Format("Account Name: {0}", Model.extension_name + ","))
                    //.AppendLine(string.Format("SIP Server: {0}", "5901.ur.mundio.com" + ","))
                                .AppendLine(string.Format("SIP Server: {0}", Model.domain_name + ","))
                                .AppendLine(string.Format("SIP User ID: {0}", Model.extension + ","))
                                .AppendLine(string.Format("Authenticate ID: \"{0}\"", Model.extension + ","))
                                .AppendLine(string.Format("Display Name: \"{0}\"", Model.extension_name));

                return sb.ToString();
            }
            catch (Exception ex)
            {
                Log.Error("Grandstream_MailContent error : {0}", ex.Message);
                throw ex;
            }
        }

        

        public static string Cisco_Content(MyAccountCompany _company, DeskPhoneModel Model, string _macaddress, Deskphone_FTP_Info resx, string strfullname)
        {
            Log.Debug("Cisco_Content");
            try
            {

                StringBuilder sb = new StringBuilder()
                           .AppendLine(string.Format("Display_Name[1] \"{0}\"{1}", Convert.ToString(Model.Effective_Caller_Id_Name), ";"))
                           .AppendLine(string.Format("User_ID[1] \"{0}\"{1}", Convert.ToString(Model.extension), ";"))
                           .AppendLine(string.Format("Password[1] \"{0}\"{1}", Convert.ToString(Model.vm_password), ";"))
                           .AppendLine(string.Format("Use_Auth_ID[1] \"{0}\"{1}", "No", ";"))
                           .AppendLine(string.Format("Auth_ID[1] \"{0}\"{1}", Convert.ToString(Model.extension), ";"))
                    //.AppendLine(string.Format("Proxy[1] \"{0}\"{1}", "5901.UR.mundio.com", ";"));
                            .AppendLine(string.Format("Proxy[1] \"{0}\"{1}", Convert.ToString(Model.domain_name), ";"))
                            .AppendLine(string.Format("Preferred_Codec[1] \"{0}\"{1}", "G711a", ";"));

                string path = @strfullname;

                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(sb.ToString());
                }
                string input = string.Format(@"{0} {1}", path, path.Replace(".txt", ".cfg"));

                ProcessStartInfo startInfo = new ProcessStartInfo();
                string CiscoToolfilename = System.Configuration.ConfigurationManager.AppSettings["CISCOTOOL"].ToString();
                startInfo.FileName = @CiscoToolfilename;//--Anees need to add in web config
                startInfo.WindowStyle = ProcessWindowStyle.Minimized;
                //startInfo.Arguments = @"Sample1.txt E:\Cisco-tool\Sampletest12.cfg";
                startInfo.Arguments = input;
                Process cmdProc = Process.Start(startInfo);
                // wait until command has finished executing 
                //cmdProc.WaitForInputIdle();

                cmdProc.WaitForExit();
                //var p = Process.Start(startInfo);
                //p.StandardInput.Write("y");
                //p.WaitForExit();

                WinSCPInvoker uploader = new WinSCPInvoker(string.Empty);
                string filename = path.Replace(".txt", ".cfg");
                string Security_Id = resx.Security_Id.ToString();
                Log.Debug("Cisco_Content_upload:" + "FTPDeskPhone:" + GlobalConfig.FTPDeskPhone + "FTPDeskPhoneRemote:" + GlobalConfig.FTPDeskPhoneRemote + resx.Security_Id + filename);
                uploader.Upload(GlobalConfig.FTPDeskPhone, GlobalConfig.FTPDeskPhoneRemote + resx.Security_Id + "/", filename);
                Log.Debug("Remote Server Upload Completed");
                return sb.ToString();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                throw ex;
            }
        }

        public static string YeaLinkContent(MyAccountCompany In, DeskPhoneModel Model, int customerID)
        {
            Log.Debug("MyAccountCompany In : {0}", JsonConvert.SerializeObject(In));
            Log.Debug("DeskPhoneModel Model : {0}", JsonConvert.SerializeObject(Model));
            //Log.Debug("customerID : {0}", customerID);

            try
            {
                #region Comments
                //StringBuilder sb = new StringBuilder()
                //                .AppendLine(string.Format("^sip auth name: {0}", Convert.ToString(Model.extension) + ","))
                //                .AppendLine(string.Format("^sip password: {0}", Convert.ToString(Model.vm_password) + ","))
                //                .AppendLine(string.Format("^sip user name: {0}", Convert.ToString(Model.extension) + ","))
                //                .AppendLine(string.Format("^sip display name: \"{0}\"", Convert.ToString(Model.Effective_Caller_Id_Name) + ","))
                //                .AppendLine(string.Format("^sip screen name: \"{0}\"", Convert.ToString(Model.extension)))
                //                .AppendLine(string.Format("^sip screen name 2: \"{0}\"", Convert.ToString(Model.Effective_Caller_Id_Name) + ","))
                //                .AppendLine(string.Format("^sip line1 auth name: {0}", Convert.ToString(Model.extension) + ","))
                //                .AppendLine(string.Format("^sip line1 password: {0}", Convert.ToString(Model.vm_password) + ","))
                //                .AppendLine(string.Format("^sip line1 user name: {0}", Convert.ToString(Model.extension) + ","))
                //                .AppendLine(string.Format("^sip line1 display name: \"{0}\"", Convert.ToString(Model.Effective_Caller_Id_Name) + ","))
                //                .AppendLine(string.Format("^sip line1 screen name: \"{0}\"", Convert.ToString(Model.extension) + ","))
                //                .AppendLine(string.Format("^sip line1 screen name 2: \"{0}\"", Convert.ToString(Model.Effective_Caller_Id_Name) + ","))
                //                .AppendLine(string.Format("^sip proxy ip: {0}", Convert.ToString(In.domain_name + ",")))
                //                .AppendLine(string.Format("^sip registrar ip: {0}", Convert.ToString(In.domain_name + ",")))
                //                .AppendLine(string.Format("^sip send line: 1") + ",")
                //                 .AppendLine(string.Format("^sip pai: 0") + ",")
                //                .AppendLine(string.Format("^hostname: pbx.ext.{0}", Convert.ToString(Model.extension) + ","))
                //                .AppendLine(string.Format("^admin password: {0}", Convert.ToString(Model.admin_password) + ","))
                //                .AppendLine(string.Format("^sip stun ip: {0}", System.Configuration.ConfigurationManager.AppSettings["SIPSTUNIP"]) + ",")
                //                .AppendLine(string.Format("^sip stun port: {0}", System.Configuration.ConfigurationManager.AppSettings["SIPSTUNPORT"]) + ",");
                //return sb.ToString(); 
                #endregion

                StringBuilder sb = new StringBuilder()
                .AppendLine(string.Format("#!version:1.0.0.1"))
                .AppendLine(string.Format("### This file is the exported MAC-all.cfg."))
                .AppendLine(string.Format("account.1.auth_name = {0}", Convert.ToString(Model.extension)))
                .AppendLine(string.Format("account.1.password = {0}", Convert.ToString(Model.vm_password)))
                .AppendLine(string.Format("account.1.codec.pcmu.enable = {0}", 0))
                .AppendLine(string.Format("account.1.codec.pcmu.priority = {0}", 0))
                .AppendLine(string.Format("account.1.codec.pcma.priority = {0}", 1))
                .AppendLine(string.Format("account.1.codec.g729.enable = {0}", 0))
                .AppendLine(string.Format("account.1.codec.g729.priority = {0}", 0))
                .AppendLine(string.Format("account.1.codec.g722.enable = {0}", 0))
                .AppendLine(string.Format("account.1.codec.g722.priority = {0}", 0))
                .AppendLine(string.Format("account.1.display_name = {0}", Convert.ToString(Model.Effective_Caller_Id_Name)))
                .AppendLine(string.Format("account.1.enable = {0}", 1))
                .AppendLine(string.Format("account.1.label = {0}", Convert.ToString(Model.Effective_Caller_Id_Name)))
                .AppendLine(string.Format("account.1.sip_server.1.address = {0}", Convert.ToString(In.domain_name)))
                .AppendLine(string.Format("account.1.user_name = {0}", Convert.ToString(Model.extension)))
                .AppendLine(string.Format("account.2.codec.pcmu.priority = {0}", 1))
                .AppendLine(string.Format("account.2.codec.pcma.priority = 0"))
                .AppendLine(string.Format("account.2.codec.g729.priority = 2"))
                .AppendLine(string.Format("account.2.codec.g722.priority = 3"))
                .AppendLine(string.Format("account.3.codec.pcmu.priority = 1"))
                .AppendLine(string.Format("account.3.codec.pcma.priority = 0"))
                .AppendLine(string.Format("account.3.codec.g729.priority = 2"))
                .AppendLine(string.Format("account.3.codec.g722.priority = 3"))
                .AppendLine(string.Format("###  Static Configuration  ###"))
                .AppendLine(string.Format("static.auto_provision.dhcp_option.option60_value = %EMPTY%"))
                .AppendLine(string.Format("static.auto_provision.repeat.enable = 1"))
               // .AppendLine(string.Format("static.auto_provision.server.url = {0}", "http://prov.mundio.com/Deskphone/3WWJ46S4"))
                .AppendLine(string.Format("static.security.trust_certificates = 0"));
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("[Failed Generate Config File Deskphone] " + ex.Message);
            }
        } 


        public static string VtechContent(MyAccountCompany In, DeskPhoneModel Model, int customerID)
        {
            Log.Debug("MyAccountCompany In : {0}", JsonConvert.SerializeObject(In));
            Log.Debug("DeskPhoneModel Model : {0}", JsonConvert.SerializeObject(Model));
            //Log.Debug("customerID : {0}", customerID);

            try
            { 
                StringBuilder sb = new StringBuilder()
                      .AppendLine(string.Format("time_date.date_format = DD/MM/YY"))
                      .AppendLine(string.Format("time_date.24hr_clock = 1"))
                      .AppendLine(string.Format("time_date.ntp_dhcp_option = 0"))
                      .AppendLine(string.Format("time_date.ntp_server = 1"))
                      .AppendLine(string.Format("time_date.ntp_server_addr = europe.pool.ntp.org"))
                      .AppendLine(string.Format("time_date.ntp_server_update_interval = 1000"))
                      .AppendLine(string.Format("time_date.timezone_dhcp_option = 0"))
                      .AppendLine(string.Format("time_date.selected_timezone = Europe/London"))
                      .AppendLine(string.Format("time_date.daylight_saving_auto_adjust = 1"))
                      .AppendLine(string.Format("time_date.daylight_saving_user_defined = 0"))
                      .AppendLine(string.Format("time_date.daylight_saving_start_month = March"))
                      .AppendLine(string.Format("time_date.daylight_saving_start_week = 5"))
                      .AppendLine(string.Format("time_date.daylight_saving_start_day = Sunday"))
                      .AppendLine(string.Format("time_date.daylight_saving_start_hour = 2"))
                      .AppendLine(string.Format("time_date.daylight_saving_end_month = October"))
                      .AppendLine(string.Format("time_date.daylight_saving_end_week = 5"))
                      .AppendLine(string.Format("time_date.daylight_saving_end_day = Sunday"))
                      .AppendLine(string.Format("time_date.daylight_saving_end_hour = 2"))
                      .AppendLine(string.Format("time_date.daylight_saving_amount = 60"))
                      .AppendLine(string.Format("profile.admin.access_password = "))
                      .AppendLine(string.Format("profile.user.access_password = "))
                      .AppendLine(string.Format("remoteDir.ldap_directory_name = "))
                      .AppendLine(string.Format("remoteDir.ldap_number_filter = "))
                      .AppendLine(string.Format("remoteDir.ldap_firstname_filter = "))
                      .AppendLine(string.Format("remoteDir.ldap_lastname_filter = "))
                      .AppendLine(string.Format("remoteDir.ldap_server_address = "))
                      .AppendLine(string.Format("remoteDir.ldap_port = 389"))
                      .AppendLine(string.Format("remoteDir.ldap_authentication_type = simple"))
                      .AppendLine(string.Format("remoteDir.ldap_base = "))
                      .AppendLine(string.Format("remoteDir.ldap_user_name = "))
                      .AppendLine(string.Format("remoteDir.ldap_access_password = "))
                      .AppendLine(string.Format("remoteDir.ldap_max_hits = 200"))
                      .AppendLine(string.Format("remoteDir.ldap_work_number_attributes = "))
                      .AppendLine(string.Format("remoteDir.ldap_mobile_number_attributes = "))
                      .AppendLine(string.Format("remoteDir.ldap_other_number_attributes = "))
                      .AppendLine(string.Format("remoteDir.ldap_protocol_version = version_3"))
                      .AppendLine(string.Format("remoteDir.ldap_search_delay = 0"))
                      .AppendLine(string.Format("remoteDir.ldap_incall_lookup_enable = 0"))
                      .AppendLine(string.Format("remoteDir.ldap_outcall_lookup_enable = 0"))
                      .AppendLine(string.Format("remoteDir.ldap_enable = 0"))
                      .AppendLine(string.Format("remoteDir.ldap_firstname_attribute = "))
                      .AppendLine(string.Format("remoteDir.ldap_lastname_attribute = "))
                      .AppendLine(string.Format("remoteDir.ldap_check_certificate = 0"))
                      .AppendLine(string.Format("remoteDir.broadsoft_enable = 0"))
                      .AppendLine(string.Format("remoteDir.broadsoft_display_name = "))
                      .AppendLine(string.Format("remoteDir.broadsoft_server = "))
                      .AppendLine(string.Format("remoteDir.broadsoft_port = 0"))
                      .AppendLine(string.Format("remoteDir.broadsoft_user_name ="))
                      .AppendLine(string.Format("remoteDir.broadsoft_access_password = "))
                      .AppendLine(string.Format("remoteDir.broadsoft_dir_type = Group"))
                      .AppendLine(string.Format("remoteDir.broadsoft_check_certificate = 0"))
                      .AppendLine(string.Format("sip_account.1.sip_account_enable = 1"))
                      .AppendLine(string.Format("sip_account.1.label = {0}", Convert.ToString(Model.Effective_Caller_Id_Name)))
                      .AppendLine(string.Format("sip_account.1.display_name = {0}", Convert.ToString(Model.Effective_Caller_Id_Name)))
                      .AppendLine(string.Format("sip_account.1.user_id = {0}", Convert.ToString(Model.extension)))
                      .AppendLine(string.Format("sip_account.1.authentication_name =  {0}", Convert.ToString(Model.extension)))
                      .AppendLine(string.Format("sip_account.1.authentication_access_password =  {0}", Convert.ToString(Model.vm_password)))
                      .AppendLine(string.Format("sip_account.1.dial_plan = x+P"))
                      .AppendLine(string.Format("sip_account.1.inter_digit_timeout = 3"))
                      .AppendLine(string.Format("sip_account.1.maximum_call_number = 6"))
                      .AppendLine(string.Format("sip_account.1.auto_answer_enable = 0"))
                      .AppendLine(string.Format("sip_account.1.auto_answer_during_active_call = 0"))
                      .AppendLine(string.Format("sip_account.1.feature_sync_enable = 0"))
                      .AppendLine(string.Format("sip_account.1.music_on_hold_enable = 1"))
                      .AppendLine(string.Format("sip_account.1.mwi_enable = 0"))
                      .AppendLine(string.Format("sip_account.1.mwi_ignore_unsolicited = 1"))
                      .AppendLine(string.Format("sip_account.1.mwi_uri = "))
                      .AppendLine(string.Format("sip_account.1.mwi_subscription_expires = 3600"))
                      .AppendLine(string.Format("sip_account.1.stutter_dial_tone_enable = 1"))
                      .AppendLine(string.Format("sip_account.1.voice_encryption_enable = 0"))
                      .AppendLine(string.Format("sip_account.1.primary_sip_server_port = 5060"))
                      .AppendLine(string.Format("sip_account.1.primary_sip_server_address = {0}", Convert.ToString(In.domain_name)))
                      .AppendLine(string.Format("sip_account.1.primary_outbound_proxy_server_port = 5060"))
                      .AppendLine(string.Format("sip_account.1.primary_outbound_proxy_server_address = 0.0.0.0"))
                      .AppendLine(string.Format("sip_account.1.primary_registration_server_port = 5060"))
                      .AppendLine(string.Format("sip_account.1.primary_registration_server_address = {0}", Convert.ToString(In.domain_name)))
                      .AppendLine(string.Format("sip_account.1.primary_registration_expires = 3600"))
                      .AppendLine(string.Format("sip_account.1.access_code_page = "))
                      .AppendLine(string.Format("sip_account.1.access_code_park_call = "))
                      .AppendLine(string.Format("sip_account.1.access_code_retrieve_parked_call = "))
                      .AppendLine(string.Format("sip_account.1.access_code_retrieve_voicemail = "))
                      .AppendLine(string.Format("sip_account.1.access_code_dnd_on = "))
                      .AppendLine(string.Format("sip_account.1.access_code_dnd_off = "))
                      .AppendLine(string.Format("sip_account.1.access_code_cfa_on = "))
                      .AppendLine(string.Format("sip_account.1.access_code_cfa_off = "))
                      .AppendLine(string.Format("sip_account.1.access_code_cfna_on = "))
                      .AppendLine(string.Format("sip_account.1.access_code_cfna_off = "))
                      .AppendLine(string.Format("sip_account.1.access_code_cfb_on = "))
                      .AppendLine(string.Format("sip_account.1.access_code_cfb_off = "))
                      .AppendLine(string.Format("sip_account.1.access_code_anonymous_call_block_on = "))
                      .AppendLine(string.Format("sip_account.1.access_code_anonymous_call_block_off = "))
                      .AppendLine(string.Format("sip_account.1.access_code_outgoing_call_anonymous_on = "))
                      .AppendLine(string.Format("sip_account.1.access_code_outgoing_call_anonymous_off = "))
                      .AppendLine(string.Format("sip_account.1.access_code_call_waiting_on = "))
                      .AppendLine(string.Format("sip_account.1.access_code_call_waiting_off = "))
                      .AppendLine(string.Format("sip_account.1.access_code_group_call_pickup = "))
                      .AppendLine(string.Format("sip_account.1.access_code_direct_call_pickup = "))
                      .AppendLine(string.Format("sip_account.1.nat_traversal_stun_enable = 0"))
                      .AppendLine(string.Format("sip_account.1.nat_traversal_stun_server_port = 3478"))
                      .AppendLine(string.Format("sip_account.1.nat_traversal_stun_server_address = "))
                      .AppendLine(string.Format("sip_account.1.nat_traversal_udp_keep_alive_enable = 1"))
                      .AppendLine(string.Format("sip_account.1.nat_traversal_udp_keep_alive_interval = 30"))
                      .AppendLine(string.Format("sip_account.1.network_conference_enable = 0"))
                      .AppendLine(string.Format("sip_account.1.network_bridge_uri = "))
                      .AppendLine(string.Format("sip_account.1.sip_session_timer_enable = 0"))
                      .AppendLine(string.Format("sip_account.1.sip_session_timer_min = 90"))
                      .AppendLine(string.Format("sip_account.1.sip_session_timer_max = 1800"))
                      .AppendLine(string.Format("sip_account.1.dtmf_transport_method = auto"))
                      .AppendLine(string.Format("sip_account.1.codec_priority.1 = g711a"))
                      .AppendLine(string.Format("sip_account.1.codec_priority.2 = g711a"))
                      .AppendLine(string.Format("sip_account.1.codec_priority.3 = g722"))
                      .AppendLine(string.Format("sip_account.1.codec_priority.4 = g726"))
                      .AppendLine(string.Format("sip_account.1.codec_priority.5 = none"))
                      .AppendLine(string.Format("sip_account.1.unregister_after_reboot_enable = 0"))
                      .AppendLine(string.Format("sip_account.1.transport_mode = udp"))
                      .AppendLine(string.Format("sip_account.1.backup_outbound_proxy_server_port = 5060"))
                      .AppendLine(string.Format("sip_account.1.backup_outbound_proxy_server_address = 0.0.0.0"))
                      .AppendLine(string.Format("sip_account.1.registration_retry_time = 10"))
                      .AppendLine(string.Format("sip_account.1.local_sip_port = 5060"))
                      .AppendLine(string.Format("sip_account.1.dscp = 46"))
                      .AppendLine(string.Format("sip_account.1.sip_dscp = 26"))
                      .AppendLine(string.Format("sip_account.1.check_trusted_certificate = 0"))
                      .AppendLine(string.Format("sip_account.1.normal_jitter = 80"))
                      .AppendLine(string.Format("sip_account.1.g729_annexb_enable = 0"))
                      .AppendLine(string.Format("sip_account.1.park_variant = broadsoft"))
                      .AppendLine(string.Format("sip_account.1.preferred_ptime = 20"))
                      .AppendLine(string.Format("sip_account.1.call_rejection_response_code = 486"))
                      .AppendLine(string.Format("sip_account.2.sip_account_enable = 0"))
                      .AppendLine(string.Format("sip_account.2.label = "))
                      .AppendLine(string.Format("sip_account.2.display_name = "))
                      .AppendLine(string.Format("sip_account.2.user_id = "))
                      .AppendLine(string.Format("sip_account.2.authentication_name = "))
                      .AppendLine(string.Format("sip_account.2.authentication_access_password = "))
                      .AppendLine(string.Format("sip_account.2.dial_plan = x+P"))
                      .AppendLine(string.Format("sip_account.2.inter_digit_timeout = 3"))
                      .AppendLine(string.Format("sip_account.2.maximum_call_number = 6"))
                      .AppendLine(string.Format("sip_account.2.auto_answer_enable = 0"))
                      .AppendLine(string.Format("sip_account.2.auto_answer_during_active_call = 0"))
                      .AppendLine(string.Format("sip_account.2.feature_sync_enable = 0"))
                      .AppendLine(string.Format("sip_account.2.music_on_hold_enable = 1"))
                      .AppendLine(string.Format("sip_account.2.mwi_enable = 0"))
                      .AppendLine(string.Format("sip_account.2.mwi_ignore_unsolicited = 1"))
                      .AppendLine(string.Format("sip_account.2.mwi_uri = "))
                      .AppendLine(string.Format("sip_account.2.mwi_subscription_expires = 3600"))
                      .AppendLine(string.Format("sip_account.2.stutter_dial_tone_enable = 1"))
                      .AppendLine(string.Format("sip_account.2.voice_encryption_enable = 0"))
                      .AppendLine(string.Format("sip_account.2.primary_sip_server_port = 5060"))
                      .AppendLine(string.Format("sip_account.2.primary_sip_server_address = "))
                      .AppendLine(string.Format("sip_account.2.primary_outbound_proxy_server_port = 5060"))
                      .AppendLine(string.Format("sip_account.2.primary_outbound_proxy_server_address = "))
                      .AppendLine(string.Format("sip_account.2.primary_registration_server_port = 5060"))
                      .AppendLine(string.Format("sip_account.2.primary_registration_server_address = "))
                      .AppendLine(string.Format("sip_account.2.primary_registration_expires = 3600"))
                      .AppendLine(string.Format("sip_account.2.access_code_page = "))
                      .AppendLine(string.Format("sip_account.2.access_code_park_call ="))
                      .AppendLine(string.Format("sip_account.2.access_code_retrieve_parked_call = "))
                      .AppendLine(string.Format("sip_account.2.access_code_retrieve_voicemail = "))
                      .AppendLine(string.Format("sip_account.2.access_code_dnd_on = "))
                      .AppendLine(string.Format("sip_account.2.access_code_dnd_off = "))
                      .AppendLine(string.Format("sip_account.2.access_code_cfa_on = "))
                      .AppendLine(string.Format("sip_account.2.access_code_cfa_off = "))
                      .AppendLine(string.Format("sip_account.2.access_code_cfna_on = "))
                      .AppendLine(string.Format("sip_account.2.access_code_cfna_off = "))
                      .AppendLine(string.Format("sip_account.2.access_code_cfb_on = "))
                      .AppendLine(string.Format("sip_account.2.access_code_cfb_off = "))
                      .AppendLine(string.Format("sip_account.2.access_code_anonymous_call_block_on = "))
                      .AppendLine(string.Format("sip_account.2.access_code_anonymous_call_block_off = "))
                      .AppendLine(string.Format("sip_account.2.access_code_outgoing_call_anonymous_on = "))
                      .AppendLine(string.Format("sip_account.2.access_code_outgoing_call_anonymous_off = "))
                      .AppendLine(string.Format("sip_account.2.access_code_call_waiting_on = "))
                      .AppendLine(string.Format("sip_account.2.access_code_call_waiting_off = "))
                      .AppendLine(string.Format("sip_account.2.access_code_group_call_pickup = "))
                      .AppendLine(string.Format("sip_account.2.access_code_direct_call_pickup = "))
                      .AppendLine(string.Format("sip_account.2.nat_traversal_stun_enable = 0"))
                      .AppendLine(string.Format("sip_account.2.nat_traversal_stun_server_port = 3478"))
                      .AppendLine(string.Format("sip_account.2.nat_traversal_stun_server_address = "))
                      .AppendLine(string.Format("sip_account.2.nat_traversal_udp_keep_alive_enable = 1"))
                      .AppendLine(string.Format("sip_account.2.nat_traversal_udp_keep_alive_interval = 30"))
                      .AppendLine(string.Format("sip_account.2.network_conference_enable = 0"))
                      .AppendLine(string.Format("sip_account.2.network_bridge_uri = "))
                      .AppendLine(string.Format("sip_account.2.sip_session_timer_enable = 0"))
                      .AppendLine(string.Format("sip_account.2.sip_session_timer_min = 90"))
                      .AppendLine(string.Format("sip_account.2.sip_session_timer_max = 1800"))
                      .AppendLine(string.Format("sip_account.2.dtmf_transport_method = auto"))
                      .AppendLine(string.Format("sip_account.2.codec_priority.1 = g711u"))
                      .AppendLine(string.Format("sip_account.2.codec_priority.2 = g711a"))
                      .AppendLine(string.Format("sip_account.2.codec_priority.3 = g722"))
                      .AppendLine(string.Format("sip_account.2.codec_priority.4 = g726"))
                      .AppendLine(string.Format("sip_account.2.codec_priority.5 = none"))
                      .AppendLine(string.Format("sip_account.2.unregister_after_reboot_enable = 0"))
                      .AppendLine(string.Format("sip_account.2.transport_mode = udp"))
                      .AppendLine(string.Format("sip_account.2.backup_outbound_proxy_server_port = 5060"))
                      .AppendLine(string.Format("sip_account.2.backup_outbound_proxy_server_address = "))
                      .AppendLine(string.Format("sip_account.2.registration_retry_time = 10"))
                      .AppendLine(string.Format("sip_account.2.local_sip_port = 5070"))
                      .AppendLine(string.Format("sip_account.2.dscp = 46"))
                      .AppendLine(string.Format("sip_account.2.sip_dscp = 26"))
                      .AppendLine(string.Format("sip_account.2.check_trusted_certificate = 0"))
                      .AppendLine(string.Format("sip_account.2.normal_jitter = 80"))
                      .AppendLine(string.Format("sip_account.2.g729_annexb_enable = 0"))
                      .AppendLine(string.Format("sip_account.2.park_variant = broadsoft"))
                      .AppendLine(string.Format("sip_account.2.preferred_ptime = 20"))
                      .AppendLine(string.Format("sip_account.2.call_rejection_response_code = 486"))
                      .AppendLine(string.Format("sip_account.3.sip_account_enable = 0"))
                      .AppendLine(string.Format("sip_account.3.label = "))
                      .AppendLine(string.Format("sip_account.3.display_name = "))
                      .AppendLine(string.Format("sip_account.3.user_id = "))
                      .AppendLine(string.Format("sip_account.3.authentication_name = "))
                      .AppendLine(string.Format("sip_account.3.authentication_access_password = "))
                      .AppendLine(string.Format("sip_account.3.dial_plan = x+P"))
                      .AppendLine(string.Format("sip_account.3.inter_digit_timeout = 3"))
                      .AppendLine(string.Format("sip_account.3.maximum_call_number = 6"))
                      .AppendLine(string.Format("sip_account.3.auto_answer_enable = 0"))
                      .AppendLine(string.Format("sip_account.3.auto_answer_during_active_call = 0"))
                      .AppendLine(string.Format("sip_account.3.feature_sync_enable = 0"))
                      .AppendLine(string.Format("sip_account.3.music_on_hold_enable = 1"))
                      .AppendLine(string.Format("sip_account.3.mwi_enable = 0"))
                      .AppendLine(string.Format("sip_account.3.mwi_ignore_unsolicited = 1"))
                      .AppendLine(string.Format("sip_account.3.mwi_uri = "))
                      .AppendLine(string.Format("sip_account.3.mwi_subscription_expires = 3600"))
                      .AppendLine(string.Format("sip_account.3.stutter_dial_tone_enable = 1"))
                      .AppendLine(string.Format("sip_account.3.voice_encryption_enable = 0"))
                      .AppendLine(string.Format("sip_account.3.primary_sip_server_port = 5060"))
                      .AppendLine(string.Format("sip_account.3.primary_sip_server_address = "))
                      .AppendLine(string.Format("sip_account.3.primary_outbound_proxy_server_port = 5060"))
                      .AppendLine(string.Format("sip_account.3.primary_outbound_proxy_server_address = "))
                      .AppendLine(string.Format("sip_account.3.primary_registration_server_port = 5060"))
                      .AppendLine(string.Format("sip_account.3.primary_registration_server_address = "))
                      .AppendLine(string.Format("sip_account.3.primary_registration_expires = 3600"))
                      .AppendLine(string.Format("sip_account.3.access_code_page = "))
                      .AppendLine(string.Format("sip_account.3.access_code_park_call = "))
                      .AppendLine(string.Format("sip_account.3.access_code_retrieve_parked_call = "))
                      .AppendLine(string.Format("sip_account.3.access_code_retrieve_voicemail = "))
                      .AppendLine(string.Format("sip_account.3.access_code_dnd_on = "))
                      .AppendLine(string.Format("sip_account.3.access_code_dnd_off = "))
                      .AppendLine(string.Format("sip_account.3.access_code_cfa_on = "))
                      .AppendLine(string.Format("sip_account.3.access_code_cfa_off = "))
                      .AppendLine(string.Format("sip_account.3.access_code_cfna_on = "))
                      .AppendLine(string.Format("sip_account.3.access_code_cfna_off = "))
                      .AppendLine(string.Format("sip_account.3.access_code_cfb_on = "))
                      .AppendLine(string.Format("sip_account.3.access_code_cfb_off = "))
                      .AppendLine(string.Format("sip_account.3.access_code_anonymous_call_block_on = "))
                      .AppendLine(string.Format("sip_account.3.access_code_anonymous_call_block_off = "))
                      .AppendLine(string.Format("sip_account.3.access_code_outgoing_call_anonymous_on = "))
                      .AppendLine(string.Format("sip_account.3.access_code_outgoing_call_anonymous_off = "))
                      .AppendLine(string.Format("sip_account.3.access_code_call_waiting_on = "))
                      .AppendLine(string.Format("sip_account.3.access_code_call_waiting_off = "))
                      .AppendLine(string.Format("sip_account.3.access_code_group_call_pickup = "))
                      .AppendLine(string.Format("sip_account.3.access_code_direct_call_pickup = "))
                      .AppendLine(string.Format("sip_account.3.nat_traversal_stun_enable = 0"))
                      .AppendLine(string.Format("sip_account.3.nat_traversal_stun_server_port = 3478"))
                      .AppendLine(string.Format("sip_account.3.nat_traversal_stun_server_address = "))
                      .AppendLine(string.Format("sip_account.3.nat_traversal_udp_keep_alive_enable = 1"))
                      .AppendLine(string.Format("sip_account.3.nat_traversal_udp_keep_alive_interval = 30"))
                      .AppendLine(string.Format("sip_account.3.network_conference_enable = 0"))
                      .AppendLine(string.Format("sip_account.3.network_bridge_uri = "))
                      .AppendLine(string.Format("sip_account.3.sip_session_timer_enable = 0"))
                      .AppendLine(string.Format("sip_account.3.sip_session_timer_min = 90"))
                      .AppendLine(string.Format("sip_account.3.sip_session_timer_max = 1800"))
                      .AppendLine(string.Format("sip_account.3.dtmf_transport_method = auto"))
                      .AppendLine(string.Format("sip_account.3.codec_priority.1 = g711u"))
                      .AppendLine(string.Format("sip_account.3.codec_priority.2 = g711a"))
                      .AppendLine(string.Format("sip_account.3.codec_priority.3 = g722"))
                      .AppendLine(string.Format("sip_account.3.codec_priority.4 = g726"))
                      .AppendLine(string.Format("sip_account.3.codec_priority.5 = none"))
                      .AppendLine(string.Format("sip_account.3.unregister_after_reboot_enable = 0"))
                      .AppendLine(string.Format("sip_account.3.transport_mode = udp"))
                      .AppendLine(string.Format("sip_account.3.backup_outbound_proxy_server_port = 5060"))
                      .AppendLine(string.Format("sip_account.3.backup_outbound_proxy_server_address = "))
                      .AppendLine(string.Format("sip_account.3.registration_retry_time = 10"))
                      .AppendLine(string.Format("sip_account.3.local_sip_port = 5080"))
                      .AppendLine(string.Format("sip_account.3.dscp = 46"))
                      .AppendLine(string.Format("sip_account.3.sip_dscp = 26"))
                      .AppendLine(string.Format("sip_account.3.check_trusted_certificate = 0"))
                      .AppendLine(string.Format("sip_account.3.normal_jitter = 80"))
                      .AppendLine(string.Format("sip_account.3.g729_annexb_enable = 0"))
                      .AppendLine(string.Format("sip_account.3.park_variant = broadsoft"))
                      .AppendLine(string.Format("sip_account.3.preferred_ptime = 20"))
                      .AppendLine(string.Format("sip_account.3.call_rejection_response_code = 486"))
                      .AppendLine(string.Format("sip_account.use_first_trusted_certificate_for_all = 0"))
                      .AppendLine(string.Format("network.ip.dhcp_enable = 1"))
                      .AppendLine(string.Format("network.ip.dns1 = "))
                      .AppendLine(string.Format("network.ip.dns2 = "))
                      .AppendLine(string.Format("network.ip.static_ip_addr = "))
                      .AppendLine(string.Format("network.ip.subnet_mask = "))
                      .AppendLine(string.Format("network.ip.gateway_addr = "))
                      .AppendLine(string.Format("network.ip.dns_cache_clear_timeout = 60"))
                      .AppendLine(string.Format("network.nat.masquerading_enable = 0"))
                      .AppendLine(string.Format("network.nat.public_ip_addr = "))
                      .AppendLine(string.Format("network.nat.public_sip_port = 5060"))
                      .AppendLine(string.Format("network.nat.public_rtp_port_start = 18000"))
                      .AppendLine(string.Format("network.nat.public_rtp_port_end = 19000"))
                      .AppendLine(string.Format("network.vlan.wan.enable = 0"))
                      .AppendLine(string.Format("network.vlan.wan.id = 0"))
                      .AppendLine(string.Format("network.vlan.wan.priority = 0"))
                      .AppendLine(string.Format("network.rtp.port_start = 18000"))
                      .AppendLine(string.Format("network.rtp.port_end = 19000"))
                      .AppendLine(string.Format("network.lldp_med.enable = 1"))
                      .AppendLine(string.Format("network.lldp_med.interval = 30"))
                      .AppendLine(string.Format("network.eapol.enable = 0"))
                      .AppendLine(string.Format("network.eapol.identity = "))
                      .AppendLine(string.Format("network.eapol.access_password = "))
                      .AppendLine(string.Format("network.vendor_class_id = Vtech Vesa VCS754A"))
                      .AppendLine(string.Format("network.user_class = Vtech Vesa VCS754A"))
                      .AppendLine(string.Format("provisioning.bootup_check_enable = 1"))
                      .AppendLine(string.Format("provisioning.crypto_enable = 0"))
                      .AppendLine(string.Format("provisioning.crypto_passphrase = "))
                      .AppendLine(string.Format("provisioning.dhcp_option_enable = 1"))
                      .AppendLine(string.Format("provisioning.dhcp_option_priority_1 = 66"))
                      .AppendLine(string.Format("provisioning.dhcp_option_priority_2 = 159"))
                      .AppendLine(string.Format("provisioning.dhcp_option_priority_3 = 160"))
                      .AppendLine(string.Format("provisioning.firmware_url = "))
                      .AppendLine(string.Format("provisioning.fw_server_username = "))
                      .AppendLine(string.Format("provisioning.fw_server_access_password = "))
                      .AppendLine(string.Format("provisioning.resync_mode = config_and_firmware"))
                      .AppendLine(string.Format("provisioning.resync_time = 0"))
                    //.AppendLine(string.Format("provisioning.server_address = http://im01.unifiedring.co.uk/config/14AEDB10D9FB.cfg"))
                      .AppendLine(string.Format("provisioning.server_username = "))
                      .AppendLine(string.Format("provisioning.server_access_password = "))
                      .AppendLine(string.Format("provisioning.check_trusted_certificate = 0"))
                      .AppendLine(string.Format("provisioning.click_to_dial = 0"))
                      .AppendLine(string.Format("provisioning.remote_check_sync_enable = 1"))
                      .AppendLine(string.Format("provisioning.pnp_enable = 1"))
                      .AppendLine(string.Format("provisioning.pnp_response_timeout = 10"))
                      .AppendLine(string.Format("provisioning.schedule_mode = interval"))
                      .AppendLine(string.Format("provisioning.weekdays = "))
                      .AppendLine(string.Format("provisioning.weekdays_start_hr = 0"))
                      .AppendLine(string.Format("provisioning.weekdays_end_hr = 0"))
                      .AppendLine(string.Format("user_pref.language = en-GB"))
                      .AppendLine(string.Format("user_pref.text_input_option = uc_western,lc_western,number"))
                      .AppendLine(string.Format("user_pref.web_language = en-GB"))
                      .AppendLine(string.Format("user_pref.lcd_contrast = 4"))
                      .AppendLine(string.Format("user_pref.backlight = high"))
                      .AppendLine(string.Format("user_pref.idle_backlight = off"))
                      .AppendLine(string.Format("user_pref.backlight_timeout = 10"))
                      .AppendLine(string.Format("user_pref.absent_timeout = 30"))
                      .AppendLine(string.Format("user_pref.idle_to_logo_timeout = 0"))
                      .AppendLine(string.Format("user_pref.logo_to_idle_timeout = 60"))
                      .AppendLine(string.Format("user_pref.account.1.ringer = 1"))
                      .AppendLine(string.Format("user_pref.account.2.ringer = 1"))
                      .AppendLine(string.Format("user_pref.account.3.ringer = 1"))
                      .AppendLine(string.Format("user_pref.ringer_volume = 5"))
                      .AppendLine(string.Format("user_pref.speaker_volume = 5"))
                      .AppendLine(string.Format("user_pref.key_beep_enable = 1"))
                      .AppendLine(string.Format("user_pref.hold_reminder.enable = 1"))
                      .AppendLine(string.Format("user_pref.hold_reminder.interval = 30"))
                      .AppendLine(string.Format("user_pref.call_waiting.mode = enable"))
                      .AppendLine(string.Format("user_pref.call_waiting.tone_enable = 1"))
                      .AppendLine(string.Format("user_pref.call_waiting.tone_interval = 30"))
                      .AppendLine(string.Format("user_pref.notify.led.missed_call.enable = 0"))
                      .AppendLine(string.Format("user_pref.blf_indication_option = 2"))
                      .AppendLine(string.Format("log.syslog_enable = 0"))
                      .AppendLine(string.Format("log.syslog_level = 2"))
                      .AppendLine(string.Format("log.syslog_server_address = "))
                      .AppendLine(string.Format("log.syslog_server_port = 514"))
                      .AppendLine(string.Format("page_zone.call_priority_threshold = 2"))
                      .AppendLine(string.Format("page_zone.1.name = "))
                      .AppendLine(string.Format("page_zone.1.multicast_address = "))
                      .AppendLine(string.Format("page_zone.1.multicast_port = "))
                      .AppendLine(string.Format("page_zone.1.accept_incoming_page = 1"))
                      .AppendLine(string.Format("page_zone.1.priority = 5"))
                      .AppendLine(string.Format("page_zone.2.name = "))
                      .AppendLine(string.Format("page_zone.2.multicast_address = "))
                      .AppendLine(string.Format("page_zone.2.multicast_port = "))
                      .AppendLine(string.Format("page_zone.2.accept_incoming_page = 1"))
                      .AppendLine(string.Format("page_zone.2.priority = 5"))
                      .AppendLine(string.Format("page_zone.3.name = "))
                      .AppendLine(string.Format("page_zone.3.multicast_address = "))
                      .AppendLine(string.Format("page_zone.3.multicast_port = "))
                      .AppendLine(string.Format("page_zone.3.accept_incoming_page = 1"))
                      .AppendLine(string.Format("page_zone.3.priority = 5"))
                      .AppendLine(string.Format("page_zone.4.name = "))
                      .AppendLine(string.Format("page_zone.4.multicast_address = "))
                      .AppendLine(string.Format("page_zone.4.multicast_port = "))
                      .AppendLine(string.Format("page_zone.4.accept_incoming_page = 1"))
                      .AppendLine(string.Format("page_zone.4.priority = 5"))
                      .AppendLine(string.Format("page_zone.5.name = "))
                      .AppendLine(string.Format("page_zone.5.multicast_address = "))
                      .AppendLine(string.Format("page_zone.5.multicast_port = "))
                      .AppendLine(string.Format("page_zone.5.accept_incoming_page = 1"))
                      .AppendLine(string.Format("page_zone.5.priority = 5"))
                      .AppendLine(string.Format("page_zone.6.name = "))
                      .AppendLine(string.Format("page_zone.6.multicast_address = "))
                      .AppendLine(string.Format("page_zone.6.multicast_port = "))
                      .AppendLine(string.Format("page_zone.6.accept_incoming_page = 1"))
                      .AppendLine(string.Format("page_zone.6.priority = 5"))
                      .AppendLine(string.Format("page_zone.7.name = "))
                      .AppendLine(string.Format("page_zone.7.multicast_address = "))
                      .AppendLine(string.Format("page_zone.7.multicast_port = "))
                      .AppendLine(string.Format("page_zone.7.accept_incoming_page = 1"))
                      .AppendLine(string.Format("page_zone.7.priority = 5"))
                      .AppendLine(string.Format("page_zone.8.name = "))
                      .AppendLine(string.Format("page_zone.8.multicast_address = "))
                      .AppendLine(string.Format("page_zone.8.multicast_port = "))
                      .AppendLine(string.Format("page_zone.8.accept_incoming_page = 1"))
                      .AppendLine(string.Format("page_zone.8.priority = 5"))
                      .AppendLine(string.Format("page_zone.9.name = "))
                      .AppendLine(string.Format("page_zone.9.multicast_address = "))
                      .AppendLine(string.Format("page_zone.9.multicast_port = "))
                      .AppendLine(string.Format("page_zone.9.accept_incoming_page = 1"))
                      .AppendLine(string.Format("page_zone.9.priority = 5"))
                      .AppendLine(string.Format("page_zone.10.name = "))
                      .AppendLine(string.Format("page_zone.10.multicast_address = "))
                      .AppendLine(string.Format("page_zone.10.multicast_port = "))
                      .AppendLine(string.Format("page_zone.10.accept_incoming_page = 1"))
                      .AppendLine(string.Format("page_zone.10.priority = 5"))
                      .AppendLine(string.Format("tone.call_waiting_tone.num_of_elements = 1"))
                      .AppendLine(string.Format("tone.call_waiting_tone.element.1 = 1 440 -150 0 0 0 0 0 0 500 0 1"))
                      .AppendLine(string.Format("tone.call_waiting_tone.element.2 = "))
                      .AppendLine(string.Format("tone.call_waiting_tone.element.3 = "))
                      .AppendLine(string.Format("tone.call_waiting_tone.element.4 = "))
                      .AppendLine(string.Format("tone.call_waiting_tone.element.5 = "))
                      .AppendLine(string.Format("tone.hold_reminder.num_of_elements = 1"))
                      .AppendLine(string.Format("tone.hold_reminder.element.1 = 1 770 -120 0 0 0 0 0 0 300 0 1"))
                      .AppendLine(string.Format("tone.hold_reminder.element.2 = "))
                      .AppendLine(string.Format("tone.hold_reminder.element.3 = "))
                      .AppendLine(string.Format("tone.hold_reminder.element.4 = "))
                      .AppendLine(string.Format("tone.hold_reminder.element.5 = "))
                      .AppendLine(string.Format("tone.inside_dial_tone.num_of_elements = 1"))
                      .AppendLine(string.Format("tone.inside_dial_tone.element.1 = 2 440 -180 350 -180 0 0 0 0 4294967295 0 65535"))
                      .AppendLine(string.Format("tone.inside_dial_tone.element.2 = "))
                      .AppendLine(string.Format("tone.inside_dial_tone.element.3 = "))
                      .AppendLine(string.Format("tone.inside_dial_tone.element.4 = "))
                      .AppendLine(string.Format("tone.inside_dial_tone.element.5 = "))
                      .AppendLine(string.Format("tone.stutter_dial_tone.num_of_elements = 2"))
                      .AppendLine(string.Format("tone.stutter_dial_tone.element.1 = 2 440 -180 350 -180 0 0 0 0 100 100 10"))
                      .AppendLine(string.Format("tone.stutter_dial_tone.element.2 = 2 440 -180 350 -180 0 0 0 0 4294967295 0 65535"))
                      .AppendLine(string.Format("tone.stutter_dial_tone.element.3 = "))
                      .AppendLine(string.Format("tone.stutter_dial_tone.element.4 = "))
                      .AppendLine(string.Format("tone.stutter_dial_tone.element.5 = "))
                      .AppendLine(string.Format("tone.busy_tone.num_of_elements = 1"))
                      .AppendLine(string.Format("tone.busy_tone.element.1 = 2 480 -180 620 -180 0 0 0 0 500 500 65535"))
                      .AppendLine(string.Format("tone.busy_tone.element.2 = "))
                      .AppendLine(string.Format("tone.busy_tone.element.3 = "))
                      .AppendLine(string.Format("tone.busy_tone.element.4 = "))
                      .AppendLine(string.Format("tone.busy_tone.element.5 = "))
                      .AppendLine(string.Format("tone.ring_back_tone.num_of_elements = 1"))
                      .AppendLine(string.Format("tone.ring_back_tone.element.1 = 2 440 -180 480 -180 0 0 0 0 2000 4000 65535"))
                      .AppendLine(string.Format("tone.ring_back_tone.element.2 = "))
                      .AppendLine(string.Format("tone.ring_back_tone.element.3 = "))
                      .AppendLine(string.Format("tone.ring_back_tone.element.4 = "))
                      .AppendLine(string.Format("tone.ring_back_tone.element.5 = "))
                      .AppendLine(string.Format("web.http_port = 80"))
                      .AppendLine(string.Format("web.https_port = 443"))
                      .AppendLine(string.Format("web.https_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.1.block_anonymous_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.1.outgoing_anonymous_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.1.dnd_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.1.dnd_incoming_calls = reject"))
                      .AppendLine(string.Format("call_settings.account.1.call_fwd_always_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.1.call_fwd_always_target = "))
                      .AppendLine(string.Format("call_settings.account.1.call_fwd_busy_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.1.call_fwd_busy_target = "))
                      .AppendLine(string.Format("call_settings.account.1.cfna_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.1.cfna_target = "))
                      .AppendLine(string.Format("call_settings.account.1.cfna_delay = 6"))
                      .AppendLine(string.Format("call_settings.account.2.block_anonymous_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.2.outgoing_anonymous_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.2.dnd_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.2.dnd_incoming_calls = reject"))
                      .AppendLine(string.Format("call_settings.account.2.call_fwd_always_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.2.call_fwd_always_target = "))
                      .AppendLine(string.Format("call_settings.account.2.call_fwd_busy_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.2.call_fwd_busy_target = "))
                      .AppendLine(string.Format("call_settings.account.2.cfna_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.2.cfna_target = "))
                      .AppendLine(string.Format("call_settings.account.2.cfna_delay = 6"))
                      .AppendLine(string.Format("call_settings.account.3.block_anonymous_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.3.outgoing_anonymous_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.3.dnd_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.3.dnd_incoming_calls = reject"))
                      .AppendLine(string.Format("call_settings.account.3.call_fwd_always_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.3.call_fwd_always_target = "))
                      .AppendLine(string.Format("call_settings.account.3.call_fwd_busy_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.3.call_fwd_busy_target = "))
                      .AppendLine(string.Format("call_settings.account.3.cfna_enable = 0"))
                      .AppendLine(string.Format("call_settings.account.3.cfna_target = "))
                      .AppendLine(string.Format("call_settings.account.3.cfna_delay = 6"))
                      .AppendLine(string.Format("call_settings.missed_call_alert_enable = 1"))
                      .AppendLine(string.Format("softkey.idle = redial,line,pc_spk,call_log,blank,dir"))
                      .AppendLine(string.Format("softkey.call_active = end,transfer,conf,xferline,confline"))
                      .AppendLine(string.Format("softkey.call_held = end,new,resume,transfer,conf,xferline,confline"))
                      .AppendLine(string.Format("softkey.live_dial = backspc,input,dial"))
                      .AppendLine(string.Format("speed_dial.0.name = "))
                      .AppendLine(string.Format("speed_dial.0.number = "))
                      .AppendLine(string.Format("speed_dial.0.account = 0"))
                      .AppendLine(string.Format("speed_dial.1.name = "))
                      .AppendLine(string.Format("speed_dial.1.number = "))
                      .AppendLine(string.Format("speed_dial.1.account = 0"))
                      .AppendLine(string.Format("speed_dial.2.name = "))
                      .AppendLine(string.Format("speed_dial.2.number = "))
                      .AppendLine(string.Format("speed_dial.2.account = 0"))
                      .AppendLine(string.Format("speed_dial.3.name = "))
                      .AppendLine(string.Format("speed_dial.3.number = "))
                      .AppendLine(string.Format("speed_dial.3.account = 0"))
                      .AppendLine(string.Format("speed_dial.4.name = "))
                      .AppendLine(string.Format("speed_dial.4.number = "))
                      .AppendLine(string.Format("speed_dial.4.account = 0"))
                      .AppendLine(string.Format("speed_dial.5.name = "))
                      .AppendLine(string.Format("speed_dial.5.number = "))
                      .AppendLine(string.Format("speed_dial.5.account = 0"))
                      .AppendLine(string.Format("speed_dial.6.name = "))
                      .AppendLine(string.Format("speed_dial.6.number = "))
                      .AppendLine(string.Format("speed_dial.6.account = 0"))
                      .AppendLine(string.Format("speed_dial.7.name = "))
                      .AppendLine(string.Format("speed_dial.7.number = "))
                      .AppendLine(string.Format("speed_dial.7.account = 0"))
                      .AppendLine(string.Format("speed_dial.8.name = "))
                      .AppendLine(string.Format("speed_dial.8.number = "))
                      .AppendLine(string.Format("speed_dial.8.account = 0"))
                      .AppendLine(string.Format("speed_dial.9.name = "))
                      .AppendLine(string.Format("speed_dial.9.number = "))
                      .AppendLine(string.Format("speed_dial.9.account = 0"))
                      .AppendLine(string.Format("ringersetting.1.ringer_text = "))
                      .AppendLine(string.Format("ringersetting.1.ringer_type = 1"))
                      .AppendLine(string.Format("ringersetting.2.ringer_text = "))
                      .AppendLine(string.Format("ringersetting.2.ringer_type = 1"))
                      .AppendLine(string.Format("ringersetting.3.ringer_text = "))
                      .AppendLine(string.Format("ringersetting.3.ringer_type = 1"))
                      .AppendLine(string.Format("ringersetting.4.ringer_text = "))
                      .AppendLine(string.Format("ringersetting.4.ringer_type = 1"))
                      .AppendLine(string.Format("ringersetting.5.ringer_text = "))
                      .AppendLine(string.Format("ringersetting.5.ringer_type = 1"))
                      .AppendLine(string.Format("ringersetting.6.ringer_text = "))
                      .AppendLine(string.Format("ringersetting.6.ringer_type = 1"))
                      .AppendLine(string.Format("ringersetting.7.ringer_text = "))
                      .AppendLine(string.Format("ringersetting.7.ringer_type = 1"))
                      .AppendLine(string.Format("ringersetting.8.ringer_text = "))
                      .AppendLine(string.Format("ringersetting.8.ringer_type = 1"));
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("[Failed Generate Config File Deskphone] " + ex.Message);
            }
        }


        public static string Get_Customer_Deskhpone_Manifest(MyAccountCompany In)
        {
            //string sql = string.Format("vt_myaccount_ipphone_search_v3 {0}", In.company_id);
            string sql = string.Format("UR_Myacc_Web_ipphone_search_v3 {0}", In.company_id);

            var sb = new StringBuilder();
            using (DataMapper _imapper = new DataMapper() { CatalogName = "CRM_VB" })
            {
                //var result = new List<Deskphone_Meta_Info>();
                var result = _imapper.MapDataTo<Deskphone_Meta_Info_V1>(sql);
                foreach (Deskphone_Meta_Info_V1 item in result)
                {
                    sb.AppendLine(string.Format("{0},{1},{2}", item.Effective_Caller_Id_Name, item.Extension, item.Flag_Status));
                }
            }
            return sb.ToString();
        }

        public static Deskphone_FTP_Info Check_RemoteLocation(int CustomerId, string Extension, string Mac_Code, string User,string filename)
        {
            WinSCPInvoker uploader = new WinSCPInvoker(string.Empty);
            var resx = Get_RemoteLocation(CustomerId, Extension, Mac_Code);//db hit
            if (resx == null)
            {
                var resy = Put_RemoteLocation(CustomerId, Extension, Mac_Code, User, filename);//db hit
                if (resy)
                {
                    resx = Get_RemoteLocation(CustomerId, Extension, Mac_Code);//db hit
                    uploader.Create_Folder(GlobalConfig.FTPDeskPhone, GlobalConfig.FTPDeskPhoneRemote, GlobalConfig.FTPDeskPhoneLocal, resx.Security_Id);//stringbuilder
                }
            }
            return resx;
        }

        public static Deskphone_FTP_Info Get_RemoteLocation(int CustomerId, string Extension, string MAC_Address)
        {
            //string sql = string.Format("crm_get_deskphone_remotefolder @customer_id ={0},@ext='{1}'", CustomerId, Extension);
            string sql = string.Format("UR_Myacc_Web_get_deskphone_remotefolder @customer_id ={0},@ext='{1}'", CustomerId, Extension);
            var result = new Deskphone_FTP_Info();
            using (DataMapper _imapper = new DataMapper() { CatalogName = "CRM_VB" })
            {
                result = _imapper.MapDataToSingle<Deskphone_FTP_Info>(sql);
            }
            return result;
        }

        public static bool Put_RemoteLocation(int CustomerId, string Extension, string MAC_Address, string User, string filename)
        {
            //string sql = string.Format("crm_put_deskphone_remotefolder @customer_id ={0},@ext='{1}',@mac_code='{2}',@user='{3}'", CustomerId, Extension, MAC_Address, User);
            string sql = string.Format("UR_Myacc_Web_put_deskphone_remotefolder @customer_id ={0},@ext='{1}',@mac_code='{2}',@user='{3}',@filename='{4}'", CustomerId, Extension, MAC_Address, User,filename);
            var result = new cOutMessage();
            using (DataMapper _imapper = new DataMapper() { CatalogName = "CRM_VB" })
            {
                result = _imapper.MapDataToSingle<cOutMessage>(sql);
            }
            if (result.ErrCode != 0)
                return false;
            else
                return true;
        }

        public static cOutMessage CreateAastraConfig(int customerID, string user)
        {
            cOutMessage _cOutMessage = new cOutMessage();
            string str = string.Format("usp_pbx_deskphone_config_update {0},'{1}','{2}'", customerID, "aastra.cfg", user);
            var result = new cOutMessage();

            using (DataMapper dataMapper = new DataMapper())
            {
                dataMapper.CatalogName = GlobalConfig.Catalog_Name;
                str = string.Format(str, customerID, user);
                result = dataMapper.MapDataToSingle<cOutMessage>(str);

            }
            return _cOutMessage;
        }

        //public static string Get_Customer_Deskhpone_Manifest(MyAccountCompany In)
        //{
        //    string sql = string.Format("vt_myaccount_ipphone_search_v3 {0}", In.company_id);
        //    var sb = new StringBuilder();
        //    using (DataMapper _imapper = new DataMapper() { CatalogName = "CRM_VB" })
        //    {
        //        //var result = new List<Deskphone_Meta_Info>();
        //        var result = _imapper.MapDataTo<Deskphone_Meta_Info_V1>(sql);
        //        foreach (Deskphone_Meta_Info_V1 item in result)
        //        {
        //            sb.AppendLine(string.Format("{0},{1},{2}", item.Effective_Caller_Id_Name, item.Extension, item.Flag_Status));
        //        }
        //    }
        //    return sb.ToString();
        //}


    }
}