﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace unifiedringmyaccountwebapi.Helpers
{
    public interface IDataMapper
    {
        string CatalogName { get; set; }
        bool IsBuffered { get; set; }
        IEnumerable<T> ExecDataTo<T>(string sqlExpression, System.Collections.Generic.IList<System.Data.SqlClient.SqlParameter> sqlParams);
        DataTable ExecForDataTable(string expression, System.Collections.Generic.IList<System.Data.SqlClient.SqlParameter> paramcollection);
        IEnumerable<T> MapDataTo<T>(string sqlExpression);
        DataTable QueryForDataTable(string expression);
        T MapDataToSingle<T>(string sqlExpression);
        DataTable GetErrorCode(string expression, DataTable dt);
    }
}
