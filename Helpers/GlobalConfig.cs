﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace unifiedringmyaccountwebapi.Helpers
{
    public class GlobalConfig
    {
        //public static string SMEPBXConn { get { return ConfigurationManager.ConnectionStrings["SMEPBXConn"].ConnectionString; } }
        public static string WinSCP { get { return ConfigurationManager.AppSettings["WinSCP_COMPath"]; } }
        public static string FTPDeskPhone { get { return ConfigurationManager.AppSettings["FTPDeskPhone"]; } }
        public static string FTPDeskPhoneLocal { get { return ConfigurationManager.AppSettings["FTPDeskPhoneLocal"]; } }
        public static string FTPDeskPhoneRemote { get { return ConfigurationManager.AppSettings["FTPDeskPhoneRemote"]; } }
        public static string GrandstreamFileTemplate { get { return ConfigurationManager.AppSettings["GrandstreamFileTemplate"]; } }
        public static FormsAuthenticationTicket LoginTicket { get { return ((FormsIdentity)HttpContext.Current.User.Identity).Ticket; } }
        public static string Catalog_Name { get { return ("CRM_VB"); } }
        public static string BrandName
        {
            get
            {
                return ConfigurationManager.AppSettings["BRANDNAME"];
            }
        }
        public static string Email_Body_HTML { get { return ConfigurationManager.AppSettings["Email_Body_HTML"]; } }
        public static string Bill_Report { get { return ConfigurationManager.AppSettings["Bill_Report"]; } }
        public static string Email_Brandname { get { return ConfigurationManager.AppSettings["EMAIL_BRAND"]; } }
        public static string Email_Root_Url { get { return ConfigurationManager.AppSettings["Email_ROOT_URL"]; } }
        public static string Email_Root_Url_UR { get { return ConfigurationManager.AppSettings["emailTemplateVB"]; } }
        public static string URL_SMS_Porting_Api { get { return ConfigurationManager.AppSettings["URL_SMS_Porting_Api"]; } }
        public static string URL_SMS_Porting_Api_ParamSC1 { get { return ConfigurationManager.AppSettings["URL_SMS_Porting_Api_ParamSC1"]; } }
        public static string URL_SMS_Porting_Api_ParamST1 { get { return ConfigurationManager.AppSettings["URL_SMS_Porting_Api_ParamST1"]; } }
    }
}