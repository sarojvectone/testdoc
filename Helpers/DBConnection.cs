﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace unifiedringmyaccountwebapi.Helpers
{
    public class DBConnection : iDBConnection
    {
        private const string DATABASE_CONNECTION_CONFIG_KEY = "DBConnKeyDefault";

        private const string DATABASE_CONNECTION_CONFIG_KEY_UF = "DBConnKeyUNITEDFONE";

        private const string DATABASE_CONNECTION_CONFIG_KEY_BF = "DBConnKeyDefaultConfig";


        private static ConnectionInformation connectionInformation;
        private static SqlConnection underlyingConnection;
        private static SqlConnection secondaryConnection;
        //private static IDbConnection nativeConnection;

        private class ConnectionInformation
        {
            private DbProviderFactory providerFactory;
            private ConnectionStringSettings settings;


            public ConnectionInformation(ConnectionStringSettings settings)
            {
                this.providerFactory = DbProviderFactories.GetFactory(settings.ProviderName);
                this.settings = settings;
            }

            public IDbConnection CreateConnection()
            {
                IDbConnection connection = providerFactory.CreateConnection();
                connection.ConnectionString = settings.ConnectionString;

                return connection;
            }
        }

        public DBConnection()
        {
            InitializeConnectionInformation();
            OpenUnderlyingConnection();
        }
        public DBConnection(string DBKeyName)
        {
            InitializeConnectionInformation();
            Open2ndConnection(DBKeyName);
        }

        private void OpenUnderlyingConnection()
        {
            if (underlyingConnection != null && underlyingConnection.State != ConnectionState.Open)
                underlyingConnection.Open();
            else
            {
                underlyingConnection = (SqlConnection)connectionInformation.CreateConnection();
                underlyingConnection.Open();
            }
        }
        private void Open2ndConnection(string key)
        {
            string Conection = "";
            //key = "";


            //if (HttpContext.Current.Session["StagingDataBase"] != "" && HttpContext.Current.Session["StagingDataBase"] != null)
            //{
            //    key = HttpContext.Current.Session["StagingDataBase"].ToString();
            //}
            //HttpContext.Current.Session["StagingDataBase"] = "unifiedring" in website;

            //Changed by anees on 06Mar2019
            //key = "unifiedring";
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString);
            key = builder.InitialCatalog;


            string _IsProduction = ConfigurationManager.AppSettings["IsProduction"].ToString();
            if (secondaryConnection != null && secondaryConnection.State != ConnectionState.Open)
                secondaryConnection.Open();
            else
            {
                secondaryConnection = null;
                OpenUnderlyingConnection();
                DataTable table = new DataTable();
                using (IDataReader reader = this.CreateCommandFor(string.Format("Select * from m_crm_connections (nolock) where name = '{0}' and IsProduction ='{1}'", key, _IsProduction)).ExecuteReader())
                {
                    cDbConnection oDbconnection = new cDbConnection();
                    while (reader.Read())
                    {
                        var result = new cDbConnection()
                        {
                            Idx = int.Parse(reader[0].ToString()),
                            Name = reader[1].ToString(),
                            Alias = reader[2].ToString(),
                            IP = reader[3].ToString(),
                            DBName = reader[4].ToString(),
                            Instance = reader[5].ToString(),
                            Username = reader[6].ToString(),
                            Password = reader[7].ToString(),
                            Provider = reader[8].ToString(),
                            Enable = int.Parse(reader[9].ToString()),
                            Description = reader[10].ToString()
                        };
                        oDbconnection = result;
                    }
                    secondaryConnection = new SqlConnection(oDbconnection.ConnectionString);
                    //secondaryConnection.ConnectionString = oDbconnection.ConnectionString;
                    secondaryConnection.Open();
                }
            }
            this.CloseUnderlyingConnection();
        }
        public void Close()
        {
            if (underlyingConnection != null)
                underlyingConnection.Close();
            //if (nativeConnection != null)
            //    nativeConnection.Close();
            if (secondaryConnection != null)
                secondaryConnection.Close();
        }
        public void CloseUnderlyingConnection()
        {
            if (underlyingConnection != null)
                underlyingConnection.Close();
        }
        private void InitializeConnectionInformation()
        {
            string ConnectionString = "";
            ConnectionString = "Reseller";
            // commented by bharathiraja...for " the connection string property has not been initialized"
            //connectionInformation = new ConnectionInformation(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings[DATABASE_CONNECTION_CONFIG_KEY_BF]]);

            connectionInformation = new ConnectionInformation(ConfigurationManager.ConnectionStrings["DefaultConfig"]);


        }

        public void Dispose()
        {
            //try
            //{
            if (underlyingConnection != null)
            {
                underlyingConnection.Dispose();
            }
            if (secondaryConnection != null)
            {
                secondaryConnection.Dispose();
            }

            underlyingConnection = null;
            secondaryConnection = null;
            //}
            //catch (Exception ex)
            //{
            //}
            //finally
            //{
            //if (underlyingConnection != null)
            //{
            //underlyingConnection.Dispose();
            //}
            //if (secondaryConnection != null)
            //{
            //secondaryConnection.Dispose();
            // }

            // underlyingConnection = null;
            //secondaryConnection = null;
            //}
        }

        public SqlConnection UnderlyingConnection
        {
            get { return underlyingConnection; }
        }
        public SqlConnection SecondaryConnection { get { return secondaryConnection; } }
        public SqlCommand CreateCommandFor(string dynamicSqlExpression)
        {
            SqlCommand command = underlyingConnection.CreateCommand();
            command.CommandType = CommandType.Text;

            command.CommandTimeout = 360;
            command.CommandText = dynamicSqlExpression;
            return command;
        }
        public SqlCommand CreateCommandForSP(string dynamicSqlExpression, IList<SqlParameter> listSQLParams)
        {
            SqlCommand command = underlyingConnection.CreateCommand();
            command.CommandTimeout = 360;
            command.CreateParameter();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = dynamicSqlExpression;
            command.Parameters.AddRange(listSQLParams.ToArray());
            return command;
        }

    }
}

