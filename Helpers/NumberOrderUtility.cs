﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace unifiedringmyaccountwebapi.Helpers
{
    #region Common
    public class BICSErrorResponse
    {
        public string code { get; set; }
        public string description { get; set; }
        public string timestamp { get; set; }
    }

    public class CommonOutput
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }
    #endregion

    #region OrderSingleNumber
    public class Routing
    {
        public string accessType { get; set; }
        public string accessNetwork { get; set; }
        public string crnType { get; set; }
        public string crnValue { get; set; }
        public string pop { get; set; }
    }

    public class OrderSingleNumberInput
    {
        public string product { get; set; }
        public string country { get; set; }
        public string location { get; set; }
        public string areaCode { get; set; }
        public int quantity { get; set; }
        public string addressReference { get; set; }
        public List<Routing> routing { get; set; }
    }
    public class OrderSingleNumberOutput : CommonOutput
    {
        public string number { get; set; }
        public string product { get; set; }
        public string country { get; set; }
        public string accessArea { get; set; }
        public string location { get; set; }
        public string areaCode { get; set; }
        public string orderId { get; set; }
        public string addressReference { get; set; }
        public List<Routing> routing { get; set; }
    }
    public class OrderItem
    {
        public string number { get; set; }
        public string location { get; set; }
        public string areaCode { get; set; }
        public string addressReference { get; set; }
        public List<Routing> routing { get; set; }
    }

    public class OrderItemOutput : CommonOutput
    {
        public string orderId { get; set; }
        public string status { get; set; }
        public string product { get; set; }
        public string country { get; set; }
        public string location { get; set; }
        public string areaCode { get; set; }
        public string accessArea { get; set; }
        public string profile { get; set; }
        public List<OrderItem> orderItems { get; set; }
    }
    # endregion

    public static class NumberOrderUtility
    {
        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion    
        #region Order Single number
        public static List<OrderSingleNumberOutput> PostOrderSingleNumber(OrderSingleNumberInput objOrderSingleNumberInput)
        {
            List<OrderSingleNumberOutput> output = new List<OrderSingleNumberOutput>();
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                string URL = string.Format(ConfigurationManager.AppSettings["BICSAPIENDPOINT"].ToString() + "order");

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                httpWebRequest.Headers.Add("Authorization", "Bearer " + ConfigurationManager.AppSettings["BICSAPIKEY"]);
                httpWebRequest.Accept = "application/json";
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Timeout = 180000;
                httpWebRequest.Method = "POST";

                using (HttpWebResponse httpResponce = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    using (Stream resStreams = httpResponce.GetResponseStream())
                    {
                        using (StreamReader readers = new StreamReader(resStreams))
                        {
                            string response = readers.ReadToEnd();
                            Log.Info("OrderSingleNumberOutput : Response : " + response);
                            List<OrderSingleNumberOutput> lstGetAvailableNumbers = JsonConvert.DeserializeObject<List<OrderSingleNumberOutput>>(response);
                            if (lstGetAvailableNumbers != null && lstGetAvailableNumbers.Count > 0)
                            {
                                output[0].errcode = 0;
                                output[0].errmsg = "Success";
                                output[0].routing = lstGetAvailableNumbers[0].routing;
                            }
                            else
                            {
                                output[0].errcode = -1;
                                output[0].errmsg = "No records found!";
                            }
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                Log.Error(ex.Message);
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    try
                    {
                        HttpWebResponse err = ex.Response as HttpWebResponse;
                        if (err != null)
                        {
                            string errResponse = new StreamReader(err.GetResponseStream()).ReadToEnd();
                            Log.Info("Exception : Response : " + errResponse);
                            List<BICSErrorResponse> response = JsonConvert.DeserializeObject<List<BICSErrorResponse>>(errResponse);
                            if (response != null && response.Count > 0)
                            {
                                output[0].errcode = -1;
                                output[0].errmsg = response.ElementAt(0).code + ":" + response.ElementAt(0).description;
                            }
                        }
                    }
                    catch (Exception innerEx)
                    {
                        Log.Error(innerEx.Message);
                        output[0].errcode = -1;
                        output[0].errmsg = innerEx.Message;
                    }
                }
                else
                {
                    output[0].errcode = -1;
                    output[0].errmsg = ex.Message;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output[0].errcode = -1;
                output[0].errmsg = ex.Message;
            }
            return output;
        }
        #endregion

    }


}