﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace unifiedringmyaccountwebapi.Helpers
{
    public class Payment
    {
        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        public string MerchantReferenceCode { get; private set; }
        public string ReasonCode { get; private set; }
        public string Description { get; private set; }
        public string Decision { get; private set; }
        public string AcsURL { get; private set; }
        public string PaReq { get; private set; }
        public string SubscriptionId { get; private set; }
        public string SRD { get; private set; }

        /// <summary>
        /// Initializes an instance of Payment class
        /// </summary>
        public Payment() { }

        /// <summary>
        /// Initialized an instance of Payment class using the specified paramaters.
        /// </summary>
        public Payment(string reasonCode, string decision, string description)
        {
            ReasonCode = reasonCode;
            Decision = decision.ToUpper();
            Description = description;
        }

        /// <summary>
        /// Calls web service from the specified uriTemplate
        /// </summary>
        public static HttpWebResponse CallService(string uriTemplate, string method, string contentType, string contentBody, string applicationCode)
        {
            return CallService(ConfigurationManager.AppSettings["CTP.Topup.CC.RecSvcUrl"], uriTemplate, applicationCode, method,
                contentType,
                contentBody);
        }

        public static HttpWebResponse CallService(string serviceUrl, string uriTemplate, string userAgent, string method, string contentType, string contentBody)
        {
            HttpWebResponse result = null;
            try
            {
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] contentByte = encoding.GetBytes(contentBody.ToString());

                //26-Aug-2019 : Moorthy : Added for the Realex TLS 1.2 Update
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                HttpWebRequest svc = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}",
                    serviceUrl,
                    uriTemplate));
                svc.Method = method;
                svc.UserAgent = userAgent;
                svc.ContentType = contentType;
                svc.ContentLength = contentByte.Length;

                // Write content body
                if (!method.Equals(WebRequestMethods.Http.Get))
                {
                    Stream svcContent = svc.GetRequestStream();
                    svcContent.Write(contentByte, 0, contentByte.Length);
                    svcContent.Close();
                }

                result = (HttpWebResponse)svc.GetResponse();
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                Log.Error("Delightcallingappgetpaymenttopupdetail : paymentRef : {0}", ex.Message);
            }
            return result;
        }

        public static Payment Receiptin(
            string siteCode, string applicationCode, string fullReferenceCode, string payment_agent, string subscriptionID, string grandTotalAmount,
            string mobileNo, string paymentMode, string serviceType, string currency, string email, string Country, string cvn = "")
        {
            Payment result = new Payment();
            result.MerchantReferenceCode = string.Empty;
            result.ReasonCode = "-1";
            result.Description = "Internal Error";
            result.Decision = "ERROR";
            try
            {
                StringBuilder content = new StringBuilder()
                    .AppendFormat("site-code={0}", siteCode).AppendLine()
                    .AppendFormat("application-code={0}", applicationCode).AppendLine()
                    .AppendFormat("reference-code={0}", fullReferenceCode).AppendLine()
                    .AppendFormat("payment-agent={0}", payment_agent).AppendLine()
                    .AppendFormat("product-code={0}", mobileNo).AppendLine()
                    .AppendFormat("service-type={0}", serviceType).AppendLine()
                    .AppendFormat("payment-mode={0}", paymentMode).AppendLine()
                    .AppendFormat("account-id={0}", mobileNo).AppendLine()
                    .AppendFormat("amount={0:0.00}", grandTotalAmount).AppendLine()
                    .AppendFormat("currency={0}", currency).AppendLine()
                    .AppendFormat("subscription_id={0}", subscriptionID).AppendLine()
                    //.AppendFormat("client-ip={0}", "192.168.12.16").AppendLine()
                    .AppendFormat("email={0}", email).AppendLine()
                    .AppendFormat("country={0}", Country).AppendLine()
                    //13-Jul-2018 : added for cvn
                    .AppendFormat("cvn={0}", cvn).AppendLine();

                string svcLocation = "enrollment/receiptin";
                NameValueCollection infoCollection = new NameValueCollection();
                HttpWebResponse res = CallService(svcLocation, WebRequestMethods.Http.Post, "text/plain", content.ToString(), applicationCode);
                if (res != null)
                {
                    StreamReader reader = new StreamReader(res.GetResponseStream());
                    List<string> data = new List<string>();
                    while (reader.Peek() > 0)
                        data.Add(reader.ReadLine());

                    foreach (string info in data)
                    {
                        string[] chunk;
                        if (data.IndexOf(info) == 0)
                        {
                            chunk = info.Split(":".ToCharArray(), 3);
                            infoCollection.Add("Description", chunk.Length == 3 ? chunk[2] : "Invalid data");
                        }
                        else
                        {
                            chunk = info.Split("=:".ToCharArray(), 2);
                            if (chunk.Length == 2)
                                infoCollection.Add(chunk[0], chunk[1]);
                        }
                    }
                }

                result.MerchantReferenceCode = infoCollection["MerchantReferenceCode"] ?? string.Empty;
                result.ReasonCode = infoCollection["ReasonCode"] ?? "-1";
                result.Description = infoCollection["Description"] ?? "Internal Error";
                result.Decision = (infoCollection["Decision"] ?? "ERROR").ToUpper();
                result.AcsURL = infoCollection["AcsURL"] ?? string.Empty;
                result.PaReq = infoCollection["PaReq"] ?? string.Empty;
                result.SubscriptionId = infoCollection["SubscriptionId"] ?? string.Empty;
                //24-Aug-2019 : Moorthy added for storing SRD value
                result.SRD = infoCollection["SRD"] ?? string.Empty;
            }
            catch (Exception ex)
            {
                Log.Error("Receiptin : {0}", ex.Message);
            }
            return result;
        }
    }
}