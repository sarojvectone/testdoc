﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;

namespace unifiedringmyaccountwebapi.Helpers
{
    public class DataMapper : IDataMapper, IDisposable
    {
        private static bool baseConnection = true;       

        public DataTable GetErrorCode(string expression, DataTable dts)
        {
            DataTable dt = new DataTable();
            try
            {
                if (!string.IsNullOrEmpty(_catalogName))
                {
                    using (iDBConnection _connection = new DBConnection(_catalogName))
                    {
                        if (_connection.SecondaryConnection != null && _connection.SecondaryConnection.State != ConnectionState.Open)
                        {
                            _connection.SecondaryConnection.Open();
                        }

                        using (SqlCommand command = new SqlCommand(expression))
                        {
                            command.Connection = _connection.SecondaryConnection;
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add("@tvp_ext_numbers", System.Data.SqlDbType.Structured).Value = dts;
                            using (SqlDataAdapter sda = new SqlDataAdapter(command))
                            {
                                sda.Fill(dt);
                            }
                            if (_connection.SecondaryConnection != null)
                            {
                                _connection.SecondaryConnection.Close();
                            }
                        }

                    }

                }
                else
                {
                    using (iDBConnection _connection = new DBConnection(_catalogName))
                    {
                        if (_connection.UnderlyingConnection != null && _connection.UnderlyingConnection.State != ConnectionState.Open)
                        {
                            _connection.UnderlyingConnection.Open();
                        }
                        using (SqlCommand command = new SqlCommand(expression))
                        {
                            command.Connection = _connection.SecondaryConnection;
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add("@tvp_ext_numbers", System.Data.SqlDbType.Structured).Value = dts;
                            using (SqlDataAdapter sda = new SqlDataAdapter(command))
                            {
                                sda.Fill(dt);
                            }
                            if (_connection.UnderlyingConnection != null)
                            {
                                _connection.UnderlyingConnection.Close();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return dt;
        }


 

        public IEnumerable<T> MapDataTo<T>(string sqlExpression)
        {
            IEnumerable<T> result = null;
            if (!string.IsNullOrEmpty(_catalogName))
            {
                using (iDBConnection _connection = new DBConnection(_catalogName))
                {
                    if (_connection.SecondaryConnection != null && _connection.SecondaryConnection.State != ConnectionState.Open)
                    {
                        _connection.SecondaryConnection.Open();
                    }
                    result = _connection.SecondaryConnection.Query<T>(sqlExpression, null, null, isBuffered, commandTimeout: 300);
                    if (_connection.SecondaryConnection != null)
                    {
                        _connection.SecondaryConnection.Close();
                    }
                    return result;
                }
            }
            else
            {
                using (iDBConnection _connection = new DBConnection())
                {
                    if (_connection.UnderlyingConnection != null && _connection.UnderlyingConnection.State != ConnectionState.Open)
                    {
                        _connection.UnderlyingConnection.Open();
                    }
                    result = _connection.UnderlyingConnection.Query<T>(sqlExpression, null, null, isBuffered, commandTimeout: 300);
                    if (_connection.UnderlyingConnection != null)
                    {
                        _connection.UnderlyingConnection.Close();
                    }
                    return result;
                }
            }
        }
        public T MapDataToSingle<T>(string sqlExpression)
        {
            IEnumerable<T> result = null;
            if (!string.IsNullOrEmpty(_catalogName))
            {
                using (iDBConnection _connection = new DBConnection(_catalogName))
                {
                    if (_connection.SecondaryConnection != null && _connection.SecondaryConnection.State != ConnectionState.Open)
                    {
                        _connection.SecondaryConnection.Open();
                    }
                    result = _connection.SecondaryConnection.Query<T>(sqlExpression, null, null, isBuffered);
                    if (_connection.SecondaryConnection != null)
                    {
                        _connection.SecondaryConnection.Close();
                    }
                    return result.SingleOrDefault();
                }
            }
            else
            {
                using (iDBConnection _connection = new DBConnection())
                {
                    if (_connection.UnderlyingConnection != null && _connection.UnderlyingConnection.State != ConnectionState.Open)
                    {
                        _connection.UnderlyingConnection.Open();
                    }
                    result = _connection.UnderlyingConnection.Query<T>(sqlExpression, null, null, isBuffered);
                    if (_connection.UnderlyingConnection != null)
                    {
                        _connection.UnderlyingConnection.Close();
                    }
                    return result.SingleOrDefault();
                }
            }
        }
        public IEnumerable<T> ExecDataTo<T>(string sqlExpression, IList<SqlParameter> sqlParams)
        {
            IEnumerable<T> result = null;
            //result = connection.UnderlyingConnection.Query<T>(sqlExpression,);
            //connection.Close();
            return result;
        }
        public DataTable QueryForDataTable(string expression)
        {
            DataTable table = new DataTable();
            using (iDBConnection _connection = new DBConnection())
            {
                using (IDataReader reader = _connection.CreateCommandFor(expression).ExecuteReader(CommandBehavior.CloseConnection))
                {
                    table.Load(reader);
                }
            }
            return table;
        }
        public DataTable ExecForDataTable(string expression, IList<SqlParameter> paramcollection)
        {

            DataTable table = new DataTable();
            using (iDBConnection _connection = new DBConnection())
            {
                using (IDataReader reader = _connection.CreateCommandForSP(expression, paramcollection).ExecuteReader(CommandBehavior.CloseConnection))
                {
                    table.Load(reader);
                }
            }
            return table;
        }

        public void Dispose()
        {
            baseConnection = true;
            //connection.Dispose();
        }
        private string _catalogName;
        public string CatalogName
        {
            get
            {
                return _catalogName;
            }
            set
            {
                _catalogName = value;
            }
        }
        private bool isBuffered = true;
        public bool IsBuffered { get { return isBuffered; } set { isBuffered = value; } }
    }
}
