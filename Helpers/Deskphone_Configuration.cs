﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using unifiedringmyaccountwebapi.Models;

namespace unifiedringmyaccountwebapi.Helpers
{
    public class Deskphone_Configuration
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        static string ErrMsg = "";
        public static string displayforusermail = "";
        public static string displayforuser = "";

        //public static DeskPhoneModel Create_DeskphoneConfig_File(int CustomerId, string Order_Number, string Extension, string Order_Item, string MAC_Address, DirectoryInfo DirInfo, string TemplateFilename, string User, string DeskPhoneModel)
        public static DeskPhoneModel Create_DeskphoneConfig_File(int CustomerId, string Order_Number, string Extension, string Order_Item, string MAC_Address,  string User, string DeskPhoneModel,string operation)
        {
            string Prov_URL = string.Empty;

            DirectoryInfo DirInfo = new DirectoryInfo(Path.Combine(@"{0}/{1}", AppDomain.CurrentDomain.BaseDirectory + "App_Data", GlobalConfig.FTPDeskPhoneLocal));

            string TemplateFilename = Path.Combine(string.Format(@"{0}/{1}", AppDomain.CurrentDomain.BaseDirectory + @"App_Data\Templates\", GlobalConfig.GrandstreamFileTemplate));

            DeskPhoneModel objOutput = new DeskPhoneModel();
            Log.Debug("Entering Create_DeskphoneConfig_File");
            try
            {
                //Commended by Prabhu
                //var _deskphone = OrderBL.GetDeskphoneInfo(Extension, Order_Number);//db hit
                //DirectoryInfo file_folder = new DirectoryInfo(Server.MapPath(string.Format(@"{0}/{1}", Url.Content("~/App_Data"), GlobalConfig.FTPDeskPhoneLocal)));
                DirectoryInfo file_folder = DirInfo;
                if (!file_folder.Exists)
                    file_folder.Create();
                string Full_Filename = string.Format(@"{0}\{1}.cfg", file_folder.FullName, MAC_Address.ToUpper().Trim());
                var _domainId = OrderBL.Get_Customer_DomainId_By(CustomerId);//db hit
                var _deskphoneModel = OrderBL.GetDeskphoneModel(_domainId, Extension);//db hit
                var _company = OrderBL.GetCompanyDetailByDomain(_domainId);//db hit


                if (_deskphoneModel == null)
                {
                    _deskphoneModel = new DeskPhoneModel()
                    {
                        extension = Extension,
                        vm_password = GenerateIdentifier(12),
                        deskphone_mac = MAC_Address,
                        domain_id = _domainId,
                        admin_password = GenerateIdentifier(12) 
                    };

                }
                objOutput = _deskphoneModel;
                if (_deskphoneModel.admin_password == null)
                {
                    string randomNumberString = GenerateIdentifierNum(4);
                    string sql = string.Format("crm_set_deskphone_admin_password {0},'{1}'", _company.company_id, randomNumberString);
                    using (DataMapper _imapper = new DataMapper() { CatalogName = "CRM_VB" })
                    {
                        var result = _imapper.MapDataToSingle<cOutMessage>(sql);
                        if (!result.ErrCode.Equals(0))
                        {
                            throw new Exception("Failed to generate deskphone admin password");
                        }
                        _deskphoneModel.admin_password = randomNumberString;
                    }
                }



                #region SNOM
                if (DeskPhoneModel.ToLower().Contains("snom"))
                {
                    Log.Debug("Entering SNOM Configuration");
                    //Customer ID 
                    var content = OrderBL.SNOM_Content(_company, _deskphoneModel, CustomerId);//formation of string builder

                    var c_content = OrderBL.Aastra_Config_Content(_company, _deskphoneModel, CustomerId);// formation of string builder
                    var m_content = OrderBL.Get_Customer_Deskhpone_Manifest(_company);//db hit
                    var resx = OrderBL.Check_RemoteLocation(CustomerId, Extension, MAC_Address, User, MAC_Address.ToUpper().Trim() + ".xml");//db hit

                    Full_Filename = string.Format(@"{0}\{1}\{2}.xml", file_folder.FullName, resx.Security_Id, MAC_Address.ToLower().Trim());

                    var displayformail = OrderBL.SNOM_MailContent(_company, _deskphoneModel);
                    displayforusermail = displayformail;
                    displayforuser = content;
                    var manifestfile = string.Format(@"{0}\{1}\{2}.csv", file_folder.FullName, resx.Security_Id, "directory1");

                    var globalconfig = string.Format(@"{0}\{1}\{2}.cfg", file_folder.FullName, resx.Security_Id, "SNOM");
                    //var result = OrderBL.GenerateDeskPhoneConfigAastra(Full_Filename, content,resx)


                    file_folder = file_folder.CreateSubdirectory(resx.Security_Id);
                    if (!file_folder.Exists)
                    {
                        file_folder.Create();
                    }
                    //Delete file
                    if (resx != null)
                    {
                        if (!string.IsNullOrEmpty(resx.Filename) && operation == "Update")
                        {
                            if (OrderBL.DeleteConfigAastrafile(globalconfig, c_content, resx, CustomerId, User) == false)//winscp
                            {
                                //if (!string.IsNullOrEmpty(ErrMsg))
                                //    throw new Exception(string.Format("Delete File Global Config: {0}", ErrMsg));
                                //else
                                //    throw new Exception("Delete File Global Config: Unable to delete existing File");
                            }
                        }
                    }
                    //Validation directory


                    if (OrderBL.GenerateDeskPhoneConfigAastra(Full_Filename, content, resx) == false)//winscp
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("SNOM File Extension: {0}", ErrMsg));
                        else
                            throw new Exception("SNOM File Extension: Unable to Upload File");
                    }
                    if (OrderBL.GenerateDeskPhoneConfigAastrafile(globalconfig, c_content, resx, CustomerId, User) == false)//winscp
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("SNOM File Global Config: {0}", ErrMsg));
                        else
                            throw new Exception("SNOM File Global Config: Unable to Upload File");
                    }
                    //Update directory details
                    if (OrderBL.GenerateDeskPhoneConfigAastra(manifestfile, m_content, resx) == false)
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("SNOM Customer Meta File: {0}", ErrMsg));
                        else
                            throw new Exception("SNOM Customer Meta File: Unable to Upload File");
                    }

                    Prov_URL = resx.Security_Id;
                    Log.Debug("SNOM Configuration Success");
                }
                #endregion


                #region VTECH
                if (DeskPhoneModel.ToLower().Contains("vtech"))
                {
                    Log.Debug("Entering vtech Configuration");

                    //Customer ID 
                    var content = OrderBL.VtechContent(_company, _deskphoneModel, CustomerId);// formation of string builder
                    var c_content = OrderBL.Aastra_Config_Content(_company, _deskphoneModel, CustomerId);// formation of string builder
                    var m_content = OrderBL.Get_Customer_Deskhpone_Manifest(_company);//db hit

                    var resx = OrderBL.Check_RemoteLocation(CustomerId, Extension, MAC_Address, User, MAC_Address.ToUpper().Trim() + ".cfg");//db hit

                    Full_Filename = string.Format(@"{0}\{1}\{2}.cfg", file_folder.FullName, resx.Security_Id, "VCS754A_" + MAC_Address.ToUpper().Trim());

                    var manifestfile = string.Format(@"{0}\{1}\{2}.csv", file_folder.FullName, resx.Security_Id, "directory1");



                    var globalconfig = string.Format(@"{0}\{1}\{2}.cfg", file_folder.FullName, resx.Security_Id, "aastra");
                    //var result = OrderBL.GenerateDeskPhoneConfigAastra(Full_Filename, content,resx)


                    file_folder = file_folder.CreateSubdirectory(resx.Security_Id);
                    if (!file_folder.Exists)
                    {
                        file_folder.Create();
                    }

                    //Delete file

                    if (resx != null)
                    {
                        if (!string.IsNullOrEmpty(resx.Filename) && operation == "Update")
                        {
                            if (OrderBL.DeleteConfigAastrafile(globalconfig, c_content, resx, CustomerId, User) == false)//winscp
                            {
                                //if (!string.IsNullOrEmpty(ErrMsg))
                                //    throw new Exception(string.Format("Delete File Global Config: {0}", ErrMsg));
                                //else
                                //    throw new Exception("Delete File Global Config: Unable to delete existing File");
                            }
                        }
                    }


                    if (OrderBL.GenerateDeskPhoneConfigAastra(Full_Filename, content, resx) == false)//winscp
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("Vtech File Extension: {0}", ErrMsg));
                        else
                            throw new Exception("Vtech File content: Unable to Upload File");
                    }
                    //Validation directory 
                    if (OrderBL.GenerateDeskPhoneConfigAastrafile(globalconfig, c_content, resx, CustomerId, User) == false)//winscp
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("Vtech File Global Config: {0}", ErrMsg));
                        else
                            throw new Exception("Vtech File Global Config: Unable to Upload File");
                    }

                    //Update directory details
                    if (OrderBL.GenerateDeskPhoneConfigAastra(manifestfile, m_content, resx) == false)
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("Vtech Customer Meta File: {0}", ErrMsg));
                        else
                            throw new Exception("Vtech Customer Meta File: Unable to Upload File");
                    }
                    Prov_URL = resx.Security_Id;
                    Log.Debug("Vtech Configuration Success");
                } 
                #endregion

                #region YeaLink
                else if (DeskPhoneModel.ToLower().Contains("yealink"))             
                {
                    Log.Debug("Entering yealink Configuration");
                    //Customer ID 
                    var c_content = OrderBL.YeaLinkContent(_company, _deskphoneModel, CustomerId);// formation of string builder
                    var m_content = OrderBL.Get_Customer_Deskhpone_Manifest(_company);//db hit

                    var resx = OrderBL.Check_RemoteLocation(CustomerId, Extension, MAC_Address, User, MAC_Address.ToUpper().Trim() + ".cfg");//db hit

                    Full_Filename = string.Format(@"{0}\{1}\{2}.cfg", file_folder.FullName, resx.Security_Id, MAC_Address.ToUpper().Trim());

                    var manifestfile = string.Format(@"{0}\{1}\{2}.csv", file_folder.FullName, resx.Security_Id, "directory1");



                    var globalconfig = string.Format(@"{0}\{1}\{2}.cfg", file_folder.FullName, resx.Security_Id, "aastra");
                    //var result = OrderBL.GenerateDeskPhoneConfigAastra(Full_Filename, content,resx)


                    file_folder = file_folder.CreateSubdirectory(resx.Security_Id);
                    if (!file_folder.Exists)
                    {
                        file_folder.Create();
                    }

                    //Delete file

                    if (resx != null)
                    {
                        if (!string.IsNullOrEmpty(resx.Filename) && operation == "Update")
                        {
                            if (OrderBL.DeleteConfigAastrafile(globalconfig, c_content, resx, CustomerId, User) == false)//winscp
                            {
                                //if (!string.IsNullOrEmpty(ErrMsg))
                                //    throw new Exception(string.Format("Delete File Global Config: {0}", ErrMsg));
                                //else
                                //    throw new Exception("Delete File Global Config: Unable to delete existing File");
                            }
                        }
                    }


                    //Validation directory 
                    if (OrderBL.GenerateDeskPhoneConfigAastrafile(Full_Filename, c_content, resx, CustomerId, User) == false)//winscp
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("yealink File Global Config: {0}", ErrMsg));
                        else
                            throw new Exception("yealink File Global Config: Unable to Upload File");
                    }

                    //Update directory details
                    if (OrderBL.GenerateDeskPhoneConfigAastra(manifestfile, m_content, resx) == false)
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("Aastra Customer Meta File: {0}", ErrMsg));
                        else
                            throw new Exception("Aastra Customer Meta File: Unable to Upload File");
                    }
                    Prov_URL = resx.Security_Id;
                    Log.Debug("yealink Configuration Success");
                }  
                #endregion

                #region Aastra
                else if (DeskPhoneModel.ToLower().Contains("aastra"))
               
                {
                    Log.Debug("Entering Aastra Configuration");
                    //Customer ID 
                    var content = OrderBL.Aastra_Content(_company, _deskphoneModel, CustomerId);//formation of string builder
                    var c_content = OrderBL.Aastra_Config_Content(_company, _deskphoneModel, CustomerId);// formation of string builder
                    var m_content = OrderBL.Get_Customer_Deskhpone_Manifest(_company);//db hit

                    var resx = OrderBL.Check_RemoteLocation(CustomerId, Extension, MAC_Address, User, MAC_Address.ToUpper().Trim() + ".cfg");//db hit

                    Full_Filename = string.Format(@"{0}\{1}\{2}.cfg", file_folder.FullName, resx.Security_Id, MAC_Address.ToUpper().Trim());
                    var displayformail = OrderBL.Aastra_Content1(_company, _deskphoneModel);
                    displayforusermail = displayformail;
                    displayforuser = content;


                    var manifestfile = string.Format(@"{0}\{1}\{2}.csv", file_folder.FullName, resx.Security_Id, "directory1");



                    var globalconfig = string.Format(@"{0}\{1}\{2}.cfg", file_folder.FullName, resx.Security_Id, "aastra");
                    //var result = OrderBL.GenerateDeskPhoneConfigAastra(Full_Filename, content,resx)


                    file_folder = file_folder.CreateSubdirectory(resx.Security_Id);
                    if (!file_folder.Exists)
                    {
                        file_folder.Create();
                    }

                    //Delete file

                    if (resx != null)
                    {
                        if (!string.IsNullOrEmpty(resx.Filename) && operation == "Update")
                        {
                            if (OrderBL.DeleteConfigAastrafile(globalconfig, c_content, resx, CustomerId, User) == false)//winscp
                            {
                                //if (!string.IsNullOrEmpty(ErrMsg))
                                //    throw new Exception(string.Format("Delete File Global Config: {0}", ErrMsg));
                                //else
                                //    throw new Exception("Delete File Global Config: Unable to delete existing File");
                            }
                        }
                    }


                    //Validation directory


                    if (OrderBL.GenerateDeskPhoneConfigAastra(Full_Filename, content, resx) == false)//winscp
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("Aastra File Extension: {0}", ErrMsg));
                        else
                            throw new Exception("Aastra File Extension: Unable to Upload File");
                    }
                    if (OrderBL.GenerateDeskPhoneConfigAastrafile(globalconfig, c_content, resx, CustomerId, User) == false)//winscp
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("Aastra File Global Config: {0}", ErrMsg));
                        else
                            throw new Exception("Aastra File Global Config: Unable to Upload File");
                    }
                    //Update directory details
                    if (OrderBL.GenerateDeskPhoneConfigAastra(manifestfile, m_content, resx) == false)
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("Aastra Customer Meta File: {0}", ErrMsg));
                        else
                            throw new Exception("Aastra Customer Meta File: Unable to Upload File");
                    }
                    Prov_URL = resx.Security_Id;
                    Log.Debug("Aastra Configuration Success");
                } 
                #endregion

                #region Grandstream
                else if (DeskPhoneModel.ToLower().Contains("grandstream"))               
                {
                    Log.Debug("Entering Grandstream Configuration");
                    //Customer ID 
                    var content = OrderBL.Grandstream_Content(_company, _deskphoneModel, CustomerId);//formation of string builder
                    var c_content = OrderBL.Aastra_Config_Content(_company, _deskphoneModel, CustomerId);// formation of string builder
                    var m_content = OrderBL.Get_Customer_Deskhpone_Manifest(_company);//db hit
                    var resx = OrderBL.Check_RemoteLocation(CustomerId, Extension, MAC_Address, User, MAC_Address.ToUpper().Trim() + ".xml");//db hit

                    Full_Filename = string.Format(@"{0}\{1}\{2}.xml", file_folder.FullName, resx.Security_Id, "cfg" + MAC_Address.ToLower().Trim());

                    var displayformail = OrderBL.Grandstream_MailContent(_company, _deskphoneModel);
                    displayforusermail = displayformail;
                    displayforuser = content;

                    var manifestfile = string.Format(@"{0}\{1}\{2}.csv", file_folder.FullName, resx.Security_Id, "directory1");



                    var globalconfig = string.Format(@"{0}\{1}\{2}.cfg", file_folder.FullName, resx.Security_Id, "grandstream");
                    //var result = OrderBL.GenerateDeskPhoneConfigAastra(Full_Filename, content,resx)


                    file_folder = file_folder.CreateSubdirectory(resx.Security_Id);
                    if (!file_folder.Exists)
                    {
                        file_folder.Create();
                    }
                    //Delete file
                    if (resx != null)
                    {
                        if (!string.IsNullOrEmpty(resx.Filename) && operation == "Update")
                        {
                            if (OrderBL.DeleteConfigAastrafile(globalconfig, c_content, resx, CustomerId, User) == false)//winscp
                            {
                                //if (!string.IsNullOrEmpty(ErrMsg))
                                //    throw new Exception(string.Format("Delete File Global Config: {0}", ErrMsg));
                                //else
                                //    throw new Exception("Delete File Global Config: Unable to delete existing File");
                            }
                        }
                    }
                    //Validation directory


                    if (OrderBL.GenerateDeskPhoneConfigAastra(Full_Filename, content, resx) == false)//winscp
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("Grandstream File Extension: {0}", ErrMsg));
                        else
                            throw new Exception("Grandstream File Extension: Unable to Upload File");
                    }
                    if (OrderBL.GenerateDeskPhoneConfigAastrafile(globalconfig, c_content, resx, CustomerId, User) == false)//winscp
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("Grandstream File Global Config: {0}", ErrMsg));
                        else
                            throw new Exception("Grandstream File Global Config: Unable to Upload File");
                    }
                    //Update directory details
                    if (OrderBL.GenerateDeskPhoneConfigAastra(manifestfile, m_content, resx) == false)
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("Grandstream Customer Meta File: {0}", ErrMsg));
                        else
                            throw new Exception("Grandstream Customer Meta File: Unable to Upload File");
                    }
                    Prov_URL = resx.Security_Id;
                    Log.Debug("Grandstream Configuration Success");
                } 
                #endregion

                #region CISCO
                else if (DeskPhoneModel.ToLower().Contains("cisco"))               
                {
                    Log.Debug("Entering Cisco Configuration");

                    var c_content = OrderBL.Aastra_Config_Content(_company, _deskphoneModel, CustomerId);// formation of string builder
                    var m_content = Get_Customer_Deskhpone_Manifest(_company);//db hit --anees question same method not calling twise
                    var resx = OrderBL.Check_RemoteLocation(CustomerId, Extension, MAC_Address, User, MAC_Address.ToUpper().Trim() + ".txt");//db hit

                    if (resx != null)
                    {
                        // cfg 
                        Full_Filename = string.Format(@"{0}\{1}\{2}.txt", file_folder.FullName, Convert.ToString(resx.Security_Id), MAC_Address.ToUpper().Trim());



                        //csv
                        var manifestfile = string.Format(@"{0}\{1}\{2}.csv", file_folder.FullName, Convert.ToString(resx.Security_Id), "directory1");

                        //cfg
                        var globalconfig = string.Format(@"{0}\{1}\{2}.cfg", file_folder.FullName, Convert.ToString(resx.Security_Id), "cisco");

                        file_folder = file_folder.CreateSubdirectory(Convert.ToString(resx.Security_Id));
                        if (!file_folder.Exists)
                        {
                            file_folder.Create();
                        }
                        var content = OrderBL.Cisco_Content(_company, _deskphoneModel, MAC_Address.ToLower(), resx, Full_Filename);

                        var displayformail = OrderBL.Aastra_Content1(_company, _deskphoneModel);
                        displayforusermail = displayformail;
                        displayforuser = content;
                        //Delete file
                        if (resx != null)
                        {
                            if (!string.IsNullOrEmpty(resx.Filename) && operation == "Update")
                            {
                                if (OrderBL.DeleteConfigAastrafile(globalconfig, c_content, resx, CustomerId, User) == false)//winscp
                                {
                                    //if (!string.IsNullOrEmpty(ErrMsg))
                                    //    throw new Exception(string.Format("Delete File Global Config: {0}", ErrMsg));
                                    //else
                                    //    throw new Exception("Delete File Global Config: Unable to delete existing File");
                                }
                            }
                        }
                        if (OrderBL.GenerateDeskPhoneConfigAastra(Full_Filename, content, resx) == false)//winscp
                        {
                            if (!string.IsNullOrEmpty(ErrMsg))
                                throw new Exception(string.Format("Cisco File Extension: {0}", ErrMsg));
                            else
                                throw new Exception("Cisco File Extension: Unable to Upload File");
                        }
                        if (OrderBL.GenerateDeskPhoneConfigAastrafile(globalconfig, c_content, resx, CustomerId, User) == false)//winscp
                        {
                            if (!string.IsNullOrEmpty(ErrMsg))
                                throw new Exception(string.Format("Cisco File Global Config: {0}", ErrMsg));
                            else
                                throw new Exception("Cisco File Global Config: Unable to Upload File");
                        }
                        //Update directory details
                        if (OrderBL.GenerateDeskPhoneConfigAastra(manifestfile, m_content, resx) == false)
                        {
                            if (!string.IsNullOrEmpty(ErrMsg))
                                throw new Exception(string.Format("Cisco Customer Meta File: {0}", ErrMsg));
                            else
                                throw new Exception("Cisco Customer Meta File: Unable to Upload File");
                        }
                    }

                    Prov_URL = resx.Security_Id;
                    Log.Debug("Cisco Configuration Success");
                } 
                #endregion

                #region Polycon
                else if (DeskPhoneModel.ToLower().Contains("polycom"))               
                {
                    Log.Debug("Entering Polycom Configuration");
                    //Customer ID 
                    var content = OrderBL.Aastra_Content(_company, _deskphoneModel, CustomerId);//formation of string builder
                    var c_content = OrderBL.Aastra_Config_Content(_company, _deskphoneModel, CustomerId);// formation of string builder
                    var m_content = OrderBL.Get_Customer_Deskhpone_Manifest(_company);//db hit
                    var resx = OrderBL.Check_RemoteLocation(CustomerId, Extension, MAC_Address, User, MAC_Address.ToUpper().Trim() + ".cfg");//db hit

                    Full_Filename = string.Format(@"{0}\{1}\{2}.cfg", file_folder.FullName, resx.Security_Id, MAC_Address.ToUpper().Trim());


                    var manifestfile = string.Format(@"{0}\{1}\{2}.csv", file_folder.FullName, resx.Security_Id, "directory1");



                    var globalconfig = string.Format(@"{0}\{1}\{2}.cfg", file_folder.FullName, resx.Security_Id, "polycom");
                    //var result = OrderBL.GenerateDeskPhoneConfigAastra(Full_Filename, content,resx)


                    file_folder = file_folder.CreateSubdirectory(resx.Security_Id);
                    if (!file_folder.Exists)
                    {
                        file_folder.Create();
                    }
                    //Delete file
                    if (resx != null)
                    {
                        if (!string.IsNullOrEmpty(resx.Filename) && operation == "Update")
                        {
                            if (OrderBL.DeleteConfigAastrafile(globalconfig, c_content, resx, CustomerId, User) == false)//winscp
                            {
                                //if (!string.IsNullOrEmpty(ErrMsg))
                                //    throw new Exception(string.Format("Delete File Global Config: {0}", ErrMsg));
                                //else
                                //    throw new Exception("Delete File Global Config: Unable to delete existing File");
                            }
                        }
                    }
                    //Validation directory


                    if (OrderBL.GenerateDeskPhoneConfigAastra(Full_Filename, content, resx) == false)//winscp
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("Polycom File Extension: {0}", ErrMsg));
                        else
                            throw new Exception("Polycom File Extension: Unable to Upload File");
                    }
                    if (OrderBL.GenerateDeskPhoneConfigAastrafile(globalconfig, c_content, resx, CustomerId, User) == false)//winscp
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("Polycom File Global Config: {0}", ErrMsg));
                        else
                            throw new Exception("Polycom File Global Config: Unable to Upload File");
                    }
                    //Update directory details
                    if (OrderBL.GenerateDeskPhoneConfigAastra(manifestfile, m_content, resx) == false)
                    {
                        if (!string.IsNullOrEmpty(ErrMsg))
                            throw new Exception(string.Format("Polycom Customer Meta File: {0}", ErrMsg));
                        else
                            throw new Exception("Polycom Customer Meta File: Unable to Upload File");
                    }
                    Prov_URL = resx.Security_Id;
                    Log.Debug("Polycom Configuration Success");
                } 
                #endregion

                string filename = "";
                if(DeskPhoneModel.ToLower().Contains("aastra"))
                {
                    filename = MAC_Address.ToUpper().Trim() + ".cfg";
                }
                else if (DeskPhoneModel.ToLower().Contains("grandstream"))
                {
                    filename = "cfg"+MAC_Address.Trim() + ".xml";
                }
                else if (DeskPhoneModel.ToLower().Contains("cisco"))
                {
                    filename = MAC_Address.ToUpper().Trim() + ".txt";
                }
                else if (DeskPhoneModel.ToLower().Contains("polycom"))
                {
                    filename = MAC_Address.ToUpper().Trim() + ".cfg";
                }
                var resultLocation = OrderBL.Put_RemoteLocation(CustomerId, Extension, MAC_Address, User, filename);
            }
            catch (Exception ex)
            {
                DeskPhoneModel objOutput1 = new DeskPhoneModel();
                return objOutput1;
                //throw new Exception("[Failed Generate Config File Deskphone] " + ex.Message);
            }

            //return vm_password;
            objOutput.SecurityID = Prov_URL;
            return objOutput;
        }


        public static string UpdateDirectory(int CustomerId, string Extension)
        {
            DirectoryInfo DirInfo = new DirectoryInfo(Path.Combine(@"{0}/{1}", AppDomain.CurrentDomain.BaseDirectory + "App_Data", GlobalConfig.FTPDeskPhoneLocal));
            var resx = OrderBL.Get_RemoteLocation(CustomerId, Extension, "");//db hit
            var _domainId = OrderBL.Get_Customer_DomainId_By(CustomerId);//db hit
            var _company = OrderBL.GetCompanyDetailByDomain(_domainId);//db hit 
            var m_content = OrderBL.Get_Customer_Deskhpone_Manifest(_company);//db hit
            DirectoryInfo file_folder = DirInfo;
            if (!file_folder.Exists)
                file_folder.Create();
            var manifestfile = string.Format(@"{0}\{1}\{2}.csv", file_folder.FullName, resx.Security_Id, "directory1");
            //Update directory details
            if (OrderBL.GenerateDeskPhoneConfigAastra(manifestfile, m_content, resx) == false)
            {
                if (!string.IsNullOrEmpty(ErrMsg))
                    throw new Exception(string.Format("Aastra Customer Meta File: {0}", ErrMsg));
                else
                    throw new Exception("Aastra Customer Meta File: Unable to Upload File");
            }
            return "Success";
        }


        internal static string GenerateIdentifier(int length)
        {
            char[] identifier = new char[length];
            byte[] randomData = new byte[length];

            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(randomData);
            }

            for (int idx = 0; idx < identifier.Length; idx++)
            {
                int pos = randomData[idx] % AvailableCharacters.Length;
                identifier[idx] = AvailableCharacters[pos];
            }

            return new string(identifier);
        }

        private static readonly char[] AvailableCharacters =
            {
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
            };

        internal static string GenerateIdentifierNum(int length)
        {
            char[] identifier = new char[length];
            byte[] randomData = new byte[length];

            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(randomData);
            }

            for (int idx = 0; idx < identifier.Length; idx++)
            {
                int pos = randomData[idx] % AvailableCharactersNum.Length;
                identifier[idx] = AvailableCharactersNum[pos];
            }

            return new string(identifier);
        }

        private static readonly char[] AvailableCharactersNum =
            {
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
            };

        public static string Get_Customer_Deskhpone_Manifest(MyAccountCompany In)
        {
            //string sql = string.Format("vt_myaccount_ipphone_search_v3 {0}", In.company_id);
            string sql = string.Format("UR_Myacc_Web_ipphone_search_v3 {0}", In.company_id);
            var sb = new StringBuilder();
            using (DataMapper _imapper = new DataMapper() { CatalogName = "CRM_VB" })
            {
                //var result = new List<Deskphone_Meta_Info>();
                var result = _imapper.MapDataTo<Deskphone_Meta_Info_V1>(sql);
                foreach (Deskphone_Meta_Info_V1 item in result)
                {
                    sb.AppendLine(string.Format("{0},{1},{2}", item.Effective_Caller_Id_Name, item.Extension, item.Flag_Status));
                }
            }
            return sb.ToString();
        }
    }
}