﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace unifiedringmyaccountwebapi.Helpers
{
    public class cDbConnection : IDisposable
    {
        public int Idx { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string IP { get; set; }
        public string DBName { get; set; }
        public string Instance { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Provider { get; set; }
        public int Enable { get; set; }
        public string Description { get; set; }
        public string ConnectionString
        {
            //22-Dec-2016 : Added connection timeout to the connection string
            get
            {
                return "Data source=" + this.IP + /*(this.Instance ?? "") + */";Initial Catalog=" + this.DBName
                    + ";Max Pool Size=100;Persist Security Info=True;User ID=" + this.Username + ";password="
                    + this.Password + ";Connection Timeout=300;";
            }
        }

        public void Dispose()
        {
            this.Dispose();
        }
    }
}
