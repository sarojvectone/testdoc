﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace unifiedringmyaccountwebapi.Helpers
{
    public class WinSCPInvoker
    {
        public enum SFTPErrCode
        {
            Success = 0,
            Failure = 1,
            CommandFileNotExist = -1,
            ProcessTimeOut = -2,
        }

        Process _process;
        string _winSCPProfile;

        public WinSCPInvoker(string winSCPProfile)
        {
            _winSCPProfile = winSCPProfile;
            //_process = new Process();
        }

        ~WinSCPInvoker()
        {
            //_process.Dispose();
        }

        protected SFTPErrCode RunWithArg(string winSCPArg)
        {
            SFTPErrCode err;

            try
            {

                using (Process _process1 = new Process())
                {
                    ProcessStartInfo info1 = new ProcessStartInfo(GlobalConfig.WinSCP, winSCPArg);

                    info1.RedirectStandardOutput = true;
                    info1.UseShellExecute = false;
                    info1.RedirectStandardOutput = true;
                    // Do not create the black window.
                    info1.CreateNoWindow = true;
                    // Now create a process, assign its ProcessStartInfo and start it
                    _process1.StartInfo = info1;

                    _process1.Start();
                    string output = _process1.StandardOutput.ReadToEnd();
                    _process1.WaitForExit();

                    if (_process1.HasExited)
                        err = (SFTPErrCode)_process1.ExitCode;
                    else
                        err = SFTPErrCode.ProcessTimeOut;
                }

            }
            catch (Exception ex)
            {
                err = SFTPErrCode.Failure;
            }

            return err;
        }


        public SFTPErrCode UploadAndRename(string remoteFolder, string localFolder, string fileName, string renamedFileName)
        {
            if (!localFolder.EndsWith("\\"))
                localFolder = localFolder + "\\";

            string winSCPArg =
                "/command \"open \"\"{0}\"\"\" \"option batch abort\" \"option confirm off\" " +
                "\"cd \"\"{1}\"\"\" \"put \"\"{2}\"\"\" \"mv \"\"{3}\"\" \"\"{4}\"\"\" \"exit\"";

            winSCPArg = string.Format(
                winSCPArg,
                _winSCPProfile,
                remoteFolder, localFolder + fileName, fileName, renamedFileName);

            SFTPErrCode err = RunWithArg(winSCPArg);
            return err;
        }

        public SFTPErrCode Download(string localFolder, string remoteFolder, string fileName)
        {
            if (!localFolder.EndsWith("\\"))
                localFolder = localFolder + "\\";

            string winSCPArg =
                "/command \"open {0}\" \"option batch abort\" \"option confirm off\" " +
                "\"lcd {1}\" \"cd {2}\" \"get {3}\" \"exit\"";

            winSCPArg = string.Format(
                winSCPArg,
                _winSCPProfile,
                localFolder, remoteFolder, fileName);

            SFTPErrCode err = RunWithArg(winSCPArg);
            return err;
        }
        public SFTPErrCode DeleteFile(string ftpServer, string remoteLocation, string localFile, string remoteFile)
        {
            string scriptLocation = localFile + "_deleteFile" + ".script";



            try
            {

                StreamWriter script = new StreamWriter(scriptLocation);
                script.WriteLine("option batch abort");
                script.WriteLine("option confirm off");
                script.WriteLine(string.Format("open {0}", ftpServer));
                script.WriteLine("cd \"{0}\"", remoteLocation);
                script.WriteLine("rm \"{0}\"", remoteFile);
                script.WriteLine("close");
                script.WriteLine("exit");
                script.Close();
                SFTPErrCode err = RunWithArg(string.Format("/script=\"{0}\"", scriptLocation));
                FileInfo objFile = new FileInfo(scriptLocation);
                if (objFile.Exists)
                    objFile.Delete();
                return err;
            }
            catch (Exception)
            {
                FileInfo objFile = new FileInfo(scriptLocation);
                if (objFile.Exists)
                    objFile.Delete();
            }

            return SFTPErrCode.Success;
        }

        public SFTPErrCode Upload(string ftpServer, string remoteLocation, string localFile)
        {
            FileInfo uploadFile = new FileInfo(localFile);

            try
            {
                string scriptLocation = localFile + ".script";
                StreamWriter script = new StreamWriter(scriptLocation);
                script.WriteLine("option batch abort");
                script.WriteLine("option confirm off");
                script.WriteLine(string.Format("open {0}", ftpServer));
                script.WriteLine("cd \"{0}\"", remoteLocation);
                script.WriteLine("option transfer binary");
                script.WriteLine("put \"{0}\"", localFile);
                script.WriteLine("close");
                script.WriteLine("exit");
                script.Close();

                SFTPErrCode err = RunWithArg(string.Format("/script=\"{0}\"", scriptLocation));
                return err;
            }
            catch (Exception) { }

            return SFTPErrCode.Success;
        }
        public SFTPErrCode Upload(string ftpServer, string remoteLocation, List<string> localFile)
        {
            FileInfo uploadFile = new FileInfo(localFile[0]);

            try
            {
                string scriptLocation = localFile + ".script";
                StreamWriter script = new StreamWriter(scriptLocation);
                script.WriteLine("option batch abort");
                script.WriteLine("option confirm off");
                script.WriteLine(string.Format("open {0}", ftpServer));
                script.WriteLine("cd \"{0}\"", remoteLocation);
                script.WriteLine("option transfer binary");
                foreach (string _file in localFile)
                {
                    script.WriteLine("put \"{0}\"", _file);
                }
                script.WriteLine("close");
                script.WriteLine("exit");
                script.Close();

                SFTPErrCode err = RunWithArg(string.Format("/script=\"{0}\"", scriptLocation));
                return err;
            }
            catch (Exception) { }

            return SFTPErrCode.Success;
        }
        public SFTPErrCode Create_Folder(string ftpServer, string remoteLocation, string localFile, string companyFolder)
        {
            FileInfo uploadFile = new FileInfo(localFile);
            DirectoryInfo dirFile = new DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath(string.Format(@"{0}/{1}", @"~/App_Data", localFile + "/" + companyFolder + "/")));
            if (!dirFile.Exists)
                dirFile.Create();
            try
            {
                string scriptLocation = dirFile.FullName + @"\" + companyFolder + ".script";
                StreamWriter script = new StreamWriter(scriptLocation);
                script.WriteLine("option batch abort");
                script.WriteLine("option confirm off");
                script.WriteLine(string.Format("open {0}", ftpServer));
                script.WriteLine("cd \"{0}\"", remoteLocation);
                script.WriteLine("mkd \"{0}\"", companyFolder);
                script.WriteLine("option transfer binary");
                script.WriteLine("close");
                script.WriteLine("exit");
                script.Close();
                SFTPErrCode err = RunWithArg(string.Format("/script=\"{0}\"", scriptLocation));
                return err;
            }
            catch (Exception) { }
            return SFTPErrCode.Success;
        }
    }
}
