﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace unifiedringmyaccountwebapi.Helpers
{
    public interface iDBConnection : IDisposable
    {
        void Close();
        SqlCommand CreateCommandFor(string dynamicSqlExpression);
        SqlCommand CreateCommandForSP(string dynamicSqlExpression, IList<SqlParameter> listSQLParams);
        SqlConnection UnderlyingConnection { get; }
        SqlConnection SecondaryConnection { get; } 
    }
}
