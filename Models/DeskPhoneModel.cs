﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace unifiedringmyaccountwebapi.Models
{
    public class DeskPhoneModel
    {
        public long domain_id { get; set; }
        public string extension { get; set; }
        public string company_name { get; set; }
        public string site_code { get; set; }
        public string domain_name { get; set; }
        public string extension_name { get; set; }
        public string vm_password { get; set; }
        public string admin_password { get; set; }

        [Required(ErrorMessage = "(*) Please enter mac address")]
        public string deskphone_mac { get; set; }
        public string deskphone_model { get; set; }
        public string Effective_Caller_Id_Name { get; set; }
        public string SecurityID { get; set; }
    }

    public class MyAccountCompany
    {
        public int domain_id { get; set; }
        public string company_name { get; set; }
        public int company_id { get; set; }
        public string domain_name { get; set; }
        public int sme_enterprise_id { get; set; }
        public string account_status { get; set; }
        public string contact_title { get; set; }
        public string contact_first_name { get; set; }
        public string contact_last_name { get; set; }
        public DateTime contact_birth_date { get; set; }
        public string contact_phone { get; set; }
        public string postcode { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string dd_account_number { get; set; }
        public string dd_account_name { get; set; }
        public string dd_sort_code { get; set; }
        public string dd_status { get; set; }
        public string dd_bank { get; set; }
        public string email { get; set; }
        public int customer_id { get; set; }
        public string contract_type_alias { get; set; }
    }
    public class Deskphone_FTP_Info
    {
        public int Customer_Id { get; set; }
        public string Security_Id { get; set; }
        public string Ext { get; set; }
        public string Remote_Folder { get; set; }
        public string Local_Folder { get; set; }
        public string Filename { get; set; }
    }

    public class Deskphone_Meta_Info_V1
    {
        public string Effective_Caller_Id_Name { get; set; }
        public string Extension { get; set; }
        public string Status { get; set; }
        public int Flag_Status
        {
            get
            {
                return Status == "Active" ? 1 : 0;
            }
        }
    }

    public class cOutMessage
    {
        public int ErrCode { get; set; }
        public string ErrMsg { get; set; }
    }
}