﻿using System.Security.Principal;

namespace unifiedringmyaccountwebapi
{
    public interface IProvidePrincipal
    {
        IPrincipal CreatePrincipal(string username, string password, string UrlKey);

        //IPrincipal ReturnTokenStatus(int projID,string tokenID,bool refresh);


        IPrincipal ReturnTokenStatus(string Namespace, string UrlKey);
    }
}