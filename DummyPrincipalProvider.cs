﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using System.Security.Principal;


namespace unifiedringmyaccountwebapi
{
    public class DummyPrincipalProvider : IProvidePrincipal
    {
        //private const string Username = "uaGu7hIRT77acx";
        //private const string Password = "83b10fa4e2ba4675a3959b07b2f8336c";

        //private const string User_Id = "test";
        //private const string Pwd = "@123";

        //public static UspapireturntokenstatusOutput ApiReturnTokenStatus(UspapireturntokenstatusInput req)
        //{
        //    var obj = new UspapireturntokenstatusOutput();
        //    try
        //    {

        //        try
        //        {
        //            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["API_logger"].ConnectionString))
        //            {
        //                conn.Open();
        //                var sp = "Usp_API_Return_Token_Status";
        //                var result = conn.Query<dynamic>(
        //                        sp, new
        //                        {
        //                            @Proj_ID = req.Proj_ID,
        //                            @Token_ID = req.Token_ID,
        //                            @Refresh = req.Refresh,
        //                        },
        //                        commandType: CommandType.StoredProcedure);
        //                if (Convert.ToString(result.ElementAt(0).Status) == "Ok")
        //                {
        //                    obj.Status = result.ElementAt(0).Status;
        //                }
        //                else
        //                {
        //                    obj.Status = result.ElementAt(0).Status;

        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            throw;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return obj;
        //}

        //public IPrincipal ReturnTokenStatus(int projID, string tokenID, bool refresh)


        public IPrincipal ReturnTokenStatus(string ProjectName, string tokenID)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["API_logger"].ConnectionString))
            {
                conn.Open();
                try
                {
                    var result = conn.Query<dynamic>(
                            "Usp_API_Return_Token_Status", new
                            {
                                @Proj_Name = ProjectName,
                                @Token_ID = tokenID
                            },
                            commandType: CommandType.StoredProcedure);

                    if (result.ElementAt(0).Status == "OK")
                    {
                        var identity = new GenericIdentity(tokenID);
                        IPrincipal principal = new GenericPrincipal(identity, new[] { "User" });                      
                        return principal;
                    }
                    else
                    {
                        //NotAuthorizedHttpException err = new NotAuthorizedHttpException(tokenID);

                        UspapireturntokenstatusOutput output = new UspapireturntokenstatusOutput();
                        output.Status = result.ElementAt(0).Status;


                        return null;
                    }
                }
                catch (Exception ex)
                {
                }
            }
            return null;
        }

        public IPrincipal CreatePrincipal(string username, string password, string projectid)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["API_logger"].ConnectionString))
            {
                conn.Open();
                try
                {
                    var result = conn.Query<dynamic>(
                            "USP_API_Get_API_Details", new
                            {
                                @User_Id = username,
                                @Pwd = password,
                                @Proj_Id = projectid
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result.ElementAt(0).errcode == 1 && result.ElementAt(0).errmsg == "Success")
                    {
                        var identity = new GenericIdentity(username);
                        IPrincipal principal = new GenericPrincipal(identity, new[] { "User" });
                        return principal;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                }
            }
            return null;
        }
    }
}