using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdevicegetdeliveryaddressController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdevicegetdeliveryaddressInput
        {
			public int company_id { get; set; }
			public int daid { get; set; }  	                   	    
        }
        public class UrdevicegetdeliveryaddressOutput
        {
			public int daid { get; set; }
			public string customer_name { get; set; }
			public string address1 { get; set; }
			public string address2 { get; set; }
			public string city { get; set; }
			public string postcode { get; set; }
			public string country { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdevicegetdeliveryaddressInput req)
        {
            List<UrdevicegetdeliveryaddressOutput> OutputList = new List<UrdevicegetdeliveryaddressOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_get_delivery_address";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@daid = req.daid 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrdevicegetdeliveryaddressOutput()
                        {
							daid = r.daid == null ? 0 : r.daid,
							customer_name = r.customer_name,
							address1 = r.address1,
							address2 = r.address2,
							city = r.city,
							postcode = r.postcode,
							country = r.country,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrdevicegetdeliveryaddressOutput outputobj = new UrdevicegetdeliveryaddressOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrdevicegetdeliveryaddressOutput outputobj = new UrdevicegetdeliveryaddressOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
