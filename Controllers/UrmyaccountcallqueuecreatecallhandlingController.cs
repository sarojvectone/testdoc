using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountcallqueuecreatecallhandlingController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcallqueuecreatecallhandlingInput
        {
			public int Company_id { get; set; }
			public int call_queue_id { get; set; }
			public int hour_type { get; set; }
			public int queue_Handling_Type { get; set; }
			public int max_Ring_duration { get; set; }
			public string user_info_xml { get; set; }  	                   	    
        }
        public class UrmyaccountcallqueuecreatecallhandlingOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcallqueuecreatecallhandlingInput req)
        {
            List<UrmyaccountcallqueuecreatecallhandlingOutput> OutputList = new List<UrmyaccountcallqueuecreatecallhandlingOutput>();
			Log.Info("Input : UrmyaccountcallqueuecreatecallhandlingController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_call_queue_create_call_handling";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_id = req.Company_id,
								@call_queue_id = req.call_queue_id,
								@hour_type = req.hour_type,
								@queue_Handling_Type = req.queue_Handling_Type,
								@max_Ring_duration = req.max_Ring_duration,
								@user_info_xml = req.user_info_xml 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcallqueuecreatecallhandlingOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcallqueuecreatecallhandlingOutput outputobj = new UrmyaccountcallqueuecreatecallhandlingOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcallqueuecreatecallhandlingOutput outputobj = new UrmyaccountcallqueuecreatecallhandlingOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
