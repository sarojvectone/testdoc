using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmacallqueuegetdtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmacallqueuegetdtlInput
        {
			public int domain_id { get; set; }
			public int cq_id { get; set; }  	                   	    
        }
        public class UrmacallqueuegetdtlOutput
        {
			public int? extension { get; set; }
			public string msisdn { get; set; }
			public string name { get; set; }
			public string call_queue_play_file { get; set; }
			public string call_queue_message_file { get; set; }
			public int? status { get; set; }
            public int? cq_id { get; set; }
            public int? audio_file_type { get; set; }
            public string file_type_content { get; set; }
            public int? site_id  { get; set; }
            public string site_name { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmacallqueuegetdtlInput req)
        {
            List<UrmacallqueuegetdtlOutput> OutputList = new List<UrmacallqueuegetdtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_call_queue_get_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id,
								@cq_id = req.cq_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmacallqueuegetdtlOutput()
                        {
							extension = r.extension == null ? 0 : r.extension,
							msisdn = r.msisdn,
							name = r.name,
							call_queue_play_file = r.call_queue_play_file,
							call_queue_message_file = r.call_queue_message_file,
							status = r.status == null ? 0 : r.status,
                            cq_id = r.cq_id,                            
                            audio_file_type = r.audio_file_type == null ? 0 : r.audio_file_type,
                            file_type_content = r.file_type_content,
                            site_id = r.site_id == null ? 0 : r.site_id,
                            site_name  = r.site_name ,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmacallqueuegetdtlOutput outputobj = new UrmacallqueuegetdtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmacallqueuegetdtlOutput outputobj = new UrmacallqueuegetdtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
