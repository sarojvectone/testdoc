using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmacallblastgetdtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmacallblastgetdtlInput
        {
			public int company_id { get; set; }  	                   	    
        }
        public class UrmacallblastgetdtlOutput
        {
			public int? idx { get; set; }
			public int? company_id { get; set; }
			public string message_name { get; set; }
			public string scheduled_date { get; set; }
			public string scheduled_time { get; set; }
			public string extension { get; set; }
			public int? blast_from { get; set; }
			public string type { get; set; }
            public string call_balst_status { get; set; }
            public int? status { get; set; }
            public string site_name { get; set; }
            public int site_id { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmacallblastgetdtlInput req)
        {
            List<UrmacallblastgetdtlOutput> OutputList = new List<UrmacallblastgetdtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_call_blast_get_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmacallblastgetdtlOutput()
                        {
							idx = r.idx == null ? 0 : r.idx,
							company_id = r.company_id == null ? 0 : r.company_id,
							message_name = r.message_name,
							scheduled_date = r.scheduled_date,
							scheduled_time = r.scheduled_time,
							extension = r.extension,
							blast_from = r.blast_from == null ? 0 : r.blast_from,
							type = r.type,
                            call_balst_status = r.call_balst_status,
                            status = r.status == null ? 0 : r.status,
                            site_id = r.site_id == null ? 0 : r.site_id,
                            site_name  = r.site_name ,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmacallblastgetdtlOutput outputobj = new UrmacallblastgetdtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmacallblastgetdtlOutput outputobj = new UrmacallblastgetdtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
