﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class UrphoneupdatenumbertypeController :ApiController
    {

        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrphoneupdatenumbertypeInput
        {
            public int company_id { get; set; }
            public string external_number { get; set; }
            public int num_type_id { get; set; }
        }
        public class UrphoneupdatenumbertypeOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrphoneupdatenumbertypeInput req)
        {
            List<UrphoneupdatenumbertypeOutput> OutputList = new List<UrphoneupdatenumbertypeOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_phone_update_number_type";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id,
                                @external_number = req.external_number,
                                @num_type_id = req.num_type_id
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrphoneupdatenumbertypeOutput()
                        {                            
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrphoneupdatenumbertypeOutput outputobj = new UrphoneupdatenumbertypeOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrphoneupdatenumbertypeOutput outputobj = new UrphoneupdatenumbertypeOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}