using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrshippingcarriergetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrshippingcarriergetInput
        {
			public int sc_id { get; set; }  	                   	    
        }
        public class UrshippingcarriergetOutput
        {
			public string shipp_name { get; set; }
			public double? shipp_weight { get; set; }
			public string shipp_region { get; set; }
            public double? cost { get; set; }
			public int sc_id { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrshippingcarriergetInput req)
        {
            List<UrshippingcarriergetOutput> OutputList = new List<UrshippingcarriergetOutput>();
			Log.Info("Input : UrshippingcarriergetController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_shipping_carrier_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@sc_id = req.sc_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrshippingcarriergetOutput()
                        {
							shipp_name = r.shipp_name,
							shipp_weight = r.shipp_weight == null ? 0.0F : r.shipp_weight,
							shipp_region = r.shipp_region,
							cost = r.cost == null ? 0.0F : r.cost,
							sc_id = r.sc_id == null ? 0 : r.sc_id,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrshippingcarriergetOutput outputobj = new UrshippingcarriergetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrshippingcarriergetOutput outputobj = new UrshippingcarriergetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
