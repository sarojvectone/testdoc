﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebgetcompanydtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebgetcompanydtlInput
        {
            public int company_id { get; set; }
        }
        public class UrmyaccwebgetcompanydtlOutput
        {
            public dynamic company_name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string address3 { get; set; }
            public string city { get; set; }
            public dynamic postcode { get; set; }
            public string country { get; set; }
            public int Is_enable_Multisite { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebgetcompanydtlInput req)
        {
            List<UrmyaccwebgetcompanydtlOutput> OutputList = new List<UrmyaccwebgetcompanydtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_get_company_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebgetcompanydtlOutput()
                        {
                            company_name = r.company_name,
                            address1 = r.address1,
                            address2 = r.address2,
                            address3 = r.address3,
                            city = r.city,
                            postcode = r.postcode,
                            country = r.country,
                            Is_enable_Multisite = r.Is_enable_Multisite == null ? 0 : r.Is_enable_Multisite,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebgetcompanydtlOutput outputobj = new UrmyaccwebgetcompanydtlOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebgetcompanydtlOutput outputobj = new UrmyaccwebgetcompanydtlOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}