using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappgetteaminfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappgetteaminfoInput
        {
            public int team_id { get; set; }
            public int company_id { get; set; }
            public int extension { get; set; }
            public string SIPID{ get; set; }

        }
        public class UrappgetteaminfoOutput
        {
            public int? id { get; set; }
            public int? company_id { get; set; }
            public string caller_id { get; set; }
            public int? team_type { get; set; }
            public string description { get; set; }
            public string team_members { get; set; }
            public int? except_guest { get; set; }
            public int? post_msg { get; set; }
            public int? mention { get; set; }
            public int? integration { get; set; }
            public int? pin_post { get; set; }
            public int? is_favourite { get; set; }
            public string team_guid { get; set; }
            public int? created_by { get; set; }
            public int? make_admin { get; set; }
            public int close_conversation { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
            public int? Mute_Notification { get; set; }
            public int? Phone_Notification { get; set; }
            public string admin_members { get; set; }
            public string profile_image_url{ get; set; }
            public int? archived { get; set; }
            public string team_id_prefix { get; set; }
            public string leaveTeamMembers { get; set; }
            public int?  mute_notif_id{ get; set; }
            public DateTime? createdate { get; set; }
        }

        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappgetteaminfoInput req)
        {
            List<UrappgetteaminfoOutput> OutputList = new List<UrappgetteaminfoOutput>();
            Log.Info("Input : UrappgetteaminfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_get_team_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @team_id = req.team_id,
                                @company_id = req.company_id,
                                //@extension = req.extension
                                @SIPID = req.SIPID 
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        //Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        Log.Info("Output DB : " + "Received:Commented to void log entry");
                        OutputList.AddRange(result.Select(r => new UrappgetteaminfoOutput()
                        {
                            id = r.team_id == null ? 0 : r.team_id,
                            company_id = r.company_id == null ? 0 : r.company_id,
                            caller_id = r.team_name,
                            team_type = r.team_type == null ? 0 : r.team_type,
                            description = r.description,
                            team_members = r.team_members,
                            except_guest = r.except_guest == null ? 0 : r.except_guest,
                            post_msg = r.post_msg == null ? 0 : r.post_msg,
                            mention = r.mention == null ? 0 : r.mention,
                            integration = r.integration == null ? 0 : r.integration,
                            pin_post = r.pin_post == null ? 0 : r.pin_post,
                            is_favourite = r.fav_status == null ? 0 : r.fav_status,
                            team_guid = r.team_guid,
                            created_by = r.created_by == null ? 0 : r.created_by,
                            make_admin = r.make_admin == null ? 0 : r.make_admin,
                            close_conversation = r.close_conversation == null ? 0 : r.close_conversation,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg,
                            Mute_Notification = r.Mute_Notification == null ? 0 : r.Mute_Notification,
                            admin_members = r.admin_members,
                            Phone_Notification = r.Phone_Notification == null ? 0 : r.Phone_Notification,
                            profile_image_url= r.profile_image_url,
                            archived = r.archived == null ? 0 : r.archived,
                            team_id_prefix = r.team_id_prefix,
                            leaveTeamMembers = r.leaveTeamMembers,
                            mute_notif_id = r.mute_notif_id == null ? 0 : r.mute_notif_id,
                            createdate = r.createdate == null ? null : r.createdate,
                        }));
                    }
                    else
                    {
                        Log.Info("Output DB : " + "Empty result");
                        UrappgetteaminfoOutput outputobj = new UrappgetteaminfoOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                UrappgetteaminfoOutput outputobj = new UrappgetteaminfoOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
           
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
