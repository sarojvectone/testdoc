using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebupdateuserextensiondtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebupdateuserextensiondtlInput
        {
			public int? Company_id { get; set; }
			public int? Basket_id { get; set; }
			public string Extension_Number { get; set; }
			public string Firstname { get; set; }
			public string Surname { get; set; }
			public string Department { get; set; }
			public string Email { get; set; }
			public string Mobileno { get; set; }
			public string Direct_Number { get; set; }
			public string Recorded_usename_filepath { get; set; }
			public int? Status { get; set; }
			public string User_Language { get; set; }
			public string Greeting_Language { get; set; }
			public string Regional_Language { get; set; }
			public string Home_Country_Code { get; set; }
			public string Time_zone { get; set; }
			public int? Time_format { get; set; }
			public int? Role_Id { get; set; }
			public int? update_type { get; set; }
            public int? Recorded_by { get; set; }
            public int? Recorded_type { get; set; }
            public string Recorded_greeting { get; set; }
            public string extn_audio_file { get; set; }
            public string extn_name_audio_file { get; set; }
            public int? mapwithphone { get; set; }
            public string emp_id { get; set; }
            public int Hubspot_contactid { get; set; }
            public string Phone_id { get; set; }
            public int? user_type { get; set; }

        }
        public class UrmyaccwebupdateuserextensiondtlOutput
        {
            public int  extn_greeting_by_ah { get; set; }
            public string working_business_type { get; set; }
            public int extn_greeting_by { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebupdateuserextensiondtlInput req)
        {
            Log.Debug("Urmyaccwebupdateuserextensiondtl");
            Log.Debug("Input : {0}", JsonConvert.SerializeObject(req));
            List<UrmyaccwebupdateuserextensiondtlOutput> OutputList = new List<UrmyaccwebupdateuserextensiondtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_Update_User_Extension_Dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_id = req.Company_id,
								@Basket_id = req.Basket_id,
								@Extension_Number = req.Extension_Number,
								@Firstname = req.Firstname,
								@Surname = req.Surname,
								@Department = req.Department,
								@Email = req.Email,
								@Mobileno = req.Mobileno,
								@Direct_Number = req.Direct_Number,
								@Recorded_usename_filepath = req.Recorded_usename_filepath,
								@Status = req.Status,
								@User_Language = req.User_Language,
								@Greeting_Language = req.Greeting_Language,
								@Regional_Language = req.Regional_Language,
								@Home_Country_Code = req.Home_Country_Code,
								@Time_zone = req.Time_zone,
								@Time_format = req.Time_format,
								@Role_Id = req.Role_Id,
								@update_type = req.update_type,
                                @Recorded_by = req.Recorded_by,
                                @Recorded_type = req.Recorded_type,
                                @Recorded_greeting = req.Recorded_greeting,
                                @extn_audio_file = req.extn_audio_file,
                                @extn_name_audio_file = req.extn_name_audio_file,
                                @mapwithphone = req.mapwithphone,
                                @emp_id = req.emp_id,
                                @Hubspot_contactid = req.Hubspot_contactid,
                                @Phone_id = req.Phone_id,
                                @user_type = req.user_type
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Debug("SP result : {0}", JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebupdateuserextensiondtlOutput()
                        {
                            extn_greeting_by_ah = r.extn_greeting_by_ah == null ? 0 : r.extn_greeting_by_ah,
                            working_business_type = r.working_business_type,
                            extn_greeting_by = r.extn_greeting_by == null ? 0 : r.extn_greeting_by,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebupdateuserextensiondtlOutput outputobj = new UrmyaccwebupdateuserextensiondtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebupdateuserextensiondtlOutput outputobj = new UrmyaccwebupdateuserextensiondtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
