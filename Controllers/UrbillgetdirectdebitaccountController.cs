using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrbillgetdirectdebitaccountController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrbillgetdirectdebitaccountInput
        {
			public int? company_id { get; set; }  	                   	    
        }
        public class UrbillgetdirectdebitaccountOutput
        {
            public string user_first_name  { get; set; }
			public string firstname { get; set; }
			public string surname { get; set; }
			public string mobileno { get; set; }
			public string email { get; set; }
			public string address1 { get; set; }
			public string address2 { get; set; }
			public string city { get; set; }
			public string postcode { get; set; }
			public string country { get; set; }
            public int? status { get; set; }
            public string  bank_name { get; set; }
            public string account_number { get; set; }
            public string sort_code { get; set; }
            public int? id { get; set; }
            public string user_last_name { get; set; }
            public string user_email { get; set; }
            public string user_bill_address1 { get; set; }
            public string user_bill_address2 { get; set; }
            public string user_city { get; set; }
            public string user_postcode { get; set; }
            public string user_country { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrbillgetdirectdebitaccountInput req)
        {
            List<UrbillgetdirectdebitaccountOutput> OutputList = new List<UrbillgetdirectdebitaccountOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_bill_get_directdebit_account";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrbillgetdirectdebitaccountOutput()
                        {
							firstname = r.firstname,
							surname = r.surname,
							mobileno = r.mobileno,
							email = r.email,
							address1 = r.address1,
							address2 = r.address2,
							city = r.city,
							postcode = r.postcode,
							country = r.country,
                            status = r.status == null ? 0 : r.status,
                            bank_name = r.bank_name,
                            account_number = r.account_number,
                            sort_code = r.sort_code,
                            id = r.id == null ? 0 : r.id,
                            user_last_name = r.user_last_name,
                            user_email = r.user_email,
                            user_bill_address1 = r.user_bill_address1,
                            user_bill_address2 = r.user_bill_address2,
                            user_city = r.user_city,
                            user_postcode = r.user_postcode,
                            user_country = r.user_country,
                            user_first_name = r.user_first_name,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));

                    }
                    else
                    {
                        UrbillgetdirectdebitaccountOutput outputobj = new UrbillgetdirectdebitaccountOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrbillgetdirectdebitaccountOutput outputobj = new UrbillgetdirectdebitaccountOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
