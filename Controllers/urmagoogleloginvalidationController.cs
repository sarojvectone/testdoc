using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace unifiedringappapi.Controllers
{
    public class urmagoogleloginvalidationController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class urmagoogleloginvalidationInput
        {
            public string login_user_name { get; set; }
            public string login_password { get; set; }
            public string login_source { get; set; }
            public string login_device_id { get; set; }
            public string login_ipaddress { get; set; }
        }
        public class UrmagoogleloginvalidationOutput
        {
            public int? error_code { get; set; }
            public string error_msg { get; set; }
            public string URL { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, urmagoogleloginvalidationInput req)
        {
            List<UrmagoogleloginvalidationOutput> OutputList = new List<UrmagoogleloginvalidationOutput>();
            try
            {
                Log.Info("urmaloginvalidationController");
                Log.Info("Input : " + JsonConvert.SerializeObject(req));

                string client_id = ConfigurationManager.AppSettings["Google.ClientID"];
                string redirect_uri = ConfigurationManager.AppSettings["Google.RedirectUrl"];
                string URL = "https://accounts.google.com/o/oauth2/v2/auth?client_id="+client_id+"&response_type=code&scope=openid&redirect_uri="+redirect_uri+"&state=abcdef";

                UrmagoogleloginvalidationOutput outputobj = new UrmagoogleloginvalidationOutput();
                outputobj.error_code = 0;
                outputobj.error_msg = "Success";
                outputobj.URL = URL;
                OutputList.Add(outputobj);
                return Request.CreateResponse(HttpStatusCode.OK, OutputList);
                //var httpWebRequest1 = (HttpWebRequest)WebRequest.Create(webAddr1);
                //httpWebRequest1.ContentType = "application/json";
                //httpWebRequest1.Method = "POST";
                //using (var streamWriter = new StreamWriter(httpWebRequest1.GetRequestStream()))
                //{
                //    streamWriter.Write(jsonresult);
                //    streamWriter.Flush();
                //}
                //httpResponse1 = (HttpWebResponse)httpWebRequest1.GetResponse();
                //WebHeaderCollection header = httpResponse1.Headers;

                //var encoding = ASCIIEncoding.ASCII;
                //responseText = "";
                //using (var reader = new System.IO.StreamReader(httpResponse1.GetResponseStream(), encoding))
                //{
                //    responseText = reader.ReadToEnd();
                //}
 
            }
            catch (Exception ex)
            {
                UrmagoogleloginvalidationOutput outputobj = new UrmagoogleloginvalidationOutput();
                outputobj.error_code = -1;
                outputobj.error_msg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        } 
    } 
}
