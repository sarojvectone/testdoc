﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebvoicemsgninsertextentionaudiofileController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebvoicemsgninsertextentionaudiofileInput
        {
            public int Company_id { get; set; }
            public int extension { get; set; }
            public int? Hour_type { get; set; }
            public int Greeting_by { get; set; }
            public int Greeting_type { get; set; }
            public string Language { get; set; }
            public string Audio_file { get; set; }
            public int Active { get; set; }
            public int update_type { get; set; }
        }
        public class UrmyaccwebvoicemsgninsertextentionaudiofileOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebvoicemsgninsertextentionaudiofileInput req)
        {
            List<UrmyaccwebvoicemsgninsertextentionaudiofileOutput> OutputList = new List<UrmyaccwebvoicemsgninsertextentionaudiofileOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_Voice_Msgn_Insert_extention_audio_file";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @Company_id = req.Company_id,
                                @extension = req.extension,
                                @Hour_type = req.Hour_type,
                                @Greeting_by = req.Greeting_by,
                                @Greeting_type = req.Greeting_type,
                                @Language = req.Language,
                                @Audio_file = req.Audio_file,
                                @Active = req.Active,
                                @update_type = req.update_type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebvoicemsgninsertextentionaudiofileOutput()
                        {
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebvoicemsgninsertextentionaudiofileOutput outputobj = new UrmyaccwebvoicemsgninsertextentionaudiofileOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebvoicemsgninsertextentionaudiofileOutput outputobj = new UrmyaccwebvoicemsgninsertextentionaudiofileOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
