using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrecomgetuserrangemasterdetailController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrecomgetuserrangemasterdetailInput
        {
        }
        public class UrecomgetuserrangemasterdetailOutput
        {
			public int? Range_typeid { get; set; }
			public int? From_user_Range { get; set; }
			public int? To_user_Range { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Get(string id, HttpRequestMessage request, UrecomgetuserrangemasterdetailInput req)
        {
            List<UrecomgetuserrangemasterdetailOutput> OutputList = new List<UrecomgetuserrangemasterdetailOutput>();
			Log.Info("Input : UrecomgetuserrangemasterdetailController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ECom_get_User_Range_Master_Detail";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrecomgetuserrangemasterdetailOutput()
                        {
							Range_typeid = r.Range_typeid == null ? 0 : r.Range_typeid,
							From_user_Range = r.From_user_Range == null ? 0 : r.From_user_Range,
							To_user_Range = r.To_user_Range == null ? 0 : r.To_user_Range,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrecomgetuserrangemasterdetailOutput outputobj = new UrecomgetuserrangemasterdetailOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrecomgetuserrangemasterdetailOutput outputobj = new UrecomgetuserrangemasterdetailOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
