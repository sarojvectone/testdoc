using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdevicegetstockorderController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdevicegetstockorderInput
        {
			public int company_id { get; set; }  	                   	    
        }
        public class UrdevicegetstockorderOutput
        {
            public int order_id { get; set; }
            public string referenceid { get; set; }
            public DateTime? createdate { get; set; }         
            public double? tot_charge { get; set; }
            public int tot_device { get; set; }
            public int quantity { get; set; }
            public int device_approval { get; set; }
            public string  order_source { get; set; }
            public int rtn_quantity { get; set; }
            public int rpl_quantity { get; set; }
            public string tracking_no { get; set; }
            public DateTime? delivery_date { get; set; }
            public DateTime? last_update { get; set; }
            public string delivery_comment { get; set; }
            public string  comment { get; set; }
            public string name_of_the_device { get; set; }
            public string serial_number { get; set; }
            public string site_name { get; set; }
            public string mac_address { get; set; }
            public string phone_model { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }      
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdevicegetstockorderInput req)
        {
            List<UrdevicegetstockorderOutput> OutputList = new List<UrdevicegetstockorderOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_get_stock_order";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrdevicegetstockorderOutput()
                        {
							order_id = r.order_id == null ? 0 : r.order_id,
							referenceid = r.referenceid,					
                            createdate = r.createdate == null ? null : r.createdate,					
							tot_charge = r.tot_charge,
                            tot_device = r.tot_device == null ? 0 : r.tot_device,
                            quantity = r.quantity == null ? 0 : r.quantity,
                            order_source = r.order_source,
                            device_approval = r.device_approval == null ? 0 : r.device_approval,
                            rtn_quantity = r.rtn_quantity == null ? 0 : r.rtn_quantity,
                            rpl_quantity = r.rpl_quantity == null ? 0 : r.rpl_quantity,
                            tracking_no = r.tracking_no,
                            delivery_date = r.delivery_date == null ? null : r.delivery_date,
                            last_update = r.last_update == null ? null : r.last_update,
                            delivery_comment = r.delivery_comment,
                            comment = r.comment,
                            name_of_the_device = r.name_of_the_device,
                            serial_number = r.serial_number,
                            site_name = r.site_name,
                            mac_address	 = r.mac_address,
                            phone_model = r.phone_model,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg							
                        }));
                    }
                    else
                    {
                        UrdevicegetstockorderOutput outputobj = new UrdevicegetstockorderOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrdevicegetstockorderOutput outputobj = new UrdevicegetstockorderOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
