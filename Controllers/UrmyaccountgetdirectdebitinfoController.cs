using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccountgetdirectdebitinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountgetdirectdebitinfoInput
        {
			public int company_id { get; set; }
			public int id { get; set; }  	                   	    
        }
        public class UrmyaccountgetdirectdebitinfoOutput
        {
			public int? company_id { get; set; }
			public string first_name { get; set; }
			public string last_name { get; set; }
			public string email { get; set; }
			public string country { get; set; }
			public string bank_name { get; set; }
			public string account_number { get; set; }
			public string sort_code { get; set; }
			public string bill_address1 { get; set; }
			public string bill_address2 { get; set; }
			public string city { get; set; }
			public string postcode { get; set; }
			public int? Status { get; set; }
            public string company_name { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountgetdirectdebitinfoInput req)
        {
            List<UrmyaccountgetdirectdebitinfoOutput> OutputList = new List<UrmyaccountgetdirectdebitinfoOutput>();
			Log.Info("Input : UrmyaccountgetdirectdebitinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_get_direct_debit_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@id = req.id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountgetdirectdebitinfoOutput()
                        {
							company_id = r.company_id == null ? 0 : r.company_id,
							first_name = r.first_name,
							last_name = r.last_name,
							email = r.email,
							country = r.country,
							bank_name = r.bank_name,
							account_number = r.account_number,
							sort_code = r.sort_code,
							bill_address1 = r.bill_address1,
							bill_address2 = r.bill_address2,
							city = r.city,
							postcode = r.postcode,
							Status = r.Status == null ? 0 : r.Status,
                            company_name = r.company_name,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountgetdirectdebitinfoOutput outputobj = new UrmyaccountgetdirectdebitinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountgetdirectdebitinfoOutput outputobj = new UrmyaccountgetdirectdebitinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
