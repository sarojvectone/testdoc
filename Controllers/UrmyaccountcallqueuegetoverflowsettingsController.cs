using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountcallqueuegetoverflowsettingsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcallqueuegetoverflowsettingsInput
        {
			public int Company_id { get; set; }
			public int call_queue_id { get; set; }  	                   	    
        }
        public class UrmyaccountcallqueuegetoverflowsettingsOutput
        {
			public int? Queue_id { get; set; }
			public string Queue_name { get; set; }
			public int? call_Queue_extension { get; set; }
			public int? is_overflow_enabled { get; set; }
			public int? info_order { get; set; }
            //public int? Queue_overflow_max_time_reached { get; set; }
            //public int? Queue_Overflow_caller_limit_reach { get; set; }
            //public int? Queue_Overflow_status { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcallqueuegetoverflowsettingsInput req)
        {
            List<UrmyaccountcallqueuegetoverflowsettingsOutput> OutputList = new List<UrmyaccountcallqueuegetoverflowsettingsOutput>();
			Log.Info("Input : UrmyaccountcallqueuegetoverflowsettingsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_call_queue_get_overflow_settings";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_id = req.Company_id,
								@call_queue_id = req.call_queue_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcallqueuegetoverflowsettingsOutput()
                        {
							Queue_id = r.Queue_id == null ? 0 : r.Queue_id,
							Queue_name = r.Queue_name,
							call_Queue_extension = r.call_Queue_extension == null ? 0 : r.call_Queue_extension,
							is_overflow_enabled = r.is_overflow_enabled == null ? 0 : r.is_overflow_enabled,
							info_order = r.info_order == null ? 0 : r.info_order,
                            //Queue_overflow_max_time_reached = r.Queue_overflow_max_time_reached == null ? 0 : r.Queue_overflow_max_time_reached,
                            //Queue_Overflow_caller_limit_reach = r.Queue_Overflow_caller_limit_reach == null ? 0 : r.Queue_Overflow_caller_limit_reach,
                            //Queue_Overflow_status = r.Queue_Overflow_status == null ? 0 : r.Queue_Overflow_status,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcallqueuegetoverflowsettingsOutput outputobj = new UrmyaccountcallqueuegetoverflowsettingsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcallqueuegetoverflowsettingsOutput outputobj = new UrmyaccountcallqueuegetoverflowsettingsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
