using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrcrmuserloginController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrcrmuserloginInput
        {
            public string username { get; set; }
            public string password { get; set; } 
        }
        public class UrcrmuserloginOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public int? user_role { get; set; }
            public int? userid { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrcrmuserloginInput req)
        {
            List<UrcrmuserloginOutput> OutputList = new List<UrcrmuserloginOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_crm_user_login";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @username = req.username,
                                @password = req.password
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrcrmuserloginOutput()
                        {
							errcode = r.errcode == null ? -1 : r.errcode,
							errmsg = r.errmsg == null ? "Failure" : r.errmsg,
                            user_role = r.user_role == null ? -1 : r.user_role,
                            userid = r.userid 
                        }));
                    }
                    else
                    {
                        UrcrmuserloginOutput outputobj = new UrcrmuserloginOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrcrmuserloginOutput outputobj = new UrcrmuserloginOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
