using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebgetuserprefextensioninfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebgetuserprefextensioninfoInput
        {
			public int company_id { get; set; }
			public int orderid { get; set; }
			public int extension_number { get; set; }  	                   	    
        }
        public class UrmyaccwebgetuserprefextensioninfoOutput
        {
			public int? Basket_id { get; set; }
			public int? Company_id { get; set; }
			public string Extension_Number { get; set; }
			public string Firstname { get; set; }
			public string Surname { get; set; }
			public string Department { get; set; }
			public string Email { get; set; }
			public string Mobileno { get; set; } 
			public string Direct_Number { get; set; }
			public string User_Language { get; set; }
			public string Greeting_Language { get; set; }
			public string Regional_Language { get; set; }
			public string Role_Name { get; set; }
            public string extn_audio_file { get; set; }
            public string extn_name_audio_file { get; set; }
            public string working_business_type { get; set; }
            public string app_login_user_name { get; set; }

            public int? Recorded_by { get; set; }
            public int? Recorded_type { get; set; }
            public string Recorded_greeting { get; set; }
			
			public int? Status { get; set; }

			public string Record_usename_filepath { get; set; }
			public string Home_Country_Code { get; set; }
			public int? Time_format { get; set; }
			public string Time_zone { get; set; }
            public int? is_mapped { get; set; }
			
			public string emp_id { get; set; }
			public string app_login_pwd { get; set; }
			public string company_name { get; set; }
			public int? site_id { get; set; }
			public string site_name { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebgetuserprefextensioninfoInput req)
        {
            List<UrmyaccwebgetuserprefextensioninfoOutput> OutputList = new List<UrmyaccwebgetuserprefextensioninfoOutput>();
			Log.Info("Input : UrmyaccwebgetuserprefextensioninfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_get_user_pref_extension_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@orderid = req.orderid,
								@extension_number = req.extension_number 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccwebgetuserprefextensioninfoOutput()
                        {
							Basket_id = r.Basket_id == null ? 0 : r.Basket_id,
                            Company_id = r.Company_id == null ? 0 : r.Company_id,
                            Extension_Number = r.Extension_Number,
                            Firstname = r.Firstname,
                            Surname = r.Surname,
                            Department = r.Department,
                            Email = r.Email,
                            Mobileno = r.Mobileno,
                            is_mapped = r.is_mapped == null ? 0 : r.is_mapped,
                            Direct_Number = r.Direct_Number,
                            User_Language = r.User_Language,
                            Greeting_Language = r.Greeting_Language,
                            Regional_Language = r.Regional_Language,
                            Role_Name = r.Role_Name,
                            Status = r.Status == null ? 0 : r.Status,
                            Record_usename_filepath = r.Record_usename_filepath,
                            Home_Country_Code = r.Home_Country_Code,
                            Time_format = r.Time_format == null ? 0 : r.Time_format,
                            Time_zone = r.Time_zone,
                            Recorded_by = r.Recorded_by == null ? 0 : r.Recorded_by,
                            Recorded_type = r.Recorded_type == null ? 0 : r.Recorded_type,
                            Recorded_greeting = r.Recorded_greeting,
                            extn_audio_file = r.extn_audio_file,
                            extn_name_audio_file = r.extn_name_audio_file,
                            working_business_type = r.working_business_type,
                            app_login_user_name = r.app_login_user_name,
                            emp_id = r.emp_id,
                            app_login_pwd = r.app_login_pwd,
                            company_name = r.company_name,
                            site_id = r.site_id == null ? 0 : r.site_id,
                            site_name = r.site_name,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccwebgetuserprefextensioninfoOutput outputobj = new UrmyaccwebgetuserprefextensioninfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccwebgetuserprefextensioninfoOutput outputobj = new UrmyaccwebgetuserprefextensioninfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
