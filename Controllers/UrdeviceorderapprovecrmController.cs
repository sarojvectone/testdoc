using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdeviceorderapprovecrmController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdeviceorderapprovecrmInput
        {
            public DateTime? delivery_date { get; set; }
            public string delivery_comment { get; set; }
			public int orderid { get; set; }
			public int approval_flag { get; set; }
            public string comment { get; set; }             	    
        }
        public class UrdeviceorderapprovecrmOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdeviceorderapprovecrmInput req)
        {
            List<UrdeviceorderapprovecrmOutput> OutputList = new List<UrdeviceorderapprovecrmOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_order_approve_crm";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @delivery_date = req.delivery_date,
                                @delivery_comment = req.delivery_comment,
								@orderid = req.orderid,
								@approval_flag = req.approval_flag,
                                @comment = req.comment                                    
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrdeviceorderapprovecrmOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrdeviceorderapprovecrmOutput outputobj = new UrdeviceorderapprovecrmOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrdeviceorderapprovecrmOutput outputobj = new UrdeviceorderapprovecrmOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
