using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class VbticketinsertController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class VbticketinsertInput
        {
            public int ticketid { get; set; }
			public string reportername { get; set; }
			public string reporteremail { get; set; }
			public string issuesummary { get; set; }
			public int issuetypeid { get; set; }
			public int issuesubtypeid { get; set; }
			public string issuedescription { get; set; }
			public string priority { get; set; }
			public int? assigneeuserid { get; set; }
			public int? issueimpact { get; set; }
			public int? specificcustomerid { get; set; }
			public string orginalfilename { get; set; }
			public string renamedfilename { get; set; }
			public string filepath { get; set; }
			public string createdby { get; set; }
			public string ticketstatus { get; set; }
			public int? noteid { get; set; }  	                   	    
        }
        public class VbticketinsertOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, VbticketinsertInput req)
        {
            List<VbticketinsertOutput> OutputList = new List<VbticketinsertOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "vb_ticket_insert";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @ticketid = req.ticketid,
								@reportername = req.reportername,
								@reporteremail = req.reporteremail,
								@issuesummary = req.issuesummary,
								@issuetypeid = req.issuetypeid,
								@issuesubtypeid = req.issuesubtypeid,
								@issuedescription = req.issuedescription,
								@priority = req.priority,
								@assigneeuserid = req.assigneeuserid,
								@issueimpact = req.issueimpact,
								@specificcustomerid = req.specificcustomerid,
								@orginalfilename = req.orginalfilename,
								@renamedfilename = req.renamedfilename,
								@filepath = req.filepath,
								@createdby = req.createdby,
								@ticketstatus = req.ticketstatus,
								@noteid = req.noteid 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new VbticketinsertOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        VbticketinsertOutput outputobj = new VbticketinsertOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                VbticketinsertOutput outputobj = new VbticketinsertOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
