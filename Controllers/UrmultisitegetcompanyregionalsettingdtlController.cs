using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmultisitegetcompanyregionalsettingdtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmultisitegetcompanyregionalsettingdtlInput
        {
			public int Company_Id { get; set; }
            public int site_id { get; set; }           	    
        }
        public class UrmultisitegetcompanyregionalsettingdtlOutput
        {
			public string Time_zone { get; set; }
			public string Home_country_code { get; set; }
			public int? Greetings_language { get; set; }
			public int? User_language { get; set; }
			public string Regional_format { get; set; }
			public int? Time_format { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmultisitegetcompanyregionalsettingdtlInput req)
        {
            List<UrmultisitegetcompanyregionalsettingdtlOutput> OutputList = new List<UrmultisitegetcompanyregionalsettingdtlOutput>();
			Log.Info("Input : UrmultisitegetcompanyregionalsettingdtlController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Multisite_get_Company_Regional_Setting_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_Id = req.Company_Id,
                                @site_id = req.site_id                                   
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmultisitegetcompanyregionalsettingdtlOutput()
                        {
							Time_zone = r.Time_zone,
							Home_country_code = r.Home_country_code,
							Greetings_language = r.Greetings_language == null ? 0 : r.Greetings_language,
							User_language = r.User_language == null ? 0 : r.User_language,
							Regional_format = r.Regional_format,
							Time_format = r.Time_format == null ? 0 : r.Time_format,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmultisitegetcompanyregionalsettingdtlOutput outputobj = new UrmultisitegetcompanyregionalsettingdtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmultisitegetcompanyregionalsettingdtlOutput outputobj = new UrmultisitegetcompanyregionalsettingdtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
