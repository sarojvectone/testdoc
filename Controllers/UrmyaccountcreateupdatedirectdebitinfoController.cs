using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccountcreateupdatedirectdebitinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcreateupdatedirectdebitinfoInput
        {
			public int company_id { get; set; }
			public string first_name { get; set; }
			public string last_name { get; set; }
			public string email { get; set; }
			public string country { get; set; }
			public string bank_name { get; set; }
			public string account_number { get; set; }
			public string sort_code { get; set; }
			public string bill_address1 { get; set; }
			public string bill_address2 { get; set; }
			public string city { get; set; }
			public string postcode { get; set; }
			public int status { get; set; }
			public int processtype { get; set; }
            public int id { get; set; }
            public string company_name { get; set; }    
        }
        public class UrmyaccountcreateupdatedirectdebitinfoOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcreateupdatedirectdebitinfoInput req)
        {
            List<UrmyaccountcreateupdatedirectdebitinfoOutput> OutputList = new List<UrmyaccountcreateupdatedirectdebitinfoOutput>();
			Log.Info("Input : UrmyaccountcreateupdatedirectdebitinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_create_update_direct_debit_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@first_name = req.first_name,
								@last_name = req.last_name,
								@email = req.email,
								@country = req.country,
								@bank_name = req.bank_name,
								@account_number = req.account_number,
								@sort_code = req.sort_code,
								@bill_address1 = req.bill_address1,
								@bill_address2 = req.bill_address2,
								@city = req.city,
								@postcode = req.postcode,
								@status = req.status,
								@processtype = req.processtype,
                                @id = req.id,
                                @company_name = req.company_name   
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcreateupdatedirectdebitinfoOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcreateupdatedirectdebitinfoOutput outputobj = new UrmyaccountcreateupdatedirectdebitinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcreateupdatedirectdebitinfoOutput outputobj = new UrmyaccountcreateupdatedirectdebitinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
