using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrwebgettmppaymentinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrwebgettmppaymentinfoInput
        {
			public string sitecode { get; set; }
			public string reference_id { get; set; }
			public int step_no { get; set; }  	                   	    
        }


 
        public class UrwebgettmppaymentinfoOutput
        {
			public string payment_info { get; set; }
			public int step_no { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrwebgettmppaymentinfoInput req)
        {
            List<UrwebgettmppaymentinfoOutput> OutputList = new List<UrwebgettmppaymentinfoOutput>();
			Log.Info("Input : UrwebgettmppaymentinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_web_get_tmp_payment_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@sitecode = req.sitecode,
								@reference_id = req.reference_id,
								@step_no = req.step_no 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrwebgettmppaymentinfoOutput()
                        {
							payment_info = r.payment_info,
							step_no = r.step_no == null ? 0 : r.step_no,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrwebgettmppaymentinfoOutput outputobj = new UrwebgettmppaymentinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrwebgettmppaymentinfoOutput outputobj = new UrwebgettmppaymentinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
