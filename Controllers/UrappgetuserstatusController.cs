using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappgetuserstatusController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappgetuserstatusInput
        {
			public int domain_id { get; set; }
			public int extension { get; set; }  	                   	    
        }
        public class UrappgetuserstatusOutput
        {
			public string status_msg { get; set; }
			public int status_indicator_id { get; set; }
			public string status_indicator { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public int call_queue_status { get; set; }    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappgetuserstatusInput req)
        {
            List<UrappgetuserstatusOutput> OutputList = new List<UrappgetuserstatusOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_get_user_Status";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id,
								@extension = req.extension 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrappgetuserstatusOutput()
                        {
							status_msg = r.status_msg,
							status_indicator_id = r.status_indicator_id == null ? 0 : r.status_indicator_id,
							status_indicator = r.status_indicator,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg,
                            call_queue_status = r.call_queue_status == null ? 0 : r.call_queue_status
                        }));
                    }
                    else
                    {
                        UrappgetuserstatusOutput outputobj = new UrappgetuserstatusOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrappgetuserstatusOutput outputobj = new UrappgetuserstatusOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
