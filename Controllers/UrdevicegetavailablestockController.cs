using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdevicegetavailablestockController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdevicegetavailablestockInput
        {
            public string category_id { get; set; }
            public string inventory_id { get; set; }
            public string prod_model { get; set; }
        }
        public class UrdevicegetavailablestockOutput
        {
			public string inventory_name { get; set; }
			public string category { get; set; }
			public string prod_model { get; set; }
			public string prod_code { get; set; }
			public string prod_name { get; set; }
			public double? buy_price { get; set; }
			public double? ws_price { get; set; }
			public double? ur_price { get; set; }
			public string brand { get; set; }
			public double? prod_weight { get; set; }
			public string prod_short_desc { get; set; }
			public string prod_desc { get; set; }
			public string image_path { get; set; }
			public string data_sheet_path { get; set; }
			public string user_guide_path { get; set; }
			public int total_qty { get; set; }
			public int order_quantity { get; set; }
			public int available_qty { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdevicegetavailablestockInput req)
        {
            List<UrdevicegetavailablestockOutput> OutputList = new List<UrdevicegetavailablestockOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_get_available_stock";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                category_id =  req.category_id,
                                inventory_id = req.inventory_id,
                                prod_model = req.prod_model
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrdevicegetavailablestockOutput()
                        {
							inventory_name = r.inventory_name,
							category = r.category,
							prod_model = r.prod_model,
							prod_code = r.prod_code,
							prod_name = r.prod_name,
							buy_price = r.buy_price == null ? 0.0D : r.buy_price,
							ws_price = r.ws_price == null ? 0.0D : r.ws_price,
							ur_price = r.ur_price == null ? 0.0D : r.ur_price,
							brand = r.brand,
							prod_weight = r.prod_weight == null ? 0.0D : r.prod_weight,
							prod_short_desc = r.prod_short_desc,
							prod_desc = r.prod_desc,
							image_path = r.image_path,
							data_sheet_path = r.data_sheet_path,
							user_guide_path = r.user_guide_path,
							total_qty = r.total_qty == null ? 0 : r.total_qty,
							order_quantity = r.order_quantity == null ? 0 : r.order_quantity,
							available_qty = r.available_qty == null ? 0 : r.available_qty,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrdevicegetavailablestockOutput outputobj = new UrdevicegetavailablestockOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrdevicegetavailablestockOutput outputobj = new UrdevicegetavailablestockOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
