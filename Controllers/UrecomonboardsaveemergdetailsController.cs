using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrecomonboardsaveemergdetailsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrecomonboardsaveemergdetailsInput
        {
			public int company_id { get; set; }
			public int extension { get; set; }
			public string firstname { get; set; }
			public string lastname { get; set; }
			public string city { get; set; }
			public string post_code { get; set; }
			public string country { get; set; }
			public string address_line_1 { get; set; }
			public string address_line_2 { get; set; }
			public string address_line_3 { get; set; }  	                   	    
        }
        public class UrecomonboardsaveemergdetailsOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrecomonboardsaveemergdetailsInput req)
        {
            List<UrecomonboardsaveemergdetailsOutput> OutputList = new List<UrecomonboardsaveemergdetailsOutput>();
			Log.Info("Input : UrecomonboardsaveemergdetailsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ecom_onboard_save_emerg_details";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@extension = req.extension,
								@firstname = req.firstname,
								@lastname = req.lastname,
								@city = req.city,
								@post_code = req.post_code,
								@country = req.country,
								@address_line_1 = req.address_line_1,
								@address_line_2 = req.address_line_2,
								@address_line_3 = req.address_line_3 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrecomonboardsaveemergdetailsOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrecomonboardsaveemergdetailsOutput outputobj = new UrecomonboardsaveemergdetailsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrecomonboardsaveemergdetailsOutput outputobj = new UrecomonboardsaveemergdetailsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
