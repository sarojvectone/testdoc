using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using unifiedringmyaccountwebapi.Controllers;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappcreateteaminfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappcreateteaminfoInput
        {
            public int company_id { get; set; }
            public int team_id { get; set; }
            public string team_name { get; set; }
            public int team_type { get; set; }
            public string description { get; set; }
            public int created_by { get; set; }
            public int processtype { get; set; }
            public int except_guest { get; set; }
            public int post_msg { get; set; }
            public int mention { get; set; }
            public int integration { get; set; }
            public int pin_post { get; set; }
            public string add_members { get; set; }
            public string team_guid { get; set; }
            public string photo_info { get; set; }
            public string Delete_image { get; set; }
            public string get_image { get; set; }
            public int archived { get; set; }
            public string team_id_prefix { get; set; }

        }
        public class UrappcreateteaminfoOutput
        {
            public int team_id { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
            public string ImageName { get; set; }
            public string ImageURL { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappcreateteaminfoInput req)
        {
            List<UrappcreateteaminfoOutput> OutputList = new List<UrappcreateteaminfoOutput>();
            Log.Info("Input : UrappcreateteaminfoController:" + (req.company_id + " " +req.team_id));

            UrappcreateteaminfoOutput result1 = new UrappcreateteaminfoOutput();


            #region uploadImage
            try
            {
                Log.Info("UrdisplayTeampicture -Start");
                //Log.Info("Input : {0}", Newtonsoft.Json.JsonConvert.SerializeObject(req));
                string path = "";
                String Datetime = DateTime.Now.ToString("yyyyMMddHHmmss");

                string oldimg = req.company_id + "_" + req.team_id + "_*";
                string img = req.company_id + "_" + req.team_id +"_"+Datetime;

                if (!String.IsNullOrEmpty(req.team_name))
                {
                    if (!String.IsNullOrEmpty(req.photo_info) && req.Delete_image == "false")
                    {
                        path = LoadImageHelper.ReplaceImage(oldimg, img, req.photo_info, "Team-Picture");
                        path = path + ".jpg";
                    }
                    else if (req.Delete_image == "true")
                    {
                        path = LoadImageHelper.DeleteImage(oldimg, "Team-Picture");
                    }
                    //else if (req.get_image == "true")
                    //{
                    //    path = LoadImageHelper.FindImage(oldimg + "*", "Profile-Picture");
                    //    // path = String.IsNullOrEmpty(path) ? "default_trainer.jpg" : path;
                    //    Log.Info("filepath : {0}, fileName : {1}", path, oldimg);
                    //}
                }
                else
                {
                    UrappcreateteaminfoOutput outputobj = new UrappcreateteaminfoOutput();
                    outputobj.errcode = -1;
                    outputobj.errmsg = "Extention ID null/Mismatch";
                    OutputList.Add(outputobj);
                    return Request.CreateResponse(HttpStatusCode.OK, OutputList);
                }

                if (req != null && !string.IsNullOrEmpty(req.photo_info))
                {

                    OutputList = new List<UrappcreateteaminfoOutput>();
                    result1.ImageName = img;
                    result1.ImageURL = ConfigurationManager.AppSettings["TeamPic"] + "/" + img.Replace("\\", "/") + ".jpg";
                    result1.errcode = 0;
                    result1.errmsg = "Success";

                }
                else if (string.IsNullOrEmpty(req.photo_info) && req.Delete_image == "true")
                {
                    //UrappcreateteaminfoOutput outputobj = new UrappcreateteaminfoOutput();
                    //outputobj.errcode = 0;
                    //outputobj.errmsg = "Image Deleted Successfully";
                }
                //else if (req.get_image == "true")
                //{

                //    result1.ImageName = img;
                //    if (!string.IsNullOrEmpty(path))
                //    {
                //        //if (path.ToLower().Contains("wwwroot"))
                //        //{
                //        //    result1.ImageURL = ConfigurationManager.AppSettings["ProfilePicLocal"] + "/" + path.Replace("\\", "/");
                //        //}
                //        //else
                //        //{
                //        result1.ImageURL = ConfigurationManager.AppSettings["ProfilePic"] + "/" + path.Replace("\\", "/");
                //        //}
                //        result1.errcode = 0;
                //        result1.errmsg = "Success";
                //    }
                //    else
                //    {
                //        result1.ImageURL = "";
                //        result1.errcode = -1;
                //        //result1.errmsg = "Failed"; // 11/06/2019 -- Error MESSAGE
                //        result1.errmsg = "Please update/upload you profile picture";
                //    }
                //    OutputList.Add(result1);
                //}
            }
            catch (Exception ex)
            {
                UrappcreateteaminfoOutput outputobj = new UrappcreateteaminfoOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }

            Log.Info("UrdisplayTeampicture-End"); 
            #endregion


            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_create_team_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id,
                                @team_id = req.team_id,
                                @team_name = req.team_name,
                                @team_type = req.team_type,
                                @description = req.description,
                                @created_by = req.created_by,
                                @processtype = req.processtype,
                                @except_guest = req.except_guest,
                                @post_msg = req.post_msg,
                                @mention = req.mention,
                                @integration = req.integration,
                                @pin_post = req.pin_post,
                                @add_members = req.add_members,
                                @team_guid = req.team_guid,
                                @profile_image_url = result1.ImageURL,
                                @archived = req.archived,
                                @team_id_prefix = req.team_id_prefix
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrappcreateteaminfoOutput()
                        {
                            team_id = r.team_id == null ? 0 : r.team_id,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            ImageURL = result1.ImageURL,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        Log.Info("Output DB : " + "Empty result");
                        UrappcreateteaminfoOutput outputobj = new UrappcreateteaminfoOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                UrappcreateteaminfoOutput outputobj = new UrappcreateteaminfoOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
