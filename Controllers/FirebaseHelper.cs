﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class Result
    {
        public string error { get; set; }
        public string message_id { get; set; }
    }

    public class ResultDetailsFCM
    {
        public long multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
        public int canonical_ids { get; set; }
        public List<Result> results { get; set; }
    }
    public class FirebaseHelper {
        // title = PushNotification Title
        // message = PushNotification Message/Body
        // dataObject = Extra data which you want to send with notification
        // token = Single token
        public static ResultDetailsFCM SendNotification(String title, String message, String token, object dataObject = null)
        {
            List<string> tokens = new List<string>();
            tokens.Add(token);

           var result = SendNotification(title, message, tokens, dataObject);
           return result;
        }

        // tokens = List of tokens
        public static ResultDetailsFCM SendNotification(String title, String message1, List<String> tokens, object dataObject = null)
        {
            ResultDetailsFCM result = null;
            try {
                // Add FirebaseServerKey, FirebaseSenderId and ServerUrl on web config file
                string firebaseServerKey = WebConfigurationManager.AppSettings["FirebaseServerKey"];
                string senderId = WebConfigurationManager.AppSettings["FirebaseSenderId"];

                // Add logo in project images folder for show the logo on notification, you can also update
                // location of png file
               // var icon = 'site_url' + "/images/logo.png";

                var response = "";

                string[] message = message1.Split('~');
                string messagetext; 
                string iuidText =string.Empty;
                if (message.Count() > 0)
                {
                    messagetext = message[0];
                    iuidText = message[1];
                }
                else
                {
                    messagetext = message1;
                }

                //foreach(var token in tokens)
                //{

                    WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                    //Change to Push notification for displaying 
                      //notification = new
                      //  {
                      //      body = message1,
                      //      title,
                      //      // icon,
                      //      sound = "default",
                      //      sipid = "01",
                      //      priority = 10
                      //  },
                    


                    //call message notification
                    var data1 = new
                    {
                        registration_ids = tokens,
                        //to = token,
                        priority = 10,
                        message = new
                        {
                            //body = message1,
                            body = messagetext,
                            title,
                            // icon,
                            sound = "default",
                            sipid = "01",
                            priority = 10,
                            iuid = iuidText

                        },
                        data = new
                        {
                            sipid = "01",
                            priority = 10,
                            iuid = iuidText
                        },
                        time_to_live = 3000 
                        //data1 = dataObject
                    };

                    var serializer = new JavaScriptSerializer();
                    var json = serializer.Serialize(data1);

                    Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                    tRequest.Method = "post";
                    tRequest.Headers.Add(string.Format("Authorization: key={0}", firebaseServerKey));
                    tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                    //tRequest.ContentLength = byteArray.Length;

                    tRequest.ContentType = "application/json";

                    using(Stream dataStream = tRequest.GetRequestStream())
                    {
                        dataStream.Write(byteArray, 0, byteArray.Length);

                        using(WebResponse tResponse = tRequest.GetResponse())
                        {
                            using(Stream dataStreamResponse = tResponse.GetResponseStream())
                            {
                                using(StreamReader tReader = new StreamReader(dataStreamResponse))
                                {
                                    String sResponseFromServer = tReader.ReadToEnd();
                                  //  string result = JObject.Parse(sResponseFromServer);
                                    result = JsonConvert.DeserializeObject<ResultDetailsFCM>(sResponseFromServer);
                                    //return response += string.Format("{0}\n", sResponseFromServer);
                                    return result;
                                }
                            }
                        }
                    }
                //}
            } catch (Exception ex) {
               
            }
            return result;
        }


        public static ResultDetailsFCM SendSilentNotification(String title, String message1, List<String> tokens, string SIP_ID, object dataObject = null)
        {
            ResultDetailsFCM result = null;
            try
            {
                // Add FirebaseServerKey, FirebaseSenderId and ServerUrl on web config file
                string firebaseServerKey = WebConfigurationManager.AppSettings["FirebaseServerKey"];
                string senderId = WebConfigurationManager.AppSettings["FirebaseSenderId"];

                // Add logo in project images folder for show the logo on notification, you can also update
                // location of png file
                // var icon = 'site_url' + "/images/logo.png";

                var response = "";



                //foreach(var token in tokens)
                //{

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                //Change to Push notification for displaying 
                //notification = new
                //  {
                //      body = message1,
                //      title,
                //      // icon,
                //      sound = "default",
                //      sipid = "01",
                //      priority = 10
                //  },

                //List<string> tokens = new List<string>();
                //tokens.Add(token);


                //call message notification
                var data1 = new
                {
                    registration_ids = tokens,
                    //to = token,
                    priority = 10,
                    message = new
                    {
                        body = message1,
                        title,
                        // icon,
                      //  sound = "default",
                        sipid = SIP_ID,
                        priority = 10
                    },
                    data = new
                    {
                        sipid = SIP_ID,
                        priority = 10
                    },
                    time_to_live = 3000
                    //data1 = dataObject
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data1);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Method = "post";
                tRequest.Headers.Add(string.Format("Authorization: key={0}", firebaseServerKey));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                //tRequest.ContentLength = byteArray.Length;

                tRequest.ContentType = "application/json";

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                //  string result = JObject.Parse(sResponseFromServer);
                                result = JsonConvert.DeserializeObject<ResultDetailsFCM>(sResponseFromServer);
                                //return response += string.Format("{0}\n", sResponseFromServer);
                                return result;
                            }
                        }
                    }
                }
                //}
            }
            catch (Exception ex)
            {

            }
            return result;
        }
    }
}