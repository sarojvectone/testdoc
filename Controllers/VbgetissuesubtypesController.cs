using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class VbgetissuesubtypesController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class VbgetissuesubtypesInput
        {
			public int IssueTypeId { get; set; }  	                   	    
        }
        public class VbgetissuesubtypesOutput
        {
			public int IssueTypeId { get; set; }
			public string IssueTypeName { get; set; }
			public int IssueSubTypeId { get; set; }
			public string IssueSubTypeName { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, VbgetissuesubtypesInput req)
        {
            List<VbgetissuesubtypesOutput> OutputList = new List<VbgetissuesubtypesOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "vb_get_issueSubtypes";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@IssueTypeId = req.IssueTypeId 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new VbgetissuesubtypesOutput()
                        {
							IssueTypeId = r.IssueTypeId == null ? 0 : r.IssueTypeId,
							IssueTypeName = r.IssueTypeName,
							IssueSubTypeId = r.IssueSubTypeId == null ? 0 : r.IssueSubTypeId,
							IssueSubTypeName = r.IssueSubTypeName,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        VbgetissuesubtypesOutput outputobj = new VbgetissuesubtypesOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                VbgetissuesubtypesOutput outputobj = new VbgetissuesubtypesOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
