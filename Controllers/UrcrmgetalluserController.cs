using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{

    public class UrcrmgetalluserController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrcrmgetalluserInput
        {
        }
        public class UrcrmgetalluserOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
            public int? userid { get; set; }
            public string username { get; set; }
            public string password { get; set; }
            public int? user_role { get; set; }
            public string status { get; set; }
            public DateTime? createdate { get; set; }
            public string createdate_str { get { return createdate != null ? Convert.ToDateTime(createdate).ToString("dd-MM-yyyy") : ""; } }
            public string email { get; set; }
            public string role { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrcrmgetalluserInput req)
        {
            List<UrcrmgetalluserOutput> OutputList = new List<UrcrmgetalluserOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_crm_get_all_user";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrcrmgetalluserOutput()
                        {
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg,
                            userid = r.userid,
                            username = r.username,
                            password = r.password,
                            user_role = r.user_role,
                            status = r.status,
                            createdate = r.createdate,
                            email = r.email,
                            role = r.role
                        }));
                    }
                    else
                    {
                        UrcrmgetalluserOutput outputobj = new UrcrmgetalluserOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrcrmgetalluserOutput outputobj = new UrcrmgetalluserOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
