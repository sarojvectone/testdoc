using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountgetuserextensionController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountgetuserextensionInput
        {
			public int company_id { get; set; }
			public int plan_id { get; set; }
			public int addon_id { get; set; }
			public int addon_type { get; set; }
			public int search_type { get; set; }  	                   	    
        }
        public class UrmyaccountgetuserextensionOutput
        {
            public int? Hubspot_contactid { get; set; }
			public string extension { get; set; }
			public string name { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountgetuserextensionInput req)
        {
            List<UrmyaccountgetuserextensionOutput> OutputList = new List<UrmyaccountgetuserextensionOutput>();
			Log.Info("Input : UrmyaccountgetuserextensionController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_get_user_extension";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@plan_id = req.plan_id,
								@addon_id = req.addon_id,
								@addon_type = req.addon_type,
								@search_type = req.search_type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountgetuserextensionOutput()
                        {
                            extension = r.Extension_Number,
							name = r.name,
                            Hubspot_contactid = r.Hubspot_contactid == null ? 0 : r.Hubspot_contactid,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountgetuserextensionOutput outputobj = new UrmyaccountgetuserextensionOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountgetuserextensionOutput outputobj = new UrmyaccountgetuserextensionOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
