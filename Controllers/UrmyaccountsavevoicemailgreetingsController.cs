using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccountsavevoicemailgreetingsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountsavevoicemailgreetingsInput
        {
			public string extension { get; set; }
			public int domain_id { get; set; }
			public string voicemail_type { get; set; }
			public string content { get; set; }
			public string audio_file { get; set; }
			public string custom_type { get; set; }
			public int process_type { get; set; }
			public int id { get; set; }
            public int hour_type { get; set; }      	    
        }
        public class UrmyaccountsavevoicemailgreetingsOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountsavevoicemailgreetingsInput req)
        {
            List<UrmyaccountsavevoicemailgreetingsOutput> OutputList = new List<UrmyaccountsavevoicemailgreetingsOutput>();
			Log.Info("Input : UrmyaccountsavevoicemailgreetingsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_save_voicemail_greetings";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@extension = req.extension,
								@domain_id = req.domain_id,
								@voicemail_type = req.voicemail_type,
								@content = req.content,
								@audio_file = req.audio_file,
								@custom_type = req.custom_type,
								@process_type = req.process_type,
								@id = req.id, 
			                    @hour_type = req.hour_type                                   
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountsavevoicemailgreetingsOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountsavevoicemailgreetingsOutput outputobj = new UrmyaccountsavevoicemailgreetingsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountsavevoicemailgreetingsOutput outputobj = new UrmyaccountsavevoicemailgreetingsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
