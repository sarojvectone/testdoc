using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrinsertlinkaccounttoalexaController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrinsertlinkaccounttoalexaInput
        {
			public int? company_id { get; set; }
			public string extension { get; set; }
            public string alexa_token { get; set; }
			public int? status { get; set; }
            public string called_by { get; set; }  	                   	    
        }
        public class UrinsertlinkaccounttoalexaOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrinsertlinkaccounttoalexaInput req)
        {
            List<UrinsertlinkaccounttoalexaOutput> OutputList = new List<UrinsertlinkaccounttoalexaOutput>();
			Log.Info("Input : UrinsertlinkaccounttoalexaController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_insert_link_account_to_alexa";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@extension = req.extension,
								@alexa_token = req.alexa_token,
								@status = req.status,
								@called_by = req.called_by 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrinsertlinkaccounttoalexaOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrinsertlinkaccounttoalexaOutput outputobj = new UrinsertlinkaccounttoalexaOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrinsertlinkaccounttoalexaOutput outputobj = new UrinsertlinkaccounttoalexaOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
