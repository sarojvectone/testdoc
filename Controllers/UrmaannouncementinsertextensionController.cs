using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaannouncementinsertextensionController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaannouncementinsertextensionInput
        {
			public int company_id { get; set; }
			public int domain_id { get; set; }
			public string extn_name { get; set; }
			public int extn_number { get; set; }
			public string email { get; set; }  	                   	    
        }
        public class UrmaannouncementinsertextensionOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public int? ar_id  { get; set; }	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaannouncementinsertextensionInput req)
        {
            List<UrmaannouncementinsertextensionOutput> OutputList = new List<UrmaannouncementinsertextensionOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_announcement_insert_extension";
                            
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@domain_id = req.domain_id,
								@extn_name = req.extn_name,
								@extn_number = req.extn_number,
								@email = req.email 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmaannouncementinsertextensionOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg,
                            ar_id  = r.ar_id
                        }));
                    }
                    else
                    {
                        UrmaannouncementinsertextensionOutput outputobj = new UrmaannouncementinsertextensionOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmaannouncementinsertextensionOutput outputobj = new UrmaannouncementinsertextensionOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
