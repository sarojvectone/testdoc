using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountcallqueuesavecustomhoursController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcallqueuesavecustomhoursInput
        {
			public int Company_id { get; set; }
			public int call_queue_id { get; set; }
			public string custom_hour { get; set; }
			public string create_by { get; set; }  	                   	    
        }
        public class UrmyaccountcallqueuesavecustomhoursOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcallqueuesavecustomhoursInput req)
        {
            List<UrmyaccountcallqueuesavecustomhoursOutput> OutputList = new List<UrmyaccountcallqueuesavecustomhoursOutput>();
			Log.Info("Input : UrmyaccountcallqueuesavecustomhoursController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_call_queue_save_custom_hours";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_id = req.Company_id,
								@call_queue_id = req.call_queue_id,
								@custom_hour = req.custom_hour,
								@create_by = req.create_by 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcallqueuesavecustomhoursOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcallqueuesavecustomhoursOutput outputobj = new UrmyaccountcallqueuesavecustomhoursOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcallqueuesavecustomhoursOutput outputobj = new UrmyaccountcallqueuesavecustomhoursOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
