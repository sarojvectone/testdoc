using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class IvrsitegetgenerickeypressesinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class IvrsitegetgenerickeypressesinfoInput
        {
			public int ivr_id { get; set; }  	                   	    
        }
        public class IvrsitegetgenerickeypressesinfoOutput
        {
			public int? company_id { get; set; }
			public int? site_id { get; set; }
			public int? ivr_id { get; set; }
			public int? press_type { get; set; }
			public int? press_hash { get; set; }
			public int? press_star { get; set; }
			public int? caller_noaction { get; set; }
			public int? caller_connect_ext { get; set; }
			public DateTime? create_date { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, IvrsitegetgenerickeypressesinfoInput req)
        {
            List<IvrsitegetgenerickeypressesinfoOutput> OutputList = new List<IvrsitegetgenerickeypressesinfoOutput>();
			Log.Info("Input : IvrsitegetgenerickeypressesinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ivr_site_get_generic_key_presses_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@ivr_id = req.ivr_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new IvrsitegetgenerickeypressesinfoOutput()
                        {
							company_id = r.company_id == null ? 0 : r.company_id,
							site_id = r.site_id == null ? 0 : r.site_id,
							ivr_id = r.ivr_id == null ? 0 : r.ivr_id,
							press_type = r.press_type == null ? 0 : r.press_type,
							press_hash = r.press_hash == null ? 0 : r.press_hash,
							press_star = r.press_star == null ? 0 : r.press_star,
							caller_noaction = r.caller_noaction == null ? 0 : r.caller_noaction,
							caller_connect_ext = r.caller_connect_ext == null ? 0 : r.caller_connect_ext,
							create_date = r.create_date == null ? null : r.create_date,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        IvrsitegetgenerickeypressesinfoOutput outputobj = new IvrsitegetgenerickeypressesinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                IvrsitegetgenerickeypressesinfoOutput outputobj = new IvrsitegetgenerickeypressesinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
