using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using System.Security.Cryptography.X509Certificates;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class urpushnotificationnewController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public class PushNotificationInput
        {
            public string sender { get; set; }
            public string receiver { get; set; }
            public string message { get; set; }
            public bool isvoip { get; set; }
            public bool isLive { get; set; }
            public string iuid { get; set; }
        }

        public class PushNotificationOutput : Output
        {

        }

        public class Output
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public HttpResponseMessage Get(string origin, string target, bool isvoip, string media, int iscancel, bool isLive,string iuid)
        {
            Log.Info("urpushnotificationnew");
            PushNotificationOutput output = new PushNotificationOutput();
            try
            {
                Log.Info("Input : origin=" + origin + "&target=" + target + "&isvoip=" + isvoip + "&media=" + media + "&iscancel=" + iscancel + "&iuid=" + iuid);
                //Log.Info("sender : " + origin + " & receiver : " + target);
                bool iscancelled = false;
                if (iscancel != 0)
                    iscancelled = true;
                string ToNumber = target; //GetMobileNo(target);
                if (ToNumber != "")
                {
                    Log.Info("To mobile no : " + ToNumber);
                    string FromNumber = origin.IndexOf("_") > -1 ? origin.Split(new string[] { "_" }, StringSplitOptions.None)[1] : origin;//GetName(origin);origin; //GetMobileNo(origin);
                    Log.Info("From mobile no : " + FromNumber);
                    //Device Token
                    String deviceToken = GetDeviceToken(ToNumber, isvoip);
                    if (deviceToken == "")
                    {
                        output.errcode = -1;
                        output.errmsg = "device not found";
                        Log.Info("device not found");
                    }
                    else
                    {
                        Log.Info("deviceToken : " + deviceToken);
                        string name;
                        if (origin.IndexOf("_") > -1)
                            name = GetName(origin);//target.Split(new string[] { "_" }, StringSplitOptions.None)[1];//GetName(origin);
                        else
                            name = origin;
                        //name = origin;
                        //if (name.Length == 0)
                        //    name = origin;
                        Log.Info(string.Format("Extension : {0}", name));

                        //Todo : Anees added to get Contact name
                        //FromNumber = GetName(FromNumber);
                        FromNumber = String.IsNullOrEmpty(FromNumber) ? name : FromNumber;
                        String message = (!iscancelled) ? string.Format(APNSHelper.push_message, name) : "Cancelled";
                        //String message = string.Format(APNSHelper.push_message, origin);
                        Log.Info("message : " + message);
                        string p12File = "";
                        string p12FilePwd = "";
                        if (isvoip)
                        {
                            p12File = APNSHelper.voip_key_file;
                            p12FilePwd = APNSHelper.voip_key_pwd;
                        }
                        else
                        {
                            p12File = APNSHelper.message_key_file;
                            p12FilePwd = APNSHelper.message_key_pwd;
                        }
                        ApnsConfiguration config = null;
                        //TODO : Temp need to remove || target.Contains("472") || target.Contains("354")
                        //if (APNSHelper.push_hostname.ToLower() == "gateway.sandbox.push.apple.com" || target.Contains("472") || target.Contains("354") || origin.Contains("472") || origin.Contains("354"))
                        if (isLive)
                        {
                            APNSHelper.push_hostname = APNSHelper.push_hostname.ToLower();
                        }
                        else
                        {
                            APNSHelper.push_hostname = APNSHelper.push_staging_hostname.ToLower();
                        }

                        if (!isLive)
                        { 
                            config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, p12File, p12FilePwd, !isvoip);
                        }
                        else
                        {
                            config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, p12File, p12FilePwd, !isvoip);
                        }

                        string result = GetPushNotification(deviceToken, p12File, message,name,iuid,media,deviceToken,FromNumber);
                        output.errcode = 0;
                        output.errmsg = result;
                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = "mobile no not found";
                    Log.Info(output.errmsg);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
        public HttpResponseMessage Post(PushNotificationInput input)
        {
            Log.Info("urpushnotification");
            PushNotificationOutput output = new PushNotificationOutput();
            if (input == null)
            {
                output.errcode = -1;
                output.errmsg = "input details not found";
            }
            else
            {
                try
                {
                    Log.Info("sender : " + input.sender + " & receiver : " + input.receiver + " & message : " + input.message);
                    string ToNumber = GetMobileNo(input.receiver);
                    if (ToNumber != "")
                    {
                        Log.Info("To mobile no : " + ToNumber);
                        //Device Token
                        String deviceToken = GetDeviceToken(ToNumber, input.isvoip);
                        if (deviceToken == "")
                        {
                            output.errcode = -1;
                            output.errmsg = "device not found";
                            Log.Info("device not found");
                        }
                        else
                        {
                            Log.Info("deviceToken : " + deviceToken);
                            String message = input.message.Trim().Length == 0 ? string.Format(APNSHelper.push_message, input.sender) : input.message;
                            Log.Info("message : " + message);

                            string[] Names = input.sender.Split('_');//GetName(input.sender);
                            string Name = Names[1];//GetName(Names[1]);
                            Log.Info("Name : " + Name);
                            string FromNumber = GetMobileNo(input.sender);
                            Log.Info("MobileNo : " + FromNumber);
                            string p12File = "";
                            string p12FilePwd = "";
                            if (input.isvoip)
                            {
                                p12File = APNSHelper.voip_key_file;
                                p12FilePwd = APNSHelper.voip_key_pwd;
                            }
                            else
                            {
                                p12File = APNSHelper.message_key_file;
                                p12FilePwd = APNSHelper.message_key_pwd;
                            }
                            ApnsConfiguration config = null;
                            if (APNSHelper.push_hostname.ToLower() == "gateway.sandbox.push.apple.com")
                                config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, p12File, p12FilePwd, !input.isvoip);
                            else
                                config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, p12File, p12FilePwd, !input.isvoip);
                            // Create a new broker
                            var apnsBroker = new ApnsServiceBroker(config);
                            // Wire up events
                            apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                            {
                                aggregateEx.Handle(ex =>
                                {
                                    output.errcode = -1;
                                    // See what kind of exception it was to further diagnose
                                    if (ex is ApnsNotificationException)
                                    {
                                        var notificationException = (ApnsNotificationException)ex;
                                        var apnsNotification = notificationException.Notification;
                                        var statusCode = notificationException.ErrorStatusCode;
                                        output.errmsg = String.Format("Apple Notification Failed: ID={0}, Code={1}", apnsNotification.Identifier, statusCode);
                                        Log.Info(output.errmsg);
                                    }
                                    else
                                    {
                                        output.errmsg = String.Format("Apple Notification Failed for some unknown reason : {0}", ex.InnerException);
                                        Log.Info(output.errmsg);
                                    }
                                    return true;
                                });
                            };

                            apnsBroker.OnNotificationSucceeded += (notification) =>
                            {
                                output.errcode = 0;
                                output.errmsg = "Notification Sent!";
                                Log.Info(output.errmsg);
                            };

                            apnsBroker.Start();
                            apnsBroker.QueueNotification(new ApnsNotification
                            {
                                DeviceToken = deviceToken,
                               // Expiration = DateTime.UtcNow,                                                           
                                Payload = JObject.Parse("{\"aps\":{\"alert\":\"" + message + " @" + Name + "(" + FromNumber + ")" + "\",\"badge\":\"" + APNSHelper.push_badge + "\",\"name\":\"" + Name + "\",\"mobileno\":\"" + FromNumber + "\",\"sound\":\"" + APNSHelper.push_sound + "\"}}")
                            });

                            Log.Info("Queue Input : " + JsonConvert.SerializeObject(apnsBroker));
                            apnsBroker.Stop();
                        }
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = "mobile no not found";
                        Log.Info(output.errmsg);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
        private string GetDeviceToken(string deviceId, bool isvoip)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["apns_provider"].ConnectionString))
                {
                    conn.Open();
                    var sp = "usp_getdevicetoken";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @DeviceID = deviceId
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        if (isvoip)
                            return result.ElementAt(0).voip_token;
                        else
                            return result.ElementAt(0).message_token;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return "";
        }

        private string GetName(string accountid)
        {
            string custName = "";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringappapi"].ConnectionString))
                {
                    var sp = "ur_ma_push_notification_get_username";

                    conn.Open();

                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @mobileno = accountid.Split(new string[] { "_" }, StringSplitOptions.None)[1],
                         @domain_id = accountid.Split(new string[] { "_" }, StringSplitOptions.None)[0]
                     },
                     commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0 && result.ElementAt(0).errcode != null && result.ElementAt(0).errcode == 0)
                    {
                        custName = result.ElementAt(0).name;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return custName;
        }

        private string GetMobileNo(string accountid)
        {
            string custName = "";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MVNO.CTP"].ConnectionString))
                {
                    var sp = "ctp_get_mobileno";

                    conn.Open();

                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @accountid = accountid
                     },
                     commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        custName = result.ElementAt(0).mobileno;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return custName;
        }

        public string GetPushNotification(string devicetoken, string certpath, string message, string name, string iuid,
            string media, string deviceToken, string FromNumber)
        {
            var url = string.Format("https://api.push.apple.com/3/device/{0}",devicetoken);
           var certData = System.IO.File.ReadAllBytes(certpath);
           var certificate = new X509Certificate2(certData, "123456", X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);

           string result;
           
           //using (var httpHandler = new HttpClientHandler { SslProtocols = System.Security.Authentication.SslProtocols.Tls12 })
           using (var httpHandler = new HttpClientHandler())
            {
                ServicePointManager.Expect100Continue = true;
               
                
     

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
               | SecurityProtocolType.Tls11
               | SecurityProtocolType.Tls12
               | SecurityProtocolType.Ssl3;


                httpHandler.ClientCertificates.Add(certificate);

                using (var httpClient = new HttpClient(httpHandler, true))
               
                 using (var request = new HttpRequestMessage(HttpMethod.Post, url))
                {
                    request.Headers.Add("apns-id", Guid.NewGuid().ToString("D"));
                   // request.Headers.Add("apns-push-type", "alert");
                   // request.Headers.Add("apns-priority", "10");
                  //  request.Headers.Add("apns-topic", topic);
                   request.Headers.Add("apns-expiration", "0");
                  // request.Headers.Add("KeepAlive", "false");
                    var payload = new Newtonsoft.Json.Linq.JObject{
                        {
                            "aps", new Newtonsoft.Json.Linq.JObject
                            {
                                { "content-available", 1 },
                                {"name",name},
                                {"iuid",iuid},
                                {"media",media},
                                {"UUID",deviceToken},
                                { "alert", message }, 
                                {"badge",APNSHelper.push_badge},
                                {"phoneno",FromNumber},
                                {"sound",APNSHelper.push_sound},
                            }
                        }
                    };
                    
                    request.Content = new StringContent(payload.ToString());
                    

                    //request.Version = new Version(2, 0);
                    try 
                    {
                        using (var httpResponseMessage = httpClient.SendAsync(request).GetAwaiter().GetResult())
                        {
                           var  responseContent = httpResponseMessage.Content.ReadAsStringAsync();
                           result = responseContent.ToString();
                            
                        }
                    }
                    catch (Exception e)
                    {
                        result = "Exception:"+e.InnerException.ToString();
                    }
                }
            }

           return result;
        }
    }
}
