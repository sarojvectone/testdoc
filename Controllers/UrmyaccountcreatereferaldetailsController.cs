using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountcreatereferaldetailsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcreatereferaldetailsInput
        {
			public int Company_Id { get; set; }
			public string name { get; set; }
			public string Company_Name { get; set; }
			public string email { get; set; }
			public int No_of_employee { get; set; }
			public string Referral_link { get; set; }
            public string business_phoneno { get; set; }      	    
        }
        public class UrmyaccountcreatereferaldetailsOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcreatereferaldetailsInput req)
        {
            List<UrmyaccountcreatereferaldetailsOutput> OutputList = new List<UrmyaccountcreatereferaldetailsOutput>();
			Log.Info("Input : UrmyaccountcreatereferaldetailsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_create_referal_details";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_Id = req.Company_Id,
								@name = req.name,
								@Company_Name = req.Company_Name,
								@email = req.email,
								@No_of_employee = req.No_of_employee,
								@Referral_link = req.Referral_link,
                                @business_phoneno = req.business_phoneno 
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcreatereferaldetailsOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcreatereferaldetailsOutput outputobj = new UrmyaccountcreatereferaldetailsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcreatereferaldetailsOutput outputobj = new UrmyaccountcreatereferaldetailsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
