﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebvoicemsgngetextentionaudiofileController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebvoicemsgngetextentionaudiofileInput
        {
            public int Company_id { get; set; }
            public int extension { get; set; }
            public int? Hour_type { get; set; }
        }
        public class UrmyaccwebvoicemsgngetextentionaudiofileOutput
        {
            public int Greeting_by { get; set; }
            public int Greeting_type { get; set; }
            public int Language { get; set; }
            public string Audio_file { get; set; }
            public int Active { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebvoicemsgngetextentionaudiofileInput req)
        {
            List<UrmyaccwebvoicemsgngetextentionaudiofileOutput> OutputList = new List<UrmyaccwebvoicemsgngetextentionaudiofileOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_Voice_Msgn_get_extention_audio_file";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @Company_id = req.Company_id,
                                @extension = req.extension,
                                @Hour_type = req.Hour_type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebvoicemsgngetextentionaudiofileOutput()
                        {
                            Greeting_by = r.Greeting_by,
                            Greeting_type = r.Greeting_type,
                            Language  =r.Language,
                            Audio_file = r.Audio_file,
                            Active = r.Active,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebvoicemsgngetextentionaudiofileOutput outputobj = new UrmyaccwebvoicemsgngetextentionaudiofileOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebvoicemsgngetextentionaudiofileOutput outputobj = new UrmyaccwebvoicemsgngetextentionaudiofileOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
