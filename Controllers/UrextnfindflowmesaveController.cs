using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrextnfindflowmesaveController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrextnfindflowmesaveInput
        {
			public int domain_id { get; set; }
			public int extension { get; set; }
			public string name { get; set; }
			public string mobileno { get; set; }
			public DateTime? schedule_from { get; set; }
			public DateTime? schedule_to { get; set; }
			public int status { get; set; }
			public int id { get; set; }
			public int type { get; set; }  	                   	    
        }
        public class UrextnfindflowmesaveOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrextnfindflowmesaveInput req)
        {
            List<UrextnfindflowmesaveOutput> OutputList = new List<UrextnfindflowmesaveOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_extn_find_flow_me_save";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id,
								@extension = req.extension,
								@name = req.name,
								@mobileno = req.mobileno,
								@schedule_from = req.schedule_from == null ? null : req.schedule_from,
								@schedule_to = req.schedule_to == null ? null : req.schedule_to,
								@status = req.status,
								@id = req.id,
								@type = req.type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrextnfindflowmesaveOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrextnfindflowmesaveOutput outputobj = new UrextnfindflowmesaveOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrextnfindflowmesaveOutput outputobj = new UrextnfindflowmesaveOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
