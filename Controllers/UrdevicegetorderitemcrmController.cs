using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdevicegetorderitemcrmController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdevicegetorderitemcrmInput
        {
			public int orderid { get; set; }  	                   	    
        }
        public class UrdevicegetorderitemcrmOutput
        {
			public int itemid { get; set; }
			public string category { get; set; }
			public string phone_id { get; set; }
			public string phone_model { get; set; }
			public string phone_desc { get; set; }
			public double? price { get; set; }
			public double? weight { get; set; }
			public string ean_no { get; set; }
			public int quantity { get; set; }
			public int status { get; set; }
			public int inventory_id { get; set; }
			public double? discount_price { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdevicegetorderitemcrmInput req)
        {
            List<UrdevicegetorderitemcrmOutput> OutputList = new List<UrdevicegetorderitemcrmOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_get_order_item_crm";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@orderid = req.orderid 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrdevicegetorderitemcrmOutput()
                        {
							itemid = r.itemid == null ? 0 : r.itemid,
							category = r.category,
							phone_id = r.phone_id,
							phone_model = r.phone_model,
							phone_desc = r.phone_desc,
							price = r.price == null ? 0.0D : r.price,
							weight = r.weight == null ? 0.0D : r.weight,
							ean_no = r.ean_no,
							quantity = r.quantity == null ? 0 : r.quantity,
							status = r.status == null ? 0 : r.status,
							inventory_id = r.inventory_id == null ? 0 : r.inventory_id,
							discount_price = r.discount_price == null ? 0.0D : r.discount_price,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrdevicegetorderitemcrmOutput outputobj = new UrdevicegetorderitemcrmOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrdevicegetorderitemcrmOutput outputobj = new UrdevicegetorderitemcrmOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
