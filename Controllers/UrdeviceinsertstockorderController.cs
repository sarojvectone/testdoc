using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdeviceinsertstockorderController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();


        public class UrdeviceinsertstockorderInput
        {
            //public int company_id { get; set; }
            //public int order_extension { get; set; }
            //public string referenceid { get; set; }
            //public double? phone_charge { get; set; }
            //public double? discount { get; set; }
            //public double? tax_fee { get; set; }
            //public double? charge { get; set; }
            //public string stock_item { get; set; }
            //public double? tax_per { get; set; }
            //public double? number_charge { get; set; }   


            public int? company_id { get; set; }
            public string referenceid { get; set; }
            public double? phone_charge { get; set; }
            public double? discount { get; set; }
            public double? tax_fee { get; set; }
            public double? charge { get; set; }
            public int? order_extension { get; set; }
            public string stock_item { get; set; }
            public double? number_charge { get; set; }
            public double? tax_per { get; set; }
            public int? is_prepaid_charging { get; set; }
            public string pp_Pay_Reference { get; set; }
            public double? pp_pay_amount { get; set; }
            public string pp_Paystatus { get; set; }
            public double pp_tax_per { get; set; }
            public int? pp_tax_fee { get; set; }
            public double? pp_total_charge { get; set; }
            public double? pp_prorata_adjustment { get; set; }
            public string pp_last4digit_cc { get; set; } 
        }        
        public class UrdeviceinsertstockorderOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public string  domain_name { get; set; }
            public string extn_pwd { get; set; }   
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdeviceinsertstockorderInput req)
        {
            List<UrdeviceinsertstockorderOutput> OutputList = new List<UrdeviceinsertstockorderOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_insert_stock_order";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                //@company_id = req.company_id,
                                //@order_extension = req.order_extension,
                                //@referenceid = req.referenceid,
                                //@phone_charge = req.phone_charge,
                                //@discount = req.discount,
                                //@tax_fee = req.tax_fee,
                                //@charge = req.charge,
                                //@stock_item = req.stock_item,
                                //@tax_per = req.tax_per,
                                //@number_charge = req.number_charge   
 
                                @company_id = req.company_id,
                                @referenceid = req.referenceid,
                                @phone_charge = req.phone_charge,
                                @discount = req.discount,
                                @tax_fee = req.tax_fee,
                                @charge = req.charge,
                                @order_extension = req.order_extension,
                                @stock_item = req.stock_item,
                                @number_charge = req.number_charge,
                                @tax_per = req.tax_per,
                                @is_prepaid_charging = req.is_prepaid_charging,
                                @pp_Pay_Reference = req.pp_Pay_Reference,
                                @pp_pay_amount = req.pp_pay_amount,
                                @pp_Paystatus = req.pp_Paystatus,
                                @pp_tax_per = req.pp_tax_per,
                                @pp_tax_fee = req.pp_tax_fee,
                                @pp_total_charge = req.pp_total_charge,
                                @pp_prorata_adjustment = req.pp_prorata_adjustment,
                                @pp_last4digit_cc = req.pp_last4digit_cc 	                           
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrdeviceinsertstockorderOutput()
                        {
                            domain_name = r.domain_name,
                            extn_pwd = r.extn_pwd,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrdeviceinsertstockorderOutput outputobj = new UrdeviceinsertstockorderOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrdeviceinsertstockorderOutput outputobj = new UrdeviceinsertstockorderOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
