using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrDirectoryUpdateController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrDirectoryUpdateInput
        {
            public int CustomerId { get; set; }
			public string extension { get; set; } 
        }
        public class UrDirectoryUpdateOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrDirectoryUpdateInput req)
        {
            Log.Info("Input :UrDirectoryUpdateController" + JsonConvert.SerializeObject(req));

            List<UrDirectoryUpdateOutput> OutputList = new List<UrDirectoryUpdateOutput>();
            try
            {
               string result =  unifiedringmyaccountwebapi.Helpers.Deskphone_Configuration.UpdateDirectory(req.CustomerId, req.extension);
               UrDirectoryUpdateOutput outputobj = new UrDirectoryUpdateOutput();
               outputobj.errcode = 0;
               outputobj.errmsg = result;
               OutputList.Add(outputobj);
            }
            catch (Exception ex)
            {
                UrDirectoryUpdateOutput outputobj = new UrDirectoryUpdateOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            Log.Info("Output :UrDirectoryUpdateController" + JsonConvert.SerializeObject(OutputList));
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}