using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using unifiedringmyaccountwebapi.Helpers;
using System.IO;
using Newtonsoft.Json;
using unifiedringmyaccountwebapi.Models;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebprovisionorderdeskphoneController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebprovisionorderdeskphoneInput
        {
            public string order_number { get; set; }
            public string order_item { get; set; }
            public string mac { get; set; }
            public string note { get; set; }
            public string user_login { get; set; }
            public string desk_code { get; set; }
            //public string DeskPhoneModel { get; set; }
        }
        public class UrmyaccwebprovisionorderdeskphoneOutput
        {
            public int ext { get; set; }
            public int customer_id { get; set; }
            public int company_id { get; set; }
            public int domain { get; set; }
            public string Pass { get; set; }
            public string domain_id { get; set; }
            public string domain_name { get; set; }
            public int dir_user_id { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
            public string admin_password { get; set; }
            public string Prov_URL { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebprovisionorderdeskphoneInput req)
        {
            Log.Debug("Urmyaccwebprovisionorderdeskphone");
            Log.Debug("Input : {0}", JsonConvert.SerializeObject(req));
            List<UrmyaccwebprovisionorderdeskphoneOutput> OutputList = new List<UrmyaccwebprovisionorderdeskphoneOutput>();
            try
            {
                //DirectoryInfo DirInfo;
                string DeskphoneConfig_Out = string.Empty;
                DeskPhoneModel DeskPhoneModelresult = new DeskPhoneModel();
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_provision_order_deskphone";
                    Log.Debug("sp : {0}", sp);
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @order_number = Convert.ToInt32(req.order_number),
                                @order_item = Convert.ToInt32(req.order_item),
                                @mac = req.mac,
                                @note = req.note,
                                @user_login = req.user_login,
                                @desk_code = req.desk_code
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Debug("SP_result : {0}", JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebprovisionorderdeskphoneOutput()
                        {
                            ext = r.ext == null ? 0 : r.ext,
                            customer_id = r.customer_id == null ? 0 : r.customer_id,
                            company_id = r.company_id == null ? 0 : r.company_id,
                            domain = r.domain == null ? 0 : r.domain,
                            dir_user_id = r.dir_user_id == null ? 0 : r.dir_user_id,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));

                        if (OutputList.Count != 0 && OutputList[0].errcode == 0)
                        {
                            //DirInfo = new DirectoryInfo(Path.Combine(@"{0}/{1}", AppDomain.CurrentDomain.BaseDirectory + "App_Data", GlobalConfig.FTPDeskPhoneLocal));
                            //string TemplateFilename = Path.Combine(string.Format(@"{0}/{1}", AppDomain.CurrentDomain.BaseDirectory + @"App_Data\Templates\", GlobalConfig.GrandstreamFileTemplate));

                            //Hardcore data to test - Anees
                            //req.order_number = "9326";
                            //req.order_item = "211";
                            //req.mac = "C4729567C6B0";
                            //req.user_login = "1765";
                            //req.desk_code = "Cisco";
                            //OutputList[0].customer_id = 1765;
                            //OutputList[0].ext = 733;

                            //DeskPhoneModelresult = Deskphone_Configuration.Create_DeskphoneConfig_File(OutputList[0].customer_id, req.order_number, Convert.ToString(OutputList[0].ext), req.order_item, req.mac, DirInfo, TemplateFilename, req.user_login, req.desk_code);

                            DeskPhoneModelresult = Deskphone_Configuration.Create_DeskphoneConfig_File(OutputList[0].customer_id, req.order_number, Convert.ToString(OutputList[0].ext), req.order_item, req.mac, req.user_login, req.desk_code,"Insert");

                            if (!String.IsNullOrEmpty(DeskPhoneModelresult.vm_password))
                            {
                                //OutputList[0].Pass = DeskphoneConfig_Out.Split(':')[0];
                                //OutputList[0].domain_id = DeskphoneConfig_Out.Split(':')[1];


                                OutputList[0].Pass = DeskPhoneModelresult.vm_password;
                                OutputList[0].domain_name = DeskPhoneModelresult.domain_name;
                                OutputList[0].admin_password = DeskPhoneModelresult.admin_password;
                                OutputList[0].Prov_URL = ConfigurationManager.AppSettings["ProvURL"] + DeskPhoneModelresult.SecurityID+"/";
                                //Update Mac Deskphone
                                var sp2 = "UR_Myacc_Web_update_mac_deskphone";
                                Log.Debug("sp2 : {0}", sp2);
                                var result2 = conn.Query<dynamic>(
                                        sp2, new
                                        {
                                            @dir_user_id = OutputList[0].dir_user_id,
                                            @deskphone_mac = req.mac
                                        },
                                        commandType: CommandType.StoredProcedure);
                                Log.Debug("SP2_result : {0}", JsonConvert.SerializeObject(result2));
                            }
                        }
                    }
                    else
                    {
                        UrmyaccwebprovisionorderdeskphoneOutput outputobj = new UrmyaccwebprovisionorderdeskphoneOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("General Error : {0}", ex.Message);
                UrmyaccwebprovisionorderdeskphoneOutput outputobj = new UrmyaccwebprovisionorderdeskphoneOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
