﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmagetdomainnameController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmagetdomainnameInput
        {
            public string domain_id { get; set; }
        }
        public class UrmagetdomainnameOutput
        {
            public string domain_name { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmagetdomainnameInput req)
        {
            Log.Debug("Urmagetdomainname");
            Log.Debug("Input : {0}", JsonConvert.SerializeObject(req));
            List<UrmagetdomainnameOutput> OutputList = new List<UrmagetdomainnameOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_get_domain_name";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @domain_id = req.domain_id
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Debug("SP result : {0}", JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmagetdomainnameOutput()
                        {
                            domain_name = r.domain_name,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmagetdomainnameOutput outputobj = new UrmagetdomainnameOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmagetdomainnameOutput outputobj = new UrmagetdomainnameOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
