﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using unifiedringmyaccountwebapi.Helpers;
using System.IO;
using PushSharp.Core;
using System.Text;
using System.Web.Script.Serialization;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class BICSOrderNumbersController : ApiController
    {
        public class OrderSingleNumberInput
        {
            public string product { get; set; }
            public string country { get; set; }
            public string location { get; set; }
            public string areaCode { get; set; }
            public string addressReference { get; set; }
            public List<Routing> routing { get; set; }
        }
        public class CompanyOutput2
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
            public int UserId { get; set; }
            public string Product { get; set; }
        }
        public class Output
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        public class OrderSingleNumberOutput : CommonOutput
        {
            public string number { get; set; }
            public string product { get; set; }
            public string country { get; set; }
            public string accessArea { get; set; }
            public string location { get; set; }
            public string areaCode { get; set; }
            public string orderId { get; set; }
            public string addressReference { get; set; }
            public List<Routing> routing { get; set; }
        }

        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, OrderSingleNumberInput OrderSingleNumberInput)
        {
            List<OrderSingleNumberOutput> output = new List<OrderSingleNumberOutput>();
            OrderSingleNumberOutput result = new OrderSingleNumberOutput();
           
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var httpRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["BICSAPIENDPOINT"]+"/order");
                string authenticationtoken = ConfigurationManager.AppSettings["BICSAPIKEY"];
                httpRequest.UserAgent = "Fiddler";
                httpRequest.Headers.Add("Authorization", "Bearer "+authenticationtoken);
                httpRequest.Accept = "application/json";
                httpRequest.ContentType = "application/json";
                httpRequest.Timeout = 180000;
                httpRequest.Method = "POST";
                string Bicsnumbersresult = "Error";
                using (var streamBicsWriters = new StreamWriter(httpRequest.GetRequestStream()))
                {
                    string jsonBics = new JavaScriptSerializer().Serialize(OrderSingleNumberInput);
                    streamBicsWriters.Write(jsonBics);
                    streamBicsWriters.Flush();
                    streamBicsWriters.Dispose();
                    HttpWebResponse httpBicsResponce = (HttpWebResponse)httpRequest.GetResponse();
                    Stream BicsnumbersStream = httpBicsResponce.GetResponseStream();
                    StreamReader Bicsnumbersreader = new StreamReader(BicsnumbersStream);
                    Bicsnumbersresult = Bicsnumbersreader.ReadToEnd();
                    result = JsonConvert.DeserializeObject<OrderSingleNumberOutput>(Bicsnumbersresult); 
                    BicsnumbersStream.Close();
                    Bicsnumbersreader.Close();

                }
            }
            catch (WebException ex)
            {
                Log.Error(ex.Message);
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    try
                    {
                        HttpWebResponse err = ex.Response as HttpWebResponse;
                        if (err != null)
                        {
                            string errResponse = new StreamReader(err.GetResponseStream()).ReadToEnd();
                            Log.Info("Exception : Response : " + errResponse);
                            List<BICSErrorResponse> response = JsonConvert.DeserializeObject<List<BICSErrorResponse>>(errResponse);
                            if (response != null && response.Count > 0)
                            {
                                OrderSingleNumberOutput objOrderSingleNumberOutput = new OrderSingleNumberOutput();
                                objOrderSingleNumberOutput.errcode = -1;
                                objOrderSingleNumberOutput.errmsg = response.ElementAt(0).code + ":" + response.ElementAt(0).description;
                                output.Add(objOrderSingleNumberOutput);
                                return Request.CreateResponse(HttpStatusCode.OK, output);
                            }
                        }
                    }
                    catch (Exception innerEx)
                    {
                        Log.Error(innerEx.Message);
                        OrderSingleNumberOutput objOrderSingleNumberOutput = new OrderSingleNumberOutput();
                        objOrderSingleNumberOutput.errcode = -1;
                        objOrderSingleNumberOutput.errmsg = innerEx.Message;
                        output.Add(objOrderSingleNumberOutput);
                        return Request.CreateResponse(HttpStatusCode.OK, output);

                    }

                }
                else
                {
                    OrderSingleNumberOutput objOrderSingleNumberOutput = new OrderSingleNumberOutput();
                    objOrderSingleNumberOutput.errcode = -1;
                    objOrderSingleNumberOutput.errmsg = ex.Message;
                    output.Add(objOrderSingleNumberOutput);
                    return Request.CreateResponse(HttpStatusCode.OK, output);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }       
    }
}
