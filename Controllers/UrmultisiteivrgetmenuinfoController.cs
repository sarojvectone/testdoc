using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmultisiteivrgetmenuinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmultisiteivrgetmenuinfoInput
        {
			public int ivr_id { get; set; }
			public int siteid { get; set; }  	                   	    
        }
        public class UrmultisiteivrgetmenuinfoOutput
        {
			public string site_name { get; set; }
			public string menu_name { get; set; }
			public int ivr_id { get; set; }
			public string langauge { get; set; }
			public string lang_code { get; set; }
			public string extension_no { get; set; }
			public string external_number { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmultisiteivrgetmenuinfoInput req)
        {
            List<UrmultisiteivrgetmenuinfoOutput> OutputList = new List<UrmultisiteivrgetmenuinfoOutput>();
			Log.Info("Input : UrmultisiteivrgetmenuinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_multisite_ivr_get_menu_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@ivr_id = req.ivr_id,
								@siteid = req.siteid 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmultisiteivrgetmenuinfoOutput()
                        {
							site_name = r.site_name,
							menu_name = r.menu_name,
							ivr_id = r.ivr_id == null ? 0 : r.ivr_id,
							langauge = r.langauge,
							lang_code = r.lang_code,
							extension_no = r.extension_no,
							external_number = r.external_number,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmultisiteivrgetmenuinfoOutput outputobj = new UrmultisiteivrgetmenuinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmultisiteivrgetmenuinfoOutput outputobj = new UrmultisiteivrgetmenuinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
