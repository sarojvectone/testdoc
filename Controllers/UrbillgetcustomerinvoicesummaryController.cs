using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrbillgetcustomerinvoicesummaryController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrbillgetcustomerinvoicesummaryInput
        {
			public int? company_id { get; set; }  
            public int? invoice_id { get; set; }
            public string Search_type { get; set; }
            public int status_type { get; set; }
           // public string status_type { get; set; }
        }
        public class UrbillgetcustomerinvoicesummaryOutput
        {
			public int invoice_id { get; set; }
			public DateTime? bill_date { get; set; }
			public string bill_duration { get; set; }
			public DateTime? bill_due_date { get; set; }
			public string reference_no { get; set; }
			public string description { get; set; }
			public double? charge { get; set; }
            public double? adjustment { get; set; }
            public double? tax_percentage { get; set; }
            public double? tax_fee { get; set; }
            public double? total_bill { get; set; }
            public double? total_paid { get; set; }
			public string pay_type { get; set; }
			public DateTime? payment_date { get; set; }
			public string payment_reference { get; set; }
			public string comp_name { get; set; }
			public string comp_regno { get; set; }
			public string comp_address { get; set; }
			public string comp_building { get; set; }
			public string comp_street { get; set; }
			public string comp_city { get; set; }
			public string comp_country { get; set; }
			public string service_plan { get; set; }
            public double? total_delivery_fee { get; set; }
            public string type { get; set; }
            public string billing_type { get; set; }
            public string status { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrbillgetcustomerinvoicesummaryInput req)
        {
            List<UrbillgetcustomerinvoicesummaryOutput> OutputList = new List<UrbillgetcustomerinvoicesummaryOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_bill_get_customer_invoice_summary";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id ,
                                @invoice_id = req.invoice_id,
                                @Search_type = req.Search_type,
                                @status_type = req.status_type
			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrbillgetcustomerinvoicesummaryOutput()
                        {
							invoice_id = r.invoice_id == null ? 0 : r.invoice_id,
							bill_date = r.bill_date == null ? null : r.bill_date,
							bill_duration = r.bill_duration,
							bill_due_date = r.bill_due_date == null ? null : r.bill_due_date,
							reference_no = r.reference_no,
							description = r.description,
							charge = r.charge,
							adjustment = r.adjustment == null ? 0.0F : r.adjustment,
							tax_percentage = r.tax_percentage == null ? 0.0F : r.tax_percentage,
							tax_fee = r.tax_fee == null ? 0.0F : r.tax_fee,
							total_bill = r.total_bill == null ? 0.0F : r.total_bill,
							total_paid = r.total_paid == null ? 0.0F : r.total_paid,
							pay_type = r.pay_type,
							payment_date = r.payment_date == null ? null : r.payment_date,
							payment_reference = r.payment_reference,
							comp_name = r.comp_name,
							comp_regno = r.comp_regno,
							comp_address = r.comp_address,
							comp_building = r.comp_building,
							comp_street = r.comp_street,
							comp_city = r.comp_city,
							comp_country = r.comp_country,
							service_plan = r.service_plan,
                            total_delivery_fee = r.total_delivery_fee == null ? 0.0F : r.total_delivery_fee,
                            type = r.type,
                            billing_type = r.billing_type,
                            status  = r.status,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg  
                        }));
                    }
                    else
                    {
                        UrbillgetcustomerinvoicesummaryOutput outputobj = new UrbillgetcustomerinvoicesummaryOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrbillgetcustomerinvoicesummaryOutput outputobj = new UrbillgetcustomerinvoicesummaryOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
