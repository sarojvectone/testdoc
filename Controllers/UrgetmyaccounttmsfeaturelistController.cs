using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrgetmyaccounttmsfeaturelistController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrgetmyaccounttmsfeaturelistInput
        {
			public int company_id { get; set; }  	                   	    
        }
        public class UrgetmyaccounttmsfeaturelistOutput
        {
			public int? feature_id { get; set; }
			public string feature_name { get; set; }
			public string category { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrgetmyaccounttmsfeaturelistInput req)
        {
            List<UrgetmyaccounttmsfeaturelistOutput> OutputList = new List<UrgetmyaccounttmsfeaturelistOutput>();
			Log.Info("Input : UrgetmyaccounttmsfeaturelistController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_get_myaccount_tms_feature_list";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrgetmyaccounttmsfeaturelistOutput()
                        {
							feature_id = r.feature_id == null ? 0 : r.feature_id,
							feature_name = r.feature_name,
							category = r.category,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrgetmyaccounttmsfeaturelistOutput outputobj = new UrgetmyaccounttmsfeaturelistOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrgetmyaccounttmsfeaturelistOutput outputobj = new UrgetmyaccounttmsfeaturelistOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
