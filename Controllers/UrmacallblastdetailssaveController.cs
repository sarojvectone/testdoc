using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmacallblastdetailssaveController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmacallblastdetailssaveInput
        {
			public int company_id { get; set; }
			public string cb_dtl_name { get; set; }
			public string file_name { get; set; }
			public string file_path { get; set; }
			public string sch_date { get; set; }
			public string extentions { get; set; }
			public string login_id { get; set; }
			public int intiate_ext { get; set; }
            public int site_id  { get; set; }            	    
        }
        public class UrmacallblastdetailssaveOutput
        {
            public int? cb_dtl_idx { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmacallblastdetailssaveInput req)
        {
            List<UrmacallblastdetailssaveOutput> OutputList = new List<UrmacallblastdetailssaveOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_call_blast_details_save";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@cb_dtl_name = req.cb_dtl_name,
								@file_name = req.file_name,
								@file_path = req.file_path,
								@sch_date = req.sch_date,
								@extentions = req.extentions,
								@login_id = req.login_id,
								@intiate_ext = req.intiate_ext,
                                @site_id = req.site_id                                   
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmacallblastdetailssaveOutput()
                        {
                            cb_dtl_idx = r.cb_dtl_idx == null ? 0 : r.cb_dtl_idx,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmacallblastdetailssaveOutput outputobj = new UrmacallblastdetailssaveOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmacallblastdetailssaveOutput outputobj = new UrmacallblastdetailssaveOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
