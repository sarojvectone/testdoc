using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebgetaudiofileController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebgetaudiofileInput
        {
			public int? Audio_Type { get; set; }
            public int? Company_Id { get; set; }
            public int site_id { get; set; }
        }
        public class UrmyaccwebgetaudiofileOutput
        {
            public int? ivr_id { get; set; }
            public string ivr_extension { get; set; }
            public string ivr_name { get; set; }
            public int? Greeting { get; set; }
            public int? Language { get; set; }
			public string Voice { get; set; }
			public string Content { get; set; }
			public string Audio_file { get; set; }
			public int? Action { get; set; }
            public int? Greeting_Play_Type { get; set; }
            public string Greeting_Custom_Type { get; set; }
            public string Extension { get; set; }
            public string Extension_Name { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebgetaudiofileInput req)
        {
            List<UrmyaccwebgetaudiofileOutput> OutputList = new List<UrmyaccwebgetaudiofileOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_Get_audio_file";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Audio_Type = req.Audio_Type,
                                @Company_Id = req.Company_Id,
                                @site_id = req.site_id
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebgetaudiofileOutput()
                        {

                            ivr_id = r.ivr_id == null ? 0 : r.ivr_id,
                            ivr_extension = r.ivr_extension,
                            ivr_name = r.ivr_name,
                            Greeting = r.Greeting == null ? 0 : r.Greeting,
                            Language = r.Language == null ? 0 : r.Language,
							Voice = r.Voice,
							Content = r.Content,
							Audio_file = r.Audio_file,
                            Action = r.Action == null ? 0 : r.Action,
                            Greeting_Play_Type = r.Greeting_Play_Type == null ? 0 : r.Greeting_Play_Type,
                            Greeting_Custom_Type = r.Greeting_Custom_Type,
                            Extension = r.Extension,
                            Extension_Name = r.Extension_Name,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebgetaudiofileOutput outputobj = new UrmyaccwebgetaudiofileOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebgetaudiofileOutput outputobj = new UrmyaccwebgetaudiofileOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
