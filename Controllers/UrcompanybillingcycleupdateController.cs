using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrcompanybillingcycleupdateController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrcompanybillingcycleupdateInput
        {
			public int company_id { get; set; }
			public int bill_cycle { get; set; }
            public string reference_no { get; set; }
            public string bundle_cycle_description { get; set; }
            public double? bundle_cycle_charge { get; set; }
            public double? adjustment_charge { get; set; }
            public double? tax_fee { get; set; }
            public int tax_per { get; set; }
            public double? total_bundle_cycle_charge { get; set; }
        }
        public class UrcompanybillingcycleupdateOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrcompanybillingcycleupdateInput req)
        {
            List<UrcompanybillingcycleupdateOutput> OutputList = new List<UrcompanybillingcycleupdateOutput>();
			Log.Info("Input : UrcompanybillingcycleupdateController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_company_billing_cycle_update";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@bill_cycle = req.bill_cycle,
                                @reference_no = req.reference_no,
                                @bundle_cycle_description = req.bundle_cycle_description,
                                @bundle_cycle_charge = req.bundle_cycle_charge,
                                @adjustment_charge = req.adjustment_charge,
                                @tax_fee = req.tax_fee,
                                @tax_per = req.tax_per,
                                @total_bundle_cycle_charge = req.total_bundle_cycle_charge
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcompanybillingcycleupdateOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrcompanybillingcycleupdateOutput outputobj = new UrcompanybillingcycleupdateOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrcompanybillingcycleupdateOutput outputobj = new UrcompanybillingcycleupdateOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
