using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UronboardinsertuserdetailsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UronboardinsertuserdetailsInput
        {
			public int company_Id { get; set; }
			public int extension { get; set; }
			public string user_details { get; set; }
			public int expiry_time { get; set; }  
	        public string completed_page { get; set; }  
         	public int status { get; set; }  
            public int processtype { get; set; }
        }
        public class UronboardinsertuserdetailsOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UronboardinsertuserdetailsInput req)
        {
            List<UronboardinsertuserdetailsOutput> OutputList = new List<UronboardinsertuserdetailsOutput>();
			Log.Info("Input : UronboardinsertuserdetailsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_onboard_insert_user_details";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_Id = req.company_Id,
								@extension = req.extension,
								@user_details = req.user_details,
								@expiry_time = req.expiry_time ,
                                @completed_page = req.completed_page,
                                @status = req.status,
                                @processtype = req.processtype
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UronboardinsertuserdetailsOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UronboardinsertuserdetailsOutput outputobj = new UronboardinsertuserdetailsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UronboardinsertuserdetailsOutput outputobj = new UronboardinsertuserdetailsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
