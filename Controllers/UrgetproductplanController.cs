using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrgetproductplanController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrgetproductplanInput
        {
			public int company_id { get; set; }
			public int product { get; set; }
			public int duration { get; set; }
            public int no_of_user { get; set; }
           	    
        }
        public class UrgetproductplanOutput
        {
			public string plan_name { get; set; }
			public string plan_user_range { get; set; }
			public double? plan_price { get; set; }
			public string plan_desc { get; set; }
			public int pc_product { get; set; }
			public int pc_plan { get; set; }
			public int pc_duration { get; set; }
			public int pc_from_users { get; set; }
			public int pc_to_users { get; set; }
			public int bundleid { get; set; }
			public string tariff_class { get; set; }
			public string product_code { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrgetproductplanInput req)
        {
            List<UrgetproductplanOutput> OutputList = new List<UrgetproductplanOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_get_product_plan";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@product = req.product,
								@duration = req.duration,
                                @no_of_user = req.no_of_user                                    
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrgetproductplanOutput()
                        {
							plan_name = r.plan_name,
							plan_user_range = r.plan_user_range,
							plan_price = r.plan_price == null ? 0.0F : r.plan_price,
							plan_desc = r.plan_desc,
							pc_product = r.pc_product == null ? 0 : r.pc_product,
							pc_plan = r.pc_plan == null ? 0 : r.pc_plan,
							pc_duration = r.pc_duration == null ? 0 : r.pc_duration,
							pc_from_users = r.pc_from_users == null ? 0 : r.pc_from_users,
							pc_to_users = r.pc_to_users == null ? 0 : r.pc_to_users,
							bundleid = r.bundleid == null ? 0 : r.bundleid,
							tariff_class = r.tariff_class,
							product_code = r.product_code,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrgetproductplanOutput outputobj = new UrgetproductplanOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrgetproductplanOutput outputobj = new UrgetproductplanOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
