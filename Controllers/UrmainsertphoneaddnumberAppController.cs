using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmainsertphoneaddnumberAppController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmainsertphoneaddnumberAppInput
        {
            public int company_id { get; set; }
            public int domain_id { get; set; }
            public List<add_number> external_number { get; set; }
            public string tax_per { get; set; }
            public Double? tax_fee { get; set; }
            public Double? total_charge { get; set; }
            public string reference_no { get; set; }
            //public string country { get; set; }  	                   	    
        }
        public class UrmainsertphoneaddnumberAppOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        } 
     
        public class add_number
        {
            public string extension { get; set; }
            public string external_number { get; set; }
            public string assign_type { get; set; }
            public string charge_price { get; set; }
            public string prorata { get; set; }
            public string number_type { get; set; }
            public string country { get; set; }
        }

        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmainsertphoneaddnumberAppInput req)
        {

            Log.Info("UrmainsertphoneaddnumberAPP Input " + JsonConvert.SerializeObject(req));
            List<UrmainsertphoneaddnumberAppOutput> OutputList = new List<UrmainsertphoneaddnumberAppOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_insert_phone_add_number";
                    string strPhoneNumber = "";
                    if (req.external_number.Count() > 0)
                    {
                        strPhoneNumber = CreateXML(req.external_number);

                        strPhoneNumber = strPhoneNumber.Substring(139, strPhoneNumber.Length - 159);

                        strPhoneNumber = "<phone_number>" + strPhoneNumber + "</phone_number>";
                    }
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id,
                                @domain_id = req.domain_id,
                                //@external_number = req.external_number,
                                @external_number = String.IsNullOrEmpty(strPhoneNumber) ? null : strPhoneNumber,
                                @tax_per = req.tax_per,
                                @tax_fee = req.tax_fee,
                                @total_charge = req.total_charge,
                                @reference_no = req.reference_no
                                //@country = req.country 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Info("UrmainsertphoneaddnumberAPP Output:- " + JsonConvert.SerializeObject(req));
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmainsertphoneaddnumberAppOutput()
                        {
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmainsertphoneaddnumberAppOutput outputobj = new UrmainsertphoneaddnumberAppOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmainsertphoneaddnumberAppOutput outputobj = new UrmainsertphoneaddnumberAppOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
        public string CreateXML(Object YourClassObject)
        {
            XmlDocument xmlDoc = new XmlDocument();   //Represents an XML document, 
            // Initializes a new instance of the XmlDocument class.          
            XmlSerializer xmlSerializer = new XmlSerializer(YourClassObject.GetType());
            // Creates a stream whose backing store is memory. 
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, YourClassObject);
                xmlStream.Position = 0;
                //Loads the XML document from the specified string.
                xmlDoc.Load(xmlStream);
                return xmlDoc.InnerXml;
            }
        }
    }

}
