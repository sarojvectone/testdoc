using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaassignunassignphoneaddnumberdtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaassignunassignphoneaddnumberdtlInput
        {
			public int company_id { get; set; }
			public int domain_id { get; set; }
			public string extension { get; set; }
			public string external_number { get; set; }
			public int assign_type { get; set; }
            public string name { get; set; }
            public int assign_type_id  { get; set; }
            public int number_Type_id  { get; set; }
        }
        public class UrmaassignunassignphoneaddnumberdtlOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaassignunassignphoneaddnumberdtlInput req)
        {
            List<UrmaassignunassignphoneaddnumberdtlOutput> OutputList = new List<UrmaassignunassignphoneaddnumberdtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_assign_unassign_phone_add_number_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@domain_id = req.domain_id,
								@extension = req.extension,
								@external_number = req.external_number,
								@assign_type = req.assign_type,
                                @name = req.name,
                                @assign_type_id = req.assign_type_id,
                                @number_Type_id = req.number_Type_id
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmaassignunassignphoneaddnumberdtlOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmaassignunassignphoneaddnumberdtlOutput outputobj = new UrmaassignunassignphoneaddnumberdtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmaassignunassignphoneaddnumberdtlOutput outputobj = new UrmaassignunassignphoneaddnumberdtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
