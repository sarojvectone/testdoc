using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmahuntgroupsgetringstypeController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmahuntgroupsgetringstypeInput
        {
        }
        public class UrmahuntgroupsgetringstypeOutput
        {
			public int ring_id { get; set; }
			public string ring_type { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Get(string id, HttpRequestMessage request, UrmahuntgroupsgetringstypeInput req)
        {
            List<UrmahuntgroupsgetringstypeOutput> OutputList = new List<UrmahuntgroupsgetringstypeOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_hunt_groups_get_rings_type";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmahuntgroupsgetringstypeOutput()
                        {
							ring_id = r.ring_id == null ? 0 : r.ring_id,
							ring_type = r.ring_type,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmahuntgroupsgetringstypeOutput outputobj = new UrmahuntgroupsgetringstypeOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmahuntgroupsgetringstypeOutput outputobj = new UrmahuntgroupsgetringstypeOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
