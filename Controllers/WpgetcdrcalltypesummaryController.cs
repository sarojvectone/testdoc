using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class WpgetcdrcalltypesummaryController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class WpgetcdrcalltypesummaryInput
        {
			public int domain_id { get; set; }
			public int type { get; set; }  	                   	    
        }
        public class WpgetcdrcalltypesummaryOutput
        {
			public string call_type { get; set; }
			public int tot_duration { get; set; }
			public string tariff_duration { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, WpgetcdrcalltypesummaryInput req)
        {
            List<WpgetcdrcalltypesummaryOutput> OutputList = new List<WpgetcdrcalltypesummaryOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_get_cdr_call_type_summary";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id,
								@type = req.type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new WpgetcdrcalltypesummaryOutput()
                        {
							call_type = r.call_type,
							tot_duration = r.tot_duration == null ? 0 : r.tot_duration,
							tariff_duration = r.tariff_duration,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        WpgetcdrcalltypesummaryOutput outputobj = new WpgetcdrcalltypesummaryOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                WpgetcdrcalltypesummaryOutput outputobj = new WpgetcdrcalltypesummaryOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
