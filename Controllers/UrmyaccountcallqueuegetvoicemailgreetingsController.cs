using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountcallqueuegetvoicemailgreetingsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcallqueuegetvoicemailgreetingsInput
        {
			public int company_id { get; set; }
			public int call_queue_id { get; set; }
			public int Hour_type { get; set; }  	                   	    
        }
        public class UrmyaccountcallqueuegetvoicemailgreetingsOutput
        {
			public string greeting_type { get; set; }
			public string greeting_audio_type { get; set; }
			public string greeting_audio_file_name { get; set; }
			public string greeting_content { get; set; }
			public int? status { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcallqueuegetvoicemailgreetingsInput req)
        {
            List<UrmyaccountcallqueuegetvoicemailgreetingsOutput> OutputList = new List<UrmyaccountcallqueuegetvoicemailgreetingsOutput>();
			Log.Info("Input : UrmyaccountcallqueuegetvoicemailgreetingsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_call_queue_get_voicemail_greetings";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@call_queue_id = req.call_queue_id,
								@Hour_type = req.Hour_type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcallqueuegetvoicemailgreetingsOutput()
                        {
							greeting_type = r.greeting_type,
							greeting_audio_type = r.greeting_audio_type,
							greeting_audio_file_name = r.greeting_audio_file_name,
							greeting_content = r.greeting_content,
							status = r.status == null ? 0 : r.status,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcallqueuegetvoicemailgreetingsOutput outputobj = new UrmyaccountcallqueuegetvoicemailgreetingsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcallqueuegetvoicemailgreetingsOutput outputobj = new UrmyaccountcallqueuegetvoicemailgreetingsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
