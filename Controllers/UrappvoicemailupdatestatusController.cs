using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappvoicemailupdatestatusController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappvoicemailupdatestatusInput
        {
        public string message_id { get; set; }
            public List<messageidList> messageidList { get; set; }
			public int type { get; set; }  	                   	    
        }
        public class UrappvoicemailupdatestatusOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        public class messageidList
        {
            public string message_id { get; set; }
        }

        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappvoicemailupdatestatusInput req)
        {
            List<UrappvoicemailupdatestatusOutput> OutputList = new List<UrappvoicemailupdatestatusOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_voicemail_update_status";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@message_id = req.message_id,
								@type = req.type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrappvoicemailupdatestatusOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrappvoicemailupdatestatusOutput outputobj = new UrappvoicemailupdatestatusOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrappvoicemailupdatestatusOutput outputobj = new UrappvoicemailupdatestatusOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
