using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using System.IO;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappregisterinvitecontactController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappregisterinvitecontactInput
        {
			public int company_id { get; set; }
			public int referred_by_extn { get; set; }
			public string company_name { get; set; }
			public string firstname { get; set; }
			public string lastname { get; set; }
			public string email { get; set; }
			public string password { get; set; }
                
        }
        public class UrappregisterinvitecontactOutput
        {
			public int sip_id { get; set; }
			public string sip_password { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public int? platform{ get; set; }
            public string host_address { get; set; }     	
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappregisterinvitecontactInput req)
        {
            Log.Info("Input : " + JsonConvert.SerializeObject(req));
            List<UrappregisterinvitecontactOutput> OutputList = new List<UrappregisterinvitecontactOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_register_invite_contact";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@referred_by_extn = req.referred_by_extn,
								@company_name = req.company_name,
								@firstname = req.firstname,
								@lastname = req.lastname,
								@email = req.email,
								@password = req.password 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrappregisterinvitecontactOutput()
                        {
							sip_id = r.sip_id == null ? 0 : r.sip_id,
							sip_password = r.sip_password,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg,
                            platform = r.platform == null ? 0 : r.platform,
                        }));
                        Log.Info("Output ur_app_register_invite_contact SP: " + JsonConvert.SerializeObject(result));

                        if (OutputList.Count > 0)
                        {
                            if (OutputList[0].sip_id != 0)
                            {
                                if (ConfigurationManager.AppSettings["ejabberdchat_mobileregister_enabled"] == "0")
                                {
                                    try
                                    {
                                        if (ConfigurationManager.AppSettings["ejabberdchat_cluster"] == "1")
                                        {

                                            //var webAddr1 = ConfigurationManager.AppSettings["ejabberdchat_mobileregister"];
                                            var webAddr1 = ConfigurationManager.AppSettings["ejabberdchat_mobileregister_host"];
                                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                            var httpWebRequest1 = (HttpWebRequest)WebRequest.Create(webAddr1);
                                            httpWebRequest1.ContentType = "application/json";
                                            httpWebRequest1.Method = "POST";
                                            // string json = "{\"userid\": \"" + OutputList[0].sip_id + "\",\"password\": \"" + OutputList[0].sip_password + "\"}";

                                            string autorization = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImcuYW5uYW1hbGFpQHZlY3RvbmUuY29tIiwicGFzc3dvcmQiOiIxMjM0NTYiLCJkZXZpY2VJZCI6ImFiY2QiLCJzaXBMb2dpbklkIjoiMzE3OCIsInJvbGVJZCI6MywiaWF0IjoxNTkyMjIyNzY5fQ.6CE_6uTy8mOvzOwrB3B75jMNAkDPGAbWKG1rB1ePy9s";
                                            //autorization = autorization;
                                            httpWebRequest1.Headers.Add("AUTHORIZATION", autorization);

                                            string json = "{\"userid\": \"" + OutputList[0].sip_id + "\",\"company_id\": \"" + req.company_id + "\",\"host\": \"" + OutputList[0].host_address + "\",\"password\": \"" + OutputList[0].sip_password + "\"}";

                                            using (var streamWriter = new StreamWriter(httpWebRequest1.GetRequestStream()))
                                            {
                                                streamWriter.Write(json);
                                                streamWriter.Flush();
                                            }
                                            var httpResponse1 = (HttpWebResponse)httpWebRequest1.GetResponse();
                                            Log.Info("Output ejabberdchat_mobileregister_cluster: " + JsonConvert.SerializeObject(httpResponse1));

                                        }
                                        else
                                        {
                                            var webAddr1 = ConfigurationManager.AppSettings["ejabberdchat_mobileregister"];
                                            var httpWebRequest1 = (HttpWebRequest)WebRequest.Create(webAddr1);
                                            httpWebRequest1.ContentType = "application/json";
                                            httpWebRequest1.Method = "POST";
                                            // string json = "{\"userid\": \"" + OutputList[0].sip_id + "\",\"password\": \"" + OutputList[0].sip_password + "\"}";

                                            string autorization = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImcuYW5uYW1hbGFpQHZlY3RvbmUuY29tIiwicGFzc3dvcmQiOiIxMjM0NTYiLCJkZXZpY2VJZCI6ImFiY2QiLCJzaXBMb2dpbklkIjoiMzE3OCIsInJvbGVJZCI6MywiaWF0IjoxNTkyMjIyNzY5fQ.6CE_6uTy8mOvzOwrB3B75jMNAkDPGAbWKG1rB1ePy9s";
                                            //autorization = autorization;
                                            httpWebRequest1.Headers.Add("AUTHORIZATION", autorization);

                                            string json = "{\"userid\": \"" + OutputList[0].sip_id + "\",\"company_id\": \"" + req.company_id + "\",\"password\": \"" + OutputList[0].sip_password + "\"}";

                                            using (var streamWriter = new StreamWriter(httpWebRequest1.GetRequestStream()))
                                            {
                                                streamWriter.Write(json);
                                                streamWriter.Flush();
                                            }
                                            var httpResponse1 = (HttpWebResponse)httpWebRequest1.GetResponse();
                                            Log.Info("Output ejabberdchat_mobileregister: " + JsonConvert.SerializeObject(httpResponse1));
                                        }
                                        
                                      
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }
                                else if (ConfigurationManager.AppSettings["ejabberdchat_mobileregister_enabled"] == "1")
                                {
                                    var ejabberdchat_mobileregister_new = ConfigurationManager.AppSettings["ejabberdchat_mobileregister_new"];
                                    var httpWebRequest1_ejabberdchat_mobileregister_new = (HttpWebRequest)WebRequest.Create(ejabberdchat_mobileregister_new);
                                    httpWebRequest1_ejabberdchat_mobileregister_new.ContentType = "application/json";
                                    httpWebRequest1_ejabberdchat_mobileregister_new.Method = "POST";
                                    string autorization = "OGFrMzdnSlVZcTJoUno6MGRpdWQ3NjVqZzk0YnNpODRqZmdqMHczamZoNzgyMmo=";
                                    autorization = "Basic " + autorization;
                                    httpWebRequest1_ejabberdchat_mobileregister_new.Headers.Add("AUTHORIZATION", autorization);

                                    string platform = OutputList[0].platform + ".unifiedring.co.uk";

                                    string json_ejabberdchat_mobileregister_new = "{\"userid\": \"" + OutputList[0].sip_id + "\",\"password\": \"" + OutputList[0].sip_password + "\",\"platform\": \"" + platform + "\"}";

                                    using (var streamWriter = new StreamWriter(httpWebRequest1_ejabberdchat_mobileregister_new.GetRequestStream()))
                                    {
                                        streamWriter.Write(json_ejabberdchat_mobileregister_new);
                                        streamWriter.Flush();
                                    }
                                    var httpResponse1_ejabberdchat_mobileregister_new = (HttpWebResponse)httpWebRequest1_ejabberdchat_mobileregister_new.GetResponse();
                                    using (var streamReader = new StreamReader(httpResponse1_ejabberdchat_mobileregister_new.GetResponseStream()))
                                    {
                                        var response = streamReader.ReadToEnd();
                                    }
                                    Log.Info("Output ejabberdchat_mobileregister: " + JsonConvert.SerializeObject(httpResponse1_ejabberdchat_mobileregister_new));
                                }
                                //Push value to MongoDB Serveer
                            }
                           
                        }

                     
                    }
                    else
                    {
                        UrappregisterinvitecontactOutput outputobj = new UrappregisterinvitecontactOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrappregisterinvitecontactOutput outputobj = new UrappregisterinvitecontactOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
