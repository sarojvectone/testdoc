using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappaddcustomnumberflipinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappaddcustomnumberflipinfoInput
        {
			public int extension { get; set; }
			public int company_id { get; set; }
			public string dev_product_name { get; set; }
			public string number { get; set; }
			public int Ring_type { get; set; }  	                   	    
        }
        public class UrappaddcustomnumberflipinfoOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappaddcustomnumberflipinfoInput req)
        {
            List<UrappaddcustomnumberflipinfoOutput> OutputList = new List<UrappaddcustomnumberflipinfoOutput>();
			Log.Info("Input : UrappaddcustomnumberflipinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_add_custom_number_flip_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@extension = req.extension,
								@company_id = req.company_id,
								@dev_product_name = req.dev_product_name,
								@number = req.number,
								@Ring_type = req.Ring_type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrappaddcustomnumberflipinfoOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrappaddcustomnumberflipinfoOutput outputobj = new UrappaddcustomnumberflipinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrappaddcustomnumberflipinfoOutput outputobj = new UrappaddcustomnumberflipinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
