using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using System.IO;
using Newtonsoft.Json;
using System.Text;
using Newtonsoft.Json.Linq;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappPublishHubspotController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappgetdesktopadminInput
        {
            public string api_url { get; set; }
            public string method { get; set; }
            public string query_string { get; set; }
            public object parameter { get; set; }
        }
        public class UrappgetdesktopadminOutput
        {

            public int errcode { get; set; }
            public string errmsg { get; set; }
            public object result { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappgetdesktopadminInput req)
        {
            Log.Info("UrappPublishHubspotController Input : " + JsonConvert.SerializeObject(req));
            List<UrappgetdesktopadminOutput> OutputList = new List<UrappgetdesktopadminOutput>();
            string endpoint;
            string hapikey = ConfigurationManager.AppSettings["HubSpotKey"];

            try
            {
                if (!string.IsNullOrEmpty(req.query_string))
                {
                    endpoint = req.api_url + "?hapikey=" + hapikey + "&" + req.query_string;
                }
                else
                {
                    endpoint = req.api_url + "?hapikey=" + hapikey;
                }
                //Call Hubspot API
                Log.Info("UrappPublishHubspotController URL : " + JsonConvert.SerializeObject(endpoint));
                HttpWebRequest httpWReq = null;
                Encoding encoding = new UTF8Encoding();
                httpWReq = (HttpWebRequest)WebRequest.Create(endpoint);
                httpWReq.ProtocolVersion = HttpVersion.Version11;
                httpWReq.Method = req.method.ToUpper();
                httpWReq.ContentType = "application/json";
                //httpWReq.Timeout = 180000;

                if (req.method != "GET")
                {
                    string postData = JsonConvert.SerializeObject(req.parameter);
                    byte[] data = encoding.GetBytes(postData);
                    Stream stream = httpWReq.GetRequestStream();
                    stream.Write(data, 0, data.Length);
                    stream.Close();
                } 

                #region GetNewValueResponse
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                string s = response.ToString();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                String jsonresponse = "";
                String temp = null;

                var text = reader.ReadToEnd();

                while ((temp = reader.ReadLine()) != null)
                {
                    jsonresponse += temp;
                }
                //Assigning Unique values for update
                //var dataResult = (JObject)JsonConvert.DeserializeObject(jsonresponse);
                var dataResult = (JObject)JsonConvert.DeserializeObject(text);
                //End Hubspot API
                # endregion
                Log.Info("UrappPublishHubspotController Response : " + JsonConvert.SerializeObject(text));
                UrappgetdesktopadminOutput outputobj = new UrappgetdesktopadminOutput();
                outputobj.errcode = 0;
                outputobj.errmsg = "Success";
                outputobj.result = text;
            }
            catch (WebException ex)
            {

                try
                {

                    using (WebResponse response = ex.Response)
                    {

                        HttpWebResponse httpResponse = (HttpWebResponse)response;
                      
                        using (Stream dataException = response.GetResponseStream())
                        using (var reader = new StreamReader(dataException))
                        {
                            var text = reader.ReadToEnd();
                            Log.Info("UrappPublishHubspotController Response : " + JsonConvert.SerializeObject(text));
                            UrappgetdesktopadminOutput outputobj = new UrappgetdesktopadminOutput();
                            outputobj.errcode = -1;
                            outputobj.errmsg = text.ToString();
                            OutputList.Add(outputobj);

                        }
                    }
                }
                catch (Exception ex1)
                {
                    UrappgetdesktopadminOutput outputobj = new UrappgetdesktopadminOutput();
                    outputobj.errcode = -1;
                    outputobj.errmsg = ex1.Message;
                    OutputList.Add(outputobj);
                } 
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
