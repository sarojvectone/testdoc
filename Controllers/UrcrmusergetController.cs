using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrcrmusergetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrcrmusergetInput
        {
			public string username { get; set; }  	                   	    
        }
        public class UrcrmusergetOutput
        {
			public string username { get; set; }
			public string password { get; set; }
			public int  user_role { get; set; }
			public string email { get; set; }
			public string firstname { get; set; }
			public string lastname { get; set; }
			public string address1 { get; set; }
			public string address2 { get; set; }
			public string mobileno { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrcrmusergetInput req)
        {
            List<UrcrmusergetOutput> OutputList = new List<UrcrmusergetOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_crm_user_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@username = req.username 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrcrmusergetOutput()
                        {
							username = r.username,
							password = r.password,
                            user_role = r.user_role == null ? 0 : r.user_role,
							email = r.email,
							firstname = r.firstname,
							lastname = r.lastname,
							address1 = r.address1,
							address2 = r.address2,
							mobileno = r.mobileno,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrcrmusergetOutput outputobj = new UrcrmusergetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrcrmusergetOutput outputobj = new UrcrmusergetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
