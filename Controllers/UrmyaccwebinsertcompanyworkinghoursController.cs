using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebinsertcompanyworkinghoursController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebinsertcompanyworkinghoursInput
        {
			public int Company_Id { get; set; }
			public string Working_Business_Type { get; set; }
			public string Working_Hours { get; set; }
            public string Extension_Number { get; set; }
            public int Type { get; set; }
            public int site_id  { get; set; }
        }
        public class UrmyaccwebinsertcompanyworkinghoursOutput
        {
           // public int? site_id { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebinsertcompanyworkinghoursInput req)
        {
            List<UrmyaccwebinsertcompanyworkinghoursOutput> OutputList = new List<UrmyaccwebinsertcompanyworkinghoursOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_Insert_Company_Working_Hours";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_Id = req.Company_Id,
								@Working_Business_Type = req.Working_Business_Type,
								@Working_Hours = req.Working_Hours,
                                @Extension_Number = req.Extension_Number,
                                @Type = req.Type,
                                @site_id = req.site_id 
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebinsertcompanyworkinghoursOutput()
                        {
                            //site_id = r.site_id == null ? 0 : r.site_id,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebinsertcompanyworkinghoursOutput outputobj = new UrmyaccwebinsertcompanyworkinghoursOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebinsertcompanyworkinghoursOutput outputobj = new UrmyaccwebinsertcompanyworkinghoursOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
