using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using System.Net.Mail;
using System.Text;
using System.IO;
using Newtonsoft.Json;

namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrapplinkappController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrapplinkappInput
        {
            public string Username { get; set; }
            public List<invite_list> invite_list { get; set; }
            public string ApplinkURL { get; set; }
            public int company_id { get; set; }
            public string extn { get; set; }
        }
        public class UrapplinkappOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        public class invite_list
        {
            public string InvitedPersonname { get; set; }
            public string InvitedPersonEmail { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrapplinkappInput req)
        {
            Log.Info("Input :UrapplinkappController: " + JsonConvert.SerializeObject(req));
            List<UrapplinkappOutput> OutputList = new List<UrapplinkappOutput>();
            try
            {
                if (req.invite_list.Count > 0)
                {
                    foreach (invite_list objinvite_list in req.invite_list)
                    {

                        string ApplinkURL = "company=" + req.company_id + "&user=" + req.extn + "&Email=" + objinvite_list.InvitedPersonEmail;
                        string ApplinkDecode = Base64Encode(ApplinkURL);
                        string signuppageURL = "https://unifiedring.co.uk/myaccount/signupbyinvite?" + ApplinkDecode;

                        Log.Info("signuppageURL : " + signuppageURL);
                        SendMail(req.Username, objinvite_list.InvitedPersonEmail, objinvite_list.InvitedPersonname, signuppageURL, "Unifiedring Invitation");
                        Log.Info("Output :UrapplinkappController: objinvite_list.InvitedPersonEmail: " + objinvite_list.InvitedPersonEmail + "Send Mail Completed");
                    }
                    UrapplinkappOutput outputobj = new UrapplinkappOutput();
                    outputobj.errcode = 0;
                    outputobj.errmsg = "success";
                    OutputList.Add(outputobj);
                }
                else
                {
                    UrapplinkappOutput outputobj = new UrapplinkappOutput();
                    outputobj.errcode = -1;
                    outputobj.errmsg = "No Invite list received";
                    OutputList.Add(outputobj);
                }
            }
            catch (Exception ex)
            {
                UrapplinkappOutput outputobj = new UrapplinkappOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
                Log.Error("Exception :UrapplinkappController: " + JsonConvert.SerializeObject(ex.Message.ToString()));
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        private static void SendMail(string UserName, string InvitedPersonEmail, string InvitedUserName, string AppLink, string subject)
        {
            try
            {
                string from = ConfigurationManager.AppSettings["FromList"];

                StreamReader mailContent_file = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Template", "UnifiedRing_Guest_User.html"));
                string mailContent = mailContent_file.ReadToEnd();
                mailContent = mailContent.Replace("*|UserName|*", UserName);
                mailContent = mailContent.Replace("*|InvitedUserName|*", InvitedUserName);
                mailContent = mailContent.Replace("*|SUBJECT|*", subject);
                mailContent = mailContent.Replace("*|AppLink|*", AppLink);
                mailContent = mailContent.Replace("*|MC_PREVIEW_TEXT|*", "Unifiedring Invitation");

                mailContent_file.Close();
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.To.Add(InvitedPersonEmail);
                mail.From = new MailAddress(from, "Unified ring");
                mail.Subject = subject;
                mail.IsBodyHtml = true;
                mail.Body = mailContent;
                // mail.BodyEncoding = Encoding.UTF8;
                SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"]);
                emailClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SmtpUser"], ConfigurationManager.AppSettings["SmtpPass"]);
                emailClient.Send(mail);
            }
            catch (Exception ex)
            {
                Log.Info("Send Mail failed..", ex.Message.ToString());

            }
            Log.Info("Send Mail completed..");

        }

    }
}
