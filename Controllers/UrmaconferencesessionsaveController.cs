using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaconferencesessionsaveController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaconferencesessionsaveInput
        {
			public int company_id { get; set; }
			public int domain_id { get; set; }
			public string created_by { get; set; }
			public string requested_by { get; set; }
			public string topic { get; set; }
			public string description { get; set; }
			public DateTime? start_time { get; set; }
			public DateTime? end_time { get; set; }
			public int duration_limit { get; set; }
			public string attendees { get; set; }
			public int call_screening { get; set; }
			public int conference_recording { get; set; }
			public int type { get; set; }
			public int conf_Id { get; set; }
            public int status { get; set; }
            public int site_id { get; set; }
        }
        public class UrmaconferencesessionsaveOutput
        {
			public string conference_id { get; set; }
			public string participant_pin { get; set; }
			public string host_pin { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaconferencesessionsaveInput req)
        {
            List<UrmaconferencesessionsaveOutput> OutputList = new List<UrmaconferencesessionsaveOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_conference_session_save";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@domain_id = req.domain_id,
								@created_by = req.created_by,
								@requested_by = req.requested_by,
								@topic = req.topic,
								@description = req.description,
								@start_time = req.start_time == null ? null : req.start_time,
								@end_time = req.end_time == null ? null : req.end_time,
								@duration_limit = req.duration_limit,
								@attendees = req.attendees,
								@call_screening = req.call_screening,
								@conference_recording = req.conference_recording,
								@type = req.type,
								@conf_Id = req.conf_Id,
                                @status = req.status,
                                @site_id = req.site_id    
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmaconferencesessionsaveOutput()
                        {
							conference_id = r.conference_id,
							participant_pin = r.participant_pin,
							host_pin = r.host_pin,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmaconferencesessionsaveOutput outputobj = new UrmaconferencesessionsaveOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found"; 
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmaconferencesessionsaveOutput outputobj = new UrmaconferencesessionsaveOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
