using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class SmscontentgettextController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class SmscontentgettextInput
        {
			public int sms_id { get; set; }
			public string scenerio { get; set; }
			public string sitecode { get; set; }  	                   	    
        }
        public class SmscontentgettextOutput
        {
			public int Sms_id { get; set; }
			public string Scenerio { get; set; }
			public string Sms_text { get; set; }
			public DateTime Create_date { get; set; }
			public string Status { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, SmscontentgettextInput req)
        {
            List<SmscontentgettextOutput> OutputList = new List<SmscontentgettextOutput>();
			Log.Info("Input : SmscontentgettextController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "sms_content_get_text";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@sms_id = req.sms_id,
								@scenerio = req.scenerio,
								@sitecode = req.sitecode 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new SmscontentgettextOutput()
                        {
							Sms_id = r.Sms_id == null ? 0 : r.Sms_id,
							Scenerio = r.Scenerio,
							Sms_text = r.Sms_text,
							Create_date = r.Create_date == null ? null : r.Create_date,
							Status = r.Status,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        SmscontentgettextOutput outputobj = new SmscontentgettextOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                SmscontentgettextOutput outputobj = new SmscontentgettextOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
