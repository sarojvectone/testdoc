using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class ursubscriptionController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public class ursubscription
        {
            public string message_token { get; set; }
            public string voip_token { get; set; }
            public string device_id { get; set; }
            public string application { get; set; }
            public string devicetype { get; set; }
            public string dev_identy  { get; set; }
        }

        public class ursubscriptionOutput : Output
        {
            //public string message_token { get; set; }
            //public string voip_token { get; set; }
            //public string device_id { get; set; }
            //public string application { get; set; }
            //public string devicetype { get; set; } 	    
        }

        public class Output
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, ursubscription input)
        {
            //List<ursubscriptionOutput> OutputList = new List<ursubscriptionOutput>();
            var output = new Output();
            try
            {
                Log.Info("ursubscriptionController");
                Log.Info("Input : " + JsonConvert.SerializeObject(input));
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["apns_provider"].ConnectionString))
                {
                    conn.Open();
                    var sp = "usp_insert_subscription";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @DeviceID = input.device_id,
                                @MessageToken = input.message_token,
                                @VOIPToken = input.voip_token,
                                @Application = input.application,
                                @DeviceType = input.devicetype == "IOS" ? 1 : 2,
                                @dev_identy = input.dev_identy
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        output.errcode = result.ElementAt(0).errcode;
                        output.errmsg = result.ElementAt(0).errmsg;
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = "Failed"; 
                    }
                }
            }
            catch (Exception ex)
            {
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            //OutputList.Add(output);
            //return Request.CreateResponse(HttpStatusCode.OK, OutputList);
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
    }
}
