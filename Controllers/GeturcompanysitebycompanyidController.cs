using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class GeturcompanysitebycompanyidController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class GeturcompanysitebycompanyidInput
        {
			public int company_id { get; set; }  	                   	    
        }
        public class GeturcompanysitebycompanyidOutput
        {
			public int? site_id { get; set; }
			public int? company_id { get; set; }
			public string site_name { get; set; }
			public string site_extn { get; set; }
			public string site_address { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, GeturcompanysitebycompanyidInput req)
        {
            List<GeturcompanysitebycompanyidOutput> OutputList = new List<GeturcompanysitebycompanyidOutput>();
			Log.Info("Input : GeturcompanysitebycompanyidController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "get_UR_Company_Site_by_company_id";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new GeturcompanysitebycompanyidOutput()
                        {
							site_id = r.site_id == null ? 0 : r.site_id,
							company_id = r.company_id == null ? 0 : r.company_id,
							site_name = r.site_name,
							site_extn = r.site_extn,
							site_address = r.site_address,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        GeturcompanysitebycompanyidOutput outputobj = new GeturcompanysitebycompanyidOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                GeturcompanysitebycompanyidOutput outputobj = new GeturcompanysitebycompanyidOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
