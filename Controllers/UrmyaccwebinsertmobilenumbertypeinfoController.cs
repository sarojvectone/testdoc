using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebinsertmobilenumbertypeinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebinsertmobilenumbertypeinfoInput
        {
			public int Company_id { get; set; }
			public int Extension { get; set; }
			public int Hour_Type { get; set; }
			public int Incoming_call_type { get; set; }
			public int After_Hours_Answered_Type { get; set; }
			public string Announcement_file { get; set; }
			public string play_language { get; set; }
			public int call_forward_status { get; set; }
			public int call_forward_type { get; set; }
			public string mobile_number_type { get; set; }  	                   	    
        }
        public class UrmyaccwebinsertmobilenumbertypeinfoOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebinsertmobilenumbertypeinfoInput req)
        {
            List<UrmyaccwebinsertmobilenumbertypeinfoOutput> OutputList = new List<UrmyaccwebinsertmobilenumbertypeinfoOutput>();
			Log.Info("Input : UrmyaccwebinsertmobilenumbertypeinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_insert_mobile_number_type_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_id = req.Company_id,
								@Extension = req.Extension,
								@Hour_Type = req.Hour_Type,
								@Incoming_call_type = req.Incoming_call_type,
								@After_Hours_Answered_Type = req.After_Hours_Answered_Type,
								@Announcement_file = req.Announcement_file,
								@play_language = req.play_language,
								@call_forward_status = req.call_forward_status,
								@call_forward_type = req.call_forward_type,
								@mobile_number_type = req.mobile_number_type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccwebinsertmobilenumbertypeinfoOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccwebinsertmobilenumbertypeinfoOutput outputobj = new UrmyaccwebinsertmobilenumbertypeinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccwebinsertmobilenumbertypeinfoOutput outputobj = new UrmyaccwebinsertmobilenumbertypeinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
