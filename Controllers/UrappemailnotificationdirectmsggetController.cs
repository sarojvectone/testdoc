using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappemailnotificationdirectmsggetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappemailnotificationdirectmsggetInput
        {
			 	                   	    
        }
        public class UrappemailnotificationdirectmsggetOutput
        {
            public int? email_direct_msg_id { get; set; }
            public string email_direct_msg_desc { get; set; } 
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Get(string id, HttpRequestMessage request, UrappemailnotificationdirectmsggetInput req)
        {
            List<UrappemailnotificationdirectmsggetOutput> OutputList = new List<UrappemailnotificationdirectmsggetOutput>();
			Log.Info("Input : UrappemailnotificationdirectmsggetController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_email_notification_direct_msg_get";                
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
		                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrappemailnotificationdirectmsggetOutput()
                        {
                            email_direct_msg_id = r.email_direct_msg_id == null ? 0 : r.email_direct_msg_id,
                            email_direct_msg_desc = r.email_direct_msg_desc,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrappemailnotificationdirectmsggetOutput outputobj = new UrappemailnotificationdirectmsggetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrappemailnotificationdirectmsggetOutput outputobj = new UrappemailnotificationdirectmsggetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
