﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PushSharp.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using Dapper;
using System.Data;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class BicsupdateAddressController:ApiController
    {
        public class BICSErrorResponse
        {
            public string code { get; set; }
            public string description { get; set; }
            public string timestamp { get; set; }
        }

        public class BicsupdateAddressInput 
        {
            public string alias { get; set; }
            public string street { get; set; }
            public string streetNumber { get; set; }
            public string box { get; set; }
            public string residence { get; set; }
            public string floor { get; set; }
            public string postalCode { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string country { get; set; }
            public string serviceUsage { get; set; }
            public UserIdentity userIdentity { get; set; }
            public string reference { get; set; } // Added For Url Passing Reference Number.  Method Put
        }
        public class UserIdentity
        {
            public string emailAddress { get; set; }
            public string companyName { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string vatNumber { get; set; }
            public string phoneNumber { get; set; }
        }
        public class BicsupdateAddressOutput1
        {
            public string alias { get; set; }
            public string street { get; set; }
            public string streetNumber { get; set; }
            public string box { get; set; }
            public string residence { get; set; }
            public string floor { get; set; }
            public string postalCode { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string country { get; set; }
            public string reference { get; set; }
            public string status { get; set; }
            public string comment { get; set; }
            public string serviceUsage { get; set; }
            public List<AddressDocument> addressDocuments { get; set; }
            public UserIdentity userIdentity { get; set; }
            public List<string> allowedLocations { get; set; }
            public List<AllowedLocationsAreaCode> allowedLocationsAreaCodes { get; set; }
        }

        public class BicsupdateAddressOutput :CommonOutput
        {
            public string alias { get; set; }
            public string street { get; set; }
            public string streetNumber { get; set; }
            public string box { get; set; }
            public string residence { get; set; }
            public string floor { get; set; }
            public string postalCode { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string country { get; set; }
            public string reference { get; set; }
            public string status { get; set; }
            public string comment { get; set; }
            public string serviceUsage { get; set; }
            public List<AddressDocument> addressDocuments { get; set; }
            public UserIdentity userIdentity { get; set; }
            public List<string> allowedLocations { get; set; }
            public List<AllowedLocationsAreaCode> allowedLocationsAreaCodes { get; set; }
        }

        public class AddressDocument
        {
            public string fileName { get; set; }
            public int size { get; set; }
            public string status { get; set; }
            public DateTime created { get; set; }
        }       
        public class AllowedLocationsAreaCode
        {
            public string location { get; set; }
            public string areaCode { get; set; }
            public bool nonGeographic { get; set; }
        }
        public class CommonOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

      [Authorize]
        public async Task<HttpResponseMessage> PUT(string id, HttpRequestMessage request, BicsupdateAddressInput UpdateAddressInput)
        {
            string InputReference = UpdateAddressInput.reference;
            BicsupdateAddressOutput1 result1 = new BicsupdateAddressOutput1();
            List<BicsupdateAddressOutput> output = new List<BicsupdateAddressOutput>();
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                string Inputrequest = ConfigurationManager.AppSettings["BICSAPIENDPOINT"] + "addresses/" + UpdateAddressInput.reference;
                var httpRequest = (HttpWebRequest)WebRequest.Create(Inputrequest);
                string authenticationtoken = ConfigurationManager.AppSettings["BICSAPIKEY"];
                httpRequest.UserAgent = "Fiddler";
                httpRequest.Headers.Add("Authorization", "Bearer " + authenticationtoken);
                httpRequest.Accept = "application/json";
                httpRequest.ContentType = "application/json";
                httpRequest.Timeout = 180000;
                httpRequest.Method = "PUT";
                string Bicsupdataddressresult = "Error";

                using (var streamBicsWriters = new StreamWriter(httpRequest.GetRequestStream()))
                {
                    string jsonBics = new JavaScriptSerializer().Serialize(UpdateAddressInput);
                    streamBicsWriters.Write(jsonBics);
                    streamBicsWriters.Flush();
                    streamBicsWriters.Dispose();
                    HttpWebResponse httpBicsResponce = (HttpWebResponse)httpRequest.GetResponse();
                    Stream BicsupdateaddressStream = httpBicsResponce.GetResponseStream();
                    StreamReader Bicsaddressreader = new StreamReader(BicsupdateaddressStream);
                    Bicsupdataddressresult = Bicsaddressreader.ReadToEnd();
                    result1 = JsonConvert.DeserializeObject<BicsupdateAddressOutput1>(Bicsupdataddressresult);
                    BicsupdateaddressStream.Close();
                    Bicsaddressreader.Close();
                    // Call To DB
                    if (result1.reference != null)
                    {
                        string inputreference = result1.reference;
                        string inputdid_number = "";
                        string inpputfirst_name = result1.userIdentity.firstName;
                        string inputlast_name = result1.userIdentity.lastName;
                        string inputvat_number = result1.userIdentity.vatNumber;
                        string inputcompany_name = result1.userIdentity.companyName;
                        string inputalias_name = result1.alias;
                        string inputstreet = result1.street;
                        string inputstreetNumber = result1.streetNumber;
                        string inputbox = result1.box;
                        string inputresidence = result1.residence;
                        string inputfloor_info = result1.floor;
                        string inputpostal_code = result1.postalCode;
                        string inputcity = result1.city;
                        string inputstate = result1.state;
                        string inputcountry = result1.country;
                        string inputstatus = result1.status;
                        object inputaddressDocuments = result1.addressDocuments;
                        string inputemail_id = result1.userIdentity.emailAddress;
                        using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Didportalapi"].ConnectionString))
                        {
                            conn.Open();
                            var sp = "did_number_insert_address_info";
                            var result = conn.Query<dynamic>(
                                    sp, new
                                    {
                                        reference = inputreference,
                                        did_number = inputdid_number,
                                        first_name = inpputfirst_name,
                                        last_name = inputlast_name,
                                        vat_number = inputvat_number,
                                        company_name = inputcompany_name,
                                        alias_name = inputalias_name,
                                        street = inputstreet,
                                        streetNumber = inputstreetNumber,
                                        box = inputbox,
                                        residence = inputresidence,
                                        floor_info = inputfloor_info,
                                        postal_code = inputpostal_code,
                                        city = inputcity,
                                        state = inputstate,
                                        country = inputcountry,
                                        status = inputstatus,
                                        comment = "BICS Address Updated",
                                        serviceUsage = "BICS Service",
                                        addressDocuments = "",
                                        email_id = inputemail_id
                                    },
                                    commandType: CommandType.StoredProcedure);
                            if (result != null && result.Count() > 0)
                            {

                            }
                        }

                    }
                }
            }
            catch (WebException ex)
            {
                Log.Error(ex.Message);
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    try
                    {
                        HttpWebResponse err = ex.Response as HttpWebResponse;
                        if (err != null)
                        {
                            string errResponse = new StreamReader(err.GetResponseStream()).ReadToEnd();
                            Log.Info("Exception : Response : " + errResponse);
                            List<BICSErrorResponse> response = JsonConvert.DeserializeObject<List<BICSErrorResponse>>(errResponse);
                            if (response != null && response.Count > 0)
                            {
                                BicsupdateAddressOutput objAddAddressOutput = new BicsupdateAddressOutput();
                                objAddAddressOutput.errcode = -1;
                                objAddAddressOutput.errmsg = response.ElementAt(0).code + ":" + response.ElementAt(0).description;
                                output.Add(objAddAddressOutput);
                                return Request.CreateResponse(HttpStatusCode.OK, output);
                            }
                        }
                    }
                    catch (Exception innerEx)
                    {
                        Log.Error(innerEx.Message);
                        BicsupdateAddressOutput objAddAddressOutput = new BicsupdateAddressOutput();
                        objAddAddressOutput.errcode = -1;
                        objAddAddressOutput.errmsg = innerEx.Message;
                        output.Add(objAddAddressOutput);
                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }

                }
                else
                {
                    BicsupdateAddressOutput objAddAddressOutput = new BicsupdateAddressOutput();
                    objAddAddressOutput.errcode = -1;
                    objAddAddressOutput.errmsg = ex.Message;
                    output.Add(objAddAddressOutput);
                    return Request.CreateResponse(HttpStatusCode.OK, output);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, result1);
        }



    }
}