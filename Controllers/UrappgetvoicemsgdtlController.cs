using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappgetvoicemsgdtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappgetvoicemsgdtlInput
        {
			public int domain_id { get; set; }
			public int extension { get; set; }  	                   	    
        }
        public class UrappgetvoicemsgdtlOutput
        {
			public string message_id { get; set; }
			public string cid_number { get; set; }
			public string cid_name { get; set; }
			public DateTime msg_expiry_date { get; set; }
			public DateTime created_date { get; set; }
			public string msg_filename { get; set; }
			public int msg_read_status { get; set; }
            public int sip_login_id { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }
	 

        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappgetvoicemsgdtlInput req)
        {
            string voicemailBaseURL = ConfigurationManager.AppSettings["VoiceMailPath"];

            List<UrappgetvoicemsgdtlOutput> OutputList = new List<UrappgetvoicemsgdtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_get_voicemsg_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id,
								@extension = req.extension 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    //X:\default\5901.UR.mundio.com\752\msg_4953134A-885B-48F5-964A-84145CFB372B.wav
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrappgetvoicemsgdtlOutput()
                        {
							message_id = r.message_id,
							cid_number = r.cid_number,
							cid_name = r.cid_name,
							msg_expiry_date = r.msg_expiry_date == null ? null : r.msg_expiry_date,
							created_date = r.created_date == null ? null : r.created_date,
                            msg_filename = voicemailBaseURL+r.msg_filename.Replace(@"X:\default\","/").Replace("\\","/"),
							msg_read_status = r.msg_read_status == null ? 0 : r.msg_read_status,
                            sip_login_id = r.sip_login_id == null ? 0 : r.sip_login_id,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrappgetvoicemsgdtlOutput outputobj = new UrappgetvoicemsgdtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrappgetvoicemsgdtlOutput outputobj = new UrappgetvoicemsgdtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
