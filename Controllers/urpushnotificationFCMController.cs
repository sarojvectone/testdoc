using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using System.Security.Cryptography.X509Certificates;
using unifiedringmyaccountwebapi.Controllers;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class urpushnotificationFCMController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public class PushNotificationInput
        {
            public string Title { get; set; }
            public string Message { get; set; }
            public string token { get; set; } 
        }

        public class PushNotificationOutput : Output
        {

        }

        public class Output
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public HttpResponseMessage Get(string origin, string target, bool isvoip, int iscancel, string iuid)
        {
            Log.Info("urpushnotificationFCM -Get");
            String deviceToken = string.Empty;
            ResultDetailsFCM data = new ResultDetailsFCM();
            PushNotificationOutput output = new PushNotificationOutput();
            try
            {
                Log.Info("Input : origin=" + origin + "&target=" + target + "&isvoip=" + isvoip + "&iscancel=" + iscancel + "&iuid=" + iuid);
                bool iscancelled = false;
                String message = string.Empty;
                 List<GetDeviceTokenOutput> resultToken = null;
                if (iscancel != 0)
                    iscancelled = true;
                   string ToNumber = target; //GetMobileNo(target);
                   if (ToNumber != "")
                   {
                       Log.Info("To mobile no : " + ToNumber);
                       string FromNumber = origin.IndexOf("_") > -1 ? origin.Split(new string[] { "_" }, StringSplitOptions.None)[1] : origin;//GetName(origin);origin; //GetMobileNo(origin);
                       
                       Log.Info("From mobile no : " + FromNumber);
                       //Device Token
                       resultToken = GetDeviceToken(ToNumber, isvoip, 2);
                       if (resultToken[0].errcode == -1)
                       {
                           output.errcode = -1;
                           output.errmsg = "device not found";
                           Log.Info("device not found");
                           return Request.CreateResponse(HttpStatusCode.OK, output);
                       }
                       if (resultToken.Count<0)
                       {
                           output.errcode = -1;
                           output.errmsg = "device not found";
                           Log.Info("device not found");
                           return Request.CreateResponse(HttpStatusCode.OK, output);
                       }
                       else
                       {
                           Log.Info("deviceToken : " + JsonConvert.SerializeObject(resultToken));

                           string name;
                           if (origin.IndexOf("_") > -1)
                               name = GetName(origin);//target.Split(new string[] { "_" }, StringSplitOptions.None)[1];//GetName(origin);
                           else
                               name = origin;
                           //name = origin;
                           //if (name.Length == 0)
                           //    name = origin;
                           Log.Info(string.Format("Extension : {0}", name));

                           //Todo : Anees added to get Contact name
                           //FromNumber = GetName(FromNumber);
                           FromNumber = String.IsNullOrEmpty(FromNumber) ? name : FromNumber;

                           message = (!iscancelled) ? string.Format(APNSHelper.push_message, name) : "Cancelled";
                           //String message = string.Format(APNSHelper.push_message, origin);
                           //Message = message;
                           //token = deviceToken;

                           Log.Info("message : " + message + "," + iuid);
                       }
                   }
                   if (output.errcode != -1)
                   {
                       List<string> tokens = new List<string>();
                       foreach (GetDeviceTokenOutput token in resultToken)
                       {
                           tokens.Add(token.message_token);
                       }
                       data = unifiedringmyaccountwebapi.Controllers.FirebaseHelper.SendNotification("Notification", message + "~" + iuid, tokens);
                      // data = JsonConvert.DeserializeObject<ResultDetailsFCM>(result);
                       List<string> failedTokens = null;
                       if (data.results.Count > 0)
                       {
                          failedTokens = new List<string>();
                           for (int i = 0; i < data.results.Count(); i++)
                           {
                               unifiedringmyaccountwebapi.Controllers.Result strerror = data.results[i];
                               if (!string.IsNullOrEmpty(strerror.error))
                               {
                                   failedTokens.Add(resultToken[i].message_token);
                               }
                           }
                             //Deactivate Token
                           if (failedTokens.Count > 0)
                           {
                               string failedTokensstring = string.Join(",", failedTokens.ToArray());
                               DeactivateSubscription(failedTokensstring, "");
                           }
                       }
                     
                   }
                   else
                   {
                       return Request.CreateResponse(HttpStatusCode.OK, output);
                   } 
            }
            catch (Exception ex)
            {
                //DeActivate Subscription
                string result = string.Empty; 
                result = DeactivateSubscription(deviceToken, "");

                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return Request.CreateResponse(HttpStatusCode.OK, data);
        }
        public class Result
        {
            public string error { get; set; }
        }
        
        public class ResultDetails
        {
            public long multicast_id { get; set; }
            public int success { get; set; }
            public int failure { get; set; }
            public int canonical_ids { get; set; }
            public List<Result> results { get; set; }
        }

        //[Authorize]
        public HttpResponseMessage Post(PushNotificationInput input)
        {
            Log.Info("urpushnotificationFCM -Post");
            PushNotificationOutput output = new PushNotificationOutput();
            if (input == null)
            {
                output.errcode = -1;
                output.errmsg = "input details not found";
            }
            else
            {
                try
                {   object dataObject = null;
                ResultDetailsFCM result = FirebaseHelper.SendNotification(input.Title, input.Message, input.token, dataObject);

                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                }
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
        public string DeactivateSubscription(string MessageToken, string VOIPToken)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["apns_provider"].ConnectionString))
                {
                    conn.Open();
                    var sp = "usp_deactivate_devicetoken";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @MessageToken = MessageToken,
                                @VOIPToken = VOIPToken
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        return "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return "Success";
        }
        public class GetDeviceTokenOutput
        {
            public string message_token { get; set; }
            public string voip_token { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }	    
        }
        private List<GetDeviceTokenOutput> GetDeviceToken(string deviceId, bool isvoip, int device_type)
        {
            List<GetDeviceTokenOutput> OutputList = new List<GetDeviceTokenOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["apns_provider"].ConnectionString))
                {
                    conn.Open();
                    var sp = "usp_getdevicetoken";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @DeviceID = deviceId,
                                @device_type = device_type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new GetDeviceTokenOutput()
                        {
                            message_token  = r.message_token,
                            voip_token = r.voip_token,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        Log.Info("Output DB : " + "Empty result");
                        GetDeviceTokenOutput outputobj = new GetDeviceTokenOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return OutputList;
        }

        private string GetName(string accountid)
        {
            string custName = "";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringappapi"].ConnectionString))
                {
                    var sp = "ur_ma_push_notification_get_username";

                    conn.Open();

                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @mobileno = accountid.Split(new string[] { "_" }, StringSplitOptions.None)[1],
                         @domain_id = accountid.Split(new string[] { "_" }, StringSplitOptions.None)[0]
                     },
                     commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0 && result.ElementAt(0).errcode != null && result.ElementAt(0).errcode == 0)
                    {
                        custName = result.ElementAt(0).name;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return custName;
        }

        private string GetMobileNo(string accountid)
        {
            string custName = "";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MVNO.CTP"].ConnectionString))
                {
                    var sp = "ctp_get_mobileno";

                    conn.Open();

                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @accountid = accountid
                     },
                     commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        custName = result.ElementAt(0).mobileno;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return custName;
        } 
    }
}
