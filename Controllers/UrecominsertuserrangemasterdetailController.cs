using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrecominsertuserrangemasterdetailController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrecominsertuserrangemasterdetailInput
        {
			public int Range_typeid { get; set; }
			public int From_user_Range { get; set; }
			public int To_user_Range { get; set; }
			public int processtype { get; set; }  	                   	    
        }
        public class UrecominsertuserrangemasterdetailOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrecominsertuserrangemasterdetailInput req)
        {
            List<UrecominsertuserrangemasterdetailOutput> OutputList = new List<UrecominsertuserrangemasterdetailOutput>();
			Log.Info("Input : UrecominsertuserrangemasterdetailController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ECom_insert_User_Range_Master_Detail";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Range_typeid = req.Range_typeid,
								@From_user_Range = req.From_user_Range,
								@To_user_Range = req.To_user_Range,
								@processtype = req.processtype 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrecominsertuserrangemasterdetailOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrecominsertuserrangemasterdetailOutput outputobj = new UrecominsertuserrangemasterdetailOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrecominsertuserrangemasterdetailOutput outputobj = new UrecominsertuserrangemasterdetailOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
