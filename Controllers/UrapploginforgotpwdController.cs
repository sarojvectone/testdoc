using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using System.Net.Mail;
using System.Text;
using System.IO;
using Newtonsoft.Json;

namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrapploginforgotpwdController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrapploginforgotpwdInput
        {
			public string email { get; set; }
			public string new_pwd { get; set; }
			public int type { get; set; }
            public string called_by { get; set; }
            public int log_id { get; set; }  	      
        }
        public class UrapploginforgotpwdOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public string first_name { get; set; }
            public int log_id { get; set; }
            public string ResetURL { get; set; }
            
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrapploginforgotpwdInput req)
        {
            Log.Info("UrapploginforgotpwdController : Input : " + JsonConvert.SerializeObject(req));
           
            List<UrapploginforgotpwdOutput> OutputList = new List<UrapploginforgotpwdOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_login_forgot_pwd";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@email = req.email,
								@new_pwd = req.new_pwd,
								@type = req.type,
                                @called_by = req.called_by,
                                @log_id = req.log_id         
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrapploginforgotpwdOutput()
                        {
                            first_name = r.first_name,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg,
                            log_id = r.log_id == null ? 0 : r.log_id
                        }));
                        //Date : 22-10-2019:Called by added for PHP as migrated to Hubspot
                      
                        //Send Mail
                            Log.Info("Send Mail..");
                            string URL = ConfigurationManager.AppSettings["EmailResetURL"];                           
                            string usermailId_Encrypted = Base64Encode(req.email);
                            string medium = Base64Encode(req.called_by);
                            string frame = Base64Encode(OutputList[0].log_id.ToString());
                            string body = URL + usermailId_Encrypted + "&medium=" + medium + "&frame=" + frame;
                            if (OutputList[0].errcode == 0 && req.type == 1 && req.called_by != "WEB")
                            { 
                                SendMail(URL, usermailId_Encrypted, medium, "", "Reset your UnifiedRing password", req.email, OutputList[0].first_name,frame);
                            }
                            OutputList[0].ResetURL = body;
                    }
                    else
                    {
                        UrapploginforgotpwdOutput outputobj = new UrapploginforgotpwdOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("UrapploginforgotpwdController : Error : " + ex.Message);
                UrapploginforgotpwdOutput outputobj = new UrapploginforgotpwdOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            Log.Info("UrapploginforgotpwdController  : Output : " + JsonConvert.SerializeObject(OutputList));
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        private static void SendMail(string requestUrl, string payload,string medium, string msisdn, string subject,string toaddress,string firstname,string frame)
        {
           
         
            try
            {
                string from = ConfigurationManager.AppSettings["FromList"];

                StreamReader mailContent_file = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Template", "UnifiedRing.html"));
                string body = requestUrl + payload + "&medium=" + medium + "&frame=" + frame;
                string mailContent = mailContent_file.ReadToEnd();
                mailContent = mailContent.Replace("*|reset_url|*", body);
                mailContent = mailContent.Replace("*|SUBJECT|*", subject);
                mailContent = mailContent.Replace("*|UserName|*", firstname);
                mailContent = mailContent.Replace("*|MC_PREVIEW_TEXT|*", "Reset Password");
                
                
                mailContent_file.Close();
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.To.Add(toaddress);
                mail.From = new MailAddress(from, "Unified ring");
                mail.Subject = subject + msisdn;
                mail.IsBodyHtml = true;
                mail.Body = mailContent;
               // mail.BodyEncoding = Encoding.UTF8;
                SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"]);
                emailClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SmtpUser"], ConfigurationManager.AppSettings["SmtpPass"]);
                emailClient.Send(mail);
            }
            catch (Exception ex)
            {
                Log.Info("Send Mail failed..",ex.Message.ToString());
               
            }
            Log.Info("Send Mail completed..");
            
        }

    }
}
