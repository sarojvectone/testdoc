using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappgetnotificationsettingController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappgetnotificationsettingInput
        {
			public int app_log_id { get; set; }
			public int company_id { get; set; }  	                   	    
        }
        public class UrappgetnotificationsettingOutput
        {
			public int? notification_recive { get; set; }
			public int? notification_sound { get; set; }
			public int? notification_team { get; set; }
			public int? notification_direct_msg { get; set; }
			public int? notification_mention { get; set; }
			public int? notification_call_voice_text { get; set; }
            public int? notification_join_now { get; set; }
            public int? notification_email_mention { get; set; }
            public int? notification_email_daily_digest { get; set; }
            public int? notification_email_direct_msg { get; set; }
            public int? notification_email_team_msg { get; set; }
            public int? notification_email_unread_badge_count { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappgetnotificationsettingInput req)
        {
            List<UrappgetnotificationsettingOutput> OutputList = new List<UrappgetnotificationsettingOutput>();
			Log.Info("Input : UrappgetnotificationsettingController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_get_notification_setting";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@app_log_id = req.app_log_id,
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrappgetnotificationsettingOutput()
                        {

                            notification_join_now = r.notification_join_now == null ? 0 : r.notification_join_now,
                            notification_email_mention = r.notification_email_mention == null ? 0 : r.notification_email_mention,
                            notification_email_daily_digest = r.notification_email_daily_digest == null ? 0 : r.notification_email_daily_digest,
                            notification_email_direct_msg = r.notification_email_direct_msg == null ? 0 : r.notification_email_direct_msg,
                            notification_email_team_msg = r.notification_email_team_msg == null ? 0 : r.notification_email_team_msg,
                            notification_email_unread_badge_count = r.notification_email_unread_badge_count == null ? 0 : r.notification_email_unread_badge_count,
							notification_recive = r.notification_recive == null ? 0 : r.notification_recive,
							notification_sound = r.notification_sound == null ? 0 : r.notification_sound,
							notification_team = r.notification_team == null ? 0 : r.notification_team,
							notification_direct_msg = r.notification_direct_msg == null ? 0 : r.notification_direct_msg,
							notification_mention = r.notification_mention == null ? 0 : r.notification_mention,
							notification_call_voice_text = r.notification_call_voice_text == null ? 0 : r.notification_call_voice_text,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrappgetnotificationsettingOutput outputobj = new UrappgetnotificationsettingOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrappgetnotificationsettingOutput outputobj = new UrappgetnotificationsettingOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
