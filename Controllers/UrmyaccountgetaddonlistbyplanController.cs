using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountgetaddonlistbyplanController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountgetaddonlistbyplanInput
        {
			public int planid { get; set; }
			public int user_type { get; set; }  	                   	    
        }
        public class UrmyaccountgetaddonlistbyplanOutput
        {
			public string addon_name { get; set; }
			public int? addon_id { get; set; }
			public double? addon_price { get; set; }
            public double? company_addon_price { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountgetaddonlistbyplanInput req)
        {
            List<UrmyaccountgetaddonlistbyplanOutput> OutputList = new List<UrmyaccountgetaddonlistbyplanOutput>();
			Log.Info("Input : UrmyaccountgetaddonlistbyplanController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_get_addon_list_by_plan";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@planid = req.planid,
								@user_type = req.user_type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountgetaddonlistbyplanOutput()
                        {
							addon_name = r.addon_name,
							addon_id = r.addon_id == null ? 0 : r.addon_id,
							addon_price = r.addon_price == null ? 0.0D : r.addon_price,
                            company_addon_price = r.company_addon_price == null ? 0.0D : r.company_addon_price,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountgetaddonlistbyplanOutput outputobj = new UrmyaccountgetaddonlistbyplanOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountgetaddonlistbyplanOutput outputobj = new UrmyaccountgetaddonlistbyplanOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
