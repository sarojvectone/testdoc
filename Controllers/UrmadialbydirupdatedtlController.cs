using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmadialbydirupdatedtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmadialbydirupdatedtlInput
        {
			public int company_id { get; set; }
			public int domain_id { get; set; }
			public int dir_enable { get; set; }
			public int dir_search_by { get; set; }
			public int dir_extension { get; set; }
			public string user_extension { get; set; }
			public int type { get; set; }
            public int site_id { get; set; }       	    
        }
        public class UrmadialbydirupdatedtlOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmadialbydirupdatedtlInput req)
        {
            List<UrmadialbydirupdatedtlOutput> OutputList = new List<UrmadialbydirupdatedtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_dialby_dir_update_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@domain_id = req.domain_id,
								@dir_enable = req.dir_enable,
								@dir_search_by = req.dir_search_by,
								@dir_extension = req.dir_extension,
								@user_extension = req.user_extension,
								@type = req.type,
                                @site_id = req.site_id                                    
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmadialbydirupdatedtlOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmadialbydirupdatedtlOutput outputobj = new UrmadialbydirupdatedtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmadialbydirupdatedtlOutput outputobj = new UrmadialbydirupdatedtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
