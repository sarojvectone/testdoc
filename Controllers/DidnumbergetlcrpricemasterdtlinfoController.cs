﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Unifiedringmyaccountwebapi.Helper;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class DidnumbergetlcrpricemasterdtlinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        List<WebsitecentralbicsgetinventoryOutput> output = new List<WebsitecentralbicsgetinventoryOutput>();
        List<WebsitecentralTELESERVOutput> teleservoutput = new List<WebsitecentralTELESERVOutput>();
        public class DidnumbergetlcrpricemasterdtlinfoInput
        {
            public string country { get; set; }
            public string location { get; set; }
            public string area_code { get; set; }
        }
        public class DidnumbergetlcrpricemasterdtlinfoOutput
        {
            public string country { get; set; }
            public string location { get; set; }
            public string area_code { get; set; }
            public double? monthly_fee { get; set; }
            public double? installation_fee { get; set; }
            public string source_name { get; set; }
            public string currency { get; set; }
            public string number_Type { get; set; }
            public string status { get; set; }
            public string iso3 { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public class OutputResult
        {
            public int id { get; set; }
            public string number { get; set; }
            public string Prod_Source { get; set; }
           public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, DidnumbergetlcrpricemasterdtlinfoInput req)
        {
            List<DidnumbergetlcrpricemasterdtlinfoOutput> OutputList = new List<DidnumbergetlcrpricemasterdtlinfoOutput>();

            List<OutputResult> objListOutputResult = new List<OutputResult>();
            Log.Info("Didnumbergetlcrpricemasterdtlinfo : Input : " + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Didportalapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "did_number_get_lcr_price_master_dtl_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @country = req.country,
                                @location = req.location,
                                @area_code = req.area_code
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new DidnumbergetlcrpricemasterdtlinfoOutput()
                        {
                            country = r.country,
                            location = r.location,
                            area_code = r.area_code,
                            //monthly_fee = r.monthly_fee == null ? 0.0D : r.monthly_fee,
                            //installation_fee = r.installation_fee == null ? 0.0D : r.installation_fee,
                            source_name = r.source_name,
                            //currency = r.currency,
                            // number_Type= r.number_Type, 
                            status = "Active",
                            iso3 = r.iso3,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));

                        #region BICS Calling
                        //Calling for bics

                        if (OutputList[0].source_name == "BICS")
                        {
                            string product = "IBN";
                            string country = OutputList[0].iso3;
                            string areaCode = OutputList[0].area_code;
                            try
                            {
                                output = Utility.GetInventory(product, country, areaCode);

                                return Request.CreateResponse(HttpStatusCode.OK, output[0].lstGetInventory);
                            }
                            catch (System.Exception ex)
                            {
                                Log.Error("WebsitecentralbicsgetinventoryController : " + ex.Message);
                                output[0].errcode = -1;
                                output[0].errmsg = ex.Message;
                            }
                            Log.Info("BISC  : Output : {0}", JsonConvert.SerializeObject(output[0].lstGetInventory));
                            return Request.CreateResponse(HttpStatusCode.OK, output);

                        }
                        #endregion
                        #region TELSERV Calling
                        if (OutputList[0].source_name == "TELSERV")
                        {
                            string country = OutputList[0].country;
                            string city = OutputList[0].location;

                            try
                            {
                                teleservoutput = Utility.GetInventoryTELSERVDetails(country, city);
                                //if (teleservoutput[0].lstGetTELESERVNUMBERS != null && teleservoutput[0].lstGetTELESERVNUMBERS.Count == 0)
                                //{
                                //}
                                //return Request.CreateResponse(HttpStatusCode.OK, teleservoutput[0].lstGetTELESERVNUMBERS);
                                return Request.CreateResponse(HttpStatusCode.OK, teleservoutput);
                            }
                            catch (System.Exception ex)
                            {
                                Log.Error("TeleservController : " + ex.Message);
                                output[0].errcode = -1;
                                output[0].errmsg = ex.Message;
                            }
                            Log.Info("TeleservController : Output : {0}", JsonConvert.SerializeObject(teleservoutput));
                            return Request.CreateResponse(HttpStatusCode.OK, teleservoutput);

                        }

                        #endregion
                        #region DIDWW Calling
                        if (OutputList[0].source_name == "DIDWW")
                        {
                            string InCountry = OutputList[0].country;
                            string InCity = OutputList[0].location;
                            List<Unifiedringmyaccountwebapi.Helper.Utility.resultNumbers> objDIDWW = new List<Utility.resultNumbers>();
                            try
                            {
                                objDIDWW = Utility.GetInventoryDIDWW(InCountry, InCity);
                                return Request.CreateResponse(HttpStatusCode.OK, objDIDWW);
                            }
                            catch (System.Exception ex)
                            {
                                Log.Error("DIDWWController : " + ex.Message);
                                output[0].errcode = -1;
                                output[0].errmsg = ex.Message;
                            }
                            Log.Info(" DIDWWController : Output : {0}", JsonConvert.SerializeObject(objDIDWW));
                            return Request.CreateResponse(HttpStatusCode.OK, objDIDWW);


                        }
                        #endregion

                    }
                    else
                    {
                        OutputResult outputobj = new OutputResult();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        objListOutputResult.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                OutputResult outputobj = new OutputResult();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                objListOutputResult.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, objListOutputResult);
        }
    }
}