using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using System.IO;
using System.Net.Mail;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebupdateextensionloginpasswordController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebupdateextensionloginpasswordInput
        {
            public int company_id { get; set; }
            public int extension { get; set; }
            public string email { get; set; }
            public string password { get; set; }
            public int type  { get; set; }
        }
        public class UrmyaccwebupdateextensionloginpasswordOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebupdateextensionloginpasswordInput req)
        {
            List<UrmyaccwebupdateextensionloginpasswordOutput> OutputList = new List<UrmyaccwebupdateextensionloginpasswordOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_update_extension_login_password";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                company_id = req.company_id,
                                extension = req.extension,
                                email = req.email,
                                password = req.password,
                                type = req.type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebupdateextensionloginpasswordOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebupdateextensionloginpasswordOutput outputobj = new UrmyaccwebupdateextensionloginpasswordOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebupdateextensionloginpasswordOutput outputobj = new UrmyaccwebupdateextensionloginpasswordOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
        private static void SendMail(string UserName, string InvitedPersonEmail, string InvitedUserName, string AppLink, string subject)
        {
            try
            {
                string from = ConfigurationManager.AppSettings["FromList"];

                StreamReader mailContent_file = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Template", "UnifiedRing_Guest_User.html"));
                string mailContent = mailContent_file.ReadToEnd();
                mailContent = mailContent.Replace("*|UserName|*", UserName);
                mailContent = mailContent.Replace("*|InvitedUserName|*", InvitedUserName);
                mailContent = mailContent.Replace("*|SUBJECT|*", subject);
                mailContent = mailContent.Replace("*|AppLink|*", AppLink);
                mailContent = mailContent.Replace("*|MC_PREVIEW_TEXT|*", "Unifiedring Invitation");

                mailContent_file.Close();
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.To.Add(InvitedPersonEmail);
                mail.From = new MailAddress(from, "Unified ring");
                mail.Subject = subject;
                mail.IsBodyHtml = true;
                mail.Body = mailContent;
                // mail.BodyEncoding = Encoding.UTF8;
                SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["SmtpHost"]);
                emailClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SmtpUser"], ConfigurationManager.AppSettings["SmtpPass"]);
                emailClient.Send(mail);
            }
            catch (Exception ex)
            {
                Log.Info("Send Mail failed..", ex.Message.ToString());

            }
            Log.Info("Send Mail completed..");

        }
    }
}
