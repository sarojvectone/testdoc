using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
using System.IO;
using System.Web.Script.Serialization;

namespace Mobilecmsapi.Controllers
{
    public class UrcreateandupdatemandateController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrcreateandupdatemandateInput
        {
            public string account_number { get; set; }
            public string CustBankID { get; set; }
            public string Contract { get; set; }
            public int company_id { get; set; }
            public DateTime subscription_date { get; set; }
            public string email { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string city { get; set; }
            public string postcode { get; set; }
            public string countrycode { get; set; }
            public string Cust_Contract { get; set; }
            public string sort_code { get; set; }
            public string account_name { get; set; }
            public string description_plan { get; set; }
            public string bank_reference { get; set; }
            public string sitecode { get; set; }
            public bool isinsert { get; set; }
            public string subscribrid { get; set; }
        }

        public class UrcreateandupdatemandateOutput
        {
            public string mandateid { get; set; }
            public string mandatestatus { get; set; }
            public string gocard_customerid { get; set; }
            public string pp_customer_id { get; set; }
            public int isvalid { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrcreateandupdatemandateInput req)
        {
            Log.Info("Urcreateandupdatemandate");
            UrcreateandupdatemandateOutput output = new UrcreateandupdatemandateOutput();
            try
            {
                if (req != null)
                {
                    Log.Info("Input : " + JsonConvert.SerializeObject(req));
                    if (req.isinsert)
                    {
                        output = CreateMandate(req);
                    }
                    else
                    {
                        output = UpdateMandate(req);
                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = "Input details not found";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            Log.Info("Output : " + JsonConvert.SerializeObject(output));
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }

        private UrcreateandupdatemandateOutput UpdateMandate(UrcreateandupdatemandateInput req)
        {
            UrcreateandupdatemandateOutput output = new UrcreateandupdatemandateOutput();
            output.errcode = -1;
            output.errmsg = "Failure";
            try
            {
                string gocardcustomer = goCardnewcustomer(req.firstname.ToString(), req.email, req.lastname.ToString(), req.address1.ToString(), req.address2.ToString(), req.city, req.postcode.ToString(), req.countrycode.ToString(), req.Cust_Contract.ToString());
                if (gocardcustomer == "Error")
                {

                }
                else
                {
                    //create customer bankid
                    string[] retVal = gocardcustomer.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    string CustID = "";
                    CustID = retVal[0].Split(':')[2].ToString(); ;
                    CustID = CustID.Replace("\"", "");
                    string gocardbankaccount = goCardnewbankAccount(req.firstname.ToString(), req.account_number, req.sort_code, req.countrycode, CustID);

                    if (gocardbankaccount == "Error")
                    {

                    }
                    else
                    {
                        retVal = gocardbankaccount.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        string CustBankID = "";
                        CustBankID = retVal[0].Split(':')[2].ToString(); ;
                        CustBankID = CustBankID.Replace("\"", "");
                        string createmandate = goCardNewMandate(req.company_id.ToString(), CustBankID, req.Cust_Contract.ToString());
                        if (createmandate == "Error")
                        {

                        }
                        else
                        {
                            string mandateid = "";
                            retVal = createmandate.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            mandateid = retVal[0].Split(':')[2].ToString(); ;
                            mandateid = mandateid.Replace("\"", "");
                            string mandatestatus = retVal[3].Split(':')[1].ToString();
                            mandatestatus = mandatestatus.Replace("\"", "");

                            output.errcode = 0;
                            output.isvalid = 0;
                            output.errmsg = "Success";
                            output.mandateid = mandateid;
                            output.mandatestatus = mandatestatus;
                            output.gocard_customerid = CustID;
                            if (!String.IsNullOrEmpty(req.subscribrid))
                                output.pp_customer_id = req.Cust_Contract;

                            string paymentGocardresult = "";
                            paymentGocardresult = MandateUpdate(req.company_id, req.account_number, mandateid, 1, CustID, mandatestatus);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Update Mandate : " + ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return output;
        }

        private UrcreateandupdatemandateOutput CreateMandate(UrcreateandupdatemandateInput req)
        {
            UrcreateandupdatemandateOutput output = new UrcreateandupdatemandateOutput();
            output.errcode = -1;
            output.errmsg = "Failure";
            try
            {
                string gocardcustomer = goCardnewcustomer(req.firstname.ToString(), req.email, req.lastname.ToString(), req.address1.ToString(), req.address2.ToString(), req.city, req.postcode.ToString(), req.countrycode.ToString(), req.Cust_Contract.ToString());
                if (gocardcustomer == "Error")
                {

                }
                else
                {
                    //create customer bankid
                    string[] retVal = gocardcustomer.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    string CustID = "";
                    CustID = retVal[0].Split(':')[2].ToString(); ;
                    CustID = CustID.Replace("\"", "");

                    string gocardbankaccount = goCardnewbankAccount(req.firstname.ToString(), req.account_number, req.sort_code, req.countrycode, CustID);
                    if (gocardbankaccount == "Error")
                    {

                    }
                    else
                    {
                        retVal = gocardbankaccount.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        string CustBankID = "";
                        CustBankID = retVal[0].Split(':')[2].ToString(); ;
                        CustBankID = CustBankID.Replace("\"", "");
                        string createmandate = goCardNewMandate(Convert.ToString(req.company_id), CustBankID, req.Cust_Contract.ToString());
                        if (createmandate == "Error")
                        {

                        }
                        else
                        {
                            string mandateid = "";
                            retVal = createmandate.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            mandateid = retVal[0].Split(':')[2].ToString(); ;
                            mandateid = mandateid.Replace("\"", "");
                            string mandatestatus = retVal[3].Split(':')[1].ToString();
                            mandatestatus = mandatestatus.Replace("\"", "");

                            output.errcode = 0;
                            output.isvalid = 0;
                            output.errmsg = "Success";
                            output.mandateid = mandateid;
                            output.mandatestatus = mandatestatus;
                            output.gocard_customerid = CustID;
                            if (!String.IsNullOrEmpty(req.subscribrid))
                                output.pp_customer_id = req.Cust_Contract;

                            string paymentGocardresult = "";
                            paymentGocardresult = MandateInsert(req.company_id, req.account_number, mandateid, 1, CustID, mandatestatus);

                            output.errmsg = paymentGocardresult;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Create Mandate : " + ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return output;
        }

        private string goCardNewMandate(string companyid, string CustBankID, string Contract)
        {
            string paymentGocardresult = "Error";
            MandatesInput GMI = new MandatesInput();
            GMI.scheme = "bacs";
            MandateLinks GML = new MandateLinks();
            GML.customer_bank_account = CustBankID; // "BA0001WPDC9NXP";  //  Praveen Bank Account
            GML.creditor = "CR0000B22HFFS3";
            MandateIn GMIn = new MandateIn();
            GMIn.contract = Contract; // "VMUK447465047756";
            GMI.metadata = GMIn;
            GMI.links = GML;
            MandateCreate GMCobj = new MandateCreate();
            GMCobj.mandates = GMI;
            try
            {
                //19-Nov-2019 : Moorthy : Added for TLS1.2
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                //  GoCardless Bank Account Creation 
                var httpGocardRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["GOCARDLESSURL"] + "mandates");
                string authenticationtoken = ConfigurationManager.AppSettings["GOCARDLESSKEY"];
                httpGocardRequest.UserAgent = "Fiddler";
                httpGocardRequest.Headers.Add("GoCardless-Version", "2015-07-06");
                httpGocardRequest.Headers.Add("Authorization", "Bearer " + authenticationtoken);


                httpGocardRequest.Accept = "application/json";
                httpGocardRequest.ContentType = "application/json";
                httpGocardRequest.Timeout = 180000;
                httpGocardRequest.Method = "POST";
                using (var streamGocardWriters = new StreamWriter(httpGocardRequest.GetRequestStream()))
                {
                    string jsonGocard = new JavaScriptSerializer().Serialize(GMCobj);
                    streamGocardWriters.Write(jsonGocard);
                    streamGocardWriters.Flush();
                    streamGocardWriters.Dispose();
                    HttpWebResponse httpGocardResponce = (HttpWebResponse)httpGocardRequest.GetResponse();
                    Stream paymentGocardStream = httpGocardResponce.GetResponseStream();
                    StreamReader paymentGocardreader = new StreamReader(paymentGocardStream);
                    paymentGocardresult = paymentGocardreader.ReadToEnd();
                    paymentGocardStream.Close();
                    paymentGocardreader.Close();
                }
            }
            catch (WebException ex)
            {
                Console.Write(ex);
            }
            return paymentGocardresult;

        } 

        private string goCardnewbankAccount(string ACholdername, string ACnumber, string ACbranch, string countrycode, string customerid)
        {
            string paymentGocardresult = "Error";
            CustomerBankAccounts GCBA = new CustomerBankAccounts();
            GCBA.account_holder_name = ACholdername;
            GCBA.account_number = ACnumber;
            GCBA.branch_code = ACbranch; // Sortcode
            GCBA.country_code = countrycode; // Twodigit
            CustBankLinks GCBL = new CustBankLinks();
            GCBL.customer = customerid;// "CU000252WCM7CZ"
            GCBA.links = GCBL;
            CustBankMainInput myBankAcc = new CustBankMainInput();
            myBankAcc.customer_bank_accounts = GCBA;
            try
            {
                //19-Nov-2019 : Moorthy : Added for TLS1.2
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                //  GoCardless Bank Account Creation
                var httpGocardRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["GOCARDLESSURL"] + "customer_bank_accounts");
                string authenticationtoken = ConfigurationManager.AppSettings["GOCARDLESSKEY"];
                httpGocardRequest.UserAgent = "Fiddler";
                httpGocardRequest.Headers.Add("GoCardless-Version", "2015-07-06");
                httpGocardRequest.Headers.Add("Authorization", "Bearer " + authenticationtoken);

                httpGocardRequest.Accept = "application/json";
                httpGocardRequest.ContentType = "application/json";
                httpGocardRequest.Timeout = 180000;
                httpGocardRequest.Method = "POST";
                using (var streamGocardWriters = new StreamWriter(httpGocardRequest.GetRequestStream()))
                {
                    string jsonGocard = new JavaScriptSerializer().Serialize(myBankAcc);
                    streamGocardWriters.Write(jsonGocard);
                    streamGocardWriters.Flush();
                    streamGocardWriters.Dispose();
                    HttpWebResponse httpGocardResponce = (HttpWebResponse)httpGocardRequest.GetResponse();
                    Stream paymentGocardStream = httpGocardResponce.GetResponseStream();
                    StreamReader paymentGocardreader = new StreamReader(paymentGocardStream);
                    paymentGocardresult = paymentGocardreader.ReadToEnd();
                    paymentGocardStream.Close();
                    paymentGocardreader.Close();
                }
            }
            catch (WebException ex)
            {
                Log.Error("goCardnewbankAccount : " + ex.Message);
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    try
                    {
                        HttpWebResponse err = ex.Response as HttpWebResponse;
                        if (err != null)
                        {
                            string errResponse = new StreamReader(err.GetResponseStream()).ReadToEnd();
                        }
                    }
                    catch (Exception innerEx)
                    {
                    }
                }
            }
            return paymentGocardresult;
        }
        private string MandateInsert(int company_id, string account_number, string mandate_id, int isvalid, string gocard_customerid, string mandate_status)
        {
            string returnResult = "Error";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_gocard_create_mandate_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = company_id,
                                @account_number = account_number,
                                @mandate_id = mandate_id,
                                @isvalid = isvalid,
                                @gocard_customerid = gocard_customerid,
                                @mandate_status = mandate_status
                            },
                            commandType: CommandType.StoredProcedure);
                   returnResult =  result.ElementAt(0).errmsg;
                   // returnResult = "Success";
                }
            }
            catch (Exception ex)
            {
                Log.Error("MandateInsert : " + ex.Message);
            }
            return returnResult;
        }

        private string MandateUpdate(int companyid, string accountnumber, string mandate_id, int isvalid, string gocard_customerid, string mandate_status)
        {
            string returnResult = "Error";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AT_CRM"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_gocard_create_mandate_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = companyid,
                                @account_number = accountnumber,
                                @mandate_id = mandate_id,
                                @isvalid = isvalid,
                                @gocard_customerid = gocard_customerid,
                                @mandate_status = mandate_status
                            },
                            commandType: CommandType.StoredProcedure);
                    returnResult = "Success";
                }
            }
            catch (Exception ex)
            {
                Log.Error("MandateUpdate : " + ex.Message);
            }
            return returnResult;
        }

        private static string goCardnewcustomer(string name, string email, string familyname, string address1, string address2, string city, string postcode, string countrycode, string salesid)
        {
            if (email == "")
            {
                email = "test@test.com";
            }
            if (address1 == "")
            {
                address1 = name;
            }
            if (address2 == "")
            {
                address2 = address1;
            }
            if (city == "")
            {
                city = "London";
            }
            if (postcode == "")
            {
                postcode = city;
            }
            if (countrycode == "")
            {
                postcode = "GB";
            }
            string paymentGocardresult = "Error";
            GoCardCustomers cs = new GoCardCustomers();
            cs.email = email;
            cs.given_name = name;
            cs.family_name = familyname;
            cs.address_line1 = address1;
            cs.address_line2 = address2;
            cs.city = city;
            cs.postal_code = postcode;
            cs.country_code = countrycode;
            MetadataType md = new MetadataType();
            md.salesforce_id = salesid;
            EnterGoCard myobject = new EnterGoCard();
            cs.metadata = md;
            myobject.customers = cs;

            try
            {
                //19-Nov-2019 : Moorthy : Added for TLS1.2
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                //Go-cardless payment
                var httpGocardRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["GOCARDLESSURL"] + "customers");
                string authenticationtoken = ConfigurationManager.AppSettings["GOCARDLESSKEY"];
                httpGocardRequest.UserAgent = "Fiddler";
                httpGocardRequest.Headers.Add("GoCardless-Version", "2015-07-06");
                httpGocardRequest.Headers.Add("Authorization", "Bearer " + authenticationtoken);
                httpGocardRequest.Accept = "application/json";
                httpGocardRequest.ContentType = "application/json";
                httpGocardRequest.Timeout = 180000;
                httpGocardRequest.Method = "POST";
                using (var streamGocardWriters = new StreamWriter(httpGocardRequest.GetRequestStream()))
                {
                    string jsonGocard = new JavaScriptSerializer().Serialize(myobject);
                    streamGocardWriters.Write(jsonGocard);
                    streamGocardWriters.Flush();
                    streamGocardWriters.Dispose();
                    HttpWebResponse httpGocardResponce = (HttpWebResponse)httpGocardRequest.GetResponse();
                    Stream paymentGocardStream = httpGocardResponce.GetResponseStream();
                    StreamReader paymentGocardreader = new StreamReader(paymentGocardStream);
                    paymentGocardresult = paymentGocardreader.ReadToEnd();
                    paymentGocardStream.Close();
                    paymentGocardreader.Close();
                }
            }
            catch (WebException ex)
            {
                Log.Error("goCardnewcustomer : " + ex.Message);
            }
            return paymentGocardresult;
        }

        public class GoCardCustomers
        {
            public string email { get; set; }
            public string given_name { get; set; }
            public string family_name { get; set; }
            public string address_line1 { get; set; }
            public string address_line2 { get; set; }
            public string city { get; set; }
            public string postal_code { get; set; }
            public string country_code { get; set; }
            public MetadataType metadata { get; set; }
        }

        public class MetadataType
        {
            public string salesforce_id { get; set; }
        }

        public class EnterGoCard
        {
            public GoCardCustomers customers { get; set; }
        }

        static string BytesToStringConverted(byte[] bytes)
        {
            using (var stream = new MemoryStream(bytes))
            {
                using (var streamReader = new StreamReader(stream))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }

        public class CustomerBankAccounts
        {
            public string account_number { get; set; }
            public string branch_code { get; set; }
            public string account_holder_name { get; set; }
            public string country_code { get; set; }
            public CustBankLinks links { get; set; }
        }

        public class CustBankLinks
        {
            public string customer { get; set; }
        }

        public class CustBankMainInput
        {
            public CustomerBankAccounts customer_bank_accounts { get; set; }
        }

        public class MandatesInput
        {
            public string scheme { get; set; }
            public MandateIn metadata { get; set; }
            public MandateLinks links { get; set; }
        }

        public class MandateLinks
        {
            public string customer_bank_account { get; set; }
            public string creditor { get; set; }
        }

        public class MandateIn
        {
            public string contract { get; set; }
        }

        public class Mandatereinstate
        {
            public metadatareinstate metadata { get; set; }
        }

        public class metadatareinstate
        {
            public string ticket_id { get; set; }
        }

        public class MandatereinstateCreate
        {
            public Mandatereinstate data { get; set; }
        }

        public class MandateCreate
        {
            public MandatesInput mandates { get; set; }
        }
    }
}
