using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace unifiedringappapi.Controllers
{
    public class GetGoogleUserController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class GoogleUserInput
        {
            public string AccessToken { get; set; }
        }
        public class GoogleUserOutput
        {

            [JsonProperty("id")]
            public string Id { get; set; }

            [JsonProperty("email")]
            public string Email { get; set; }

            [JsonProperty("email_verified")]
            public bool email_verified { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("given_name")]
            public string GivenName { get; set; }

            [JsonProperty("family_name")]
            public string FamilyName { get; set; }

            [JsonProperty("link")]
            public string Link { get; set; }

            [JsonProperty("picture")]
            public string Picture { get; set; }

            [JsonProperty("gender")]
            public string Gender { get; set; }

            [JsonProperty("locale")]
            public string Locale { get; set; }

            public int? error_code { get; set; }
            public string error_msg { get; set; }
        }
         [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, GoogleUserInput req)
        {
            List<GoogleUserOutput> OutputList = new List<GoogleUserOutput>();
            try
            {
                Log.Info("GetGoogleUserController");
                Log.Info("Input : " + JsonConvert.SerializeObject(req));
                //GoogleUserOutput obj = await GetuserProfile(req.AccessToken);//This is for access token
                GoogleUserOutput obj = await GetuserProfileIDToken(req.AccessToken);//This is for access token
                obj.error_code = 0;
                obj.error_msg = "Success";
                OutputList.Add(obj);
                Log.Info("Output : " + JsonConvert.SerializeObject(OutputList));
                
            }
            catch (Exception ex)
            {

                GoogleUserOutput outputobj = new GoogleUserOutput();
                outputobj.error_code = -1;
                outputobj.error_msg = ex.Message;
                OutputList.Add(outputobj);
                Log.Error("Output Error: " + ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        
        }
        public async Task<GoogleUserOutput> GetuserProfile(string accesstoken)
        {
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://www.googleapis.com")
            };
            //string url = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token="+accesstoken;
            string url = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json";
            httpClient.DefaultRequestHeaders.Authorization
                         = new AuthenticationHeaderValue("Bearer", accesstoken);
            var response = await httpClient.GetAsync(url);
            return JsonConvert.DeserializeObject<GoogleUserOutput>(await response.Content.ReadAsStringAsync());
        }
        public async Task<GoogleUserOutput> GetuserProfileIDToken(string idtoken)
        {
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://oauth2.googleapis.com")
            };
            string url = "https://oauth2.googleapis.com/tokeninfo?id_token=" + idtoken;
            //httpClient.DefaultRequestHeaders.Authorization
            //             = new AuthenticationHeaderValue("Bearer", accesstoken);
            var response = await httpClient.GetAsync(url);
            return JsonConvert.DeserializeObject<GoogleUserOutput>(await response.Content.ReadAsStringAsync());
        }

    }

}
