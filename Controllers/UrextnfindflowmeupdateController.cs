using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrextnfindflowmeupdateController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrextnfindflowmeupdateInput
        {
			public int domain_id { get; set; }
			public int id { get; set; }
			public int status { get; set; }
			public int type { get; set; }  	                   	    
        }
        public class UrextnfindflowmeupdateOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrextnfindflowmeupdateInput req)
        {
            List<UrextnfindflowmeupdateOutput> OutputList = new List<UrextnfindflowmeupdateOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_extn_find_flow_me_update";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id,
								@id = req.id,
								@status = req.status,
								@type = req.type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrextnfindflowmeupdateOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrextnfindflowmeupdateOutput outputobj = new UrextnfindflowmeupdateOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrextnfindflowmeupdateOutput outputobj = new UrextnfindflowmeupdateOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
