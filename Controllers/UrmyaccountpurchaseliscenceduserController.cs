using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountpurchaseliscenceduserController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountpurchaseliscenceduserInput
        {
			public int company_id { get; set; }
			public int no_liscence { get; set; }
			public double? price_per_user { get; set; }
			public double? tax_per { get; set; }
			public double? tax_fee { get; set; }
			public double? total_charge { get; set; }
			public double? prorata_adjustment { get; set; }
			public string pay_reference { get; set; }
			public string login_user { get; set; }
			public string didno { get; set; }  	                   	    
        }
        public class UrmyaccountpurchaseliscenceduserOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountpurchaseliscenceduserInput req)
        {
            List<UrmyaccountpurchaseliscenceduserOutput> OutputList = new List<UrmyaccountpurchaseliscenceduserOutput>();
			Log.Info("Input : UrmyaccountpurchaseliscenceduserController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_purchase_liscenced_user";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@no_liscence = req.no_liscence,
								@price_per_user = req.price_per_user,
								@tax_per = req.tax_per,
								@tax_fee = req.tax_fee,
								@total_charge = req.total_charge,
								@prorata_adjustment = req.prorata_adjustment,
								@pay_reference = req.pay_reference,
								@login_user = req.login_user,
								@didno = req.didno 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountpurchaseliscenceduserOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountpurchaseliscenceduserOutput outputobj = new UrmyaccountpurchaseliscenceduserOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountpurchaseliscenceduserOutput outputobj = new UrmyaccountpurchaseliscenceduserOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
