using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappcreateincomingcallcustomnumberController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappcreateincomingcallcustomnumberInput
        {
			public int company_id { get; set; }
			public int extension { get; set; }
			public string dev_product_name { get; set; }
            public string number { get; set; }
            public int Ring_type { get; set; }    
        }
        public class UrappcreateincomingcallcustomnumberOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappcreateincomingcallcustomnumberInput req)
        {
            List<UrappcreateincomingcallcustomnumberOutput> OutputList = new List<UrappcreateincomingcallcustomnumberOutput>();
			Log.Info("Input : UrappcreateincomingcallcustomnumberController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_app_create_incoming_call_custom_number";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@extension = req.extension,
								@dev_product_name = req.dev_product_name,
			                    @number = req.number,
                                @Ring_type = req.Ring_type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrappcreateincomingcallcustomnumberOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrappcreateincomingcallcustomnumberOutput outputobj = new UrappcreateincomingcallcustomnumberOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrappcreateincomingcallcustomnumberOutput outputobj = new UrappcreateincomingcallcustomnumberOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
