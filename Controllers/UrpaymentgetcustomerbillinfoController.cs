using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrpaymentgetcustomerbillinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrpaymentgetcustomerbillinfoInput
        {
        }
        public class UrpaymentgetcustomerbillinfoOutput
        {
			public string period { get; set; }
			public int? company_id { get; set; }
			public string company_name { get; set; }
			public string address1 { get; set; }
			public string address2 { get; set; }
			public string address3 { get; set; }
			public string city { get; set; }
			public string country { get; set; }
			public string email { get; set; }
			public DateTime? bill_date { get; set; }
			public DateTime? bill_due_date { get; set; }
			public double? total_bill { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Get(string id, HttpRequestMessage request, UrpaymentgetcustomerbillinfoInput req)
        {
            List<UrpaymentgetcustomerbillinfoOutput> OutputList = new List<UrpaymentgetcustomerbillinfoOutput>();
			Log.Info("Input : UrpaymentgetcustomerbillinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_payment_get_customer_bill_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrpaymentgetcustomerbillinfoOutput()
                        {
							period = r.period,
							company_id = r.company_id == null ? 0 : r.company_id,
							company_name = r.company_name,
							address1 = r.address1,
							address2 = r.address2,
							address3 = r.address3,
							city = r.city,
							country = r.country,
							email = r.email,
							bill_date = r.bill_date == null ? null : r.bill_date,
							bill_due_date = r.bill_due_date == null ? null : r.bill_due_date,
							total_bill = r.total_bill == null ? 0.0D : r.total_bill,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrpaymentgetcustomerbillinfoOutput outputobj = new UrpaymentgetcustomerbillinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrpaymentgetcustomerbillinfoOutput outputobj = new UrpaymentgetcustomerbillinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
