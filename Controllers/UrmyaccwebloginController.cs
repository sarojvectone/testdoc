using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebloginController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebloginInput
        {
			public string mobileno { get; set; }
			public string password { get; set; }
            public int extension { get; set; } 
  	        public string device_type { get; set; }
            public string browser { get; set; }
            public string login_type { get; set; }
        }

        

        public class UrmyaccwebloginOutput
        {
			public int? company_id { get; set; }
			public string company_name { get; set; }
			public int? customer_id { get; set; }
			public int? order_id { get; set; }
            public DateTime? order_date { get; set; }
            public int? role_id { get; set; }
            public string role_descript { get; set; }
            public int? domain_id { get; set; }
            public string switch_board_no { get; set; }
            public string order_comp_name { get; set; }
            public DateTime? comp_last_login { get; set; }
            public int? extension { get; set; }
            public int? dnd_type { get; set; }
            public string device_type { get; set; }	
            public string browser { get; set; }	
            public string login_type { get; set; }
            public int? multi_site_enable { get; set; }
            public int? main_siteid { get; set; }
            public string completed_page { get; set; }
            public long? Hubspot_company_id{ get; set; }
            public int? status { get; set; }
            public string contactno { get; set; }
            public string employee_id { get; set; }
            public string errmsg2 { get; set; }
            public long? Hubspot_contactid { get; set; }
            public int? is_free_trial { get; set; }
            public string role_module_info { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public string First_name { get; set; }
            public string Last_name { get; set; }
            public string domain_name { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebloginInput req)
        {
            Log.Info("Urmyaccweblogin : Input : " + JsonConvert.SerializeObject(req));
            List<UrmyaccwebloginOutput> OutputList = new List<UrmyaccwebloginOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_login";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@mobileno = req.mobileno,
								@password = req.password,
                                @extension = req.extension,
                                @device_type = req.device_type,
                                @browser = req.browser,
                                @login_type = req.login_type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebloginOutput()
                        {
							company_id = r.company_id == null ? 0 : r.company_id,
							company_name = r.company_name,
							customer_id = r.customer_id == null ? 0 : r.customer_id,
                            order_id = r.order_id == null ? 0 : r.order_id,
                            order_date = r.order_date == null ? null : r.order_date,
                            role_id = r.role_id == null ? 0 : r.role_id,
                            role_descript = r.role_descript,
                            domain_id = r.domain_id == null ? 0 : r.domain_id,
                            switch_board_no = r.switch_board_no,
                            order_comp_name = r.order_comp_name,
                            comp_last_login = r.comp_last_login == null ? null : r.comp_last_login,
                            extension = r.extension == null ? 0 : r.extension,
                            device_type = r.device_type,
                            browser = r.browser,
                            login_type = r.login_type,							
                            multi_site_enable = r.multi_site_enable == null ? 0 : r.multi_site_enable,
                            dnd_type = r.dnd_type == null ? 0 : r.dnd_type,
                            main_siteid = r.main_siteid == null ? 0 : r.main_siteid,
                            completed_page = r.completed_page,
                            status = r.status == null ? 0 : r.status,
                            errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg,
                            Hubspot_company_id = r.Hubspot_company_id == null ? 0 : r.Hubspot_company_id,
                            Hubspot_contactid = r.Hubspot_contactid == null ? 0 : r.Hubspot_contactid,
                            contactno = r.contactno,
                            employee_id = r.employee_id,
                            errmsg2 = r.errmsg2,
                            role_module_info = r.role_module_info,
                            is_free_trial = r.is_free_trial == null ? 0 : r.is_free_trial,
                            First_name = r.First_name,
                            Last_name = r.Last_name,
                            domain_name = r.domain_name
                        }));
                    }
                    else
                    {
                        UrmyaccwebloginOutput outputobj = new UrmyaccwebloginOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                UrmyaccwebloginOutput outputobj = new UrmyaccwebloginOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            Log.Info("Urmyaccweblogin : Output : " + JsonConvert.SerializeObject(OutputList));
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
