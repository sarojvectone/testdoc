using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmultisitegetcompanyworkinghoursController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmultisitegetcompanyworkinghoursInput
        {
			public int Company_Id { get; set; }
            public string Extension { get; set; }
			public int Type { get; set; }
            public int site_id  { get; set; }            	    
        }
        public class UrmultisitegetcompanyworkinghoursOutput
        {
			public int? Company_Id { get; set; }
			public string Working_Business_Type { get; set; }
			public string Working_Day { get; set; }
			public TimeSpan From_Time { get; set; }
            public TimeSpan To_Time { get; set; }
			public int? Hour_Format { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmultisitegetcompanyworkinghoursInput req)
        {
            List<UrmultisitegetcompanyworkinghoursOutput> OutputList = new List<UrmultisitegetcompanyworkinghoursOutput>();
			Log.Info("Input : UrmultisitegetcompanyworkinghoursController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Multisite_Get_Company_Working_Hours";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_Id = req.Company_Id,
                                @Extension = req.Extension,
								@Type = req.Type,
                                @site_id = req.site_id                                    
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmultisitegetcompanyworkinghoursOutput()
                        {
							Company_Id = r.Company_Id == null ? 0 : r.Company_Id,
							Working_Business_Type = r.Working_Business_Type,
							Working_Day = r.Working_Day,
							From_Time = r.From_Time == null ? null : r.From_Time,
							To_Time = r.To_Time == null ? null : r.To_Time,
							Hour_Format = r.Hour_Format == null ? 0 : r.Hour_Format,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmultisitegetcompanyworkinghoursOutput outputobj = new UrmultisitegetcompanyworkinghoursOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmultisitegetcompanyworkinghoursOutput outputobj = new UrmultisitegetcompanyworkinghoursOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
