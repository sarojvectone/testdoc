﻿using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccgetsendvoicemailtoemailController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccgetsendvoicemailtoemailInput
        {
			public int id { get; set; }
			public int company_id { get; set; }
			public string extension { get; set; }  	                   	    
        }
        public class UrmyaccgetsendvoicemailtoemailOutput
        {
			public int company_id { get; set; }
			public int Extension { get; set; }
			public int Status { get; set; }
			public int id { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccgetsendvoicemailtoemailInput req)
        {
            List<UrmyaccgetsendvoicemailtoemailOutput> OutputList = new List<UrmyaccgetsendvoicemailtoemailOutput>();
			Log.Info("Input : UrmyaccgetsendvoicemailtoemailController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myacc_get_send_voicemail_to_email";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@id = req.id,
								@company_id = req.company_id,
								@extension = req.extension 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccgetsendvoicemailtoemailOutput()
                        {
							company_id = r.company_id == null ? 0 : r.company_id,
                            Extension = r.Extension == null ? 0 : r.Extension,
							Status = r.Status == null ? 0 : r.Status,
							id = r.id == null ? 0 : r.id,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccgetsendvoicemailtoemailOutput outputobj = new UrmyaccgetsendvoicemailtoemailOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccgetsendvoicemailtoemailOutput outputobj = new UrmyaccgetsendvoicemailtoemailOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}