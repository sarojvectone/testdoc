using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccountgetvoicemailgreetingsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountgetvoicemailgreetingsInput
        {
			public string extension { get; set; }
			public int domain_id { get; set; }
            public int hour_type { get; set; }            	    
        }
        public class UrmyaccountgetvoicemailgreetingsOutput
        {
			public string voicemail_type { get; set; }
			public string content { get; set; }
			public string audio_file { get; set; }
			public string custom_type { get; set; }
			public DateTime? create_date { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountgetvoicemailgreetingsInput req)
        {
            List<UrmyaccountgetvoicemailgreetingsOutput> OutputList = new List<UrmyaccountgetvoicemailgreetingsOutput>();
			Log.Info("Input : UrmyaccountgetvoicemailgreetingsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_get_voicemail_greetings";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@extension = req.extension,
								@domain_id = req.domain_id,
                                @hour_type = req.hour_type                                    
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountgetvoicemailgreetingsOutput()
                        {
							voicemail_type = r.voicemail_type,
							content = r.content,
							audio_file = r.audio_file,
							custom_type = r.custom_type,
							create_date = r.create_date == null ? null : r.create_date,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountgetvoicemailgreetingsOutput outputobj = new UrmyaccountgetvoicemailgreetingsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountgetvoicemailgreetingsOutput outputobj = new UrmyaccountgetvoicemailgreetingsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
