using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmultisitegetsiteaddressController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmultisitegetsiteaddressInput
        {
			public int Company_Id { get; set; }  	                   	    
        }
        public class UrmultisitegetsiteaddressOutput
        {
			public int? site_id { get; set; }
			public int? site_extn { get; set; }
			public string site_name { get; set; }
			public string address1 { get; set; }
			public string address2 { get; set; }
			public string address3 { get; set; }
			public string postcode { get; set; }
			public string country { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmultisitegetsiteaddressInput req)
        {
            List<UrmultisitegetsiteaddressOutput> OutputList = new List<UrmultisitegetsiteaddressOutput>();
			Log.Info("Input : UrmultisitegetsiteaddressController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Multisite_Get_site_address";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_Id = req.Company_Id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmultisitegetsiteaddressOutput()
                        {
							site_id = r.site_id == null ? 0 : r.site_id,
							site_extn = r.site_extn == null ? 0 : r.site_extn,
							site_name = r.site_name,
							address1 = r.address1,
							address2 = r.address2,
							address3 = r.address3,
							postcode = r.postcode,
							country = r.country,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmultisitegetsiteaddressOutput outputobj = new UrmultisitegetsiteaddressOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmultisitegetsiteaddressOutput outputobj = new UrmultisitegetsiteaddressOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
