using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrinsertproductdeviceController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrinsertproductdeviceInput
        {
            public string brand { get; set; }
            public double? prod_weight { get; set; }
			public int inventory_id { get; set; }
			//public int model_id { get; set; }
			public string prod_code { get; set; }
			public string prod_name { get; set; }
            public string  prod_model { get; set; }
            public int   category_id { get; set; }
			public int type { get; set; }
            public int status { get; set; }            	    
        }
        public class UrinsertproductdeviceOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrinsertproductdeviceInput req)
        {
            List<UrinsertproductdeviceOutput> OutputList = new List<UrinsertproductdeviceOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_insert_product_device";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@inventory_id = req.inventory_id,
								@prod_model  = req.prod_model,
                                @category_id = req.category_id,
								@prod_code = req.prod_code,
								@prod_name = req.prod_name,
								@type = req.type,
                                @brand = req.brand,
                                @prod_weight = req.prod_weight,
                                @status = req.status
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrinsertproductdeviceOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrinsertproductdeviceOutput outputobj = new UrinsertproductdeviceOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrinsertproductdeviceOutput outputobj = new UrinsertproductdeviceOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
