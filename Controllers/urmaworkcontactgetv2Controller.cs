using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class urmaworkcontactgetv2Controller : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class urmaworkcontactgetInput
        {
            public string login_user_name { get; set; }
            public string timestamp { get; set; }
        }

        public class urmaworkcontactgetOutput
        {
            public int? id { get; set; }
            public string mobileno { get; set; }
            public int? ext { get; set; }
            public string caller_id { get; set; }
            public string login_user_name { get; set; }
            public Int64? sip_login_id { get; set; }
            public string first_name { get; set; }
            public string Last_name { get; set; }
            public string surname { get; set; }
            public string mobile_number { get; set; }
            public string landline_number { get; set; }
            public string email_id { get; set; }
            public string department { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string Postcode { get; set; }
            public string city { get; set; }
            public string country { get; set; }
            public int is_muted { get; set; }
            public int is_blocked { get; set; }
            public bool available { get; set; }
            public string user_status { get; set; }
            public string status_msg { get; set; }
            public int company_id { get; set; }
            public string direct_no { get; set; }
            public string ImageURL { get; set; }
            public string company_name { get; set; }
            public int is_favourite { get; set; }// Added by saroj on 31/07/2019
            public string status { get; set; }// Added by saroj on 31/07/2019
            public string  role_name { get; set; }
            public int? is_archive { get; set; }

            public int?  muted_info { get; set; }
            public int? is_notification { get; set; }
            public string is_notification_info { get; set; }
           
            //public string time_zone { get; set; }
            //public string time_format { get; set; }
            //public string home_country { get; set; }
            //public string user_languages { get; set; }
            //public Int64 tmestmp { get; set; }
            //public int error_code { get; set; }
            //public string error_msg { get; set; }
        }


        public class Output
        {
            public int error_code { get; set; }
            public string error_msg { get; set; }
            public Int64 tmestmp { get; set; }
            public List<urmaworkcontactgetOutput> companycontacts { get; set; }
        }

        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, urmaworkcontactgetInput req)
        {
            List<urmaworkcontactgetOutput> RawOutput = new List<urmaworkcontactgetOutput>();
            Output output = new Output();
            //List<Output> OutputList = new List<Output>();
            try
            {
                Log.Info("urmaworkcontactgetController");
                Log.Info("Input : " + JsonConvert.SerializeObject(req));
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringappapi"].ConnectionString))
                {
                    conn.Open();
                    //var sp = "ur_ma_workcontact_get_test";
                    var sp = "ur_ma_workcontact_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @login_user_name = req.login_user_name,
                                @timestamp = req.timestamp
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Info("result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        RawOutput.AddRange(result.Select(r => new urmaworkcontactgetOutput()
                        {
                            id = r.id,
                            mobileno = string.IsNullOrEmpty(r.mapped_mobileno) ? "" : r.mapped_mobileno,
                            ext = r.ext,
                            caller_id = string.IsNullOrEmpty(r.caller_id) ? "" : r.caller_id,
                            login_user_name = string.IsNullOrEmpty(r.login_user_name) ? "" : r.login_user_name,
                            sip_login_id = r.sip_login_id,
                            first_name = string.IsNullOrEmpty(r.first_name) ? "" : r.first_name,
                            Last_name = string.IsNullOrEmpty(r.last_name) ? "" : r.last_name,
                            surname = string.IsNullOrEmpty(r.surname) ? "" : r.surname,
                            mobile_number = string.IsNullOrEmpty(r.mobile_number) ? "" : r.mobile_number,
                            landline_number = string.IsNullOrEmpty(r.landline_number) ? "" : r.landline_number,
                            email_id = string.IsNullOrEmpty(r.email_id) ? "" : r.email_id,
                            department = string.IsNullOrEmpty(r.department) ? "" : r.department,
                            address1 = string.IsNullOrEmpty(r.address1) ? "" : r.address1,
                            address2 = string.IsNullOrEmpty(r.address2) ? "" : r.address2,
                            Postcode = string.IsNullOrEmpty(r.Postcode) ? "" : r.Postcode,
                            city = string.IsNullOrEmpty(r.city) ? "" : r.city,
                            country = string.IsNullOrEmpty(r.country) ? "" : r.country,
                            is_muted = r.is_muted == null ? 0 : r.is_muted,
                            is_blocked = r.is_blocked == null ? 0 : r.is_blocked,
                            company_id = r.company_id == null ? 0 : r.company_id,
                            direct_no = r.direct_no,
                            available = r.available == null ? true : r.available, //need to add in DB later
                            user_status = r.user_status,
                            status_msg = r.status_msg,
                            ImageURL = r.ImageURL,
                            company_name = r.company_name,
                            is_favourite = r.is_favourite == null ? 0 : r.is_favourite, /// Added By Saroj 31/07/2019
                            role_name =r.role_name,
                            status = r.status ,
                            is_archive = r.is_archive == null ? 0 : r.is_archive, // added on 15052020
                            muted_info = r.muted_info== null ? 0 : r.muted_info,
                            is_notification = r.is_notification == null ? 0 : r.is_notification,
                            is_notification_info = r.is_notification_info ,
                            
                            //time_zone = r.time_zone,
                            //time_format = r.time_format,
                            //home_country = r.home_country,
                            //user_languages = r.user_languages,
                            //tmestmp = r.tmestmp,
                            //error_code = r.error_code == null ? 0 : r.error_code,
                            //error_msg = r.error_msg == null ? "Success" : r.error_msg,
                        }));

                        var orderByResult = (from s in result
                                            orderby s.tmestmp descending
                                            select new { s.tmestmp }).Take(1).ToList();

                        //var orderByResult2 = result.OrderBy(item => item.tmestmp).Last();

                        output.error_code = result.ElementAt(0).error_code == null ? 0 : result.ElementAt(0).error_code;
                        output.error_msg = result.ElementAt(0).error_msg == null ? "Success" : result.ElementAt(0).error_msg;
                        output.tmestmp = Convert.ToInt64(orderByResult[0].tmestmp);
                        output.companycontacts = RawOutput;
                        //OutputList.Add(output);
                    }
                    else
                    {
                        output.error_code = -1;
                        output.error_msg = "No Rec found";
                        //OutputList.Add(output);
                    }
                }
            }
            catch (Exception ex)
            {
                output.error_code = -1;
                output.error_msg = ex.Message;
                //OutputList.Add(output);
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
    }
}