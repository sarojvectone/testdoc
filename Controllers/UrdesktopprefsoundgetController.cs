using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdesktopprefsoundgetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdesktopprefsoundgetInput
        {
			public int company_id { get; set; }
			public int extn { get; set; }  	                   	    
        }
        public class UrdesktopprefsoundgetOutput
        {
			public string sound_type { get; set; }
			public string sound_status { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdesktopprefsoundgetInput req)
        {
            List<UrdesktopprefsoundgetOutput> OutputList = new List<UrdesktopprefsoundgetOutput>();
			Log.Info("Input : UrdesktopprefsoundgetController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_desktop_pref_sound_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@extn = req.extn 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrdesktopprefsoundgetOutput()
                        {
							sound_type = r.sound_type,
							sound_status = r.sound_status,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrdesktopprefsoundgetOutput outputobj = new UrdesktopprefsoundgetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrdesktopprefsoundgetOutput outputobj = new UrdesktopprefsoundgetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
