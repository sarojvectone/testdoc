using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class GeturemergencyaddressController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class GeturemergencyaddressInput
        {
            public int sip_id{ get; set; }
            public int extn_no { get; set; }
        }
        public class GeturemergencyaddressOutput
        {
			public int? company_id { get; set; }
			public string customer_name { get; set; }
			public string address1 { get; set; }
			public string address2 { get; set; }
			public string city { get; set; }
			public string postcode { get; set; }
			public string country { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }
            
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, GeturemergencyaddressInput req)
        {
            List<GeturemergencyaddressOutput> OutputList = new List<GeturemergencyaddressOutput>();
			Log.Info("Input : GeturemergencyaddressController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "get_UR_Emergency_Address";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @sip_id = req.sip_id,
                                @extn_no = req.extn_no
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new GeturemergencyaddressOutput()
                        {
							company_id = r.company_id == null ? 0 : r.company_id,
							customer_name = r.customer_name,
							address1 = r.address1,
							address2 = r.address2,
							city = r.city,
							postcode = r.postcode,
							country = r.country,

							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        GeturemergencyaddressOutput outputobj = new GeturemergencyaddressOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                GeturemergencyaddressOutput outputobj = new GeturemergencyaddressOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
