using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccountgetemailnotifController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountgetemailnotifInput
        {
            public int company_id { get; set; }
        }
        public class UrmyaccountgetemailnotifOutput
        {
			public int email_notif { get; set; }
			public int notif_to_user { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountgetemailnotifInput req)
        {
            List<UrmyaccountgetemailnotifOutput> OutputList = new List<UrmyaccountgetemailnotifOutput>();
			Log.Info("Input : UrmyaccountgetemailnotifController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_myaccount_get_email_notif";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountgetemailnotifOutput()
                        {
							email_notif = r.email_notif == null ? 0 : r.email_notif,
							notif_to_user = r.notif_to_user == null ? 0 : r.notif_to_user,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountgetemailnotifOutput outputobj = new UrmyaccountgetemailnotifOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountgetemailnotifOutput outputobj = new UrmyaccountgetemailnotifOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
