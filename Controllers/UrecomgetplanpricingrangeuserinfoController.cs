using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrecomgetplanpricingrangeuserinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrecomgetplanpricingrangeuserinfoInput
        {
			public int Duration { get; set; }
			public int Range_typeid { get; set; }  	                   	    
        }
        public class UrecomgetplanpricingrangeuserinfoOutput
        {
			public int plan_id { get; set; }
			public string plan_name { get; set; }
			public int Duration { get; set; }
			public int From_user_Range { get; set; }
			public int To_user_Range { get; set; }
			public double? PC_Price { get; set; }
            public double? Discount { get; set; }
            public int? most_popular { get; set; }
            public string plan_notes { get; set; }
            public int?  is_free_trial { get; set; }
            public int? free_trial_days { get; set; }
            public int? yearly_monthly_Duration { get; set; }
            public string plan_Type { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrecomgetplanpricingrangeuserinfoInput req)
        {
            List<UrecomgetplanpricingrangeuserinfoOutput> OutputList = new List<UrecomgetplanpricingrangeuserinfoOutput>();
			Log.Info("Input : UrecomgetplanpricingrangeuserinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ecom_Get_plan_pricing_range_user_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Duration = req.Duration,
								@Range_typeid = req.Range_typeid 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrecomgetplanpricingrangeuserinfoOutput()
                        {
							plan_id = r.plan_id == null ? 0 : r.plan_id,
							plan_name = r.plan_name,
							Duration = r.Duration == null ? 0 : r.Duration,
							From_user_Range = r.From_user_Range == null ? 0 : r.From_user_Range,
							To_user_Range = r.To_user_Range == null ? 0 : r.To_user_Range,
							PC_Price = r.PC_Price == null ? 0.0D : r.PC_Price,
                            Discount = r.Discount == null ? 0.0D : r.Discount,
                            most_popular = r.most_popular == null ? 0 : r.most_popular,
                            plan_notes = r.plan_notes,
                            is_free_trial = r.is_free_trial == null ? 0 : r.is_free_trial,
                            free_trial_days = r.free_trial_days == null ? 0 : r.free_trial_days,
                            yearly_monthly_Duration = r.yearly_monthly_Duration == null ? 0 : r.yearly_monthly_Duration,
                            plan_Type = r.plan_Type,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrecomgetplanpricingrangeuserinfoOutput outputobj = new UrecomgetplanpricingrangeuserinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrecomgetplanpricingrangeuserinfoOutput outputobj = new UrecomgetplanpricingrangeuserinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
