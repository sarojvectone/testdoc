using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmainsertphoneaddnumberController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmainsertphoneaddnumberInput
        {
            public int company_id { get; set; }
            public int domain_id { get; set; }
            public string external_number { get; set; }
            public string tax_per { get; set; }
            public double? tax_fee { get; set; }
            public double? total_charge { get; set; }
            public string reference_no { get; set; }
            public string called_by { get; set; }
            public int? site_id { get; set; }
            public string upload_path { get; set; }
            public int? ivr_id { get; set; }
            public int? is_prepaid_charging { get; set; }
            public string pp_Pay_Reference { get; set; }
            public double? pp_pay_amount { get; set; }
            public string pp_Paystatus { get; set; }
            public double? pp_tax_per { get; set; }
            public double? pp_tax_fee { get; set; }
            public double? pp_total_charge { get; set; }
            public double? pp_prorata_adjustment { get; set; }
            public string pp_last4digit_cc { get; set; }
            public int call_queue_id { get; set; }    	    
        }
        public class UrmainsertphoneaddnumberOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmainsertphoneaddnumberInput req)
        {

            Log.Info("Urmainsertphoneaddnumber Input " + JsonConvert.SerializeObject(req));
            List<UrmainsertphoneaddnumberOutput> OutputList = new List<UrmainsertphoneaddnumberOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_insert_phone_add_number";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                //@site_id = req.site_id,
                                //@upload_path  = req.upload_path,
                                //@company_id = req.company_id,
                                //@domain_id = req.domain_id,
                                //@external_number = req.external_number,
                                //@tax_per = req.tax_per,
                                //@tax_fee = req.tax_fee,
                                //@total_charge = req.total_charge,
                                //@reference_no = req.reference_no,
                                //@ivr_id = req.ivr_id
                                @company_id = req.company_id,
                                @domain_id = req.domain_id,
                                @external_number = req.external_number,
                                @tax_per = req.tax_per,
                                @tax_fee = req.tax_fee,
                                @total_charge = req.total_charge,
                                @reference_no = req.reference_no,
                                @called_by = req.called_by,
                                @site_id = req.site_id,
                                @upload_path = req.upload_path,
                                @ivr_id = req.ivr_id,
                                @is_prepaid_charging = req.is_prepaid_charging,
                                @pp_Pay_Reference = req.pp_Pay_Reference,
                                @pp_pay_amount = req.pp_pay_amount,
                                @pp_Paystatus = req.pp_Paystatus,
                                @pp_tax_per = req.pp_tax_per,
                                @pp_tax_fee = req.pp_tax_fee,
                                @pp_total_charge = req.pp_total_charge,
                                @pp_prorata_adjustment = req.pp_prorata_adjustment,
                                @pp_last4digit_cc = req.pp_last4digit_cc,
                                @call_queue_id = req.call_queue_id	                                    
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Info("Urmainsertphoneaddnumber Output " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmainsertphoneaddnumberOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmainsertphoneaddnumberOutput outputobj = new UrmainsertphoneaddnumberOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmainsertphoneaddnumberOutput outputobj = new UrmainsertphoneaddnumberOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
