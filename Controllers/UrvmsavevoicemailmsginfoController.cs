using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrvmsavevoicemailmsginfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrvmsavevoicemailmsginfoInput
        {
			public string username { get; set; }
			public string domain_name { get; set; }
			public string vm_from { get; set; }
			public string voicemail_text { get; set; }
			public string prompt_path { get; set; }
			public string prompt_name { get; set; }
			public string called_by { get; set; }  	                   	    
        }
        public class UrvmsavevoicemailmsginfoOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrvmsavevoicemailmsginfoInput req)
        {
            List<UrvmsavevoicemailmsginfoOutput> OutputList = new List<UrvmsavevoicemailmsginfoOutput>();
			Log.Info("Input : UrvmsavevoicemailmsginfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_vm_save_voicemail_msg_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@username = req.username,
								@domain_name = req.domain_name,
								@vm_from = req.vm_from,
								@voicemail_text = req.voicemail_text,
								@prompt_path = req.prompt_path,
								@prompt_name = req.prompt_name,
								@called_by = req.called_by 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrvmsavevoicemailmsginfoOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrvmsavevoicemailmsginfoOutput outputobj = new UrvmsavevoicemailmsginfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrvmsavevoicemailmsginfoOutput outputobj = new UrvmsavevoicemailmsginfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
