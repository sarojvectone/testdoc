using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebgetmobilenumbertypeinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebgetmobilenumbertypeinfoInput
        {
			public int Company_id { get; set; }
			public int Extension { get; set; }
			public int Hour_Type { get; set; }  	                   	    
        }
        public class UrmyaccwebgetmobilenumbertypeinfoOutput
        {
            public int? Company_id { get; set; }
			public int? Extension { get; set; }
			public int? Hour_Type { get; set; }
			public int? Incoming_call_type { get; set; }
			public string Incoming_call_type_name { get; set; }
			public string Number { get; set; }
			public int? After_Hours_Answered_Type { get; set; }
			public string Answered_Type { get; set; }
			public string Announcement_file { get; set; }
			public string play_language { get; set; }
			public int? call_forward_type { get; set; }
			public string mobile_number_type { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebgetmobilenumbertypeinfoInput req)
        {
            List<UrmyaccwebgetmobilenumbertypeinfoOutput> OutputList = new List<UrmyaccwebgetmobilenumbertypeinfoOutput>();
			Log.Info("Input : UrmyaccwebgetmobilenumbertypeinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_get_mobile_number_type_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @Company_id = req.Company_id,
								@Extension = req.Extension,
								@Hour_Type = req.Hour_Type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccwebgetmobilenumbertypeinfoOutput()
                        {
                            Company_id = r.Company_id == null ? 0 : r.Company_id,
							Extension = r.Extension == null ? 0 : r.Extension,
							Hour_Type = r.Hour_Type == null ? 0 : r.Hour_Type,
							Incoming_call_type = r.Incoming_call_type == null ? 0 : r.Incoming_call_type,
							Incoming_call_type_name = r.Incoming_call_type_name,
							Number = r.Number,
							After_Hours_Answered_Type = r.After_Hours_Answered_Type == null ? 0 : r.After_Hours_Answered_Type,
							Answered_Type = r.Answered_Type,
							Announcement_file = r.Announcement_file,
							play_language = r.play_language,
							call_forward_type = r.call_forward_type == null ? 0 : r.call_forward_type,
							mobile_number_type = r.mobile_number_type,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccwebgetmobilenumbertypeinfoOutput outputobj = new UrmyaccwebgetmobilenumbertypeinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccwebgetmobilenumbertypeinfoOutput outputobj = new UrmyaccwebgetmobilenumbertypeinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
