using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;

namespace Unifiedringmyaccountwebapi.Controllers
{
    public class urmalocalnumberupdateController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class urmalocalnumberupdate
        {
            public int company_id { get; set; }
            public int extension { get; set; }
            public string local_number { get; set; }
        }
        public class VtmyaccountloginOutput
        {
            public int? errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, urmalocalnumberupdate req)
        {
            List<VtmyaccountloginOutput> OutputList = new List<VtmyaccountloginOutput>();
            try
            {
                Log.Info("urmalocalnumberupdateController");
                Log.Info("Input : " + JsonConvert.SerializeObject(req));
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringappapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_local_number_update";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id,
                                @extension = req.extension,
                                @local_number = req.local_number
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Info("Output : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new VtmyaccountloginOutput()
                        {
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        VtmyaccountloginOutput outputobj = new VtmyaccountloginOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                VtmyaccountloginOutput outputobj = new VtmyaccountloginOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
