using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdeviceorderreturninsertController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdeviceorderreturninsertInput
        {
            //public int order_id { get; set; }
            //public string prod_code { get; set; }
            //public string prod_model { get; set; }
            //public double? rtn_charge { get; set; }
			public int rtn_quantity { get; set; }
            public int itemid { get; set; }
			public string rtn_reason { get; set; }  	                   	    
        }
        public class UrdeviceorderreturninsertOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdeviceorderreturninsertInput req)
        {
            List<UrdeviceorderreturninsertOutput> OutputList = new List<UrdeviceorderreturninsertOutput>();
			Log.Info("Input : UrdeviceorderreturninsertController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_order_return_insert";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @itemid = req.itemid,
								@rtn_quantity = req.rtn_quantity,
								@rtn_reason = req.rtn_reason 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrdeviceorderreturninsertOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrdeviceorderreturninsertOutput outputobj = new UrdeviceorderreturninsertOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrdeviceorderreturninsertOutput outputobj = new UrdeviceorderreturninsertOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
