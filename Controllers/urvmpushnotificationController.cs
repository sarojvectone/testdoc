using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PushSharp.Apple;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class urvmpushnotificationController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public class PushNotificationInput
        {
            public string sender { get; set; }
            public string receiver { get; set; }
        }

        public class PushNotificationOutput : Output
        {

        }

        public class Output
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public HttpResponseMessage Get(string origin, string target)
        {
            Log.Info("urvmpushnotification");
            PushNotificationOutput output = new PushNotificationOutput();
            try
            {
                Log.Info("Input : origin=" + origin + "&target=" + target);

                string ToNumber = target; //GetMobileNo(target);
                if (ToNumber != "")
                {
                    Log.Info("To mobile no : " + ToNumber);
                    string FromNumber = origin.IndexOf("_") > -1 ? origin.Split(new string[] { "_" }, StringSplitOptions.None)[1] : origin;//GetName(origin);origin; //GetMobileNo(origin);

                    Log.Info("From mobile no : " + FromNumber);
                    //Device Token
                    String deviceToken = GetDeviceToken(ToNumber, false);
                    if (deviceToken == "")
                    {
                        output.errcode = -1;
                        output.errmsg = "device not found";
                        Log.Info("device not found");
                    }
                    else
                    {
                        Log.Info("deviceToken : " + deviceToken);

                        string name;
                        if (origin.IndexOf("_") > -1)
                            name = GetName(origin);//target.Split(new string[] { "_" }, StringSplitOptions.None)[1];//GetName(origin);
                        else
                            name = origin;
                        //name = origin;
                        //if (name.Length == 0)
                        //    name = origin;
                        Log.Info(string.Format("Extension : {0}", name));

                        //Todo : Anees added to get Contact name
                        //FromNumber = GetName(FromNumber);
                        FromNumber = String.IsNullOrEmpty(FromNumber) ? name : FromNumber;

                        String message = string.Format(APNSHelper.voicemail_message, name);
                        //String message = string.Format(APNSHelper.push_message, origin);

                        Log.Info("message : " + message);

                        string p12File = APNSHelper.voicemail_key_file;
                        string p12FilePwd = APNSHelper.voicemail_key_pwd;

                        ApnsConfiguration config = null;
                        if (APNSHelper.push_hostname.ToLower() == "gateway.sandbox.push.apple.com")
                        {
                      
                            config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, p12File, p12FilePwd, true);
                        }
                        else
                        {
                            config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, p12File, p12FilePwd, true);
                        }

                        // Create a new broker
                        var apnsBroker = new ApnsServiceBroker(config);

                        // Wire up events
                        apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                        {
                            aggregateEx.Handle(ex =>
                            {
                                output.errcode = -1;
                                // See what kind of exception it was to further diagnose
                                if (ex is ApnsNotificationException)
                                {
                                    var notificationException = (ApnsNotificationException)ex;
                                    var apnsNotification = notificationException.Notification;
                                    var statusCode = notificationException.ErrorStatusCode;
                                    Log.Error(ex.InnerException);
                                    output.errmsg = String.Format("Apple Notification Failed: ID={0}, Code={1}", apnsNotification.Identifier, statusCode);
                                    Log.Error(output.errmsg);
                                    Log.Error(apnsNotification);
                                }
                                else
                                {
                                    output.errmsg = String.Format("Apple Notification Failed for some unknown reason : {0}", ex.InnerException);
                                    Log.Error(output.errmsg);
                                }
                                return true;
                            });
                        };
                       
                        apnsBroker.OnNotificationSucceeded += (notification) =>
                        {
                            
                            output.errcode = 0;
                            output.errmsg = "Notification Sent!";
                            Log.Info(output.errmsg);
                        };

                        apnsBroker.Start();

                        string jsonString = "{\"aps\":{\"content-available\":\"" + "1" + "\",\"name\":\"" + name + "\",\"media\":\"" + "audio" + "\",\"UUID\":\"" + deviceToken + "\",\"alert\":\"" + message + "\",\"badge\":\"" + APNSHelper.push_badge + "\",\"phoneno\":\"" + FromNumber + "\",\"sound\":\"" + APNSHelper.push_sound + "\"}}";

                        apnsBroker.QueueNotification(new ApnsNotification
                        {
                            DeviceToken = deviceToken,
                            Payload = JObject.Parse(jsonString)
                            //Payload = JObject.Parse("{\"aps\":{\"content-available\":\"" + "1" + "\",\"alert\":\"" + message + "\",\"badge\":\"" + APNSHelper.push_badge + "\",\"sound\":\"" + APNSHelper.push_sound + "\"}}")
                        });
                        apnsBroker.Stop();
                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = "mobile no not found";
                    Log.Info(output.errmsg);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }

        //[Authorize]
        public HttpResponseMessage Post(PushNotificationInput input)
        {
            Log.Info("urpushnotification");
            PushNotificationOutput output = new PushNotificationOutput();
            if (input == null)
            {
                output.errcode = -1;
                output.errmsg = "input details not found";
            }
            else
            {
                try
                {
                    Log.Info("sender : " + input.sender + " & receiver : " + input.receiver);
                    string ToNumber = GetMobileNo(input.receiver);
                    if (ToNumber != "")
                    {
                        Log.Info("To mobile no : " + ToNumber);
                        //Device Token
                        String deviceToken = GetDeviceToken(ToNumber, false);
                        if (deviceToken == "")
                        {
                            output.errcode = -1;
                            output.errmsg = "device not found";
                            Log.Info("device not found");
                        }
                        else
                        {
                            Log.Info("deviceToken : " + deviceToken);
                            String message = string.Format(APNSHelper.voicemail_message, input.sender);
                            Log.Info("message : " + message);

                            string[] Names = input.sender.Split('_');//GetName(input.sender);
                            string Name = Names[1];//GetName(Names[1]);
                            Log.Info("Name : " + Name);
                            string FromNumber = GetMobileNo(input.sender);
                            Log.Info("MobileNo : " + FromNumber);

                            string p12File = "";
                            string p12FilePwd = "";
                            p12File = APNSHelper.voicemail_key_file;
                            p12FilePwd = APNSHelper.voicemail_key_pwd;

                            ApnsConfiguration config = null;
                            if (APNSHelper.push_hostname.ToLower() == "gateway.sandbox.push.apple.com")
                                config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, p12File, p12FilePwd, true);
                            else
                                config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, p12File, p12FilePwd, true);

                            // Create a new broker
                            var apnsBroker = new ApnsServiceBroker(config);

                            // Wire up events
                            apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                            {
                                aggregateEx.Handle(ex =>
                                {
                                    output.errcode = -1;
                                    // See what kind of exception it was to further diagnose
                                    if (ex is ApnsNotificationException)
                                    {
                                        var notificationException = (ApnsNotificationException)ex;
                                        var apnsNotification = notificationException.Notification;
                                        var statusCode = notificationException.ErrorStatusCode;
                                        output.errmsg = String.Format("Apple Notification Failed: ID={0}, Code={1}", apnsNotification.Identifier, statusCode);
                                        Log.Info(output.errmsg);
                                    }
                                    else
                                    {
                                        output.errmsg = String.Format("Apple Notification Failed for some unknown reason : {0}", ex.InnerException);
                                        Log.Info(output.errmsg);
                                    }
                                    return true;
                                });
                            };

                            apnsBroker.OnNotificationSucceeded += (notification) =>
                            {
                                output.errcode = 0;
                                output.errmsg = "Notification Sent!";
                                Log.Info(output.errmsg);
                            };

                            apnsBroker.Start();

                            apnsBroker.QueueNotification(new ApnsNotification
                            {
                                DeviceToken = deviceToken,
                                Payload = JObject.Parse("{\"aps\":{\"alert\":\"" + message + " @" + Name + "(" + FromNumber + ")" + "\",\"badge\":\"" + APNSHelper.push_badge + "\",\"name\":\"" + Name + "\",\"mobileno\":\"" + FromNumber + "\",\"sound\":\"" + APNSHelper.push_sound + "\"}}")
                            });
                            apnsBroker.Stop();
                        }
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = "mobile no not found";
                        Log.Info(output.errmsg);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }

        private string GetDeviceToken(string deviceId, bool isvoip)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["apns_provider"].ConnectionString))
                {
                    conn.Open();
                    var sp = "usp_getdevicetoken";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @DeviceID = deviceId
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        if (isvoip)
                            return result.ElementAt(0).voip_token;
                        else
                            return result.ElementAt(0).message_token;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return "";
        }

        private string GetName(string accountid)
        {
            string custName = "";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringappapi"].ConnectionString))
                {
                    var sp = "ur_ma_push_notification_get_username";

                    conn.Open();

                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @mobileno = accountid.Split(new string[] { "_" }, StringSplitOptions.None)[1],
                         @domain_id = accountid.Split(new string[] { "_" }, StringSplitOptions.None)[0]
                     },
                     commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0 && result.ElementAt(0).errcode != null && result.ElementAt(0).errcode == 0)
                    {
                        custName = result.ElementAt(0).name;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return custName;
        }

        private string GetMobileNo(string accountid)
        {
            string custName = "";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MVNO.CTP"].ConnectionString))
                {
                    var sp = "ctp_get_mobileno";

                    conn.Open();

                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @accountid = accountid
                     },
                     commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        custName = result.ElementAt(0).mobileno;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return custName;
        }
    }
}
