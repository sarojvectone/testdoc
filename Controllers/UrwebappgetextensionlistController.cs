using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrwebappgetextensionlistController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrwebappgetextensionlistInput
        {
			public int company_id { get; set; }
			public string extension_no { get; set; }  	                   	    
        }
        public class UrwebappgetextensionlistOutput
        {
			public int sip_logid { get; set; }
			public string sip_password { get; set; }
			public string Extension_Number { get; set; }
			public string display_name { get; set; }
			public string MAC_address { get; set; }
			public string Department { get; set; }
			public string Email { get; set; }
            public string dp_password { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrwebappgetextensionlistInput req)
        {
            List<UrwebappgetextensionlistOutput> OutputList = new List<UrwebappgetextensionlistOutput>();
			Log.Info("Input : UrwebappgetextensionlistController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_webapp_get_extension_list";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@extension_no = req.extension_no 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrwebappgetextensionlistOutput()
                        {
							sip_logid = r.sip_logid == null ? 0 : r.sip_logid,
							sip_password = r.sip_password,
							Extension_Number = r.Extension_Number,
							display_name = r.display_name,
							MAC_address = r.MAC_address,
							Department = r.Department,
							Email = r.Email,
                            dp_password = r.dp_password,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrwebappgetextensionlistOutput outputobj = new UrwebappgetextensionlistOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrwebappgetextensionlistOutput outputobj = new UrwebappgetextensionlistOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
