using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountcompanysaveaddoninfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcompanysaveaddoninfoInput
        {
			public int Company_id { get; set; }
			public int Plan_id { get; set; }
			public int addon_id { get; set; }
			public int addon_type { get; set; }
			public int no_of_extension { get; set; }
			public double? addon_price { get; set; }
			public double? price_per_user { get; set; }
			public double? tax_fee { get; set; }
			public double? total_bill { get; set; }
			public string create_by { get; set; }
            public string bill_referenceno { get; set; }
            public double? tax_per { get; set; }
            public string pp_Pay_Reference { get; set; }
            public double? pp_pay_amount { get; set; }
            public string pp_Paystatus { get; set; }
            public double? pp_tax_per { get; set; }
            public double? pp_tax_fee { get; set; }
            public double? pp_total_charge { get; set; }
            public double? pp_prorata_adjustment { get; set; }
            public string pp_last4digit_cc { get; set; }
        }

        public class UrmyaccountcompanysaveaddoninfoOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcompanysaveaddoninfoInput req)
        {
            List<UrmyaccountcompanysaveaddoninfoOutput> OutputList = new List<UrmyaccountcompanysaveaddoninfoOutput>();
			Log.Info("Input : UrmyaccountcompanysaveaddoninfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_company_save_addon_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_id = req.Company_id,
								@Plan_id = req.Plan_id,
								@addon_id = req.addon_id,
								@addon_type = req.addon_type,
								@no_of_extension = req.no_of_extension,
								@addon_price = req.addon_price,
								@price_per_user = req.price_per_user,
								@tax_fee = req.tax_fee,
								@total_bill = req.total_bill,
								@create_by = req.create_by,
                                @bill_referenceno = req.bill_referenceno,
                                @tax_per = req.tax_per,
                                @pp_Pay_Reference = req.pp_Pay_Reference,
                                @pp_pay_amount = req.pp_pay_amount,
                                @pp_Paystatus = req.pp_Paystatus,
                                @pp_tax_per = req.pp_tax_per,
                                @pp_tax_fee = req.pp_tax_fee,
                                @pp_total_charge = req.pp_total_charge,
                                @pp_prorata_adjustment = req.pp_prorata_adjustment,
                                @pp_last4digit_cc = req.pp_last4digit_cc,
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcompanysaveaddoninfoOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcompanysaveaddoninfoOutput outputobj = new UrmyaccountcompanysaveaddoninfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcompanysaveaddoninfoOutput outputobj = new UrmyaccountcompanysaveaddoninfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
