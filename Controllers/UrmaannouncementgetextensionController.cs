using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaannouncementgetextensionController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaannouncementgetextensionInput
        {
			public int ar_id { get; set; }
			public int domain_id { get; set; }  	                   	    
        }
        public class UrmaannouncementgetextensionOutput
        {
			public int? ar_id { get; set; }
			public int? domain_id { get; set; }
			public string extn_name { get; set; }
			public string extn_number { get; set; }
			public string email { get; set; }
			public string company_name { get; set; }
			public string contact_no { get; set; }
			public string greeting_filename { get; set; }
			public int? status { get; set; }
			public string time_zone { get; set; }
			public int? time_format { get; set; }
			public string home_country_code { get; set; }
			public string greeting_language { get; set; }
			public string user_language { get; set; }
			public string regional_format { get; set; }
			public int? message_enable { get; set; }
			public string message_enable_duration { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }
	        public int? greeting_by { get; set; }
            public int? greeting_custom_type { get; set; }          	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaannouncementgetextensionInput req)
        {
            List<UrmaannouncementgetextensionOutput> OutputList = new List<UrmaannouncementgetextensionOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_announcement_get_extension";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@ar_id = req.ar_id,
								@domain_id = req.domain_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmaannouncementgetextensionOutput()
                        {
							ar_id = r.ar_id == null ? 0 : r.ar_id,
							domain_id = r.domain_id == null ? 0 : r.domain_id,
							extn_name = r.extn_name,
							extn_number = r.extn_number,
							email = r.email,
							company_name = r.company_name,
							contact_no = r.contact_no,
							greeting_filename = r.greeting_filename,
							status = r.status == null ? 0 : r.status,
							time_zone = r.time_zone,
							time_format = r.time_format == null ? 0 : r.time_format,
							home_country_code = r.home_country_code,
							greeting_language = r.greeting_language,
							user_language = r.user_language,
							regional_format = r.regional_format,
							message_enable = r.message_enable == null ? 0 : r.message_enable,
							message_enable_duration = r.message_enable_duration,
                            greeting_by = r.greeting_by,
                            greeting_custom_type = r.greeting_custom_type,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmaannouncementgetextensionOutput outputobj = new UrmaannouncementgetextensionOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmaannouncementgetextensionOutput outputobj = new UrmaannouncementgetextensionOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
