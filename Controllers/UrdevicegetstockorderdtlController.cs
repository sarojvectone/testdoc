using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdevicegetstockorderdtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdevicegetstockorderdtlInput
        {
			public int order_id { get; set; }  	                   	    
        }

        //rtn_quantity-int ,rpl_quantity-int
        public class UrdevicegetstockorderdtlOutput
        {
			public string category { get; set; }
			public string customer_name { get; set; }
			public string address1 { get; set; }
			public string address2 { get; set; }
			public string city { get; set; }
			public string postcode { get; set; }
			public string country { get; set; }
			public string phone_id { get; set; }
			public string phone_desc { get; set; }
			public double? price { get; set; }
			public double? weight { get; set; }
			public string ean_no { get; set; }
			public int quantity { get; set; }
            public string device_model { get; set; }
            public int  itemid { get; set; }
            public int rtn_quantity { get; set; }
            public int rpl_quantity { get; set; }
            public string MAC_address { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdevicegetstockorderdtlInput req)
        {
            List<UrdevicegetstockorderdtlOutput> OutputList = new List<UrdevicegetstockorderdtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_get_stock_order_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@order_id = req.order_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrdevicegetstockorderdtlOutput()
                        {
							category = r.category,
							customer_name = r.customer_name,
							address1 = r.address1,
							address2 = r.address2,
							city = r.city,
							postcode = r.postcode,
							country = r.country,
							phone_id = r.phone_id,
							phone_desc = r.phone_desc,
							price = r.price == null ? 0.0D : r.price,
							weight = r.weight == null ? 0.0D : r.weight,
							ean_no = r.ean_no,
							quantity = r.quantity == null ? 0 : r.quantity,
                            device_model = r.device_model,
                            itemid = r.itemid == null ? 0 : r.itemid,
                            rtn_quantity = r.rtn_quantity == null ? 0 : r.rtn_quantity,
                            rpl_quantity = r.rpl_quantity == null ? 0 : r.rpl_quantity,
                            MAC_address = r.MAC_address,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrdevicegetstockorderdtlOutput outputobj = new UrdevicegetstockorderdtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrdevicegetstockorderdtlOutput outputobj = new UrdevicegetstockorderdtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
