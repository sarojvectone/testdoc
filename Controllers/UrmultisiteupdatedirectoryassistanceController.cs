using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmultisiteupdatedirectoryassistanceController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmultisiteupdatedirectoryassistanceInput
        {
			public int company_id { get; set; }
			public int site_id { get; set; }
			public int site_main_number { get; set; }
			public int site_fax_number { get; set; }
			public string site_name { get; set; }
			public string country { get; set; }
			public string city { get; set; }
			public string site_address { get; set; }
			public string county { get; set; }
			public string postcode { get; set; }
			public string email { get; set; }  	                   	    
        }
        public class UrmultisiteupdatedirectoryassistanceOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmultisiteupdatedirectoryassistanceInput req)
        {
            List<UrmultisiteupdatedirectoryassistanceOutput> OutputList = new List<UrmultisiteupdatedirectoryassistanceOutput>();
			Log.Info("Input : UrmultisiteupdatedirectoryassistanceController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Multisite_update_directory_assistance";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@site_id = req.site_id,
								@site_main_number = req.site_main_number,
								@site_fax_number = req.site_fax_number,
								@site_name = req.site_name,
								@country = req.country,
								@city = req.city,
								@site_address = req.site_address,
								@county = req.county,
								@postcode = req.postcode,
								@email = req.email 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmultisiteupdatedirectoryassistanceOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmultisiteupdatedirectoryassistanceOutput outputobj = new UrmultisiteupdatedirectoryassistanceOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmultisiteupdatedirectoryassistanceOutput outputobj = new UrmultisiteupdatedirectoryassistanceOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
