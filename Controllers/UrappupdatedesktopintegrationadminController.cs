using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappupdatedesktopintegrationadminController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappupdatedesktopintegrationadminInput
        {
			public int company_id { get; set; }
			public int integration_gallery { get; set; }
			public int upload_mobile_computer { get; set; }
			public int google_drive { get; set; }
			public int dropbox { get; set; }
			public int box { get; set; }
			public int onedrive { get; set; }
			public int evernote { get; set; }
			public int type { get; set; }  	                   	    
        }
        public class UrappupdatedesktopintegrationadminOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappupdatedesktopintegrationadminInput req)
        {
            List<UrappupdatedesktopintegrationadminOutput> OutputList = new List<UrappupdatedesktopintegrationadminOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_update_desktop_integration_admin";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@integration_gallery = req.integration_gallery,
								@upload_mobile_computer = req.upload_mobile_computer,
								@google_drive = req.google_drive,
								@dropbox = req.dropbox,
								@box = req.box,
								@onedrive = req.onedrive,
								@evernote = req.evernote,
								@type = req.type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrappupdatedesktopintegrationadminOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrappupdatedesktopintegrationadminOutput outputobj = new UrappupdatedesktopintegrationadminOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrappupdatedesktopintegrationadminOutput outputobj = new UrappupdatedesktopintegrationadminOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
