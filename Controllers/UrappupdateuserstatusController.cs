using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappupdateuserstatusController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappupdateuserstatusInput
        {
            public int domain_id{ get; set; }
            public int extension { get; set; }
			public string status_msg { get; set; }
			public int status_indicator { get; set; }
			public int type { get; set; }  	                   	    
        }
        public class UrappupdateuserstatusOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }   
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappupdateuserstatusInput req)
        {
            List<UrappupdateuserstatusOutput> OutputList = new List<UrappupdateuserstatusOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_update_user_Status";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @domain_id = req.domain_id,
                                @extension = req.extension,
								@status_msg = req.status_msg,
								@status_indicator = req.status_indicator,
								@type = req.type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrappupdateuserstatusOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                            
                        }));
                    }
                    else
                    {
                        UrappupdateuserstatusOutput outputobj = new UrappupdateuserstatusOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrappupdateuserstatusOutput outputobj = new UrappupdateuserstatusOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
