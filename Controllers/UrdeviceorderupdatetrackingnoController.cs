using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdeviceorderupdatetrackingnoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdeviceorderupdatetrackingnoInput
        {
			public int orderid { get; set; }
			public string tracking_no { get; set; }  	                   	    
        }
        public class UrdeviceorderupdatetrackingnoOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdeviceorderupdatetrackingnoInput req)
        {
            List<UrdeviceorderupdatetrackingnoOutput> OutputList = new List<UrdeviceorderupdatetrackingnoOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_order_update_tracking_no";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@orderid = req.orderid,
								@tracking_no = req.tracking_no 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrdeviceorderupdatetrackingnoOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrdeviceorderupdatetrackingnoOutput outputobj = new UrdeviceorderupdatetrackingnoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrdeviceorderupdatetrackingnoOutput outputobj = new UrdeviceorderupdatetrackingnoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
