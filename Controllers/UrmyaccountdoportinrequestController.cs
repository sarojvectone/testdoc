using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccountdoportinrequestController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountdoportinrequestInput
        {
			public int company_id { get; set; }
			public int domain_id { get; set; }
			public string number_type { get; set; }
			public string service_provider { get; set; }
			public string provider_address { get; set; }
			public string qn_1 { get; set; }
			public string qn_2 { get; set; }
			public string qn_3 { get; set; }
			public string qn_4 { get; set; }
			public string qn_5 { get; set; }
			public string qn_6 { get; set; }
			public string qn_7 { get; set; }
			public string bill_tele_ph_no { get; set; }
			public string check_bill_telno { get; set; }
			public string transfer_no { get; set; }
			public DateTime? transfer_date { get; set; }
			public string main_didno { get; set; }
			public string extension { get; set; }
			public string account_type { get; set; }
			public string jobtitle { get; set; }
			public string firstname { get; set; }
			public string surname { get; set; }
			public string company_name { get; set; }
			public string company_reg_no { get; set; }
			public string building_name { get; set; }
			public string address1 { get; set; }
			public string address2 { get; set; }
			public string city { get; set; }
			public string country { get; set; }
			public string postcode { get; set; }
			public string upload_path { get; set; }
			public string signedLOA_path { get; set; }
			public string comments { get; set; }
			public string send_emails_to { get; set; }
			public string order_number { get; set; }
			public int reqid { get; set; }
			public int process_type { get; set; }
            public string replace_no { get; set; }            	    
        }
        public class UrmyaccountdoportinrequestOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountdoportinrequestInput req)
        {
            List<UrmyaccountdoportinrequestOutput> OutputList = new List<UrmyaccountdoportinrequestOutput>();
			Log.Info("Input : UrmyaccountdoportinrequestController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_do_portin_request";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@domain_id = req.domain_id,
								@number_type = req.number_type,
								@service_provider = req.service_provider,
								@provider_address = req.provider_address,
								@qn_1 = req.qn_1,
								@qn_2 = req.qn_2,
								@qn_3 = req.qn_3,
								@qn_4 = req.qn_4,
								@qn_5 = req.qn_5,
								@qn_6 = req.qn_6,
								@qn_7 = req.qn_7,
								@bill_tele_ph_no = req.bill_tele_ph_no,
								@check_bill_telno = req.check_bill_telno,
								@transfer_no = req.transfer_no,
								@transfer_date = req.transfer_date == null ? null : req.transfer_date,
								@main_didno = req.main_didno,
								@extension = req.extension,
								@account_type = req.account_type,
								@jobtitle = req.jobtitle,
								@firstname = req.firstname,
								@surname = req.surname,
								@company_name = req.company_name,
								@company_reg_no = req.company_reg_no,
								@building_name = req.building_name,
								@address1 = req.address1,
								@address2 = req.address2,
								@city = req.city,
								@country = req.country,
								@postcode = req.postcode,
								@upload_path = req.upload_path,
								@signedLOA_path = req.signedLOA_path,
								@comments = req.comments,
								@send_emails_to = req.send_emails_to,
								@order_number = req.order_number,
								@reqid = req.reqid,
								@process_type = req.process_type,
                                @replace_no = req.replace_no                                   
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountdoportinrequestOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountdoportinrequestOutput outputobj = new UrmyaccountdoportinrequestOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountdoportinrequestOutput outputobj = new UrmyaccountdoportinrequestOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
