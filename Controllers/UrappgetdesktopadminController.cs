using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappgetdesktopadminController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappgetdesktopadminInput
        {
			public int company_id { get; set; }
            public int type { get; set; }            	    
        }
        public class UrappgetdesktopadminOutput
        {
			public string email { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
            public string username { get; set; }
            public double? sip_login_id { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappgetdesktopadminInput req)
        {
            List<UrappgetdesktopadminOutput> OutputList = new List<UrappgetdesktopadminOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_get_desktop_admin";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
                                @type = req.type			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrappgetdesktopadminOutput()
                        {
							email = r.email,
                            username = r.username,
                            sip_login_id = r.sip_login_id == null ? 0 : r.sip_login_id,
                            errcode = r.errcode == null ? 0 : r.errcode,                        
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrappgetdesktopadminOutput outputobj = new UrappgetdesktopadminOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrappgetdesktopadminOutput outputobj = new UrappgetdesktopadminOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
