using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrbtdatfilecustomerdtlgetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrbtdatfilecustomerdtlgetInput
        {
			public int company_id { get; set; }
			public int cupid { get; set; }  	                   	    
        }
        public class UrbtdatfilecustomerdtlgetOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public string title { get; set; }
            public string  Initials  { get; set; }
            public string  Name { get; set; }
            public string  business_type { get; set; }
            public string permises { get; set; }
            public string Post_code { get; set; }
            public string address_id { get; set; }
            public string address_line_1 { get; set; }
            public string address_line_2 { get; set; }
            public string address_line_3 { get; set; }
            public string city { get; set; }
            public int ec_id { get; set; }
            public string command { get; set; }
            public string address_id_source { get; set; }
            public string telephone_number { get; set; }
            public string new_telephone_number { get; set; }
            public int cupid { get; set; }
            public int extension { get; set; }
                                                                                                                                                                           }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrbtdatfilecustomerdtlgetInput req)
        {
            List<UrbtdatfilecustomerdtlgetOutput> OutputList = new List<UrbtdatfilecustomerdtlgetOutput>();
			Log.Info("Input : UrbtdatfilecustomerdtlgetController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_BT_Dat_file_customer_dtl_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@cupid = req.cupid 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrbtdatfilecustomerdtlgetOutput()
                        {
                           title = r.title,
                           Initials = r. Initials ,
                           Name  = r. Name,
                           business_type   = r.business_type,
                           permises  = r. permises,
                           Post_code  = r. Post_code,
                           address_id= r.  address_id,
                           address_line_1  = r. address_line_1,
                           address_line_2 = r. address_line_2,
                           address_line_3= r.  address_line_3,
                           city = r. city,
                           ec_id= r. ec_id,
                           command = r. command,
                           address_id_source= r.  address_id_source,
                           telephone_number= r. telephone_number,
                           new_telephone_number  = r. new_telephone_number,
                           cupid = r.cupid == null ? 0 : r.cupid,
                           extension = r.extension == null ? 0 : r.extension,                            
						   errcode = r.errcode == null ? 0 : r.errcode,
						   errmsg = r.errmsg == null ? "Success" : r.errmsg 
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrbtdatfilecustomerdtlgetOutput outputobj = new UrbtdatfilecustomerdtlgetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrbtdatfilecustomerdtlgetOutput outputobj = new UrbtdatfilecustomerdtlgetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
