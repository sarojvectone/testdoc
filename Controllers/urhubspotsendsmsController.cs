﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using System.Text;
using System.IO;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class urhubspotsendsmsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class urhubspotsendsmsInput
        {
            public string text { get; set; }
            public string destination_number { get; set; }
        }
        public class urhubspotsendsmsOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        public class Properties1
        {
            public List<Contact> properties { get; set; }
        }
        public class Contact
        {
            public string property { get; set; }
            public string value { get; set; }
        }
        public string ConvertToDate(double inputDate)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(inputDate).ToLocalTime();
            string date = dtDateTime.ToShortDateString();
            return date;
        }


        public string Get(string text, string destination_number)
        {
            string url = "";
            url = ConfigurationManager.AppSettings["mapurl1"];
            //string sText = "Dear Customer, We are upgrading our SIM's so you can have a seamless experience. Please order your Free SIM at  https://www.vectonemobile.co.uk/free-sim/upgrade";
            string sText = text;

            ASCIIEncoding encoding = new ASCIIEncoding();
            string postData = "delivery-receipt=No" +
                   "&destination-addr=" + destination_number +
                   "&originator-addr-type=" + 5 +
                   "&originator-addr=" + "Vectone" +
                   "&payload-type=text" +
                   "&message=" + System.Web.HttpUtility.UrlEncode(sText, Encoding.Default);

            Console.WriteLine(postData);
            HttpWebRequest smsReq = (HttpWebRequest)WebRequest.Create(url);
            smsReq.Method = "POST";
            smsReq.ContentType = "application/x-www-form-urlencoded";
            smsReq.ServicePoint.Expect100Continue = false;
            ASCIIEncoding enc = new ASCIIEncoding();
            byte[] data = enc.GetBytes(postData);
            smsReq.ContentLength = data.Length;

            Stream newStream = smsReq.GetRequestStream();
            newStream.Write(data, 0, data.Length);
            newStream.Close();

            HttpWebResponse smsRes = (HttpWebResponse)smsReq.GetResponse();
            Stream resStream = smsRes.GetResponseStream();

            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
            StreamReader readStream = new StreamReader(resStream, encode);
            string sResponse = readStream.ReadToEnd().Trim();
            return sResponse;
        }

        public class SmscontentgettextInput
        {
            public int sms_id { get; set; }
            public string scenerio { get; set; }
            public string sitecode { get; set; }
        }
        public class SmscontentgettextOutput
        {
            public int Sms_id { get; set; }
            public string Scenerio { get; set; }
            public string Sms_text { get; set; }
            public DateTime Create_date { get; set; }
            public string Status { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        public class SitecodestringOutput
        {
            public string sms { get; set; }
            public string Nat_call { get; set; }
            public string Inter_nat_call { get; set; }
            public DateTime? enddate { get; set; }
            public double? price { get; set; }
            public string balance_check { get; set; }
            public string group_name { get; set; }
            public int? errcode { get; set; }
            public string errmsg { get; set; }
            public string data { get; set; }
        }


        //public async Task<HttpResponseMessage> Post(string id, object input)
        //{

        //    Log.Info(" urhubspotsendsmsController Input : " + JsonConvert.SerializeObject(input));

        //    return null;
        //}
        public async Task<HttpResponseMessage> Post(string id, InputProperties input)
        {
            //Log.Info("Input : SmscontentgettextController:" + JsonConvert.SerializeObject(input));

            // double timestamp = input.properties.hs_searchable_calculated_phone_number.versions[0].timestamp;

            string phonenumber = input.properties.hs_searchable_calculated_phone_number.value;

            // string email = input.properties.email.value;

            string countrycode = phonenumber.Substring(0, 2);

            string sitecode = string.Empty;
            if (countrycode == "44")
            {
                sitecode = "MCM";
            }


            SmscontentgettextInput req = new SmscontentgettextInput();
            req.scenerio = id;
            req.sitecode = sitecode;
            List<SitecodestringOutput> lstSitecodestringOutput = new List<SitecodestringOutput>();

            if (id == "OnlinePlansubsciptionSuccess" ||
                id == "Romaingbundlerenewal" ||
                  id == "Internationalbundlerenewal" ||
                  id == "Nationalbundlerenewal" ||
                  id == "AIObundlerenewal" ||
                  id == "Databundlerenewal")
            {
                //Get the values from DB to send SMS
                try
                {
                    if (!string.IsNullOrEmpty(input.properties.latest_bundle_id_being_purchased.value))
                    {
                        SitecodestringOutput objSitecodestringOutput = new SitecodestringOutput();

                        int bundleID = Convert.ToInt32(input.properties.latest_bundle_id_being_purchased.value);
                        using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["crm3"].ConnectionString))
                        {
                            conn.Open();
                            var sp = "Hubspot_sms_get_bundle_allowances";
                            var result = conn.Query<dynamic>(
                                    sp, new
                                    {
                                        @sitecode = req.sitecode,
                                        @mobileno = phonenumber,
                                        @bundleid = bundleID
                                    },
                                    commandType: CommandType.StoredProcedure);
                            if (result != null && result.Count() > 0)
                            {
                                Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                                lstSitecodestringOutput.AddRange(result.Select(r => new SitecodestringOutput()
                                {
                                    sms = r.sms,
                                    Nat_call = r.Nat_call,
                                    Inter_nat_call = r.Inter_nat_call,
                                    enddate = r.enddate,
                                    price = r.price == null ? 0.0F : r.price,
                                    balance_check = r.balance_check,
                                    group_name = r.group_name,
                                    data = r.data,
                                    errcode = r.errcode == null ? 0 : r.errcode,
                                    errmsg = r.errmsg == null ? "Success" : r.errmsg
                                }));
                            }
                            else
                            {
                                Log.Info("Output DB : " + "Empty result");
                                SitecodestringOutput outputobj = new SitecodestringOutput();
                                outputobj.errcode = -1;
                                outputobj.errmsg = "No Rec found";
                                lstSitecodestringOutput.Add(outputobj);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {

                }
            }



            List<SmscontentgettextOutput> OutputList = new List<SmscontentgettextOutput>();
            Log.Info("Input : SmscontentgettextController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SMS_Portal"].ConnectionString))
                {
                    conn.Open();
                    var sp = "sms_content_get_text";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @sms_id = req.sms_id,
                                @scenerio = req.scenerio,
                                @sitecode = req.sitecode
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new SmscontentgettextOutput()
                        {
                            Sms_id = r.Sms_id == null ? 0 : r.Sms_id,
                            Scenerio = r.Scenerio,
                            Sms_text = r.Sms_text,
                            //Create_date = r.Create_date == null ? null : r.Create_date,
                            Status = r.Status,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        Log.Info("Output DB : " + "Empty result");
                        SmscontentgettextOutput outputobj = new SmscontentgettextOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                SmscontentgettextOutput outputobj = new SmscontentgettextOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            string sText = "";
            if (OutputList[0].errcode == 0)
                sText = OutputList[0].Sms_text;

            //Assigning properties

            //string parameters = "firstname,latest_topup_amount1";
            //string[] arrayValues = parameters.Split(',');
            //foreach (string strParameters in arrayValues)
            //{
            //    if (sText.Contains("|*"+strParameters+"*|"))
            //    { 
            //        sText = sText.Replace("|*"+strParameters+"*|", input.properties.firstname.value);    
            //    }
            //}

            if (sText.Contains("|*firstname*|"))
                sText = sText.Replace("|*firstname*|", input.properties.firstname.value);
            if (sText.Contains("|*latest_topup_amount1*|"))
                sText = sText.Replace("|*latest_topup_amount1*|", input.properties.latest_topup_amount1.value);
            if (sText.Contains("|*nextbillingdate*|"))
                sText = sText.Replace("|*nextbillingdate*|", ConvertToDate(Convert.ToDouble(input.properties.nextbillingdate.value)));
            if (sText.Contains("|*latest_topup_amount_being_purchased*|"))
                sText = sText.Replace("|*latest_topup_amount_being_purchased*|", input.properties.latest_topup_amount_being_purchased.value);
            if (sText.Contains("|*national_bundle_data_allowance*|"))
                sText = sText.Replace("|*national_bundle_data_allowance*|", input.properties.national_bundle_data_allowance.value);
            if (sText.Contains("|*national_bundle_national_mins_total_allowance*|"))
                sText = sText.Replace("|*national_bundle_national_mins_total_allowance*|", input.properties.national_bundle_national_mins_total_allowance.value); if (sText.Contains("|*national_bundle_sms_allowance*|"))
                sText = sText.Replace("|*national_bundle_sms_allowance*|", input.properties.national_bundle_sms_allowance.value);
            if (sText.Contains("|*current_national_enddate*|"))
                sText = sText.Replace("|*current_national_enddate*|", ConvertToDate(Convert.ToDouble(input.properties.current_national_enddate.value)));
            if (sText.Contains("|*ussd_dialing_code*|"))
                sText = sText.Replace("|*ussd_dialing_code*|", input.properties.ussd_dialing_code.value);
            if (sText.Contains("|*current_balance*|"))
                sText = sText.Replace("|*current_balance*|", input.properties.current_balance.value);
            if (sText.Contains("|*latest_topup_payment_reference_number*|"))
                sText = sText.Replace("|*latest_topup_payment_reference_number*|", input.properties.latest_topup_payment_reference_number.value);
            if (sText.Contains("|*latest_bundle_name_being_purchased*|"))
                sText = sText.Replace("|*latest_bundle_name_being_purchased*|", input.properties.latest_bundle_name_being_purchased.value);
            if (sText.Contains("|*latest_bundle_id_being_purchased*|"))
                sText = sText.Replace("|*latest_bundle_id_being_purchased*|", input.properties.latest_bundle_id_being_purchased.value);

            if (OutputList[0].Scenerio.ToString().ToLower() == "onlineplansubsciptionsuccess")
            {
                if (sText.Contains("|*PlanPrice*|"))
                    sText = sText.Replace("|*PlanPrice*|", lstSitecodestringOutput[0].price.ToString());
                if (sText.Contains("|*Data*|"))
                    sText = sText.Replace("|*Data*|", lstSitecodestringOutput[0].data.ToString());
                if (sText.Contains("|*NationalMin*|"))
                    sText = sText.Replace("|*NationalMin*|", lstSitecodestringOutput[0].Nat_call.ToString());
                if (sText.Contains("|*NationalText*|"))
                    sText = sText.Replace("|*NationalText*|", lstSitecodestringOutput[0].sms.ToString());
                if (sText.Contains("|*Expirydate*|"))
                    sText = sText.Replace("|*Expirydate*|", lstSitecodestringOutput[0].enddate.ToString());
                if (sText.Contains("|*DailedCode*|"))
                    sText = sText.Replace("|*DailedCode*|", lstSitecodestringOutput[0].balance_check.ToString());
            }

            //End Assign 
            try
            {
                Log.Info(" urhubspotsendsmsController Input : " + JsonConvert.SerializeObject(input));
                Log.Info(" SMS Text : " + sText);

                string url = "";
                url = ConfigurationManager.AppSettings["mapurl1"];

                string sResponse = "-1";

                string sendSMS = ConfigurationManager.AppSettings["hubsendsms"];

                if (sendSMS == "Yes")
                {
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    string postData = "delivery-receipt=No" +
                           "&destination-addr=" + phonenumber +
                           "&originator-addr-type=" + 5 +
                           "&originator-addr=" + "Vectone" +
                           "&payload-type=text" +
                           "&message=" + System.Web.HttpUtility.UrlEncode(sText, Encoding.Default);

                    Console.WriteLine(postData);
                    HttpWebRequest smsReq = (HttpWebRequest)WebRequest.Create(url);
                    smsReq.Method = "POST";
                    smsReq.ContentType = "application/x-www-form-urlencoded";
                    smsReq.ServicePoint.Expect100Continue = false;
                    ASCIIEncoding enc = new ASCIIEncoding();
                    byte[] data = enc.GetBytes(postData);
                    smsReq.ContentLength = data.Length;

                    Stream newStream = smsReq.GetRequestStream();
                    newStream.Write(data, 0, data.Length);
                    newStream.Close();

                    HttpWebResponse smsRes = (HttpWebResponse)smsReq.GetResponse();
                    Stream resStream = smsRes.GetResponseStream();

                    Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                    StreamReader readStream = new StreamReader(resStream, encode);
                    sResponse = readStream.ReadToEnd().Trim();
                }
                else
                {
                    sResponse = "-1";
                }
                urhubspotsendsmsOutput output = new urhubspotsendsmsOutput();
                if (sResponse.Contains("0:"))
                {
                    output.errcode = 0;
                    output.errmsg = "Success";
                    Log.Info("Output : " + JsonConvert.SerializeObject(output));
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = "Send SMS Failed";
                    Log.Info("Output : " + JsonConvert.SerializeObject(output));
                }
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
            catch (Exception ex)
            {
                Log.Info("Output : " + JsonConvert.SerializeObject(ex.Message.ToString()));
                urhubspotsendsmsOutput output = new urhubspotsendsmsOutput();
                output.errcode = 0;
                output.errmsg = "Error";
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }

        }
    }
    // Root myDeserializedClass = JsonConvert.DeserializeObject(myJsonResponse); 
    public class Version
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public object source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsIsUnworked
    {
        public string value { get; set; }
        public List<Version> versions { get; set; }

    }

    public class Version2
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class Firstname
    {
        public string value { get; set; }
        public List<Version2> versions { get; set; }

    }

    public class Version3
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsLegalBasis
    {
        public string value { get; set; }
        public List<Version3> versions { get; set; }

    }

    public class Version4
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public object source_id { get; set; }
        public object source_label { get; set; }
        public int timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class NumUniqueConversionEvents
    {
        public string value { get; set; }
        public List<Version4> versions { get; set; }

    }

    public class Version5
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsAnalyticsRevenue
    {
        public string value { get; set; }
        public List<Version5> versions { get; set; }

    }

    public class Version6
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class Createdate
    {
        public string value { get; set; }
        public List<Version6> versions { get; set; }

    }

    public class Version7
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsSocialNumBroadcastClicks
    {
        public string value { get; set; }
        public List<Version7> versions { get; set; }

    }

    public class Version8
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsAnalyticsNumVisits
    {
        public string value { get; set; }
        public List<Version8> versions { get; set; }

    }

    public class Version9
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsSocialLinkedinClicks
    {
        public string value { get; set; }
        public List<Version9> versions { get; set; }

    }

    public class Version10
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HubspotOwnerId
    {
        public string value { get; set; }
        public List<Version10> versions { get; set; }

    }

    public class Version11
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsAnalyticsSource
    {
        public string value { get; set; }
        public List<Version11> versions { get; set; }

    }

    public class Version12
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public object source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsSearchableCalculatedPhoneNumber
    {
        public string value { get; set; }
        public List<Version12> versions { get; set; }

    }

    public class Version13
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsAnalyticsNumPageViews
    {
        public string value { get; set; }
        public List<Version13> versions { get; set; }

    }

    public class Version14
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public object source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsEmailDomain
    {
        public string value { get; set; }
        public List<Version14> versions { get; set; }

    }

    public class Version15
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsAllOwnerIds
    {
        public string value { get; set; }
        public List<Version15> versions { get; set; }

    }

    public class Version16
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class Email
    {
        public string value { get; set; }
        public List<Version16> versions { get; set; }

    }

    public class Version17
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsAnalyticsFirstTimestamp
    {
        public string value { get; set; }
        public List<Version17> versions { get; set; }

    }

    public class Version18
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public object source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class Lastmodifieddate
    {
        public string value { get; set; }
        public List<Version18> versions { get; set; }

    }

    public class Version19
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsSocialGooglePlusClicks
    {
        public string value { get; set; }
        public List<Version19> versions { get; set; }

    }

    public class Version20
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsLifecyclestageSubscriberDate
    {
        public string value { get; set; }
        public List<Version20> versions { get; set; }

    }

    public class Version21
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsUserIdsOfAllOwners
    {
        public string value { get; set; }
        public List<Version21> versions { get; set; }

    }

    public class Version22
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsAnalyticsAveragePageViews
    {
        public string value { get; set; }
        public List<Version22> versions { get; set; }

    }

    public class Version23
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class Lastname
    {
        public string value { get; set; }
        public List<Version23> versions { get; set; }

    }

    public class Version24
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public object source_id { get; set; }
        public string source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsPredictivescoringtier
    {
        public string value { get; set; }
        public List<Version24> versions { get; set; }

    }

    public class Version25
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsSocialFacebookClicks
    {
        public string value { get; set; }
        public List<Version25> versions { get; set; }

    }

    public class Version26
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public object source_id { get; set; }
        public string source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsPredictivecontactscoreV2
    {
        public string value { get; set; }
        public List<Version26> versions { get; set; }

    }

    public class Version27
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HubspotOwnerAssigneddate
    {
        public string value { get; set; }
        public List<Version27> versions { get; set; }

    }

    public class Version28
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class Phone
    {
        public string value { get; set; }
        public List<Version28> versions { get; set; }

    }

    public class Version29
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public object source_id { get; set; }
        public object source_label { get; set; }
        public int timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsIsContact
    {
        public string value { get; set; }
        public List<Version29> versions { get; set; }

    }

    public class Version30
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public object sourceid { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class NumConversionEvents
    {
        public string value { get; set; }
        public List<Version30> versions { get; set; }

    }

    public class Version31
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public object source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsObjectId
    {
        public string value { get; set; }
        public List<Version31> versions { get; set; }

    }

    public class Version32
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class Currentlyinworkflow
    {
        public string value { get; set; }
        public List<Version32> versions { get; set; }

    }

    public class Version33
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsLeadStatus
    {
        public string value { get; set; }
        public List<Version33> versions { get; set; }

    }

    public class Version34
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsAnalyticsNumEventCompletions
    {
        public string value { get; set; }
        public List<Version34> versions { get; set; }

    }

    public class Version35
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsAnalyticsSourceData2
    {
        public string value { get; set; }
        public List<Version35> versions { get; set; }

    }

    public class Version36
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsSocialTwitterClicks
    {
        public string value { get; set; }
        public List<Version36> versions { get; set; }

    }

    public class Version37
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class HsAnalyticsSourceData1
    {
        public string value { get; set; }
        public List<Version37> versions { get; set; }

    }

    public class Version38
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class Lifecyclestage
    {
        public string value { get; set; }
        public List<Version38> versions { get; set; }

    }

    public class Properties
    {
        public HsIsUnworked hs_is_unworked { get; set; }
        public Firstname firstname { get; set; }
        public HsLegalBasis hs_legal_basis { get; set; }
        public NumUniqueConversionEvents num_unique_conversion_events { get; set; }
        public HsAnalyticsRevenue hs_analytics_revenue { get; set; }
        public Createdate createdate { get; set; }
        public HsSocialNumBroadcastClicks hs_social_num_broadcast_clicks { get; set; }
        public HsAnalyticsNumVisits hs_analytics_num_visits { get; set; }
        public HsSocialLinkedinClicks hs_social_linkedin_clicks { get; set; }
        public HubspotOwnerId hubspot_owner_id { get; set; }
        public HsAnalyticsSource hs_analytics_source { get; set; }
        public HsSearchableCalculatedPhoneNumber hs_searchable_calculated_phone_number { get; set; }
        public HsAnalyticsNumPageViews hs_analytics_num_page_views { get; set; }
        public HsEmailDomain hs_email_domain { get; set; }
        public HsAllOwnerIds hs_all_owner_ids { get; set; }
        public Email email { get; set; }
        public HsAnalyticsFirstTimestamp hs_analytics_first_timestamp { get; set; }
        public Lastmodifieddate lastmodifieddate { get; set; }
        public HsSocialGooglePlusClicks hs_social_google_plus_clicks { get; set; }
        public HsLifecyclestageSubscriberDate hs_lifecyclestage_subscriber_date { get; set; }
        public HsUserIdsOfAllOwners hs_user_ids_of_all_owners { get; set; }
        public HsAnalyticsAveragePageViews hs_analytics_average_page_views { get; set; }
        public Lastname lastname { get; set; }
        public HsPredictivescoringtier hs_predictivescoringtier { get; set; }
        public HsSocialFacebookClicks hs_social_facebook_clicks { get; set; }
        public HsPredictivecontactscoreV2 hs_predictivecontactscore_v2 { get; set; }
        public HubspotOwnerAssigneddate hubspot_owner_assigneddate { get; set; }
        public Phone phone { get; set; }
        public HsIsContact hs_is_contact { get; set; }
        public NumConversionEvents num_conversion_events { get; set; }
        public HsObjectId hs_object_id { get; set; }
        public Currentlyinworkflow currentlyinworkflow { get; set; }
        public HsLeadStatus hs_lead_status { get; set; }
        public HsAnalyticsNumEventCompletions hs_analytics_num_event_completions { get; set; }
        public HsAnalyticsSourceData2 hs_analytics_source_data_2 { get; set; }
        public HsSocialTwitterClicks hs_social_twitter_clicks { get; set; }
        public HsAnalyticsSourceData1 hs_analytics_source_data_1 { get; set; }
        public Lifecyclestage lifecyclestage { get; set; }
        public LatestTopupAmount1 latest_topup_amount1 { get; set; }
        public Nextbillingdate nextbillingdate { get; set; }
        public CurrentBalance current_balance { get; set; }
        public LatestTopupPaymentReferenceNumber latest_topup_payment_reference_number { get; set; }
        public LatestBundleNameBeingPurchased latest_bundle_name_being_purchased { get; set; }
        public LatestTopupAmountBeingPurchased latest_topup_amount_being_purchased { get; set; }
        public NationalBundleDataAllowance national_bundle_data_allowance { get; set; }
        public NationalBundleNationalMinsTotalAllowance national_bundle_national_mins_total_allowance { get; set; }
        public NationalBundleSmsAllowance national_bundle_sms_allowance { get; set; }
        public CurrentNationalEnddate current_national_enddate { get; set; }
        public UssdDialingCode ussd_dialing_code { get; set; }
        public LatestBundleIdBeingPurchased latest_bundle_id_being_purchased { get; set; }
    }
    public class LatestBundleIdBeingPurchased
    {
        public string value { get; set; }
        public List<Version> versions { get; set; }
    }
    public class UssdDialingCode
    {
        public string value { get; set; }
        public List<Version> versions { get; set; }
    }

    public class CurrentNationalEnddate
    {
        public string value { get; set; }
        public List<Version> versions { get; set; }
    }
    public class NationalBundleSmsAllowance
    {
        public string value { get; set; }
        public List<Version> versions { get; set; }
    }
    public class NationalBundleNationalMinsTotalAllowance
    {
        public string value { get; set; }
        public List<Version> versions { get; set; }
    }
    public class NationalBundleDataAllowance
    {
        public string value { get; set; }
        public List<Version> versions { get; set; }
    }
    public class LatestTopupAmountBeingPurchased
    {
        public string value { get; set; }
        public List<Version> versions { get; set; }
    }
    public class LatestBundleNameBeingPurchased
    {
        public string value { get; set; }
        public List<Version> versions { get; set; }
    }

    public class CurrentBalance
    {
        public string value { get; set; }
        public List<Version> versions { get; set; }
    }
    public class LatestTopupPaymentReferenceNumber
    {
        public string value { get; set; }
        public List<Version> versions { get; set; }
    }

    public class Nextbillingdate
    {
        public string value { get; set; }
        public List<Version> versions { get; set; }
    }

    public class LatestTopupAmount1
    {
        public string value { get; set; }
        public List<Version88> versions { get; set; }

    }
    public class Version88
    {
        public string value { get; set; }
        public string source_type { get; set; }
        public string source_id { get; set; }
        public object source_label { get; set; }
        public long timestamp { get; set; }
        public bool selected { get; set; }

    }

    public class ListMembership
    {
        public int static_list_id { get; set; }
        public int internal_list_id { get; set; }
        public object timestamp { get; set; }
        public int vid { get; set; }
        public bool is_member { get; set; }

    }

    public class Identity
    {
        public string type { get; set; }
        public string value { get; set; }
        public object timestamp { get; set; }
        public bool is_primary { get; set; }
        public string source { get; set; }

    }

    public class IdentityProfile
    {
        public int vid { get; set; }
        public bool is_deleted { get; set; }
        public bool is_contact { get; set; }
        public int pointer_vid { get; set; }
        public int previous_vid { get; set; }
        public List<object> linked_vids { get; set; }
        public int saved_at_timestamp { get; set; }
        public int deleted_changed_timestamp { get; set; }
        public List<Identity> identities { get; set; }

    }

    public class AssociatedOwner
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string type { get; set; }
        public int hubspot_user_id { get; set; }

    }

    public class InputProperties
    {
        public int vid { get; set; }
        public int canonical_vid { get; set; }
        public List<object> merged_vids { get; set; }
        public int portal_id { get; set; }
        public bool is_contact { get; set; }
        public string profile_token { get; set; }
        public string profile_url { get; set; }
        public Properties properties { get; set; }
        public List<object> form_submissions { get; set; }
        public List<ListMembership> list_memberships { get; set; }
        public List<IdentityProfile> identity_profiles { get; set; }
        public List<object> merge_audits { get; set; }
        public AssociatedOwner associated_owner { get; set; }

    }






}
