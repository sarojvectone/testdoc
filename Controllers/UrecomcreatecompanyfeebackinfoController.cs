using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrecomcreatecompanyfeebackinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrecomcreatecompanyfeebackinfoInput
        {
			public int company_id { get; set; }
			public int company_rating { get; set; }
			public string feedback_title { get; set; }
			public string feedback_review { get; set; }
			public string called_by { get; set; }
			public string submitted_by { get; set; }  	                   	    
        }
        public class UrecomcreatecompanyfeebackinfoOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrecomcreatecompanyfeebackinfoInput req)
        {
            List<UrecomcreatecompanyfeebackinfoOutput> OutputList = new List<UrecomcreatecompanyfeebackinfoOutput>();
			Log.Info("Input : UrecomcreatecompanyfeebackinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ecom_create_company_feeback_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@company_rating = req.company_rating,
								@feedback_title = req.feedback_title,
								@feedback_review = req.feedback_review,
								@called_by = req.called_by,
								@submitted_by = req.submitted_by 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrecomcreatecompanyfeebackinfoOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrecomcreatecompanyfeebackinfoOutput outputobj = new UrecomcreatecompanyfeebackinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrecomcreatecompanyfeebackinfoOutput outputobj = new UrecomcreatecompanyfeebackinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
