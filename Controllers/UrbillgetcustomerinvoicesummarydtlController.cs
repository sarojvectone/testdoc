using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrbillgetcustomerinvoicesummarydtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrbillgetcustomerinvoicesummarydtlInput
        {
			public int? company_id { get; set; }
			public int? invoice_id { get; set; }
            public string type { get; set; }      	    
        }
        public class UrbillgetcustomerinvoicesummarydtlOutput
        {
			public string charge_type { get; set; }
			public string charge_number { get; set; }
			public double? charge { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrbillgetcustomerinvoicesummarydtlInput req)
        {
            List<UrbillgetcustomerinvoicesummarydtlOutput> OutputList = new List<UrbillgetcustomerinvoicesummarydtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_bill_get_customer_invoice_summary_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@invoice_id = req.invoice_id,
                                @type = req.type                                  
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrbillgetcustomerinvoicesummarydtlOutput()
                        {
                            charge_type = r.charge_type,
                            charge_number = r.charge_number,
                            charge = r.charge,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrbillgetcustomerinvoicesummarydtlOutput outputobj = new UrbillgetcustomerinvoicesummarydtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrbillgetcustomerinvoicesummarydtlOutput outputobj = new UrbillgetcustomerinvoicesummarydtlOutput();
				outputobj.errcode = -2;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
