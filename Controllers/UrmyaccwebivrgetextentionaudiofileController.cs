﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebivrgetextentionaudiofileController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebivrgetextentionaudiofileInput
        {
            public int Company_id { get; set; }
            public int Extension { get; set; }
            public int Hour_type { get; set; }
        }
        public class UrmyaccwebivrgetextentionaudiofileOutput
        {
            public string IVR_type { get; set; }
            public int? Hour_type { get; set; }
            public int? Greeting_by { get; set; }
            public int? Greeting_type { get; set; }
            public int? Language { get; set; }
            public int? Audio_type { get; set; }
            public int? Music_type { get; set; }
            public string Music_type_desc { get; set; }
            public int? Call_screening_type { get; set; }
            public string Audio_file { get; set; }
            public int? Active { get; set; }
            public int? errcode { get; set; }
            public string errmsg { get; set; }
            public int? greeting_custom_type { get; set; }
            public string greeting_content { get; set; }
            
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebivrgetextentionaudiofileInput req)
        {
            Log.Info("Urmyaccwebivrgetextentionaudiofile : Input : " + JsonConvert.SerializeObject(req));
            List<UrmyaccwebivrgetextentionaudiofileOutput> OutputList = new List<UrmyaccwebivrgetextentionaudiofileOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_IVR_get_extention_audio_file";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @Company_id = req.Company_id,
                                @Extension = req.Extension,
                                @Hour_type = req.Hour_type,
                          
                                
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebivrgetextentionaudiofileOutput()
                        {
                            IVR_type = r.IVR_type,
                            Hour_type = r.Hour_type,
                            Greeting_by = r.Greeting_by,
                            Greeting_type = r.Greeting_type,
                            Language = r.Language,
                            Audio_type = r.Audio_type,
                            Music_type = r.Music_type,
                            Music_type_desc = r.Music_type_desc,
                            Call_screening_type = r.Call_screening_type,
                            Audio_file = r.Audio_file,
                            Active = r.Active,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg,
                            greeting_custom_type = r.greeting_custom_type,
                            greeting_content = r.greeting_content
                        }));
                    }
                    else
                    {
                        UrmyaccwebivrgetextentionaudiofileOutput outputobj = new UrmyaccwebivrgetextentionaudiofileOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Urmyaccwebivrgetextentionaudiofile : Error : " + ex.Message);
                UrmyaccwebivrgetextentionaudiofileOutput outputobj = new UrmyaccwebivrgetextentionaudiofileOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            Log.Info("Urmyaccwebivrgetextentionaudiofile : Output : " + JsonConvert.SerializeObject(OutputList));
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
