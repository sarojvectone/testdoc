using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountdeletecarddirectdebitaccountController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountdeletecarddirectdebitaccountInput
        {
			public int? id { get; set; }
			public int? company_id { get; set; }
			public int? type { get; set; }  	                   	    
        }
        public class UrmyaccountdeletecarddirectdebitaccountOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountdeletecarddirectdebitaccountInput req)
        {
            List<UrmyaccountdeletecarddirectdebitaccountOutput> OutputList = new List<UrmyaccountdeletecarddirectdebitaccountOutput>();
			Log.Info("Input : UrmyaccountdeletecarddirectdebitaccountController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_delete_Card_direct_debit_account";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@id = req.id,
								@company_id = req.company_id,
								@type = req.type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountdeletecarddirectdebitaccountOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountdeletecarddirectdebitaccountOutput outputobj = new UrmyaccountdeletecarddirectdebitaccountOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountdeletecarddirectdebitaccountOutput outputobj = new UrmyaccountdeletecarddirectdebitaccountOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
