using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappcustomnumberflipinfogetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappcustomnumberflipinfogetInput
        {
			public int ext { get; set; }
			public int Company_ID { get; set; }  	                   	    
        }
        public class UrappcustomnumberflipinfogetOutput
        {
			public int? id { get; set; }
			public double? dir_user_id { get; set; }
			//public string phone_number { get; set; }
            public string number { get; set; }
			public string flip_key { get; set; }
            public bool is_active { get; set; }
			public string name { get; set; }
			public int? Ring_type { get; set; }
            public string Ring_type_name { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappcustomnumberflipinfogetInput req)
        {
            List<UrappcustomnumberflipinfogetOutput> OutputList = new List<UrappcustomnumberflipinfogetOutput>();
			Log.Info("Input : UrappcustomnumberflipinfogetController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_custom_number_flip_Info_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@ext = req.ext,
								@Company_ID = req.Company_ID 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrappcustomnumberflipinfogetOutput()
                        {
							 id = r.id == null ? 0.0D : r.id,
                             dir_user_id = r.dir_user_id == null ? 0.0D : r.dir_user_id,
							//phone_number = r.phone_number,
                            number = r.number,
						    flip_key = r.flip_key ,
                            is_active = r.is_active,
							name = r.name,
							Ring_type = r.Ring_type == null ? 0 : r.Ring_type,
                            Ring_type_name = r.Ring_type_name,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrappcustomnumberflipinfogetOutput outputobj = new UrappcustomnumberflipinfogetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrappcustomnumberflipinfogetOutput outputobj = new UrappcustomnumberflipinfogetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
