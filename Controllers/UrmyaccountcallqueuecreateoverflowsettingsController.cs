using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountcallqueuecreateoverflowsettingsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcallqueuecreateoverflowsettingsInput
        {
			public int Company_id { get; set; }
			public int call_queue_id { get; set; }
			public int Queue_Overflow_status { get; set; }
			public int Queue_overflow_max_time_reached { get; set; }
			public int Queue_Overflow_caller_limit_reach { get; set; }
			public string call_queue_ext_xml { get; set; }  	                   	    
        }
        public class UrmyaccountcallqueuecreateoverflowsettingsOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcallqueuecreateoverflowsettingsInput req)
        {
            List<UrmyaccountcallqueuecreateoverflowsettingsOutput> OutputList = new List<UrmyaccountcallqueuecreateoverflowsettingsOutput>();
			Log.Info("Input : UrmyaccountcallqueuecreateoverflowsettingsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_call_queue_create_overflow_settings";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_id = req.Company_id,
								@call_queue_id = req.call_queue_id,
								@Queue_Overflow_status = req.Queue_Overflow_status,
								@Queue_overflow_max_time_reached = req.Queue_overflow_max_time_reached,
								@Queue_Overflow_caller_limit_reach = req.Queue_Overflow_caller_limit_reach,
								@call_queue_ext_xml = req.call_queue_ext_xml 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcallqueuecreateoverflowsettingsOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcallqueuecreateoverflowsettingsOutput outputobj = new UrmyaccountcallqueuecreateoverflowsettingsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcallqueuecreateoverflowsettingsOutput outputobj = new UrmyaccountcallqueuecreateoverflowsettingsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
