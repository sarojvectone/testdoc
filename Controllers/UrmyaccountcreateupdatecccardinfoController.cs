using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccountcreateupdatecccardinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcreateupdatecccardinfoInput
        {
			public int company_id { get; set; }
			public string card_holder_name { get; set; }
			public string cc_no { get; set; }
			public string expiry_date { get; set; }
			public string card_type { get; set; }
			public int create_user_id { get; set; }
			public int status { get; set; }
            public int processtype { get; set; }
            public int id { get; set; }  	                   	    
        }
        public class UrmyaccountcreateupdatecccardinfoOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public int? is_subs_create{ get; set; }    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcreateupdatecccardinfoInput req)
        {
            List<UrmyaccountcreateupdatecccardinfoOutput> OutputList = new List<UrmyaccountcreateupdatecccardinfoOutput>();
			Log.Info("Input : UrmyaccountcreateupdatecccardinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_create_update_cc_card_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@card_holder_name = req.card_holder_name,
								@cc_no = req.cc_no,
								@expiry_date = req.expiry_date,
								@card_type = req.card_type,
								@create_user_id = req.create_user_id,
								@status = req.status,
								@processtype = req.processtype,
                                @id = req.id                                   
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcreateupdatecccardinfoOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg,
                            is_subs_create = r.is_subs_create == null ? 2 : r.is_subs_create
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcreateupdatecccardinfoOutput outputobj = new UrmyaccountcreateupdatecccardinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcreateupdatecccardinfoOutput outputobj = new UrmyaccountcreateupdatecccardinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
