﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using PushSharp.Apple;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebuserextesiondeleteController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebuserextesiondeleteInput
        {
            public int company_id { get; set; }
            public string extension { get; set; }
            public string status { get; set; }
            public int action_type { get; set; }
            public int domain_id { get; set; }
            public int order_id { get; set; }
        }
        public class UrmyaccwebuserextesiondeleteOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
             public string host_address { get; set; }
            public string sip_password { get; set; }
            public double? sip_login_id { get; set; }
        }
        public class GetDeviceTokenOutput
        {
            public string message_token { get; set; }
            public string voip_token { get; set; }
            public int DeviceType { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        
       
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebuserextesiondeleteInput req)
        {
            Log.Info("Input : UrmyaccwebuserextesiondeleteController:" + JsonConvert.SerializeObject(req));
            List<UrmyaccwebuserextesiondeleteOutput> OutputList = new List<UrmyaccwebuserextesiondeleteOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_User_Extesion_delete";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id,
                                @extension = req.extension,
                                @status = req.status,
                                @action_type = req.action_type,
                                @domain_id = req.domain_id,
                                @order_id = req.order_id
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebuserextesiondeleteOutput()
                        {
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg,
                            host_address = r.host_address,
                            sip_password = r.sip_password,
                            sip_login_id = r.sip_login_id == null ? 0.0D : r.sip_login_id,
                        }));

                        if (!string.IsNullOrEmpty(OutputList[0].sip_password) && OutputList[0].sip_login_id != 0.0)
                        {

                            try
                            {
                                if (ConfigurationManager.AppSettings["ejabberdchat_cluster"] == "1")
                                {
                                    //var webAddr1 = ConfigurationManager.AppSettings["ejabberdchat_mobileregister"];
                                    var webAddr1 = ConfigurationManager.AppSettings["ejabberdchat_mobileregister_host"];
                                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                    var httpWebRequest1 = (HttpWebRequest)WebRequest.Create(webAddr1);
                                    httpWebRequest1.ContentType = "application/json";
                                    httpWebRequest1.Method = "POST";
                                    // string json = "{\"userid\": \"" + OutputList[0].sip_id + "\",\"password\": \"" + OutputList[0].sip_password + "\"}";

                                    string autorization = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImcuYW5uYW1hbGFpQHZlY3RvbmUuY29tIiwicGFzc3dvcmQiOiIxMjM0NTYiLCJkZXZpY2VJZCI6ImFiY2QiLCJzaXBMb2dpbklkIjoiMzE3OCIsInJvbGVJZCI6MywiaWF0IjoxNTkyMjIyNzY5fQ.6CE_6uTy8mOvzOwrB3B75jMNAkDPGAbWKG1rB1ePy9s";
                                    //autorization = autorization;
                                    httpWebRequest1.Headers.Add("AUTHORIZATION", autorization);

                                    string json = "{\"userid\": \"" + OutputList[0].sip_login_id + "\",\"company_id\": \"" + req.company_id + "\",\"host\": \"" + OutputList[0].host_address + "\",\"password\": \"" + OutputList[0].sip_password + "\"}";

                                    using (var streamWriter = new StreamWriter(httpWebRequest1.GetRequestStream()))
                                    {
                                        streamWriter.Write(json);
                                        streamWriter.Flush();
                                    }
                                    var httpResponse1 = (HttpWebResponse)httpWebRequest1.GetResponse();
                                    Log.Info("Output UrmyaccwebuserextesiondeleteController ejabberdchat_mobileregister: " + JsonConvert.SerializeObject(httpResponse1));
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Info("Error ejabberdchat_mobileregister: " + ex.Message.ToString());
                            }
                        }



                    }
                    else
                    {
                        UrmyaccwebuserextesiondeleteOutput outputobj = new UrmyaccwebuserextesiondeleteOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebuserextesiondeleteOutput outputobj = new UrmyaccwebuserextesiondeleteOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            } 
                //End Result 
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}