using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmadialbydirgetdtlController : ApiController
    {

        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmadialbydirgetdtlInput
        {
			public int company_id { get; set; }
			public int domain_id { get; set; }
			public int type { get; set; }  	                   	    
        }
        public class UrmadialbydirgetdtlOutput
        {
			public int? dir_enable { get; set; }
			public int? dir_search_by { get; set; }
			public int? dir_extension { get; set; }
			public string extension_number { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmadialbydirgetdtlInput req)
        {
            Log.Info("UrmadialbydirgetdtlController");
            Log.Info("Input : " + JsonConvert.SerializeObject(req));
            List<UrmadialbydirgetdtlOutput> OutputList = new List<UrmadialbydirgetdtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_dialby_dir_get_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@domain_id = req.domain_id,
								@type = req.type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                 
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmadialbydirgetdtlOutput()
                        {
							dir_enable = r.dir_enable == null ? 0 : r.dir_enable,
							dir_search_by = r.dir_search_by == null ? 0 : r.dir_search_by,
							dir_extension = r.dir_extension == null ? 0 : r.dir_extension,
							extension_number = r.extension_number,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmadialbydirgetdtlOutput outputobj = new UrmadialbydirgetdtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmadialbydirgetdtlOutput outputobj = new UrmadialbydirgetdtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            Log.Info("Output : " + JsonConvert.SerializeObject(OutputList));
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
