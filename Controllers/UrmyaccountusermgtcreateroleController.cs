using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountusermgtcreateroleController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountusermgtcreateroleInput
        {
			public int? companyid { get; set; }
			public string role_name { get; set; }
			public string Role_type { get; set; }
			public string Role_desc { get; set; }
			public int? Role_status { get; set; }
			public int? Role_id { get; set; }
			public int? processtype { get; set; }
			public string module_info { get; set; }  	                   	    
        }
        public class UrmyaccountusermgtcreateroleOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountusermgtcreateroleInput req)
        {
            List<UrmyaccountusermgtcreateroleOutput> OutputList = new List<UrmyaccountusermgtcreateroleOutput>();
			Log.Info("Input : UrmyaccountusermgtcreateroleController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_usermgt_create_role";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@companyid = req.companyid,
								@role_name = req.role_name,
								@Role_type = req.Role_type,
								@Role_desc = req.Role_desc,
								@Role_status = req.Role_status,
								@Role_id = req.Role_id,
								@processtype = req.processtype,
								@module_info = req.module_info 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountusermgtcreateroleOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountusermgtcreateroleOutput outputobj = new UrmyaccountusermgtcreateroleOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountusermgtcreateroleOutput outputobj = new UrmyaccountusermgtcreateroleOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
