﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class URaddnewsiteinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class URaddnewsiteinfoInput
        {
            public int company_id { get; set; }
            public string site_name { get; set; }
            public string site_extn { get; set; }
            public string site_address { get; set; }
        }
        public class URaddnewsiteinfoOutput
        {           
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, URaddnewsiteinfoInput req)
        {
            List<URaddnewsiteinfoOutput> OutputList = new List<URaddnewsiteinfoOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_add_new_site_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id,
                                @site_name = req.site_name,
                                @site_extn = req.site_extn,
                                @site_address = req.site_address,

                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new URaddnewsiteinfoOutput()
                        {                          
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        URaddnewsiteinfoOutput outputobj = new URaddnewsiteinfoOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                URaddnewsiteinfoOutput outputobj = new URaddnewsiteinfoOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}