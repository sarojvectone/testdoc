using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using unifiedringmyaccountwebapi.Helpers;
using unifiedringmyaccountwebapi.Models;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrgetdeskphonemodelController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrcrmusergetInput
        {
            public int CustomerId { get; set; }
            public string Extension { get; set; }
            public string MAC_Address { get; set; } 
        }
        public class UrcrmusergetOutput
        {
            public long domain_id { get; set; }
            public string extension { get; set; }
            public string company_name { get; set; }
            public string site_code { get; set; }
            public string domain_name { get; set; }
            public string extension_name { get; set; }
            public string vm_password { get; set; }
            public string admin_password { get; set; } 
            public string deskphone_mac { get; set; }
            public string deskphone_model { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
            public string Effective_Caller_Id_Name { get; set; }      	   
           
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrcrmusergetInput req)
        {
            List<UrcrmusergetOutput> OutputList = new List<UrcrmusergetOutput>();
            try
            {

                var _domainId = OrderBL.Get_Customer_DomainId_By(req.CustomerId);//db hit
                var _deskphoneModel = OrderBL.GetDeskphoneModel(_domainId, req.Extension);//db hit
                var _company = OrderBL.GetCompanyDetailByDomain(_domainId);//db hit
                DeskPhoneModel objOutput = new DeskPhoneModel();
                if (_deskphoneModel == null)
                {
                    _deskphoneModel = new DeskPhoneModel()
                    {
                        extension = req.Extension,
                        vm_password = Deskphone_Configuration.GenerateIdentifier(12),
                        deskphone_mac = req.MAC_Address,
                        domain_id = _domainId,
                        admin_password = Deskphone_Configuration.GenerateIdentifier(12)
                    };

                }
                objOutput = _deskphoneModel;
                if (_deskphoneModel.admin_password == null)
                {
                    string randomNumberString = Deskphone_Configuration.GenerateIdentifierNum(4);
                    string sql = string.Format("crm_set_deskphone_admin_password {0},'{1}'", _company.company_id, randomNumberString);
                    using (DataMapper _imapper = new DataMapper() { CatalogName = "CRM_VB" })
                    {
                        var result = _imapper.MapDataToSingle<cOutMessage>(sql);
                        if (!result.ErrCode.Equals(0))
                        {
                            throw new Exception("Failed to generate deskphone admin password");
                        }
                        _deskphoneModel.admin_password = randomNumberString;
                    }
                }
                UrcrmusergetOutput outputobj = new UrcrmusergetOutput();
                outputobj.extension = _deskphoneModel.extension;
                outputobj.vm_password = _deskphoneModel.vm_password;
                outputobj.deskphone_mac = _deskphoneModel.deskphone_mac;
                outputobj.domain_id = _deskphoneModel.domain_id;
                outputobj.admin_password = _deskphoneModel.admin_password; 
                outputobj.errcode = 0;
                outputobj.errmsg = "Success";
                OutputList.Add(outputobj);
            }
            catch (Exception ex)
            {
                UrcrmusergetOutput outputobj = new UrcrmusergetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
        public class cOutMessage
        {
            public int ErrCode { get; set; }
            public string ErrMsg { get; set; }
        }
    }
}
