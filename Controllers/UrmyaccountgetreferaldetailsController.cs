using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountgetreferaldetailsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountgetreferaldetailsInput
        {
			public int company_id { get; set; }  	                   	    
        }
        public class UrmyaccountgetreferaldetailsOutput
        {
			public string Rfr_name { get; set; }
			public string Rfr_Work_email { get; set; }
			public int? No_of_employee { get; set; }
			public string Referral_link { get; set; }
			public string Link_Status { get; set; }
			public double? CreditAmount { get; set; }
			public DateTime? sent_date { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public string business_phoneno { get; set; }	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountgetreferaldetailsInput req)
        {
            List<UrmyaccountgetreferaldetailsOutput> OutputList = new List<UrmyaccountgetreferaldetailsOutput>();
			Log.Info("Input : UrmyaccountgetreferaldetailsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_get_referal_details";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountgetreferaldetailsOutput()
                        {
							Rfr_name = r.Rfr_name,
							Rfr_Work_email = r.Rfr_Work_email,
							No_of_employee = r.No_of_employee == null ? 0 : r.No_of_employee,
							Referral_link = r.Referral_link,
							Link_Status = r.Link_Status,
							CreditAmount = r.CreditAmount == null ? 0.0D : r.CreditAmount,
							sent_date = r.sent_date == null ? null : r.sent_date,
                            business_phoneno = r.business_phoneno,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountgetreferaldetailsOutput outputobj = new UrmyaccountgetreferaldetailsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountgetreferaldetailsOutput outputobj = new UrmyaccountgetreferaldetailsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
