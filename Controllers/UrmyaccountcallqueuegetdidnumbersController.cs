using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountcallqueuegetdidnumbersController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcallqueuegetdidnumbersInput
        {
			public int company_id { get; set; }
			public int queue_id { get; set; }  	                   	    
        }
        public class UrmyaccountcallqueuegetdidnumbersOutput
        {
			public int? Queue_id { get; set; }
			public string did_number { get; set; }
			public string Queue_name { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcallqueuegetdidnumbersInput req)
        {
            List<UrmyaccountcallqueuegetdidnumbersOutput> OutputList = new List<UrmyaccountcallqueuegetdidnumbersOutput>();
			Log.Info("Input : UrmyaccountcallqueuegetdidnumbersController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "urmyaccount_call_queue_get_did_numbers";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@queue_id = req.queue_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcallqueuegetdidnumbersOutput()
                        {
							Queue_id = r.Queue_id == null ? 0 : r.Queue_id,
							did_number = r.did_number,
							Queue_name = r.Queue_name,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcallqueuegetdidnumbersOutput outputobj = new UrmyaccountcallqueuegetdidnumbersOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcallqueuegetdidnumbersOutput outputobj = new UrmyaccountcallqueuegetdidnumbersOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
