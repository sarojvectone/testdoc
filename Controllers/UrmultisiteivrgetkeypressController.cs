using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmultisiteivrgetkeypressController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmultisiteivrgetkeypressInput
        {
			public int ivr_id { get; set; }
			public int key_value { get; set; }
			public int siteid { get; set; }  	                   	    
        }
        public class UrmultisiteivrgetkeypressOutput
        {
			public string site_name { get; set; }
			public string key_value { get; set; }
			public string ext_no { get; set; }
			public string Firstname { get; set; }
			public string Department { get; set; }
			public string Email { get; set; }
            public int? key_action { get; set; }
            public int? key_press_id { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmultisiteivrgetkeypressInput req)
        {
            List<UrmultisiteivrgetkeypressOutput> OutputList = new List<UrmultisiteivrgetkeypressOutput>();
			Log.Info("Input : UrmultisiteivrgetkeypressController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_multisite_ivr_get_key_press";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@ivr_id = req.ivr_id,
								@key_value = req.key_value,
								@siteid = req.siteid 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmultisiteivrgetkeypressOutput()
                        {
							site_name = r.site_name,
							key_value = r.key_value,
							ext_no = r.ext_no,
							Firstname = r.Firstname,
							Department = r.Department,
							Email = r.Email,
                            key_action = r.key_action == null ? 0 : r.key_action,
                            key_press_id = r.key_press_id == null ? 0 : r.key_press_id,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmultisiteivrgetkeypressOutput outputobj = new UrmultisiteivrgetkeypressOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmultisiteivrgetkeypressOutput outputobj = new UrmultisiteivrgetkeypressOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
