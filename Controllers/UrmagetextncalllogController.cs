using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmagetextncalllogController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmagetextncalllogInput
        {
			public int domain_id { get; set; }
			public int company_id { get; set; }
			public int extension { get; set; }
			public DateTime from_date { get; set; }
			public DateTime todate { get; set; }
            public int search_type { get; set; }
            public string search_type_text { get; set; }
            public string department { get; set; }
            public string call_type { get; set; }
        }
        public class UrmagetextncalllogOutput
        {
			public string dialled_from { get; set; }
			public string dialled_to { get; set; }
			public DateTime start_time { get; set; }
			public DateTime end_time { get; set; }
			public int duration { get; set; }
			public string call_recording { get; set; }
			public string call_action { get; set; }
			public string call_result { get; set; }
			public string dialled_from_name { get; set; }
			public string dialled_to_name { get; set; }
            public string call_type { get; set; }
            public string dialled_from_empid { get; set; }
            public string  dialled_to_empid { get; set; }
            public string  dialled_from_dept { get; set; }
            public string  dialled_to_dept { get; set; }
            public string did_type { get; set; }
            public string call_group_type { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; } 
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmagetextncalllogInput req)
        {
            List<UrmagetextncalllogOutput> OutputList = new List<UrmagetextncalllogOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_get_extn_call_log";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id,
								@company_id = req.company_id,
								@extension = req.extension,
								@from_date = req.from_date,
								@todate = req.todate ,
                                @search_type = req.search_type,
                                @search_type_text = req.search_type_text,
                                @department = req.department,
                                @call_type = req.call_type                                   
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmagetextncalllogOutput()
                        {
							dialled_from = r.dialled_from,
							dialled_to = r.dialled_to,
							start_time = r.start_time == null ? null : r.start_time,
							end_time = r.end_time == null ? null : r.end_time,
							duration = r.duration == null ? 0 : r.duration,
							call_recording = r.call_recording,
							call_action = r.call_action,
							call_result = r.call_result,
							dialled_from_name = r.dialled_from_name,
							dialled_to_name = r.dialled_to_name,
                            call_type = r.call_type,
                            dialled_from_empid = r.dialled_from_empid,
                            dialled_to_empid = r.dialled_to_empid,
                            dialled_from_dept = r.dialled_from_dept,
                            dialled_to_dept = r.dialled_to_dept,
                            did_type = r.did_type,
                            call_group_type = r.call_group_type,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmagetextncalllogOutput outputobj = new UrmagetextncalllogOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmagetextncalllogOutput outputobj = new UrmagetextncalllogOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
