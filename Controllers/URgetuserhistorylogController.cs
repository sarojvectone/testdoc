﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class URgetuserhistorylogController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class URgetuserhistorylogInput
        {
            public int company_id { get; set; }
        }
        public class URgetuserhistorylogOutput
        {
            public int? log_id { get; set; }
            public DateTime? log_date { get; set; }
            public string type { get; set; }
            public string category { get; set; }
            public string description { get; set; }
            public string created_by { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, URgetuserhistorylogInput req)
        {
            List<URgetuserhistorylogOutput> OutputList = new List<URgetuserhistorylogOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_get_user_history_log";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id

                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new URgetuserhistorylogOutput()
                        {
                            log_id = r.log_id == null ? 0 : r.log_id,
                            log_date = r.log_date == null ? null : r.log_date,
                            type = r.type,
                            category = r.category,
                            description = r.description,
                            created_by = r.created_by,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        URgetuserhistorylogOutput outputobj = new URgetuserhistorylogOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                URgetuserhistorylogOutput outputobj = new URgetuserhistorylogOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}