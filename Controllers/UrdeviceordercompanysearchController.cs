using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdeviceordercompanysearchController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdeviceordercompanysearchInput
        {
			public string company_name { get; set; }  	                   	    
        }
        public class UrdeviceordercompanysearchOutput
        {
			public int company_id { get; set; }
			public string company_name { get; set; }
			public string address1 { get; set; }
			public string address2 { get; set; }
			public string address3 { get; set; }
			public string city { get; set; }
			public string postcode { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdeviceordercompanysearchInput req)
        {
            List<UrdeviceordercompanysearchOutput> OutputList = new List<UrdeviceordercompanysearchOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_order_company_search";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_name = req.company_name 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrdeviceordercompanysearchOutput()
                        {
							company_id = r.company_id == null ? 0 : r.company_id,
							company_name = r.company_name,
							address1 = r.address1,
							address2 = r.address2,
							address3 = r.address3,
							city = r.city,
							postcode = r.postcode,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrdeviceordercompanysearchOutput outputobj = new UrdeviceordercompanysearchOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrdeviceordercompanysearchOutput outputobj = new UrdeviceordercompanysearchOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
