using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UruserextngetblocknumberController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UruserextngetblocknumberInput
        {
			public int domain_id { get; set; }
			public int extension { get; set; }  	                   	    
        }
        public class UruserextngetblocknumberOutput
        {
			public int block_id { get; set; }
			public int block_type { get; set; }
			public string ivr_file { get; set; }
			public string number { get; set; }
			public string name { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UruserextngetblocknumberInput req)
        {
            List<UruserextngetblocknumberOutput> OutputList = new List<UruserextngetblocknumberOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_user_extn_get_block_number";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id,
								@extension = req.extension 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UruserextngetblocknumberOutput()
                        {
							block_id = r.block_id == null ? 0 : r.block_id,
							block_type = r.block_type == null ? 0 : r.block_type,
							ivr_file = r.ivr_file,
							number = r.number,
							name = r.name,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UruserextngetblocknumberOutput outputobj = new UruserextngetblocknumberOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UruserextngetblocknumberOutput outputobj = new UruserextngetblocknumberOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
