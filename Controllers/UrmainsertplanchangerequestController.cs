using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmainsertplanchangerequestController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmainsertplanchangerequestInput
        {
            public int company_id { get; set; }
            public int product_id { get; set; }
            public int plan_id { get; set; }
            public int plan_duration { get; set; }
            public int bundle_id { get; set; }
            public double? plan_price { get; set; }
            public string reference_no { get; set; }
            public string plan_description { get; set; }
            public double? adjustment_charge { get; set; }
            public double? tax_fee { get; set; }
            public int tax_per { get; set; }
            public double? total_plan_charge { get; set; }
            public string card_type { get; set; }
            public string expiry_date { get; set; }
            public string cc_number { get; set; }
        }
        public class UrmainsertplanchangerequestOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmainsertplanchangerequestInput req)
        {
            List<UrmainsertplanchangerequestOutput> OutputList = new List<UrmainsertplanchangerequestOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_insert_plan_change_request";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id,
                                @product_id = req.product_id,
                                @plan_id = req.plan_id,
								@plan_duration = req.plan_duration,
                                @bundle_id = req.bundle_id,
                                @plan_price = req.plan_price,
                                @reference_no = req.reference_no,
                                @plan_description = req.plan_description,
                                @adjustment_charge = req.adjustment_charge,
                                @tax_fee = req.tax_fee,
                                @tax_per = req.tax_per,
                                @total_plan_charge = req.total_plan_charge,
                                @card_type = req.card_type,
                                @expiry_date = req.expiry_date,
                                @cc_number = req.cc_number
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmainsertplanchangerequestOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmainsertplanchangerequestOutput outputobj = new UrmainsertplanchangerequestOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmainsertplanchangerequestOutput outputobj = new UrmainsertplanchangerequestOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
