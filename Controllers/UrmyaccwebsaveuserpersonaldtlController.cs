using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebsaveuserpersonaldtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebsaveuserpersonaldtlInput
        {
			public int Company_id { get; set; }
			public string extension { get; set; }
            public string Firstname { get; set; }
            public string Lastname { get; set; }
            public string personal_number { get; set; }
			public int country_code { get; set; }  	                   	    
        }
        public class UrmyaccwebsaveuserpersonaldtlOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebsaveuserpersonaldtlInput req)
        {
            List<UrmyaccwebsaveuserpersonaldtlOutput> OutputList = new List<UrmyaccwebsaveuserpersonaldtlOutput>();
			Log.Info("Input : UrmyaccwebsaveuserpersonaldtlController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_save_User_personal_Dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_id = req.Company_id,
								@extension = req.extension,
								@Firstname = req.Firstname,
								@Lastname = req.Lastname,
								@personal_number = req.personal_number,
								@country_code = req.country_code 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccwebsaveuserpersonaldtlOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccwebsaveuserpersonaldtlOutput outputobj = new UrmyaccwebsaveuserpersonaldtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccwebsaveuserpersonaldtlOutput outputobj = new UrmyaccwebsaveuserpersonaldtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
