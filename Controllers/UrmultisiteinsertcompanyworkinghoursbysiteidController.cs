using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmultisiteinsertcompanyworkinghoursbysiteidController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmultisiteinsertcompanyworkinghoursbysiteidInput
        {
			public int Company_Id { get; set; }
			public string Working_Business_Type { get; set; }
			public string Working_Hours { get; set; }
			public string Extension_Number { get; set; }
			public int Type { get; set; }
			public int site_id { get; set; }  	                   	    
        }
        public class UrmultisiteinsertcompanyworkinghoursbysiteidOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmultisiteinsertcompanyworkinghoursbysiteidInput req)
        {
            List<UrmultisiteinsertcompanyworkinghoursbysiteidOutput> OutputList = new List<UrmultisiteinsertcompanyworkinghoursbysiteidOutput>();
			Log.Info("Input : UrmultisiteinsertcompanyworkinghoursbysiteidController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Multisite_Insert_Company_Working_Hours_by_site_id";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_Id = req.Company_Id,
								@Working_Business_Type = req.Working_Business_Type,
								@Working_Hours = req.Working_Hours,
								@Extension_Number = req.Extension_Number,
								@Type = req.Type,
								@site_id = req.site_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmultisiteinsertcompanyworkinghoursbysiteidOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmultisiteinsertcompanyworkinghoursbysiteidOutput outputobj = new UrmultisiteinsertcompanyworkinghoursbysiteidOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmultisiteinsertcompanyworkinghoursbysiteidOutput outputobj = new UrmultisiteinsertcompanyworkinghoursbysiteidOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
