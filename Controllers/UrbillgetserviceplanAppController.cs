using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrbillgetserviceplanAppController : ApiController
    {

       private static readonly Logger Log = LogManager.GetCurrentClassLogger();
       public class UrbillgetserviceplanAppInput
        {
            public int company_id { get; set; }           
        }
       public class UrbillgetserviceplanAppOutput
        {        
            public string bill_date { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }    	    
        }
        [Authorize]
       public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrbillgetserviceplanAppInput req)
        {
            List<UrbillgetserviceplanAppOutput> OutputList = new List<UrbillgetserviceplanAppOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_bill_get_service_plan";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id,                               
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrbillgetserviceplanAppOutput()
                        {            
                            bill_date = Convert.ToString(r.bill_date),
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrbillgetserviceplanAppOutput outputobj = new UrbillgetserviceplanAppOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrbillgetserviceplanAppOutput outputobj = new UrbillgetserviceplanAppOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
