﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebivrgetmusictypeController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebivrgetmusictypeInput
        {
        }
        public class UrmyaccwebivrgetmusictypeOutput
        {
            public int? MT_ID { get; set; }
            public string MT_Name { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Get(string id, HttpRequestMessage request, UrmyaccwebivrgetmusictypeInput req)
        {
            List<UrmyaccwebivrgetmusictypeOutput> OutputList = new List<UrmyaccwebivrgetmusictypeOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_IVR_get_music_type";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebivrgetmusictypeOutput()
                        {
                            MT_ID = r.MT_ID == null ? 0 : r.MT_ID,
                            MT_Name = r.MT_Name,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebivrgetmusictypeOutput outputobj = new UrmyaccwebivrgetmusictypeOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebivrgetmusictypeOutput outputobj = new UrmyaccwebivrgetmusictypeOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
