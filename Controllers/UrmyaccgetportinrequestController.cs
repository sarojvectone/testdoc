using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccgetportinrequestController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccgetportinrequestInput
        {
			public int company_id { get; set; }
			public int reqid { get; set; }  	                   	    
        }
        public class UrmyaccgetportinrequestOutput
        {
			public string number_type { get; set; }
			public string service_provider { get; set; }
			public string provider_address { get; set; }
			public string qn_1 { get; set; }
			public string qn_2 { get; set; }
			public string qn_3 { get; set; }
			public string qn_4 { get; set; }
			public string qn_5 { get; set; }
			public string qn_6 { get; set; }
			public string qn_7 { get; set; }
			public string bill_tele_ph_no { get; set; }
			public string check_bill_telno { get; set; }
			public string transfer_no { get; set; }
			public DateTime? transfer_date { get; set; }
			public string main_didno { get; set; }
			public string extension { get; set; }
			public string account_type { get; set; }
			public string jobtitle { get; set; }
			public string firstname { get; set; }
			public string surname { get; set; }
			public string company_name { get; set; }
			public string company_reg_no { get; set; }
			public string building_name { get; set; }
			public string address1 { get; set; }
			public string address2 { get; set; }
			public string city { get; set; }
			public string country { get; set; }
			public string postcode { get; set; }
			public string upload_path { get; set; }
			public string signedLOA_path { get; set; }
			public string comments { get; set; }
			public string send_emails_to { get; set; }
			public string order_number { get; set; }
            public string  replace_no { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccgetportinrequestInput req)
        {
            List<UrmyaccgetportinrequestOutput> OutputList = new List<UrmyaccgetportinrequestOutput>();
			Log.Info("Input : UrmyaccgetportinrequestController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myacc_get_portin_request";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@reqid = req.reqid 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccgetportinrequestOutput()
                        {
							number_type = r.number_type,
							service_provider = r.service_provider,
							provider_address = r.provider_address,
							qn_1 = r.qn_1,
							qn_2 = r.qn_2,
							qn_3 = r.qn_3,
							qn_4 = r.qn_4,
							qn_5 = r.qn_5,
							qn_6 = r.qn_6,
							qn_7 = r.qn_7,
							bill_tele_ph_no = r.bill_tele_ph_no,
							check_bill_telno = r.check_bill_telno,
							transfer_no = r.transfer_no,
							transfer_date = r.transfer_date == null ? null : r.transfer_date,
							main_didno = r.main_didno,
							extension = r.extension,
							account_type = r.account_type,
							jobtitle = r.jobtitle,
							firstname = r.firstname,
							surname = r.surname,
							company_name = r.company_name,
							company_reg_no = r.company_reg_no,
							building_name = r.building_name,
							address1 = r.address1,
							address2 = r.address2,
							city = r.city,
							country = r.country,
							postcode = r.postcode,
							upload_path = r.upload_path,
							signedLOA_path = r.signedLOA_path,
							comments = r.comments,
							send_emails_to = r.send_emails_to,
							order_number = r.order_number,
                            replace_no = r.replace_no,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccgetportinrequestOutput outputobj = new UrmyaccgetportinrequestOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccgetportinrequestOutput outputobj = new UrmyaccgetportinrequestOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
