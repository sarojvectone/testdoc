using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UraddphonegetdirectnumberController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UraddphonegetdirectnumberInput
        {
			public int company_id { get; set; }
			public int domain_id { get; set; }
			public int number_type { get; set; }
            public int type { get; set; }
            public string extn { get; set; }
        }
        public class UraddphonegetdirectnumberOutput
        {
			public int? od_id { get; set; }
			public string external_number { get; set; }
			public string country { get; set; }
			public string assigned_to { get; set; }
			public DateTime? assign_date { get; set; }
			public int? assign_type { get; set; }
			public int? status { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UraddphonegetdirectnumberInput req)
        {
            Log.Info("UraddphonegetdirectnumberController : Input " + JsonConvert.SerializeObject(req));
            List<UraddphonegetdirectnumberOutput> OutputList = new List<UraddphonegetdirectnumberOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_add_phone_get_direct_number";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@domain_id = req.domain_id,
								@number_type = req.number_type,
                                @type = req.type,       
                                @extn = req.extn
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UraddphonegetdirectnumberOutput()
                        {
							od_id = r.od_id == null ? 0 : r.od_id,
							external_number = r.external_number,
							country = r.country,
							assigned_to = r.assigned_to,
							assign_date = r.assign_date == null ? null : r.assign_date,
							assign_type = r.assign_type == null ? 0 : r.assign_type,
							status = r.status == null ? 0 : r.status,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UraddphonegetdirectnumberOutput outputobj = new UraddphonegetdirectnumberOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("UraddphonegetdirectnumberController :Error " + ex.Message);
                UraddphonegetdirectnumberOutput outputobj = new UraddphonegetdirectnumberOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
