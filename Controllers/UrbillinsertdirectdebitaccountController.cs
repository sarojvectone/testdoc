using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrbillinsertdirectdebitaccountController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrbillinsertdirectdebitaccountInput
        {
			public int? company_id { get; set; }
			public string firstname { get; set; }
			public string surname { get; set; }
			public string mobileno { get; set; }
			public string email { get; set; }
			public string address1 { get; set; }
			public string address2 { get; set; }
			public string city { get; set; }
			public string postcode { get; set; }
			public string country { get; set; }
			public string card_type { get; set; }
			public string expirydate { get; set; }
			public string cc_no { get; set; }
            public int process_flag { get; set; }
            public int status { get; set; }
        }
        public class UrbillinsertdirectdebitaccountOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrbillinsertdirectdebitaccountInput req)
        {
            List<UrbillinsertdirectdebitaccountOutput> OutputList = new List<UrbillinsertdirectdebitaccountOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_bill_insert_directdebit_account";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@firstname = req.firstname,
								@surname = req.surname,
								@mobileno = req.mobileno,
								@email = req.email,
								@address1 = req.address1,
								@address2 = req.address2,
								@city = req.city,
								@postcode = req.postcode,
								@country = req.country,
								@card_type = req.card_type,
								@expirydate = req.expirydate,
								@cc_no = req.cc_no,
                                @process_flag = req.process_flag,
                                @status = req.status  
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrbillinsertdirectdebitaccountOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrbillinsertdirectdebitaccountOutput outputobj = new UrbillinsertdirectdebitaccountOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrbillinsertdirectdebitaccountOutput outputobj = new UrbillinsertdirectdebitaccountOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
