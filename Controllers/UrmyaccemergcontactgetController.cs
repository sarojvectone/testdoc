using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccemergcontactgetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccemergcontactgetInput
        {
			public int company_id { get; set; }
			public string extension { get; set; }  	                   	    
        }
        public class UrmyaccemergcontactgetOutput
        {
			public int ec_id { get; set; }
			public string post_code { get; set; }
			public string address_line_1 { get; set; }
			public string address_line_2 { get; set; }
			public string address_line_3 { get; set; }
			public string city { get; set; }
			public string telephone_number { get; set; }
			public string new_telephone_number { get; set; }
			public string command { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccemergcontactgetInput req)
        {
            List<UrmyaccemergcontactgetOutput> OutputList = new List<UrmyaccemergcontactgetOutput>();
			Log.Info("Input : UrmyaccemergcontactgetController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Ur_myacc_emerg_contact_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@extension = req.extension 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccemergcontactgetOutput()
                        {
							ec_id = r.ec_id == null ? 0 : r.ec_id,
							post_code = r.post_code,
							address_line_1 = r.address_line_1,
							address_line_2 = r.address_line_2,
							address_line_3 = r.address_line_3,
							city = r.city,
							telephone_number = r.telephone_number,
							new_telephone_number = r.new_telephone_number,
							command = r.command,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccemergcontactgetOutput outputobj = new UrmyaccemergcontactgetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccemergcontactgetOutput outputobj = new UrmyaccemergcontactgetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
