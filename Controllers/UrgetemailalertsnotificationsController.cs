using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrgetemailalertsnotificationsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrgetemailalertsnotificationsInput
        {
            public int alert_type { get; set; }
            public int company_id { get; set; }
        }
        public class UrgetemailalertsnotificationsOutput
        {
			public string email_for_notif { get; set; }
			public string email_alert_notif { get; set; }
            public string  company_name { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrgetemailalertsnotificationsInput req)
        {
            List<UrgetemailalertsnotificationsOutput> OutputList = new List<UrgetemailalertsnotificationsOutput>();
			Log.Info("Input : UrgetemailalertsnotificationsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_get_email_alerts_notifications";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @alert_type = req.alert_type,
                                @company_id = req.company_id
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrgetemailalertsnotificationsOutput()
                        {
							email_for_notif = r.email_for_notif,
							email_alert_notif = r.email_alert_notif,
                            company_name = r.company_name,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrgetemailalertsnotificationsOutput outputobj = new UrgetemailalertsnotificationsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrgetemailalertsnotificationsOutput outputobj = new UrgetemailalertsnotificationsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
