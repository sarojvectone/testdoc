using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountresetpasswordbyadminController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountresetpasswordbyadminInput
        {
			public string externsion { get; set; }
			public int? company_id { get; set; }
			public string password { get; set; }
			public string ip_client { get; set; }
			public string password_updated_by { get; set; }  	                   	    
        }

        public class UrmyaccountresetpasswordbyadminOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountresetpasswordbyadminInput req)
        {
            List<UrmyaccountresetpasswordbyadminOutput> OutputList = new List<UrmyaccountresetpasswordbyadminOutput>();
			Log.Info("Input : UrmyaccountresetpasswordbyadminController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_reset_password_by_admin";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@externsion = req.externsion,
								@company_id = req.company_id,
								@password = req.password,
								@ip_client = req.ip_client,
								@password_updated_by = req.password_updated_by 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountresetpasswordbyadminOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountresetpasswordbyadminOutput outputobj = new UrmyaccountresetpasswordbyadminOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountresetpasswordbyadminOutput outputobj = new UrmyaccountresetpasswordbyadminOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
