﻿using Newtonsoft.Json;
using PushSharp.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class DidwwordernumberController:ApiController
    {
        public class DidwwordernumberInput 
        {
            public string AvailableDIDid { get; set; }
        }

        public class AvailableDID
        {
            public AvailableDIDdata data { get; set; }
        }
        public class AvailableDIDdata
        {
            public NumberAttributes attributes { get; set; }
            public string id { get; set; }
            public relationshipDID relationships { get; set; }
            public string type { get; set; }
        }
        public class relationshipDID
        {
            public relationshipDIDLinks did_group { get; set; }
        }
        public class relationshipDIDLinks
        {
            public relationshipLinks links { get; set; }
        }
        public class DIDgroup
        {
            public DIDgroupdata data { get; set; }
        }
        public class DIDgroupdata
        {
            public dataAttributes attributes { get; set; }
            public string id { get; set; }
            public OrderRelationships relationships { get; set; }
            public string type { get; set; }
            public dataMeta meta { get; set; }
        }
        public class OrderRelationships
        {
            public link country { get; set; }
            public link city { get; set; }
            public link did_group_type { get; set; }
            public link region { get; set; }
            public link stock_keeping_units { get; set; }
        }
        public class OrderDID
        {
            public Orderitem data { get; set; }
        }
        public class Orderitem
        {
            public string type = "orders";
            public OrderAttributes attributes { get; set; }
        }
        public class OrderAttributes
        {
            public string allow_back_ordering = "false";
            public List<OrderAttributesItem> items { get; set; }
            //public OrderAttributesItem items { get; set; }
        }
        public class OrderAttributesItem
        {
            public string type = "did_order_items";
            public OrderInputID attributes { get; set; }
        }
        public class OrderInputID
        {
            public string available_did_id { get; set; }
            public string sku_id { get; set; }
        }
        public class Error
        {
            public string title { get; set; }
            public string detail { get; set; }
            public string code { get; set; }
            public string status { get; set; }
        }

        public class ErrorResult
        {
            public List<Error> errors { get; set; }
        }
        public class resultOrder
        {
            public OrderData data { get; set; }
        }
        public class OrderData
        {
            public string id { get; set; }
            public string type { get; set; }
            public OrderNumberAttribute attributes { get; set; }
        }
        public class OrderNumberAttribute
        {
            public float amount { get; set; }
            public string status { get; set; }
            public string created_at { get; set; }
            public string description { get; set; }
            public string reference { get; set; }
            public List<resultOrderItems> items { get; set; }
        }
        public class resultOrderItems
        {
            public string type { get; set; }
            public resultOrderAttributes attributes { get; set; }
        }
        public class resultOrderAttributes
        {
            public int qty { get; set; }
            public float nrc { get; set; }
            public float mrc { get; set; }
            public string prorated_mrc { get; set; }
            public DateTime billed_from { get; set; }
            public DateTime billed_to { get; set; }
            public string did_group_id { get; set; }
        }
        public class NumberAttributes
        {
            public string number { get; set; }
        }
        public class relationshipLinks
        {
            public string related { get; set; }
            public string self { get; set; }
        }
        public class dataAttributes
        {
            public int prefix { get; set; }
            public int? local_prefix { get; set; }
            public List<string> features { get; set; }
            public string is_metered { get; set; }
            public string area_name { get; set; }
            public string allow_additional_channels { get; set; }
        }
        public class dataMeta
        {
            public string available_dids_enabled { get; set; }
            public string needs_registration { get; set; }
            public string is_available { get; set; }
            public string total_count { get; set; }
        }
        public class link
        {
            public relationshipLinks links { get; set; }
        }
        public class resultSKU
        {
            public List<SKUData> data { get; set; }
            public links links { get; set; }
            public meta meta { get; set; }
        }
        public class SKUData
        {
            public SKUAttributes attributes { get; set; }
            public string id { get; set; }
            public string type { get; set; }
        }
        public class SKUAttributes
        {
            public int channels_included_count { get; set; }
            public string monthly_price { get; set; }
            public string setup_price { get; set; }
        }
        public class meta
        {
            public int total_records { get; set; }
        }
        public class links
        {
            public string first { get; set; }
            public string next { get; set; }
            public string last { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, DidwwordernumberInput DidwwSingleNumberInput)
        {
            resultOrder Orderresponse = new resultOrder();
            string apiUrl = "https://api.didww.com/v3";
            WebClient client = new WebClient();
            //client.Headers["Content-Type"] = "application/json";
            client.Headers["Api-Key"] = "xkp0gtusdra585c11pzskho347aubc4h";
            client.Encoding = Encoding.UTF8;        
            //Get Available DID details
            string resultAvailableDID = client.DownloadString(apiUrl + "/available_dids/" + DidwwSingleNumberInput.AvailableDIDid);
            AvailableDID Response = JsonConvert.DeserializeObject<AvailableDID>(resultAvailableDID);
            string didGroupurl = Response.data.relationships.did_group.links.related;
            //Get DID group details
            string resultDIDgroup = client.DownloadString(didGroupurl);
            DIDgroup ResponseDIDgroup = JsonConvert.DeserializeObject<DIDgroup>(resultDIDgroup);
            string SKUurl = ResponseDIDgroup.data.relationships.stock_keeping_units.links.related;
            //Get SKU Id
            string resultSKUid = client.DownloadString(SKUurl);
            resultSKU ResponseSKU = JsonConvert.DeserializeObject<resultSKU>(resultSKUid);
            OrderDID InputData = new OrderDID();
            Orderitem Orderitem = new Orderitem();
            OrderAttributes OrderAttributes = new OrderAttributes();
            List<OrderAttributesItem> OrderAttrItemList = new List<OrderAttributesItem>();
            OrderAttributesItem OrderAttrItem = new OrderAttributesItem();
            OrderInputID OrderInID = new OrderInputID();
            //OrderInID.available_did_id = AvailableDIDid;
            OrderInID.available_did_id = DidwwSingleNumberInput.AvailableDIDid;
            OrderInID.sku_id = ResponseSKU.data[0].id;
            OrderAttrItem.attributes = OrderInID;
            OrderAttrItemList.Add(OrderAttrItem);
            OrderAttributes.items = OrderAttrItemList;
            Orderitem.attributes = OrderAttributes;
            InputData.data = Orderitem;
            string Input = JsonConvert.SerializeObject(InputData);
            //Order DID
            try
            {
                client.Headers.Add(HttpRequestHeader.ContentType, "application/vnd.api+json");
                client.Headers.Add(HttpRequestHeader.Accept, " application/vnd.api+json");
                string resultOrderDID = client.UploadString("https://api.didww.com/v3/orders", Input);
                resultOrder OrderResponse = JsonConvert.DeserializeObject<resultOrder>(resultSKUid);
                return Request.CreateResponse(HttpStatusCode.OK, OrderResponse);            
            }                
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    try
                    {
                        HttpWebResponse err = ex.Response as HttpWebResponse;
                        string errResponse = new StreamReader(err.GetResponseStream()).ReadToEnd();
                        ErrorResult response = JsonConvert.DeserializeObject<ErrorResult>(errResponse);
 
                        if (err != null)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, response);        
                        }
                    }
                    catch (Exception innerEx)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Error Ordering Number"); 
                    }
                }
                //else
                //{
                //}          
                return Request.CreateResponse(HttpStatusCode.OK, "Error Ordering Number"); 
            }
            
        }        
    }
}