﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebgetupdatecalleridnumberController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebgetupdatecalleridnumberInput
        {
            public int company_id { get; set; }
            public int extension { get; set; }
            public string caller_number { get; set; }
            //public string caller_type { get; set; }
            // string Type { get; set; }
        }



        public class UrmyaccwebgetupdatecalleridnumberOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebgetupdatecalleridnumberInput req)
        {
            List<UrmyaccwebgetupdatecalleridnumberOutput> OutputList = new List<UrmyaccwebgetupdatecalleridnumberOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_get_update_callerid_number";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id		= req.company_id,
                                @extension		= req.extension,
                                @caller_number = req.caller_number
                                //@caller_type	= req.caller_type,                             
                                //@Type			= req.Type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebgetupdatecalleridnumberOutput()
                        {
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebgetupdatecalleridnumberOutput outputobj = new UrmyaccwebgetupdatecalleridnumberOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebgetupdatecalleridnumberOutput outputobj = new UrmyaccwebgetupdatecalleridnumberOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}