using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdevicegetusertypeController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdevicegetusertypeInput
        {
			public int domain_id { get; set; }  	                   	    
        }
        public class UrdevicegetusertypeOutput
        {
			public int? user_type { get; set; }
			public int? tot_count { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public int?  unused_count { get; set; }    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdevicegetusertypeInput req)
        {
            List<UrdevicegetusertypeOutput> OutputList = new List<UrdevicegetusertypeOutput>();
			Log.Info("Input : UrdevicegetusertypeController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_get_user_type";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrdevicegetusertypeOutput()
                        {
                            unused_count = r.unused_count == null ? 0 :r.unused_count,
							user_type = r.user_type == null ? 0 : r.user_type,
							tot_count = r.tot_count == null ? 0 : r.tot_count,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrdevicegetusertypeOutput outputobj = new UrdevicegetusertypeOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrdevicegetusertypeOutput outputobj = new UrdevicegetusertypeOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
