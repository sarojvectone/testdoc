using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountgetcallqueueinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountgetcallqueueinfoInput
        {
			public int company_id { get; set; }
			public int queue_id { get; set; }  	                   	    
        }
        public class UrmyaccountgetcallqueueinfoOutput
        {
			public int Queue_id { get; set; }
			public string Queue_name { get; set; }
			public string extension { get; set; }
			public string Voicemail_pin { get; set; }
			public int Status { get; set; }
			public int Hour_Type { get; set; }
			public string Time_zone { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountgetcallqueueinfoInput req)
        {
            List<UrmyaccountgetcallqueueinfoOutput> OutputList = new List<UrmyaccountgetcallqueueinfoOutput>();
			Log.Info("Input : UrmyaccountgetcallqueueinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_get_call_queue_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@queue_id = req.queue_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountgetcallqueueinfoOutput()
                        {
							Queue_id = r.Queue_id == null ? 0 : r.Queue_id,
							Queue_name = r.Queue_name,
							extension = r.extension,
							Voicemail_pin = r.Voicemail_pin,
							Status = r.Status == null ? 0 : r.Status,
							Hour_Type = r.Hour_Type == null ? 0 : r.Hour_Type,
							Time_zone = r.Time_zone,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountgetcallqueueinfoOutput outputobj = new UrmyaccountgetcallqueueinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountgetcallqueueinfoOutput outputobj = new UrmyaccountgetcallqueueinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
