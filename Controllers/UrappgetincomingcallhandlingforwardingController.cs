using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappgetincomingcallhandlingforwardingController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappgetincomingcallhandlingforwardingInput
        {
			public int company_id { get; set; }
			public int Extension { get; set; }  	                   	    
        }
        public class UrappgetincomingcallhandlingforwardingOutput
        {
			public int CHF_ID { get; set; }
			public int Company_id { get; set; }
			public int Extension { get; set; }
			public int Hour_Type { get; set; }
			public int Incoming_call_type { get; set; }
			public int Ring_type { get; set; }
			public string Name { get; set; }
			public int App_status { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappgetincomingcallhandlingforwardingInput req)
        {
            List<UrappgetincomingcallhandlingforwardingOutput> OutputList = new List<UrappgetincomingcallhandlingforwardingOutput>();
			Log.Info("Input : UrappgetincomingcallhandlingforwardingController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_app_get_Incoming_Call_handling_forwarding";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@Extension = req.Extension 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrappgetincomingcallhandlingforwardingOutput()
                        {
							CHF_ID = r.CHF_ID == null ? 0 : r.CHF_ID,
							Company_id = r.Company_id == null ? 0 : r.Company_id,
							Extension = r.Extension == null ? 0 : r.Extension,
							Hour_Type = r.Hour_Type == null ? 0 : r.Hour_Type,
							Incoming_call_type = r.Incoming_call_type == null ? 0 : r.Incoming_call_type,
							Ring_type = r.Ring_type == null ? 0 : r.Ring_type,
							Name = r.Name,
							App_status = r.App_status == null ? 0 : r.App_status,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrappgetincomingcallhandlingforwardingOutput outputobj = new UrappgetincomingcallhandlingforwardingOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrappgetincomingcallhandlingforwardingOutput outputobj = new UrappgetincomingcallhandlingforwardingOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
