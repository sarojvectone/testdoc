using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountcallqueuesavevoicemailgreetingsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcallqueuesavevoicemailgreetingsInput
        {
			public int? company_id { get; set; }
			public int? call_queue_id { get; set; }
			public string greeting_type { get; set; }
			public string greeting_audio_type { get; set; }
			public string audio_file_name { get; set; }
			public string greeting_content { get; set; }
			public int? Hour_type { get; set; }
			public int? status { get; set; }  	                   	    
        }
        public class UrmyaccountcallqueuesavevoicemailgreetingsOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcallqueuesavevoicemailgreetingsInput req)
        {
            List<UrmyaccountcallqueuesavevoicemailgreetingsOutput> OutputList = new List<UrmyaccountcallqueuesavevoicemailgreetingsOutput>();
			Log.Info("Input : UrmyaccountcallqueuesavevoicemailgreetingsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_call_queue_save_voicemail_greetings";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@call_queue_id = req.call_queue_id,
								@greeting_type = req.greeting_type,
								@greeting_audio_type = req.greeting_audio_type,
								@audio_file_name = req.audio_file_name,
								@greeting_content = req.greeting_content,
								@Hour_type = req.Hour_type,
								@status = req.status 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcallqueuesavevoicemailgreetingsOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcallqueuesavevoicemailgreetingsOutput outputobj = new UrmyaccountcallqueuesavevoicemailgreetingsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcallqueuesavevoicemailgreetingsOutput outputobj = new UrmyaccountcallqueuesavevoicemailgreetingsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
