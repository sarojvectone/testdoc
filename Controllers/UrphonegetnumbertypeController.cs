﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class UrphonegetnumbertypeController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrphonegetnumbertypeInput
        {            
        }
        public class UrphonegetnumbertypeOutput
        {
            public int num_type_id { get; set; }
            public string num_type_desc { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Get(string id, HttpRequestMessage request, UrphonegetnumbertypeInput req)
        {
            List<UrphonegetnumbertypeOutput> OutputList = new List<UrphonegetnumbertypeOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_phone_get_number_type";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {                                
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrphonegetnumbertypeOutput()
                        {
                            num_type_id = r.num_type_id == null ? null : r.num_type_id,
                            num_type_desc = r.num_type_desc,                            
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrphonegetnumbertypeOutput outputobj = new UrphonegetnumbertypeOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrphonegetnumbertypeOutput outputobj = new UrphonegetnumbertypeOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}