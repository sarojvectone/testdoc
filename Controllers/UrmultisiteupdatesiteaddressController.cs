using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmultisiteupdatesiteaddressController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmultisiteupdatesiteaddressInput
        {
			public int company_id { get; set; }
			public int site_id { get; set; }
			public string address1 { get; set; }
			public string address2 { get; set; }
			public string address3 { get; set; }
			public string postcode { get; set; }
			public string county { get; set; }
			public string country { get; set; }
            public string city { get; set; }           	    
        }
        public class UrmultisiteupdatesiteaddressOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmultisiteupdatesiteaddressInput req)
        {
            List<UrmultisiteupdatesiteaddressOutput> OutputList = new List<UrmultisiteupdatesiteaddressOutput>();
			Log.Info("Input : UrmultisiteupdatesiteaddressController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Multisite_update_site_address";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@site_id = req.site_id,
								@address1 = req.address1,
								@address2 = req.address2,
								@address3 = req.address3,
								@postcode = req.postcode,
								@county = req.county,
								@country = req.country,
                                @city = req.city                                   
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmultisiteupdatesiteaddressOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmultisiteupdatesiteaddressOutput outputobj = new UrmultisiteupdatesiteaddressOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmultisiteupdatesiteaddressOutput outputobj = new UrmultisiteupdatesiteaddressOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
