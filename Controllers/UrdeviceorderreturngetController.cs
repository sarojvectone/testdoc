using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdeviceorderreturngetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdeviceorderreturngetInput
        {
			public int company_id { get; set; }  	                   	    
        }
        public class UrdeviceorderreturngetOutput
        {
			public int order_id { get; set; }
			public string prod_code { get; set; }
			public string prod_name { get; set; }
			public string prod_model { get; set; }
			public DateTime? rtn_date { get; set; }
			public int rtn_quantity { get; set; }
			public double? rtn_charge { get; set; }
			public string rtn_reason { get; set; }
			public int rtn_status { get; set; }
            public int id { get; set; }
            public string tracking_no { get; set; }
            public string comments { get; set; }
            public string company_name { get; set; }
            public int company_id { get; set; }
            public DateTime? last_update { get; set; }
            public string status_desc { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	   
         	   
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdeviceorderreturngetInput req)
        {
            List<UrdeviceorderreturngetOutput> OutputList = new List<UrdeviceorderreturngetOutput>();
			Log.Info("Input : UrdeviceorderreturngetController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_order_return_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrdeviceorderreturngetOutput()
                        {
							order_id = r.order_id == null ? 0 : r.order_id,
							prod_code = r.prod_code,
							prod_name = r.prod_name,
							prod_model = r.prod_model,
							rtn_date = r.rtn_date == null ? null : r.rtn_date,
							rtn_quantity = r.rtn_quantity == null ? 0 : r.rtn_quantity,
                            rtn_charge = r.rtn_charge == null ? 0.0F : r.rtn_charge, 
							//rtn_charge = r.rtn_charge,
							rtn_reason = r.rtn_reason,
							rtn_status = r.rtn_status == null ? 0 : r.rtn_status,
                            id = r.id == null ? 0 : r.id,
                            company_name = r.company_name,
                            comments = r.comments,
                            tracking_no = r.tracking_no,
                            company_id = r.company_id == null ? 0 : r.company_id,
                            last_update  = r.last_update == null ? null : r.last_update,
                            status_desc = r.status_desc,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrdeviceorderreturngetOutput outputobj = new UrdeviceorderreturngetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrdeviceorderreturngetOutput outputobj = new UrdeviceorderreturngetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
