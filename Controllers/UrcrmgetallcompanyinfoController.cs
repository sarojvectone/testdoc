using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{

    public class UrcrmgetallcompanyinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrcrmgetallcompanyinfoInput
        { 
        }
        public class UrcrmgetallcompanyinfoOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public int? company_id { get; set; }
            public int? order_id { get; set; }
            public int? customer_id { get; set; }
            public string company_name { get; set; }
            public string account_status { get; set; }
            public string postcode { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string address3 { get; set; }
            public string city { get; set; }
            public string email { get; set; }
            public DateTime? comp_last_login { get; set; }
            public string comp_last_login_str { get { return comp_last_login != null ? Convert.ToDateTime(comp_last_login).ToString("dd-MM-yyyy") : ""; } }

            public string switch_board_no { get; set; }
            public string extension { get; set; }
            public DateTime? order_date { get; set; }
            public string order_date_str { get { return order_date != null ? Convert.ToDateTime(order_date).ToString("dd-MM-yyyy") : ""; } }
            public int? role_id { get; set; }
            public int? dnd_type { get; set; }
            public int? domain_id { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrcrmgetallcompanyinfoInput req)
        {
            List<UrcrmgetallcompanyinfoOutput> OutputList = new List<UrcrmgetallcompanyinfoOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_crm_get_all_company_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrcrmgetallcompanyinfoOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg,
                            company_id = r.company_id,
                            order_id = r.order_id,
                            customer_id = r.customer_id,
                            company_name = r.company_name,
                            account_status = r.account_status,
                            postcode = r.postcode,
                            address1 = r.address1,
                            address2 = r.address2,
                            address3 = r.address3,
                            city = r.city,
                            email = r.email,
                            comp_last_login = r.comp_last_login,
                            extension = r.extension,
                            switch_board_no = r.switch_board_no,
                            order_date = r.order_date,
                            role_id = r.role_id,
                            dnd_type = r.dnd_type,
                            domain_id =r.domain_id,
                        }));
                    }
                    else
                    {
                        UrcrmgetallcompanyinfoOutput outputobj = new UrcrmgetallcompanyinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrcrmgetallcompanyinfoOutput outputobj = new UrcrmgetallcompanyinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
