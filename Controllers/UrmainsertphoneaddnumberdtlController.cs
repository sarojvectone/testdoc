using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmainsertphoneaddnumberdtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmainsertphoneaddnumberdtlInput
        {
			public int company_id { get; set; }
			public int domain_id { get; set; }
			public string extension { get; set; }
			public string external_number { get; set; }
			public int assign_type { get; set; }
			public string charge_price { get; set; }
			public float prorata { get; set; }
			public string tax_per { get; set; }
			public float tax_fee { get; set; }
			public string total_charge { get; set; }
			public string reference_no { get; set; }
            public string upload_path { get; set; } 
      	    public string pay_reference { get; set; }  
            public double? pay_maount { get; set; } 
            public string pay_status { get; set; }
            //public int call_queue_id { get; set; }
            //public string country { get; set; }
            //public string number_type { get; set; }
        }


// @company_id	int
//@domain_id	int
//@extension	varchar
//@external_number	varchar
//@assign_type	int
//@charge_price	float
//@prorata	float
//@tax_per	varchar
//@tax_fee	float
//@total_charge	float
//@reference_no	varchar
//@country	varchar
//@number_type	varchar
//@errcode	int
//@errmsg	varchar
//@called_by	varchar
//@site_id	int
//@upload_path	varchar
//@ivr_id	int
//@pay_reference	varchar
//@pay_maount	float
//@pay_status	varchar
//@call_queue_id	int

        public class UrmainsertphoneaddnumberdtlOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmainsertphoneaddnumberdtlInput req)
        {
            List<UrmainsertphoneaddnumberdtlOutput> OutputList = new List<UrmainsertphoneaddnumberdtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_insert_phone_add_number_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@domain_id = req.domain_id,
								@extension = req.extension,
								@external_number = req.external_number,
								@assign_type = req.assign_type,
								@charge_price = req.charge_price,
								@prorata = req.prorata,
								@tax_per = req.tax_per,
								@tax_fee = req.tax_fee,
								@total_charge = req.total_charge,
								@reference_no = req.reference_no,
                                @upload_path = req.upload_path,
                                @pay_reference = req.pay_reference,
                                @pay_maount = req.pay_maount,
                                @pay_status  = req.pay_status,
                                //@call_queue_id = req.call_queue_id,
                                //@country = req.country,
                                //@number_type = req.number_type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmainsertphoneaddnumberdtlOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmainsertphoneaddnumberdtlOutput outputobj = new UrmainsertphoneaddnumberdtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmainsertphoneaddnumberdtlOutput outputobj = new UrmainsertphoneaddnumberdtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
