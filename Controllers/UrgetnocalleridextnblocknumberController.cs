using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrgetnocalleridextnblocknumberController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrgetnocalleridextnblocknumberInput
        {
			public int company_id { get; set; }
			public int extension { get; set; }  	                   	    
        }
        public class UrgetnocalleridextnblocknumberOutput
        {
			public int? company_id { get; set; }
			public string Extension_Number { get; set; }
			public int? bc_no_caller_id { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrgetnocalleridextnblocknumberInput req)
        {
            List<UrgetnocalleridextnblocknumberOutput> OutputList = new List<UrgetnocalleridextnblocknumberOutput>();
			Log.Info("Input : UrgetnocalleridextnblocknumberController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_get_nocallerid_extn_block_number";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@extension = req.extension 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrgetnocalleridextnblocknumberOutput()
                        {
							company_id = r.company_id == null ? 0 : r.company_id,
							Extension_Number = r.Extension_Number,
							bc_no_caller_id = r.bc_no_caller_id == null ? 0 : r.bc_no_caller_id,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrgetnocalleridextnblocknumberOutput outputobj = new UrgetnocalleridextnblocknumberOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrgetnocalleridextnblocknumberOutput outputobj = new UrgetnocalleridextnblocknumberOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
