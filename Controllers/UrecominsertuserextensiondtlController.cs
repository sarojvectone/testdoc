using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using System.IO;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrecominsertuserextensiondtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrecominsertuserextensiondtlInput
        {
            public int? Basket_id { get; set; }
            public int? Company_id { get; set; }
            public string Firstname { get; set; }
            public string Surname { get; set; }
            public string Department { get; set; }
            public string Email { get; set; }
            public string Mobileno { get; set; }
            public string Direct_Number { get; set; }
            public string Extension_Number { get; set; }
            public string User_Language { get; set; }
            public string Greeting_Language { get; set; }
            public string Regional_Language { get; set; }
            public int? Role_Id { get; set; }
            public string extn_audio_file { get; set; }
            public string extn_name_audio_file { get; set; }
            public int site_id { get; set; }
            public int? account_plan_id { get; set; }
            public string emp_id { get; set; }
            public string original_extension { get; set; }
            public int Hubspot_contactid { get; set; }
            public string Phone_id { get; set; }
            public int? user_type { get; set; }
            public string Pay_Reference { get; set; }
            public double? pay_amount { get; set; }
            public string Paystatus { get; set; }
            public double? tax_per { get; set; }
            //public int? tax_fee { get; set; }
            public double? tax_fee { get; set; }
            public double? total_charge { get; set; }
            public double? prorata_adjustment { get; set; }
            public int? is_prepaid_charging { get; set; }
            public string pp_Pay_Reference { get; set; }
            public double? pp_pay_amount { get; set; }
            public string pp_Paystatus { get; set; }
            public double? pp_tax_per { get; set; }
            //public int? pp_tax_fee { get; set; }
            public double? pp_tax_fee { get; set; }
            public double? pp_total_charge { get; set; }
            public double? pp_prorata_adjustment { get; set; }
            public string pp_last4digit_cc { get; set; }
        }


        public class UrecominsertuserextensiondtlOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
            public string host_address { get; set; }
            public int? sip_id { get; set; }
            public string sip_password { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrecominsertuserextensiondtlInput req)
        {
            Log.Info("Input UrecominsertuserextensiondtlController: Input" + JsonConvert.SerializeObject(req));
            List<UrecominsertuserextensiondtlOutput> OutputList = new List<UrecominsertuserextensiondtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ECom_Insert_User_Extension_Dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @Basket_id = req.Basket_id,
                                @Company_id = req.Company_id,
                                @Firstname = req.Firstname,
                                @Surname = req.Surname,
                                @Department = req.Department,
                                @Email = req.Email,
                                @Mobileno = req.Mobileno,
                                @Direct_Number = req.Direct_Number,
                                @Extension_Number = req.Extension_Number,
                                @User_Language = req.User_Language,
                                @Greeting_Language = req.Greeting_Language,
                                @Regional_Language = req.Regional_Language,
                                @Role_Id = req.Role_Id,
                                @extn_audio_file = req.extn_audio_file,
                                @extn_name_audio_file = req.extn_name_audio_file,
                                @account_plan_id = req.account_plan_id,
                                @emp_id = req.emp_id,
                                @site_id = req.site_id,
                                @original_extension = req.original_extension,
                                @Hubspot_contactid = req.Hubspot_contactid,
                                @Phone_id = req.Phone_id,
                                @user_type = req.user_type,
                                @Pay_Reference= req.Pay_Reference,
                                @pay_amount = req.pay_amount,
                                @Paystatus = req.Paystatus,
                                @tax_per = req.tax_per,
                                @tax_fee = req.tax_fee,
                                @total_charge = req.total_charge,
                                @prorata_adjustment = req.prorata_adjustment,
                                @is_prepaid_charging = req.is_prepaid_charging,
                                @pp_Pay_Reference = req.pp_Pay_Reference,
                                @pp_pay_amount =  req.pp_pay_amount,
                                @pp_Paystatus = req.pp_Paystatus,
                                @pp_tax_per = req.pp_tax_per,
                                @pp_tax_fee = req.pp_tax_fee,
                                @pp_total_charge = req.pp_total_charge,
                                @pp_prorata_adjustment = req.pp_prorata_adjustment, 
                                @pp_last4digit_cc = req.pp_last4digit_cc

                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Info("Input UrecominsertuserextensiondtlController: Output" + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrecominsertuserextensiondtlOutput()
                        {
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg,
                            sip_password = r.sip_password,
                            sip_id = r.sip_id == null ? 0 : r.sip_id,
                            host_address = r.host_address
                            
                        }));
                    }
                    else
                    {
                        UrecominsertuserextensiondtlOutput outputobj = new UrecominsertuserextensiondtlOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                    
                    if (OutputList[0].errcode == 0)
                    {
                        
                        if (!string.IsNullOrEmpty(OutputList[0].sip_password) && OutputList[0].sip_id != 0)
                        {
                            //Commented  : 02Oct2020 - To avoid multiple user registration in cluster

                            //try
                            //{
                            //    //var webAddr1 = ConfigurationManager.AppSettings["ejabberdchat_mobileregister"];
                            //    var webAddr1 = ConfigurationManager.AppSettings["ejabberdchat_mobileregister_host"];
                            //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            //    var httpWebRequest1 = (HttpWebRequest)WebRequest.Create(webAddr1);
                            //    httpWebRequest1.ContentType = "application/json";
                            //    httpWebRequest1.Method = "POST";
                            //    // string json = "{\"userid\": \"" + OutputList[0].sip_id + "\",\"password\": \"" + OutputList[0].sip_password + "\"}";

                            //    string autorization = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImcuYW5uYW1hbGFpQHZlY3RvbmUuY29tIiwicGFzc3dvcmQiOiIxMjM0NTYiLCJkZXZpY2VJZCI6ImFiY2QiLCJzaXBMb2dpbklkIjoiMzE3OCIsInJvbGVJZCI6MywiaWF0IjoxNTkyMjIyNzY5fQ.6CE_6uTy8mOvzOwrB3B75jMNAkDPGAbWKG1rB1ePy9s";
                            //    //autorization = autorization;
                            //    httpWebRequest1.Headers.Add("AUTHORIZATION", autorization);

                            //    string json = "{\"userid\": \"" + OutputList[0].sip_id + "\",\"company_id\": \"" + req.Company_id + "\",\"host\": \"" + OutputList[0].host_address + "\",\"password\": \"" + OutputList[0].sip_password + "\"}";

                            //    using (var streamWriter = new StreamWriter(httpWebRequest1.GetRequestStream()))
                            //    {
                            //        streamWriter.Write(json);
                            //        streamWriter.Flush();
                            //    }
                            //    var httpResponse1 = (HttpWebResponse)httpWebRequest1.GetResponse();
                            //    Log.Info("Output ejabberdchat_mobileregister: " + JsonConvert.SerializeObject(httpResponse1));
                            //}
                            //catch (Exception ex)
                            //{
                            //    Log.Info("Error ejabberdchat_mobileregister_host: " + ex.Message.ToString());
                            //}
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                UrecominsertuserextensiondtlOutput outputobj = new UrecominsertuserextensiondtlOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
