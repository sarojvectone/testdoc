using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrgetproductdeviceController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        //New output: brand-varchar, prod_weight- float
        public class UrgetproductdeviceInput
        {
			//public int company_id { get; set; }
			public int? prod_id { get; set; }
            public string category_id { get; set; }
            public string inventory_id { get; set; }
            public string  prod_model { get; set; }
        }

        public class UrgetproductdeviceOutput
        {
			public int prod_id { get; set; }
			public int inventory_id { get; set; }
			//public int model_id { get; set; }
            public int category_id { get; set; }
            public string prod_model { get; set; }
			public string prod_code { get; set; }
			public string prod_name { get; set; }
            public int stock_add_flag{ get; set; }
            public string  brand { get; set; }
            public double? prod_weight { get; set; }
            public int status { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrgetproductdeviceInput req)
        {
            List<UrgetproductdeviceOutput> OutputList = new List<UrgetproductdeviceOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_get_product_device";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {								
								@prod_id = req.prod_id,
                                @category_id = req.category_id, 
                                @prod_model = req.prod_model,
                                @inventory_id = req.inventory_id  
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrgetproductdeviceOutput()
                        {
							prod_id = r.prod_id == null ? 0 : r.prod_id,
							inventory_id = r.inventory_id == null ? 0 : r.inventory_id,
							//model_id = r.model_id == null ? 0 : r.model_id,
                            category_id = r.category_id == null ? 0 : r.category_id,
                            prod_model = r.prod_model,
							prod_code = r.prod_code,
							prod_name = r.prod_name,
                            stock_add_flag = r.stock_add_flag == null ? 0 : r.stock_add_flag,
                            brand = r.brand,
                            prod_weight = r.prod_weight == null ? 0 : r.prod_weight,
                            status = r.status == null ? 0 : r.status,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrgetproductdeviceOutput outputobj = new UrgetproductdeviceOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrgetproductdeviceOutput outputobj = new UrgetproductdeviceOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
