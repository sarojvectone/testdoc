using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappupdatedesktopcompanysettingController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappupdatedesktopcompanysettingInput
        {
			public int company_id { get; set; }
			public int allow_invite { get; set; }
			public int allow_without_signup { get; set; }
			public int giphy_sharing { get; set; }  	                   	    
        }
        public class UrappupdatedesktopcompanysettingOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappupdatedesktopcompanysettingInput req)
        {
            List<UrappupdatedesktopcompanysettingOutput> OutputList = new List<UrappupdatedesktopcompanysettingOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_update_desktop_company_setting";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@allow_invite = req.allow_invite,
								@allow_without_signup = req.allow_without_signup,
								@giphy_sharing = req.giphy_sharing 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrappupdatedesktopcompanysettingOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrappupdatedesktopcompanysettingOutput outputobj = new UrappupdatedesktopcompanysettingOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrappupdatedesktopcompanysettingOutput outputobj = new UrappupdatedesktopcompanysettingOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
