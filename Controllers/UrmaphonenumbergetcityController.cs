using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaphonenumbergetcityController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaphonenumbergetcityInput
        {
            public int? countryid { get; set; }
            //public string country { get; set; }
            //public int state_id { get; set; }  	                   	    
        }
        public class UrmaphonenumbergetcityOutput
        {
			public int? pk_city_id { get; set; }
			public string area_code { get; set; }
			public string area { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaphonenumbergetcityInput req)
        {
            List<UrmaphonenumbergetcityOutput> OutputList = new List<UrmaphonenumbergetcityOutput>();
			Log.Info("Input : UrmaphonenumbergetcityController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_phone_number_get_city";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @countryid = req.countryid
                                //@country = req.country,
                                //@state_id = req.state_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmaphonenumbergetcityOutput()
                        {
							pk_city_id = r.pk_city_id == null ? 0 : r.pk_city_id,
                            area_code = r.area_code,
							area = r.area,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmaphonenumbergetcityOutput outputobj = new UrmaphonenumbergetcityOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmaphonenumbergetcityOutput outputobj = new UrmaphonenumbergetcityOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
