﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using PushSharp.Core;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class UrappteamcloseconversationController:ApiController
    {
        public class UrappteamcloseconversationInput
        {
            public int company_id { get; set; }
            public string team { get; set; }
            public int close_conversation { get; set; }
        }

        public class UrappteamcloseconversationOutput
        {
            //public int? close_conversation { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappteamcloseconversationInput req)
        {
            List<UrappteamcloseconversationOutput> OutputList = new List<UrappteamcloseconversationOutput>();
            Log.Info("Urappteamcloseconversation:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_team_close_conversation";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id,
                                @team = req.team,
                                @close_conversation = req.close_conversation
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        Log.Info("Urappteamcloseconversation Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrappteamcloseconversationOutput()
                        {
                            //close_conversation = r.close_conversation == null ? 0 : r.close_conversation,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        Log.Info("Urappteamcloseconversation Output DB : " + "Empty result");
                        UrappteamcloseconversationOutput outputobj = new UrappteamcloseconversationOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                UrappteamcloseconversationOutput outputobj = new UrappteamcloseconversationOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }

    }
}