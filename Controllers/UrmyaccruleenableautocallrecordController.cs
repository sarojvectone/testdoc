using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccruleenableautocallrecordController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccruleenableautocallrecordInput
        {
			public int company_id { get; set; }
			public string extension { get; set; }
			public int recording_type { get; set; }
			public int rule_type { get; set; }
			public string department { get; set; }
			public string create_by { get; set; }
            public int process_type{ get; set; }   	    
        }
        public class UrmyaccruleenableautocallrecordOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccruleenableautocallrecordInput req)
        {
            List<UrmyaccruleenableautocallrecordOutput> OutputList = new List<UrmyaccruleenableautocallrecordOutput>();
			Log.Info("Input : UrmyaccruleenableautocallrecordController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Ur_myacc_rule_enable_auto_call_record";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@extension = req.extension,
								@recording_type = req.recording_type,
								@rule_type = req.rule_type,
								@department = req.department,
								@create_by = req.create_by,
                                @process_type = req.process_type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccruleenableautocallrecordOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccruleenableautocallrecordOutput outputobj = new UrmyaccruleenableautocallrecordOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccruleenableautocallrecordOutput outputobj = new UrmyaccruleenableautocallrecordOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
