using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdeviceorderproductsearchController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdeviceorderproductsearchInput
        {
			public string product { get; set; }  	                   	    
        }
        public class UrdeviceorderproductsearchOutput
        {
			public string prod_code { get; set; }
			public string prod_name { get; set; }
			public int category_id { get; set; }
			public string prod_model { get; set; }
			public int qty_threshold { get; set; }
			public int available_qty { get; set; }
			public double buy_price { get; set; }
			public double ws_price { get; set; }
			public double ur_price { get; set; }
			public string brand { get; set; }
			public double prod_weight { get; set; }
			public string prod_short_desc { get; set; }
			public string prod_desc { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdeviceorderproductsearchInput req)
        {
            List<UrdeviceorderproductsearchOutput> OutputList = new List<UrdeviceorderproductsearchOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_order_product_search";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@product = req.product 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrdeviceorderproductsearchOutput()
                        {
							prod_code = r.prod_code,
							prod_name = r.prod_name,
							category_id = r.category_id == null ? 0 : r.category_id,
							prod_model = r.prod_model,
							qty_threshold = r.qty_threshold == null ? 0 : r.qty_threshold,
							available_qty = r.available_qty == null ? 0 : r.available_qty,
							buy_price = r.buy_price == null ? 0.0D : r.buy_price,
							ws_price = r.ws_price == null ? 0.0D : r.ws_price,
							ur_price = r.ur_price == null ? 0.0D : r.ur_price,
							brand = r.brand,
							prod_weight = r.prod_weight == null ? 0.0D : r.prod_weight,
							prod_short_desc = r.prod_short_desc,
							prod_desc = r.prod_desc,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrdeviceorderproductsearchOutput outputobj = new UrdeviceorderproductsearchOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrdeviceorderproductsearchOutput outputobj = new UrdeviceorderproductsearchOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
