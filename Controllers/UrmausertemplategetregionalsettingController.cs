using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmausertemplategetregionalsettingController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmausertemplategetregionalsettingInput
        {
			public int temp_id { get; set; }  	                   	    
        }
        public class UrmausertemplategetregionalsettingOutput
        {
			public string time_zone { get; set; }
			public string home_country_code { get; set; }
			public string greetings_language { get; set; }
			public int? time_format { get; set; }
			public string user_language { get; set; }
			public string regional_format { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmausertemplategetregionalsettingInput req)
        {
            List<UrmausertemplategetregionalsettingOutput> OutputList = new List<UrmausertemplategetregionalsettingOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_user_template_get_regional_setting";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@temp_id = req.temp_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmausertemplategetregionalsettingOutput()
                        {
							time_zone = r.time_zone,
							home_country_code = r.home_country_code,
							greetings_language = r.greetings_language,
							time_format = r.time_format == null ? 0 : r.time_format,
							user_language = r.user_language,
							regional_format = r.regional_format,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmausertemplategetregionalsettingOutput outputobj = new UrmausertemplategetregionalsettingOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmausertemplategetregionalsettingOutput outputobj = new UrmausertemplategetregionalsettingOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
