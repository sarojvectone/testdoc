using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{

    public class UrmyaccwebgetcallhandlingforwardingController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebgetcallhandlingforwardingInput
        {
            public int? Company_id { get; set; }
            public int? Extension { get; set; }
            public int? Hour_Type { get; set; }
        }
        public class UrmyaccwebgetcallhandlingforwardingOutput
        {
            public int? Company_id { get; set; }
            public int? Extension { get; set; }
            public int? Hour_Type { get; set; }
            public int? Incoming_call_type { get; set; }
            public string Incoming_call_type_name { get; set; }
            public int? Ring_type { get; set; }
            public string Ring_type_name { get; set; }
            public string Name { get; set; }
            public string Number { get; set; }
            public int? Active { get; set; }
            public int? After_Hours_Answered_Type { get; set; }
            public string Answered_Type { get; set; }
            public string Announcement_file { get; set; }
            public string play_language { get; set; }
            public string Forward_call_number { get; set; }
            public int? call_forward_order { get; set; }
            public int? call_forward_type { get; set; }
            public string announcement_name { get; set; }
            public string audio_file { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
         
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebgetcallhandlingforwardingInput req)
        {
            List<UrmyaccwebgetcallhandlingforwardingOutput> OutputList = new List<UrmyaccwebgetcallhandlingforwardingOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_get_Call_handling_forwarding";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @Company_id = req.Company_id,
                                @Extension = req.Extension,
                                @Hour_Type = req.Hour_Type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebgetcallhandlingforwardingOutput()
                        {
                            Company_id = r.Company_id == null ? 0 : r.Company_id,
                            Extension = r.Extension == null ? 0 : r.Extension,
                            Hour_Type = r.Hour_Type == null ? 0 : r.Hour_Type,
                            Incoming_call_type = r.Incoming_call_type == null ? 0 : r.Incoming_call_type,
                            Incoming_call_type_name = r.Incoming_call_type_name,
                            Ring_type = r.Ring_type == null ? 0 : r.Ring_type,
                            Ring_type_name = r.Ring_type_name,
                            Name = r.Name,
                            Number = r.Number,
                            Active = r.Active == null ? 0 : r.Active,
                            After_Hours_Answered_Type = r.After_Hours_Answered_Type == null ? 0 : r.After_Hours_Answered_Type,
                            Answered_Type = r.Answered_Type,
                            Announcement_file = r.Announcement_file,
                            play_language = r.play_language,
                            Forward_call_number = r.Forward_call_number,
                            call_forward_order = r.call_forward_order == null ? 0 : r.call_forward_order,
                            call_forward_type = r.call_forward_type == null ? 0 : r.call_forward_type,
                            announcement_name = r.announcement_name,
                            audio_file = r.audio_file,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebgetcallhandlingforwardingOutput outputobj = new UrmyaccwebgetcallhandlingforwardingOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebgetcallhandlingforwardingOutput outputobj = new UrmyaccwebgetcallhandlingforwardingOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
