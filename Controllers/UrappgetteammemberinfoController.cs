using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappgetteammemberinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappgetteammemberinfoInput
        {
			public int team_id { get; set; }
			public int extension { get; set; }
            public int SIPID { get; set; }
        }
        public class UrappgetteammemberinfoOutput
        {
			public int? id { get; set; }
			public int? team_id { get; set; }
            public int? group_type { get; set; }
            public int? SIPID{ get; set; }
			public int? extension { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappgetteammemberinfoInput req)
        {
            List<UrappgetteammemberinfoOutput> OutputList = new List<UrappgetteammemberinfoOutput>();
			Log.Info("Input : UrappgetteammemberinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_get_team_member_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@team_id = req.team_id,
								//@extension = req.extension,
                                @SIPID = req.SIPID                                   
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrappgetteammemberinfoOutput()
                        {
							id = r.id == null ? 0 : r.id,
                            SIPID = r.SIPID == null ? 0 : r.SIPID,
							team_id = r.team_id == null ? 0 : r.team_id,
                            group_type = r.group_type == null? 0:r.group_type,
							//extension = r.extension == null ? 0 : r.extension,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrappgetteammemberinfoOutput outputobj = new UrappgetteammemberinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrappgetteammemberinfoOutput outputobj = new UrappgetteammemberinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
