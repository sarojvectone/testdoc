using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappdesktopgetuserchatlogController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappdesktopgetuserchatlogInput
        {
			public int company_id { get; set; }
			public DateTime? from_date { get; set; }
			public DateTime? to_date { get; set; }  	                   	    
        }
        public class UrappdesktopgetuserchatlogOutput
        {
			public DateTime createdate { get; set; }
			public string file_type { get; set; }
			public string user_from { get; set; }
			public string user_to { get; set; }
			public string from_user_name { get; set; }
			public string to_user_name { get; set; }
			public string file_desc { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappdesktopgetuserchatlogInput req)
        {
            List<UrappdesktopgetuserchatlogOutput> OutputList = new List<UrappdesktopgetuserchatlogOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_desktop_get_user_chat_log";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@from_date = req.from_date == null ? null : req.from_date,
								@to_date = req.to_date == null ? null : req.to_date 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrappdesktopgetuserchatlogOutput()
                        {
							createdate = r.createdate == null ? null : r.createdate,
							file_type = r.file_type,
							user_from = r.user_from,
							user_to = r.user_to,
							from_user_name = r.from_user_name,
							to_user_name = r.to_user_name,
							file_desc = r.file_desc,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrappdesktopgetuserchatlogOutput outputobj = new UrappdesktopgetuserchatlogOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrappdesktopgetuserchatlogOutput outputobj = new UrappdesktopgetuserchatlogOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
