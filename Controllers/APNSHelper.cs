﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Unifiedringmyaccountwebapi
{
    public static class APNSHelper
    {
        #region Declarations
        public static string username = GetAppSettings("username");
        public static string password = GetAppSettings("password");
        public static string push_hostname = GetAppSettings("push_hostname");
        public static string push_staging_hostname = GetAppSettings("push_hostnameSandbox");
        public static string push_port = GetAppSettings("push_port");
        public static string voip_key_file = GetPushFile(GetAppSettings("voip_key_file"));
        public static string message_key_file = GetPushFile(GetAppSettings("message_key_file"));
        public static string voicemail_key_file = GetPushFile(GetAppSettings("voicemail_key_file"));
        public static string voip_key_pwd = GetAppSettings("voip_key_pwd");
        public static string message_key_pwd = GetAppSettings("message_key_pwd");
        public static string voicemail_key_pwd = GetAppSettings("voicemail_key_pwd");
        public static string push_message = GetAppSettings("push_message");
        public static string voicemail_message = GetAppSettings("voicemail_message");
        public static string push_badge = GetAppSettings("push_badge");
        public static string push_sound = GetAppSettings("push_sound");
        public static string sync_message = GetAppSettings("sync_message");
        #endregion

        #region Methods
        static string GetAppSettings(string key)
        {
            if (System.Configuration.ConfigurationManager.AppSettings[key] != null)
                return System.Configuration.ConfigurationManager.AppSettings[key];
            return "";
        }

        static string GetPushFile(string file)
        {
            return System.Web.Hosting.HostingEnvironment.MapPath(file);
        }

        public static byte[] ToByteArray(this string value)
        {
            return Encoding.Default.GetBytes(value);
        }

        public static string GetAuthToken()
        {
            return Convert.ToBase64String((APNSHelper.username + ":" + APNSHelper.password).ToByteArray());
        }

        public static string ToHexString(this byte[] bytes, int length = 0)
        {
            if (bytes == null || bytes.Length <= 0)
                return "";

            var sb = new StringBuilder();

            foreach (byte b in bytes)
            {
                sb.Append(b.ToString("x2"));

                if (length > 0 && sb.Length >= length)
                    break;
            }
            return sb.ToString();
        }
        #endregion
    }
}