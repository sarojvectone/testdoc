using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using System.IO;
using Newtonsoft.Json;
using PushSharp.Apple;
using Newtonsoft.Json.Linq;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class Urmaextnupdateapploginpwdv2Controller : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaextnupdateapploginpwdInput
        {
			public int company_id { get; set; }
			public int domain_id	 { get; set; }
			public int extension { get; set; }
			public string email { get; set; }
			public string login_user_name { get; set; }  	                   	    
            public string login_password { get; set; }
            public int type { get; set; }      
        }
        public class UrmaextnupdateapploginpwdOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public int app_log_id { get; set; }
            public string sip_password { get; set; } 
            public int?  platform{ get; set; }
            public string host_address { get; set; }
        }
        public class UrmaextnDeviceTokenOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; } 
            public int DeviceType { get; set; }
            public string message_token { get; set; }
            public string voip_token { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaextnupdateapploginpwdInput req)
        {
            Log.Info("Input : " + JsonConvert.SerializeObject(req));
            unifiedringmyaccountwebapi.Controllers.ResultDetailsFCM data = new unifiedringmyaccountwebapi.Controllers.ResultDetailsFCM();
            string strtoken = null;
            List<UrmaextnupdateapploginpwdOutput> OutputList = new List<UrmaextnupdateapploginpwdOutput>();
            List<UrmaextnDeviceTokenOutput> OutputDeviceTokenList = new List<UrmaextnDeviceTokenOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_extn_update_app_login_pwd";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id,
                                @domain_id = req.domain_id,
                                @extension = req.extension,
                                @email = req.email,
                                @login_user_name = req.login_user_name,
                                @login_password = req.login_password ,
                                @type = req.type                                
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmaextnupdateapploginpwdOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg,
                            app_log_id = r.app_log_id == null ? 0 : r.app_log_id,
                            sip_password = r.sip_password,
                            platform = r.platform,
                            host_address = r.host_address
                            
                        }));
                        Log.Info("Output ur_ma_extn_update_app_login_pwd SP: " + JsonConvert.SerializeObject(result));

                        //Adding in chat server
                        if (OutputList.Count > 0)
                        {
                            if (OutputList[0].app_log_id != 0)
                            {


                                if (ConfigurationManager.AppSettings["ejabberdchat_cluster"] == "1")
                                {
                                    var webAddr1 = ConfigurationManager.AppSettings["ejabberdchat_Changepassword_host"];
                                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                    var httpWebRequest1 = (HttpWebRequest)WebRequest.Create(webAddr1);
                                    httpWebRequest1.ContentType = "application/json";
                                    httpWebRequest1.Method = "POST";
                                    string autorization = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImcuYW5uYW1hbGFpQHZlY3RvbmUuY29tIiwicGFzc3dvcmQiOiIxMjM0NTYiLCJkZXZpY2VJZCI6ImFiY2QiLCJzaXBMb2dpbklkIjoiMzE3OCIsInJvbGVJZCI6MywiaWF0IjoxNTkyMjIyNzY5fQ.6CE_6uTy8mOvzOwrB3B75jMNAkDPGAbWKG1rB1ePy9s";
                                    //autorization = autorization;
                                    httpWebRequest1.Headers.Add("AUTHORIZATION", autorization);

                                    //string json = "{\"userid\": \"" + OutputList[0].app_log_id + "\",\"company_id\": \"" + req.company_id + "\",\"host\": \"" + OutputList[0].host_address + "\",\"password\": \"" + OutputList[0].sip_password + "\"}";
                                    string json = "{\"userid\": \"" + OutputList[0].app_log_id + "\",\"host\": \"" + OutputList[0].host_address + "\",\"sipNewPassword\": \"" + OutputList[0].sip_password + "\"}";
                                    using (var streamWriter = new StreamWriter(httpWebRequest1.GetRequestStream()))
                                    {
                                        streamWriter.Write(json);
                                        streamWriter.Flush();
                                    }
                                    var httpResponse1 = (HttpWebResponse)httpWebRequest1.GetResponse();
                                    string response = string.Empty;
                                    using (var streamReader = new StreamReader(httpResponse1.GetResponseStream()))
                                    {
                                        response = streamReader.ReadToEnd();

                                    }
                                    Log.Info("Output ejabberdchat_Change password cluster: " + JsonConvert.SerializeObject(response));

                                }                        

                            }
                            if (req.extension != 0 && req.domain_id != 0)
                            {
                                Log.Info("Output ur_getdevicetoken_info SP: " + JsonConvert.SerializeObject(result));
                                using (var conn1 = new SqlConnection(ConfigurationManager.ConnectionStrings["apns_provider"].ConnectionString))
                                {
                                    conn1.Open();
                                    try
                                    {
                                        #region GetDeviceToken
                                        var spDeviceToken = "ur_getdevicetoken_info";
                                        var resultDeviceToken = conn1.Query<dynamic>(
                                                spDeviceToken, new
                                                {
                                                    @domain_id = req.domain_id,
                                                    @extension = req.extension
                                                },
                                                commandType: CommandType.StoredProcedure);
                                        if (resultDeviceToken != null && resultDeviceToken.Count() > 0)
                                        {
                                            OutputDeviceTokenList.AddRange(resultDeviceToken.Select(result1 => new UrmaextnDeviceTokenOutput()
                                            {
                                                errcode = 0,
                                                errmsg = "Success",
                                                DeviceType = result1.DeviceType == null ? 0 : result1.DeviceType,
                                                voip_token = result1.voip_token,
                                                message_token = result1.message_token
                                            }));
                                            Log.Info("Output ur_getdevicetoken_info SP: " + JsonConvert.SerializeObject(resultDeviceToken));
                                        }
                                        #endregion
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Info("Output ur_getdevicetoken_info error: " + ex.Message.ToString());

                                    }
                                }
                            }

                            if (OutputDeviceTokenList.Count() > 0)
                            {
                                Log.Info("Push notification start");
                                try
                                {
                                    # region Send Push Notification
                                    List<string> AndroidTokens = new List<string>();
                                    foreach (UrmaextnDeviceTokenOutput objUrmaextnDeviceTokenOutput in OutputDeviceTokenList)
                                    {
                                        if (objUrmaextnDeviceTokenOutput.DeviceType == 1)
                                        {
                                            Log.Info("Send Push Notification start: " + objUrmaextnDeviceTokenOutput.message_token);
                                            //Send silent Push Notification to Android and IOS
                                            //Call push notification for IOS
                                            string p12File = "";
                                            string p12FilePwd = "";
                                            bool isLive;
                                            bool isvoip = false;
                                            p12File = APNSHelper.message_key_file;
                                            p12FilePwd = APNSHelper.message_key_pwd;
                                            ApnsConfiguration config = null;
                                            isLive = true;
                                            isvoip = false;

                                            if (isLive)
                                            {
                                                APNSHelper.push_hostname = APNSHelper.push_hostname.ToLower();
                                            }
                                            else
                                            {
                                                APNSHelper.push_hostname = APNSHelper.push_staging_hostname.ToLower();
                                            }

                                            if (!isLive)
                                            {
                                                config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, p12File, p12FilePwd, !isvoip);
                                            }
                                            else
                                            {
                                                config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, p12File, p12FilePwd, !isvoip);
                                            }

                                            ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                                            // Create a new broker
                                            var apnsBroker = new PushSharp.Apple.ApnsServiceBroker(config);

                                            string message_token = null;

                                            urpushnotificationController objurpushnotificationController = new urpushnotificationController();

                                            //message_token = "90EB35B30DA30993EE204404E57BE2EE60D67A72E4AC65BFEF8709BBBE462D4B";
                                            strtoken = objUrmaextnDeviceTokenOutput.message_token;
                                            //if (isvoip)
                                            //{
                                            //    strtoken = token.voip_token;
                                            //}
                                            //else
                                            //{
                                            //    strtoken = token.message_token;


                                            //}
                                           // UrmaextnDeviceTokenOutput objUrmaextnDeviceTokenOutput = new UrmaextnDeviceTokenOutput();
                                            // Wire up events
                                            apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                                            {
                                                aggregateEx.Handle(ex =>
                                                {
                                                    objUrmaextnDeviceTokenOutput.errcode = -1;

                                                    // See what kind of exception it was to further diagnose
                                                    if (ex is ApnsNotificationException)
                                                    {
                                                        //DeActivate Subscription
                                                        string result1 = string.Empty;

                                                        //if (isvoip)
                                                        //    result = objurpushnotificationController.DeactivateSubscription("", token.voip_token);
                                                        //else
                                                        //    result = objurpushnotificationController.DeactivateSubscription(token.message_token, "");
                                                        //DeaActivate End

                                                        var notificationException = (ApnsNotificationException)ex;
                                                        var apnsNotification = notificationException.Notification;
                                                        var statusCode = notificationException.ErrorStatusCode;
                                                        objUrmaextnDeviceTokenOutput.errmsg = String.Format("Apple Notification Failed: ID={0}, Code={1}", apnsNotification.Identifier, statusCode);
                                                        Log.Info(objUrmaextnDeviceTokenOutput.errmsg);
                                                    }
                                                    else
                                                    {
                                                        //DeActivate Subscription
                                                        string result1 = string.Empty;
                                                        //if (isvoip)
                                                        //    result = objurpushnotificationController.DeactivateSubscription("", token.voip_token);
                                                        //else
                                                        //    result = objurpushnotificationController.DeactivateSubscription(token.message_token, "");
                                                        //DeaActivate End
                                                        objUrmaextnDeviceTokenOutput.errmsg = String.Format("Apple Notification Failed for some unknown reason : {0}", ex.InnerException);
                                                        Log.Info(objUrmaextnDeviceTokenOutput.errmsg);
                                                    }
                                                    return true;
                                                });
                                            };
                                            apnsBroker.OnNotificationSucceeded += (notification) =>
                                            {
                                                objUrmaextnDeviceTokenOutput.errcode = 0;
                                                objUrmaextnDeviceTokenOutput.errmsg = "Notification Sent!";
                                                Log.Info(objUrmaextnDeviceTokenOutput.errmsg);
                                            };

                                            apnsBroker.Start();

                                            //string jsonString = "{\"aps\":{\"content-available\":\"" + "1" + "\",\"name\":\"" + "User" + "\",\"iuid\":\"" + iuid + "\",\"media\":\"" + media + "\",\"UUID\":\"" + strtoken + "\",\"alert\":\"" + message + "\",\"badge\":\"" + APNSHelper.push_badge + "\",\"phoneno\":\"" + FromNumber + "\",\"sound\":\"" + APNSHelper.push_sound + "\"}}";
                                            //                                {
                                            //    "aps" : {
                                            //        "content-available" : 1,
                                            //        "sound" : ��,
                                            //"category": "contact_refresh", //"logout"
                                            //"sip_login":"1655"
                                            //    }

                                            //}

                                            string jsonString1 = "{\"aps\": {\"content-available\":\"" + "1" + "\",\"sound\": \"\",\"category\": \"Log_out\",\"sip_login\":\"" + OutputList[0].app_log_id + "\"}}";

                                            Log.Info("payload : " + jsonString1);
                                            Log.Info("DeviceToken : " + strtoken);

                                            apnsBroker.QueueNotification(new ApnsNotification
                                            {
                                                DeviceToken = strtoken,
                                                Expiration = DateTime.UtcNow,
                                                Payload = JObject.Parse(jsonString1)
                                                //Payload = JObject.Parse("{\"aps\":{\"content-available\":\"" + "1" + "\",\"alert\":\"" + message + "\",\"badge\":\"" + APNSHelper.push_badge + "\",\"sound\":\"" + APNSHelper.push_sound + "\"}}")
                                            });
                                            apnsBroker.Stop();
                                            Log.Info("Send Push Notification end: " + OutputDeviceTokenList[0].message_token);
                                        }
                                        else if (objUrmaextnDeviceTokenOutput.DeviceType == 2)
                                        {
                                            AndroidTokens.Add(objUrmaextnDeviceTokenOutput.message_token);
                                        }
                                         
                                    }
                                    if (AndroidTokens.Count > 0)
                                    {
                                        try
                                        {
                                            Log.Info("Send Push Android Notification start: ");
                                            data = unifiedringmyaccountwebapi.Controllers.FirebaseHelper.SendSilentNotification("Notification", "Log_out", AndroidTokens, OutputList[0].app_log_id.ToString());
                                            Log.Info("Send Push Android Notification end: ");
                                        }
                                        catch (Exception ex)
                                        {
                                            Log.Info("Send Push Android Notification error: " + ex.Message.ToString());

                                        }
                                    }
                                    # endregion

                                    Log.Info("Push notification end");

                                }
                                catch (Exception ex)
                                {

                                    Log.Info("Push notification error :" + ex.Message.ToString()); 
                                }
                                
                            }
                        }
                     
                    }
                    else
                    {
                        UrmaextnupdateapploginpwdOutput outputobj = new UrmaextnupdateapploginpwdOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmaextnupdateapploginpwdOutput outputobj = new UrmaextnupdateapploginpwdOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            Log.Info("Final Output : " + JsonConvert.SerializeObject(OutputList));
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
