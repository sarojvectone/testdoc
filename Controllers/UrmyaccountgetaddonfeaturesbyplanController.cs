using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountgetaddonfeaturesbyplanController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountgetaddonfeaturesbyplanInput
        {
			public int planid { get; set; }  	                   	    
        }
        public class UrmyaccountgetaddonfeaturesbyplanOutput
        {
			public int pc_addon_id { get; set; }
			public string Feature_name { get; set; }
			public double? Feature_price { get; set; }
			public string addon_status { get; set; }
            public int?  pc_addon_status  { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountgetaddonfeaturesbyplanInput req)
        {
            List<UrmyaccountgetaddonfeaturesbyplanOutput> OutputList = new List<UrmyaccountgetaddonfeaturesbyplanOutput>();
			Log.Info("Input : UrmyaccountgetaddonfeaturesbyplanController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_get_addon_features_by_plan";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@planid = req.planid 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountgetaddonfeaturesbyplanOutput()
                        {
							pc_addon_id = r.pc_addon_id == null ? 0 : r.pc_addon_id,
							Feature_name = r.Feature_name,
							Feature_price = r.Feature_price == null ? 0.0D : r.Feature_price,
							addon_status = r.addon_status,
                            pc_addon_status = r.pc_addon_status == null ? 0 : r.pc_addon_status,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountgetaddonfeaturesbyplanOutput outputobj = new UrmyaccountgetaddonfeaturesbyplanOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountgetaddonfeaturesbyplanOutput outputobj = new UrmyaccountgetaddonfeaturesbyplanOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
