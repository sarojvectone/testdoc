using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccountinsertemailnotifController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountinsertemailnotifInput
        {
			public int email_notif { get; set; }
			public int notif_to_user { get; set; }
            public int company_id { get; set; }           	    
        }
        public class UrmyaccountinsertemailnotifOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountinsertemailnotifInput req)
        {
            List<UrmyaccountinsertemailnotifOutput> OutputList = new List<UrmyaccountinsertemailnotifOutput>();
			Log.Info("Input : UrmyaccountinsertemailnotifController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_myaccount_insert_email_notif";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@email_notif = req.email_notif,
								@notif_to_user = req.notif_to_user,
                                @company_id = req.company_id                                   
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountinsertemailnotifOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountinsertemailnotifOutput outputobj = new UrmyaccountinsertemailnotifOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountinsertemailnotifOutput outputobj = new UrmyaccountinsertemailnotifOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
