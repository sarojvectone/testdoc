using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountusermgtgetroleController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountusermgtgetroleInput
        {
			public int? companyid { get; set; }
			public int? role_id { get; set; }  	                   	    
        }
        public class UrmyaccountusermgtgetroleOutput
        {
			public int? Role_id { get; set; }
			public string role_name { get; set; }
			public int? Status { get; set; }
			public string Role_type { get; set; }
			public string Role_desc { get; set; }
			public string Module_info { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }

        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountusermgtgetroleInput req)
        {
            List<UrmyaccountusermgtgetroleOutput> OutputList = new List<UrmyaccountusermgtgetroleOutput>();
			Log.Info("Input : UrmyaccountusermgtgetroleController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_usermgt_get_role";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@companyid = req.companyid,
								@role_id = req.role_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountusermgtgetroleOutput()
                        {
							Role_id = r.Role_id == null ? 0 : r.Role_id,
							role_name = r.role_name, //== null ? 0 : r.role_name,
							Status = r.Status == null ? 0 : r.Status,
							Role_type = r.Role_type,
							Role_desc = r.Role_desc,
							Module_info = r.Module_info,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountusermgtgetroleOutput outputobj = new UrmyaccountusermgtgetroleOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountusermgtgetroleOutput outputobj = new UrmyaccountusermgtgetroleOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
