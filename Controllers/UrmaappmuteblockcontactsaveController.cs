using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaappmuteblockcontactsaveController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaappmuteblockcontactsaveInput
        {
			public int dir_user_id { get; set; }
			public int company_id { get; set; }
			public string mobileno { get; set; }
			public int status { get; set; }
			public int type { get; set; }
            public int till { get; set; }
            public int is_phone_notif_info{ get; set; }  
        }
        public class UrmaappmuteblockcontactsaveOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaappmuteblockcontactsaveInput req)
        {
            List<UrmaappmuteblockcontactsaveOutput> OutputList = new List<UrmaappmuteblockcontactsaveOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringappapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_app_mute_block_contact_save";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@dir_user_id = req.dir_user_id,
								@company_id = req.company_id,
								@mobileno = req.mobileno,
								@status = req.status,
								@type = req.type,
                                @till = req.till,
                                @is_phone_notif_info = req.is_phone_notif_info   
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmaappmuteblockcontactsaveOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmaappmuteblockcontactsaveOutput outputobj = new UrmaappmuteblockcontactsaveOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmaappmuteblockcontactsaveOutput outputobj = new UrmaappmuteblockcontactsaveOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
