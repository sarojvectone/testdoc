using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrtariffgettariffdetailsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrtariffgettariffdetailsInput
        {
			public int id { get; set; }
			public int planid { get; set; }  	                   	    
        }
        public class UrtariffgettariffdetailsOutput
        {
			public int? Plan_id { get; set; }
			public string Pln_Name { get; set; }
			public string trffclass { get; set; }
			public int? trf_type { get; set; }
			public string plan_status { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrtariffgettariffdetailsInput req)
        {
            List<UrtariffgettariffdetailsOutput> OutputList = new List<UrtariffgettariffdetailsOutput>();
			Log.Info("Input : UrtariffgettariffdetailsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_tariff_get_tariff_details";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@id = req.id,
								@planid = req.planid 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrtariffgettariffdetailsOutput()
                        {
							Plan_id = r.Plan_id == null ? 0 : r.Plan_id,
							Pln_Name = r.Pln_Name,
							trffclass = r.trffclass,
							trf_type = r.trf_type == null ? 0 : r.trf_type,
							plan_status = r.plan_status,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrtariffgettariffdetailsOutput outputobj = new UrtariffgettariffdetailsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrtariffgettariffdetailsOutput outputobj = new UrtariffgettariffdetailsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
