using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccountgetsavedcccardinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountgetsavedcccardinfoInput
        {
			public int company_id { get; set; }
			public int id { get; set; }  	                   	    
        }
        public class UrmyaccountgetsavedcccardinfoOutput
        {
			public int? company_id { get; set; }
			public string card_holder_name { get; set; }
			public string cc_no { get; set; }
			public string expiry_date { get; set; }
			public string card_type { get; set; }
            public int  card_status { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountgetsavedcccardinfoInput req)
        {
            List<UrmyaccountgetsavedcccardinfoOutput> OutputList = new List<UrmyaccountgetsavedcccardinfoOutput>();
			Log.Info("Input : UrmyaccountgetsavedcccardinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_get_saved_cc_card_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@id = req.id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountgetsavedcccardinfoOutput()
                        {
							company_id = r.company_id == null ? 0 : r.company_id,
							card_holder_name = r.card_holder_name,
							cc_no = r.cc_no,
							expiry_date = r.expiry_date,
							card_type = r.card_type, 
                            card_status = r.card_status == null ? 0 : r.card_status,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountgetsavedcccardinfoOutput outputobj = new UrmyaccountgetsavedcccardinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountgetsavedcccardinfoOutput outputobj = new UrmyaccountgetsavedcccardinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
