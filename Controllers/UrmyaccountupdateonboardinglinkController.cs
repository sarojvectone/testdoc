using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountupdateonboardinglinkController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountupdateonboardinglinkInput
        {
			public int? compnay_id { get; set; }
			public string extension { get; set; }
			public string onboarding_link { get; set; }
            public int? Hubspot_contactid { get; set; }            	    
        }


        public class UrmyaccountupdateonboardinglinkOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountupdateonboardinglinkInput req)
        {
            List<UrmyaccountupdateonboardinglinkOutput> OutputList = new List<UrmyaccountupdateonboardinglinkOutput>();
			Log.Info("Input : UrmyaccountupdateonboardinglinkController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_update_onboarding_link";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@compnay_id = req.compnay_id,
								@extension = req.extension,
								@onboarding_link = req.onboarding_link,
                                @Hubspot_contactid = req.Hubspot_contactid                               
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountupdateonboardinglinkOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountupdateonboardinglinkOutput outputobj = new UrmyaccountupdateonboardinglinkOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountupdateonboardinglinkOutput outputobj = new UrmyaccountupdateonboardinglinkOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
