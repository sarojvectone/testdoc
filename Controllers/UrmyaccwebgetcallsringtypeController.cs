using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebgetcallsringtypeController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebgetcallsringtypeInput
        {
			                  	    
        }
        public class UrmyaccwebgetcallsringtypeOutput
        {
            public int? ICR { get; set; }
            public string ICR_Name { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebgetcallsringtypeInput req)
        {
            List<UrmyaccwebgetcallsringtypeOutput> OutputList = new List<UrmyaccwebgetcallsringtypeOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_get_calls_ring_type";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
										                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebgetcallsringtypeOutput()
                        {
                            ICR = r.ICR == null ? 0 : r.ICR,
                            ICR_Name = r.ICR_Name,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebgetcallsringtypeOutput outputobj = new UrmyaccwebgetcallsringtypeOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebgetcallsringtypeOutput outputobj = new UrmyaccwebgetcallsringtypeOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
