using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebgetcompanyaddressController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebgetcompanyaddressInput
        {
            public int? company_id { get; set; }	                   	    
        }
        public class UrmyaccwebgetcompanyaddressOutput
        {
            public string company_name { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string email { get; set; }
            public string contact_number { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string address3 { get; set; }
            public string postcode { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public string country { get; set; }	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebgetcompanyaddressInput req)
        {
            List<UrmyaccwebgetcompanyaddressOutput> OutputList = new List<UrmyaccwebgetcompanyaddressOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_get_company_address";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebgetcompanyaddressOutput()
                        {
                            company_name = r.company_name,
                            first_name = r.first_name,
                            last_name = r.last_name,
                            email = r.email,
                            contact_number = r.contact_number,
                            address1 = r.address1,
                            address2 = r.address2,
                            address3 = r.address3,
                            postcode = r.postcode,
                            country = r.country,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebgetcompanyaddressOutput outputobj = new UrmyaccwebgetcompanyaddressOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebgetcompanyaddressOutput outputobj = new UrmyaccwebgetcompanyaddressOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
