using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaparklocationgetgroupmembersController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaparklocationgetgroupmembersInput
        {
			public string domain_id { get; set; }
			public int type { get; set; }
            public int park_groupid { get; set; }
        }
        public class UrmaparklocationgetgroupmembersOutput
        {
			public int? park_groupid { get; set; }
			public string extension { get; set; }
			public string group_name { get; set; }
			public string park_status { get; set; }
            public string user_extension { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaparklocationgetgroupmembersInput req)
        {
            List<UrmaparklocationgetgroupmembersOutput> OutputList = new List<UrmaparklocationgetgroupmembersOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_park_location_get_group_members";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id,
								@type = req.type,
                                @park_groupid = req.park_groupid
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmaparklocationgetgroupmembersOutput()
                        {
							park_groupid = r.park_groupid == null ? 0 : r.park_groupid,
							extension = r.extension,
							group_name = r.group_name,
							park_status = r.park_status,
                            user_extension = r.user_extension,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmaparklocationgetgroupmembersOutput outputobj = new UrmaparklocationgetgroupmembersOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmaparklocationgetgroupmembersOutput outputobj = new UrmaparklocationgetgroupmembersOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
