using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrecomonboardgetadmindetailsinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrecomonboardgetadmindetailsinfoInput
        {
			public int Domain_id { get; set; }
			public int Company_Id { get; set; }  	                   	    
        }
        public class UrecomonboardgetadmindetailsinfoOutput
        {
            public string Extension { get; set; }
			public string direct_number { get; set; }
			public int total_user_purchased { get; set; }
			public string Firstname { get; set; }
			public string lastname { get; set; }
			public string Email_address { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrecomonboardgetadmindetailsinfoInput req)
        {
            List<UrecomonboardgetadmindetailsinfoOutput> OutputList = new List<UrecomonboardgetadmindetailsinfoOutput>();
			Log.Info("Input : UrecomonboardgetadmindetailsinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ecom_onboard_get_admin_details_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Domain_id = req.Domain_id,
								@Company_Id = req.Company_Id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrecomonboardgetadmindetailsinfoOutput()
                        {
                            Extension = r.Extension,
                            direct_number = r.Direct_number,
							total_user_purchased = r.total_user_purchased == null ? 0 : r.total_user_purchased,
							Firstname = r.Firstname,
							lastname = r.lastname,
							Email_address = r.Email_address,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrecomonboardgetadmindetailsinfoOutput outputobj = new UrecomonboardgetadmindetailsinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrecomonboardgetadmindetailsinfoOutput outputobj = new UrecomonboardgetadmindetailsinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
