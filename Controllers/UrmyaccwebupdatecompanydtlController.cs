﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebupdatecompanydtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebupdatecompanydtlInput
        {
            public int company_id { get; set; }
            public string company_name { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string address3 { get; set; }
            public string city { get; set; }
            public string postcode { get; set; }
            public string country { get; set; }
            public int? Is_enable_Multisite { get; set; }
        }
        public class UrmyaccwebupdatecompanydtlOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebupdatecompanydtlInput req)
        {
            List<UrmyaccwebupdatecompanydtlOutput> OutputList = new List<UrmyaccwebupdatecompanydtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_update_company_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id,
                                @company_name = req.company_name,
                                @address1 = req.address1,
                                @address2 = req.address2,
                                @address3 = req.address3,
                                @city = req.city,
                                @postcode = req.postcode,
                                @country = req.country,
                                @Is_enable_Multisite = req.Is_enable_Multisite
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebupdatecompanydtlOutput()
                        {
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebupdatecompanydtlOutput outputobj = new UrmyaccwebupdatecompanydtlOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebupdatecompanydtlOutput outputobj = new UrmyaccwebupdatecompanydtlOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}