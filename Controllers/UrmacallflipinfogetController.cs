using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmacallflipinfogetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmacallflipinfogetInput
        {
			public int ext { get; set; }
			public int Company_ID { get; set; }  	                   	    
        }
        public class UrmacallflipinfogetOutput
        {
			public int? id { get; set; }
			public long? dir_user_id { get; set; }
			public string phone_number { get; set; }
			public string flip_key { get; set; }
			public Boolean is_active { get; set; }
			public string name { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmacallflipinfogetInput req)
        {
            List<UrmacallflipinfogetOutput> OutputList = new List<UrmacallflipinfogetOutput>();
            Log.Info("Input :UrmacallflipinfogetController: " + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_call_flip_info_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@ext = req.ext,
								@Company_ID = req.Company_ID 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmacallflipinfogetOutput()
                        {
							id = r.id == null ? 0 : r.id,
							dir_user_id = r.dir_user_id == null ? 0 : r.dir_user_id,
							phone_number = r.phone_number,
							flip_key = r.flip_key,
							is_active = r.is_active == null ? false : r.is_active,
							name = r.name,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmacallflipinfogetOutput outputobj = new UrmacallflipinfogetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmacallflipinfogetOutput outputobj = new UrmacallflipinfogetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            Log.Info("Output: UrmacallflipinfogetController: " + JsonConvert.SerializeObject(OutputList));
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
