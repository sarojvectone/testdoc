using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountcreatecallqueueinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcreatecallqueueinfoInput
        {
			public int Company_id { get; set; }
			public string Queue_name { get; set; }
			public string Queue_extension { get; set; }
			public string Voicemail_pin { get; set; }
			public int status { get; set; }
			public int Hour_Type { get; set; }
			public string Time_zone { get; set; }
			public int processtype { get; set; }
			public int call_queue_id { get; set; }  	                   	    
        }
        public class UrmyaccountcreatecallqueueinfoOutput
        {
            public int? call_queue_id { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcreatecallqueueinfoInput req)
        {
            List<UrmyaccountcreatecallqueueinfoOutput> OutputList = new List<UrmyaccountcreatecallqueueinfoOutput>();
			Log.Info("Input : UrmyaccountcreatecallqueueinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_create_call_queue_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_id = req.Company_id,
								@Queue_name = req.Queue_name,
								@Queue_extension = req.Queue_extension,
								@Voicemail_pin = req.Voicemail_pin,
								@status = req.status,
								@Hour_Type = req.Hour_Type,
								@Time_zone = req.Time_zone,
								@processtype = req.processtype,
								@call_queue_id = req.call_queue_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcreatecallqueueinfoOutput()
                        {
                            call_queue_id = r.call_queue_id == null ? 0 : r.call_queue_id,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcreatecallqueueinfoOutput outputobj = new UrmyaccountcreatecallqueueinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcreatecallqueueinfoOutput outputobj = new UrmyaccountcreatecallqueueinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
