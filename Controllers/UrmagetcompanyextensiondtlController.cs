using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmagetcompanyextensiondtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmagetcompanyextensiondtlInput
        {
			public int company_id { get; set; }
            public int type { get; set; }
        }
        public class UrmagetcompanyextensiondtlOutput
        {
			public string Firstname { get; set; }
			public string Department { get; set; }
			public string Extension_Number { get; set; }
			public string Extension_Type { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmagetcompanyextensiondtlInput req)
        {
            List<UrmagetcompanyextensiondtlOutput> OutputList = new List<UrmagetcompanyextensiondtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_get_company_extension_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
                                @type = req.type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmagetcompanyextensiondtlOutput()
                        {
							Firstname = r.Firstname,
							Department = r.Department,
							Extension_Number = r.Extension_Number,
							Extension_Type = r.Extension_Type,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmagetcompanyextensiondtlOutput outputobj = new UrmagetcompanyextensiondtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmagetcompanyextensiondtlOutput outputobj = new UrmagetcompanyextensiondtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
