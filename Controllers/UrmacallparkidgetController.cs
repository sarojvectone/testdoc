using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    
    public class UrmacallparkidgetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmacallparkidgetInput
        { 	                   	    
        }
        public class UrmacallparkidgetOutput
        {
			public int errcode { get; set; }
            public string errmsg { get; set; }
            public int? park_id { get; set; }    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Get(string id, HttpRequestMessage request, UrmacallparkidgetInput req)
        {
            List<UrmacallparkidgetOutput> OutputList = new List<UrmacallparkidgetOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringappapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_callpark_id_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {		                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmacallparkidgetOutput()
                        {
                            park_id = r.park_id,
                            errcode = r.error_code == null ? 0 : r.error_code,
                            errmsg = r.error_msg == null ? "Success" : r.error_msg
                        }));
                    }
                    else
                    {
                        UrmacallparkidgetOutput outputobj = new UrmacallparkidgetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmacallparkidgetOutput outputobj = new UrmacallparkidgetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
