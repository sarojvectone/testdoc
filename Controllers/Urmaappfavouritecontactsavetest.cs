using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaappfavouritecontactsaveTestController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaappfavouritecontactsaveTestInput
        {
			public int dir_user_id { get; set; }
			public int company_id { get; set; }
			public string mobileno { get; set; }
			public int status { get; set; }  	                   	    
        }
        public class UrmaappfavouritecontactsaveTestOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaappfavouritecontactsaveTestInput req)
        {
            List<UrmaappfavouritecontactsaveTestOutput> OutputList = new List<UrmaappfavouritecontactsaveTestOutput>();
			Log.Info("Input : UrmaappfavouritecontactsaveController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringappapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_app_favourite_contact_save_1";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@dir_user_id = req.dir_user_id,
								@company_id = req.company_id,
								@mobileno = req.mobileno,
								@status = req.status 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                      OutputList.AddRange(result.Select(r => new UrmaappfavouritecontactsaveTestOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                      UrmaappfavouritecontactsaveTestOutput outputobj = new UrmaappfavouritecontactsaveTestOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, OutputList);
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                return null;
                //UrmaappfavouritecontactsaveTestOutput outputobj = new UrmaappfavouritecontactsaveTestOutput();
                //outputobj.errcode = -1;
                //outputobj.errmsg = ex.Message;
                //OutputList.Add(outputobj);
            }
            
        }
    }
}
