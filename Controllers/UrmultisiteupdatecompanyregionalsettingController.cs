using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmultisiteupdatecompanyregionalsettingController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmultisiteupdatecompanyregionalsettingInput
        {
			public int Company_Id { get; set; }
			public string Time_zone { get; set; }
			public string Home_country_code { get; set; }
			public int Greetings_language { get; set; }
			public int User_language { get; set; }
			public string Regional_format { get; set; }
			public int Time_format { get; set; }  	                   	    
        }
        public class UrmultisiteupdatecompanyregionalsettingOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmultisiteupdatecompanyregionalsettingInput req)
        {
            List<UrmultisiteupdatecompanyregionalsettingOutput> OutputList = new List<UrmultisiteupdatecompanyregionalsettingOutput>();
			Log.Info("Input : UrmultisiteupdatecompanyregionalsettingController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Multisite_update_Company_Regional_Setting";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_Id = req.Company_Id,
								@Time_zone = req.Time_zone,
								@Home_country_code = req.Home_country_code,
								@Greetings_language = req.Greetings_language,
								@User_language = req.User_language,
								@Regional_format = req.Regional_format,
								@Time_format = req.Time_format 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmultisiteupdatecompanyregionalsettingOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmultisiteupdatecompanyregionalsettingOutput outputobj = new UrmultisiteupdatecompanyregionalsettingOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmultisiteupdatecompanyregionalsettingOutput outputobj = new UrmultisiteupdatecompanyregionalsettingOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
