using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaconferencesessiongetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaconferencesessiongetInput
        {
			public int company_id { get; set; }
			public int id { get; set; }  	                   	    
        }
        public class UrmaconferencesessiongetOutput
        {
			public int id { get; set; }
			public int room_id { get; set; }
			public string conference_id { get; set; }
			public string conference_pin { get; set; }
			public string created_by { get; set; }
			public string requested_by { get; set; }
			public string topic { get; set; }
			public string description { get; set; }
			public DateTime? start_time { get; set; }
			public DateTime? end_time { get; set; }
			public int duration_limit { get; set; }
			public string attendees { get; set; }
			public string call_screening { get; set; }
			public string conference_recording { get; set; }
			public int status { get; set; }
            public int site_id { get; set; }
            public string site_name  { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaconferencesessiongetInput req)
        {
            List<UrmaconferencesessiongetOutput> OutputList = new List<UrmaconferencesessiongetOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_conference_session_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@id = req.id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmaconferencesessiongetOutput()
                        {
							id = r.id == null ? 0 : r.id,
							room_id = r.room_id == null ? 0 : r.room_id,
							conference_id = r.conference_id,
							conference_pin = r.conference_pin,
							created_by = r.created_by,
							requested_by = r.requested_by,
							topic = r.topic,
							description = r.description,
							start_time = r.start_time == null ? null : r.start_time,
							end_time = r.end_time == null ? null : r.end_time,
							duration_limit = r.duration_limit == null ? 0 : r.duration_limit,
							attendees = r.attendees,
							call_screening = r.call_screening == true ? "1" : "0",
							conference_recording = r.conference_recording == true ? "1" : "0",
							status = r.status == null ? 0 : r.status,
                            site_id = r.site_id == null ? 0 : r.site_id,
                            site_name = r.site_name,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmaconferencesessiongetOutput outputobj = new UrmaconferencesessiongetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmaconferencesessiongetOutput outputobj = new UrmaconferencesessiongetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
