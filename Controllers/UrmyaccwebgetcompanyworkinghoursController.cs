using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebgetcompanyworkinghoursController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebgetcompanyworkinghoursInput
        {
			public int? Company_Id { get; set; }
            public string Extension { get; set; }
            public int? Type { get; set; }
            public int? site_id { get; set; }
        }
        public class UrmyaccwebgetcompanyworkinghoursOutput
        {
			public int? Company_Id { get; set; }
			public string Working_Business_Type { get; set; }
			public string Working_Day { get; set; }
			public TimeSpan? From_Time { get; set; }
			public TimeSpan? To_Time { get; set; }
            public int? Hour_Format { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebgetcompanyworkinghoursInput req)
        {
            List<UrmyaccwebgetcompanyworkinghoursOutput> OutputList = new List<UrmyaccwebgetcompanyworkinghoursOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_Get_Company_Working_Hours";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_Id = req.Company_Id,
                                //@Extension = String.IsNullOrEmpty(req.Extension) ? null : req.Extension,
                                @Extension = req.Extension,
                                @Type = req.Type,
                                @site_id = req.site_id
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebgetcompanyworkinghoursOutput()
                        {
							Company_Id = r.Company_Id == null ? 0 : r.Company_Id,
							Working_Business_Type = r.Working_Business_Type,
							Working_Day = r.Working_Day,
							From_Time = r.From_Time,
							To_Time = r.To_Time,
							Hour_Format = r.Hour_Format,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebgetcompanyworkinghoursOutput outputobj = new UrmyaccwebgetcompanyworkinghoursOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebgetcompanyworkinghoursOutput outputobj = new UrmyaccwebgetcompanyworkinghoursOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
