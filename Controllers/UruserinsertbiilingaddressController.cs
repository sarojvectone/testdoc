using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UruserinsertbiilingaddressController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UruserinsertbiilingaddressInput
        {
			public int user_id { get; set; }
			public string address1 { get; set; }
			public string address2 { get; set; }
			public string address3 { get; set; }
			public string city { get; set; }
			public string postcode { get; set; }
			public string country { get; set; }  	                   	    
        }
        public class UruserinsertbiilingaddressOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UruserinsertbiilingaddressInput req)
        {
            List<UruserinsertbiilingaddressOutput> OutputList = new List<UruserinsertbiilingaddressOutput>();
			Log.Info("Input : UruserinsertbiilingaddressController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_user_insert_biiling_address";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@user_id = req.user_id,
								@address1 = req.address1,
								@address2 = req.address2,
								@address3 = req.address3,
								@city = req.city,
								@postcode = req.postcode,
								@country = req.country 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UruserinsertbiilingaddressOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UruserinsertbiilingaddressOutput outputobj = new UruserinsertbiilingaddressOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UruserinsertbiilingaddressOutput outputobj = new UruserinsertbiilingaddressOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
