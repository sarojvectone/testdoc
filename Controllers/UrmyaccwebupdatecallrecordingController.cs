using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebupdatecallrecordingController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebupdatecallrecordingInput
        {
			public int? Company_id { get; set; }
            public int? Type { get; set; }  	    
			public string Announcement_start { get; set; }
			public string Announcement_stop { get; set; }
			public int? Greeting_by { get; set; }
			public int? Greeting_type { get; set; }
			public int? Language { get; set; }
			public string Rec_Status { get; set; }
            public string greeting_content{ get; set; }
        }
        public class UrmyaccwebupdatecallrecordingOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebupdatecallrecordingInput req)
        {
            List<UrmyaccwebupdatecallrecordingOutput> OutputList = new List<UrmyaccwebupdatecallrecordingOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_update_Call_Recording";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @Company_id = req.Company_id,
                                @Type  = req.Type,
                                @Announcement_start = req.Announcement_start,
                                @Announcement_stop = req.Announcement_stop,
                                @Greeting_by = req.Greeting_by,
                                @Greeting_type = req.Greeting_type,
                                @Language = req.Language,
                                @Rec_Status = req.Rec_Status,
                                @greeting_content = req.greeting_content
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebupdatecallrecordingOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebupdatecallrecordingOutput outputobj = new UrmyaccwebupdatecallrecordingOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebupdatecallrecordingOutput outputobj = new UrmyaccwebupdatecallrecordingOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
