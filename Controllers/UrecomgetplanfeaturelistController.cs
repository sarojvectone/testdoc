using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrecomgetplanfeaturelistController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrecomgetplanfeaturelistInput
        {
        }
        public class UrecomgetplanfeaturelistOutput
        {
			public int? PF_Idx { get; set; }
			public string PF_Name { get; set; }
			public int? Category_id { get; set; }
			public string PF_category { get; set; }
			public int? PF_status { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Get(string id, HttpRequestMessage request, UrecomgetplanfeaturelistInput req)
        {
            List<UrecomgetplanfeaturelistOutput> OutputList = new List<UrecomgetplanfeaturelistOutput>();
			Log.Info("Input : UrecomgetplanfeaturelistController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ECom_get_Plan_Feature_list";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrecomgetplanfeaturelistOutput()
                        {
							PF_Idx = r.PF_Idx == null ? 0 : r.PF_Idx,
							PF_Name = r.PF_Name,
							Category_id = r.Category_id == null ? 0 : r.Category_id,
							PF_category = r.PF_category,
							PF_status = r.PF_status == null ? 0 : r.PF_status,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrecomgetplanfeaturelistOutput outputobj = new UrecomgetplanfeaturelistOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrecomgetplanfeaturelistOutput outputobj = new UrecomgetplanfeaturelistOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
