using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccgetconferencemasterinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccgetconferencemasterinfoInput
        {
			public int company_id { get; set; }
			public int Extension_Number { get; set; }  	                   	    
        }
        public class UrmyaccgetconferencemasterinfoOutput
        {
			public string conference_number { get; set; }
			public int? host_code { get; set; }
			public int? partipation_code { get; set; }
            public string  updated_conference_number { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccgetconferencemasterinfoInput req)
        {
            List<UrmyaccgetconferencemasterinfoOutput> OutputList = new List<UrmyaccgetconferencemasterinfoOutput>();
			Log.Info("Input : UrmyaccgetconferencemasterinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myacc_get_conference_master_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@Extension_Number = req.Extension_Number 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccgetconferencemasterinfoOutput()
                        {
							conference_number = r.conference_number,
							host_code = r.host_code == null ? 0 : r.host_code,
							partipation_code = r.partipation_code == null ? 0 : r.partipation_code,
                            updated_conference_number = r.updated_conference_number,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccgetconferencemasterinfoOutput outputobj = new UrmyaccgetconferencemasterinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccgetconferencemasterinfoOutput outputobj = new UrmyaccgetconferencemasterinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
