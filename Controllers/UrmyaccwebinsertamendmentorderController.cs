using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    
    public class UrmyaccwebinsertamendmentorderController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebinsertamendmentorderInput
        {
            public int company_Id { get; set; }
            public int basket_id { get; set; }
            public int location_type { get; set; }
            public string country_id { get; set; }
            public string duration_period { get; set; }
            public double? duration_charge { get; set; }
            public double? one_off_charge { get; set; }
            public double? total_delivery_fee { get; set; }
            public string adjustment_period { get; set; }
            public double? adjustment_charge { get; set; }
            public double? vat_percetage { get; set; }
            public double? vat_charge { get; set; }
            public double? sub_total { get; set; }
            public double? total_charge { get; set; }
            public string reference_id { get; set; }
            public string device_model { get; set; }
            public string delivery_address { get; set; }
            public string order_source { get; set; }
            public int created_by { get; set; }
            //public int company_Id { get; set; }
            //public string address1 { get; set; }
            //public string address2 { get; set; }
            //public string address3 { get; set; }
            //public string city { get; set; }
            //public string country { get; set; }
            //public string full_name { get; set; }
            //public string duration_period { get; set; }
            //public float duration_charge { get; set; }
            //public float phone_charge { get; set; }
            //public string adjustment_period { get; set; }
            //public float adjustment_charge { get; set; }
            //public float vat_percetage { get; set; }
            //public float vat_charge { get; set; }
            //public float total_charge { get; set; }
            //public string reference_id { get; set; }
            //public string device_model { get; set; }
            //public int basket_id { get; set; }
            //public int? location_type{ get; set; }
            //public int? user_device_type { get; set; }
            //public string country_code { get; set; }
            //public string area_code { get; set; }
            //public int? site_id { get; set; }
        }
        public class UrmyaccwebinsertamendmentorderOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebinsertamendmentorderInput req)
        {
            List<UrmyaccwebinsertamendmentorderOutput> OutputList = new List<UrmyaccwebinsertamendmentorderOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_Insert_Amendment_Order";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_Id = req.company_Id,
                                @basket_id = req.basket_id,
                                @location_type = req.location_type,
                                @country_id = req.country_id,
                                @duration_period = req.duration_period,
                                @one_off_charge = req.one_off_charge,
                                @total_delivery_fee = req.total_delivery_fee,
                                @adjustment_period = req.adjustment_period,
                                @adjustment_charge = req.adjustment_charge,
                                @duration_charge = req.duration_charge,                             
                                @vat_percetage = req.vat_percetage,
                                @vat_charge = req.vat_charge,
                                @sub_total = req.sub_total,
                                @total_charge = req.total_charge,
                                @reference_id = req.reference_id,
                                @device_model = req.device_model,
                                @delivery_address = req.delivery_address,
                                @order_source = req.order_source,
                                @created_by = req.created_by

                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebinsertamendmentorderOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebinsertamendmentorderOutput outputobj = new UrmyaccwebinsertamendmentorderOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebinsertamendmentorderOutput outputobj = new UrmyaccwebinsertamendmentorderOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
