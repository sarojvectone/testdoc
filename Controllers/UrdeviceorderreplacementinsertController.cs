using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdeviceorderreplacementinsertController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdeviceorderreplacementinsertInput
        {
            //public int order_id { get; set; }
            //public string prod_code { get; set; }
            //public string prod_model { get; set; }
            public int itemid { get; set; }
			public int rpl_quantity { get; set; }
			public string rpl_reason { get; set; } 	    
        }
        public class UrdeviceorderreplacementinsertOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdeviceorderreplacementinsertInput req)
        {
            List<UrdeviceorderreplacementinsertOutput> OutputList = new List<UrdeviceorderreplacementinsertOutput>();
			Log.Info("Input : UrdeviceorderreplacementinsertController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_order_replacement_insert";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {    @itemid = req.itemid,                           
								@rpl_quantity = req.rpl_quantity,
								@rpl_reason = req.rpl_reason 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrdeviceorderreplacementinsertOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrdeviceorderreplacementinsertOutput outputobj = new UrdeviceorderreplacementinsertOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrdeviceorderreplacementinsertOutput outputobj = new UrdeviceorderreplacementinsertOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
