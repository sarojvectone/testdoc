using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmultisitecreateivrmenuController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmultisitecreateivrmenuInput
        {
			public int company_id { get; set; }
			public int siteid { get; set; }
			public string menu_name { get; set; }
			public string langauge { get; set; }
			public string lang_code { get; set; }
			public string extension_no { get; set; }
			public string external_number { get; set; }
			public int process_type { get; set; }
			public int ivr_id { get; set; }  	                   	    
        }
        public class UrmultisitecreateivrmenuOutput
        {
			public int ivr_id { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmultisitecreateivrmenuInput req)
        {
            List<UrmultisitecreateivrmenuOutput> OutputList = new List<UrmultisitecreateivrmenuOutput>();
			Log.Info("Input : UrmultisitecreateivrmenuController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_multisite_create_ivr_menu";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@siteid = req.siteid,
								@menu_name = req.menu_name,
								@langauge = req.langauge,
								@lang_code = req.lang_code,
								@extension_no = req.extension_no,
								@external_number = req.external_number,
								@process_type = req.process_type,
								@ivr_id = req.ivr_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmultisitecreateivrmenuOutput()
                        {
							ivr_id = r.ivr_id == null ? 0 : r.ivr_id,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmultisitecreateivrmenuOutput outputobj = new UrmultisitecreateivrmenuOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmultisitecreateivrmenuOutput outputobj = new UrmultisitecreateivrmenuOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
