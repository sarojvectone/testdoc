﻿using Unifiedringmyaccountwebapi.Controllers;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class LoadImageHelper
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public static string LoadImage(string filepath, string finename, string folder)
        {
            Log.Info("Loading New Image");
            bool bSuccess = false;
            string fileName = finename + ".jpg";
            //Log.Info("filepath : {0}, fileName : {1}", filepath, fileName);
            try
            {
                byte[] bytes = Convert.FromBase64String(filepath);

                Image image;

                try
                {
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        image = Image.FromStream(ms);
                        image.Save(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"\images\" + folder, fileName), System.Drawing.Imaging.ImageFormat.Jpeg);
                        bSuccess = true;
                        //Adding in cluster server
                        using (new NetworkConnection(ConfigurationManager.AppSettings["NetworkPath"] + folder + @"\", new NetworkCredential(ConfigurationManager.AppSettings["NetworkUsername"], ConfigurationManager.AppSettings["NetworkPassword"], ConfigurationManager.AppSettings["NetworkDomain"])))
                        {
                            image.Save(ConfigurationManager.AppSettings["NetworkPath"] + folder + @"\" + fileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                        }
                    }
                  

                }
                catch (Exception ex1)
                {
                    Log.Error(ex1.Message);
                }

            }
            catch (Exception ex)
            {
                Log.Info("Exception LoadImage : {0}", ex.Message);
                Log.Error(ex.Message);
            }
            if (bSuccess)
            {
                return Path.Combine(@"\images\" + folder + @"\", fileName);
            }
            else
                throw new Exception("Unable to save image");
        }

        public static string ReplaceImage(string finename, string newfilename, string filepath, string folder)
        {
            Log.Info("===================================");
            Log.Info("Entering Load/Replace Image");
            bool bSuccess = false;
            string fileName = FindImage(finename, folder);
            string filePath = string.Empty;
            try
            {
                if (fileName != null)
                {
                    filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"\images\" + folder, fileName);
                }
                if (File.Exists(filePath) && fileName != null)
                {
                    Log.Info("Deleting to replace Existing Image");
                    File.Delete(filePath);
                    bSuccess = true;
                } 
                if (bSuccess == true || fileName == null)
                {
                    LoadImage(filepath, newfilename, folder);
                    bSuccess = true;
                    Log.Info("Loading New Image Success");
                    Log.Info("===================================");
                }
                //Log.Info("Connecting Server2 start");
                using (new NetworkConnection(ConfigurationManager.AppSettings["NetworkPath"] + folder + @"\", new NetworkCredential(ConfigurationManager.AppSettings["NetworkUsername"], ConfigurationManager.AppSettings["NetworkPassword"], ConfigurationManager.AppSettings["NetworkDomain"])))
                {
                    if (fileName != null)
                        //filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"\Images", fileName);
                        filePath = ConfigurationManager.AppSettings["NetworkPath"] + folder + @"\"+fileName;
                    if (File.Exists(filePath) && fileName != null)
                    {
                        //filePath = ConfigurationManager.AppSettings["Imagepath2"] + fileName;
                        File.Delete(filePath);
                        bSuccess = true;
                    }
                }
                Log.Info("Connecting Server2 end");

                if (bSuccess == true || fileName == null)
                {
                    LoadImage(filepath, newfilename, folder);
                    bSuccess = true;
                    Log.Info("Loading New Image Success in Server2");
                    Log.Info("===================================");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            if (bSuccess)
                return Path.Combine(@"\images\" + folder + @"\", newfilename);
            else
                throw new Exception("Unable to delete image");
        }

        public static string DeleteImage(string finename, string folder)
        {
            Log.Info("===================================");
            Log.Info("Entering Delete Image");
            bool bSuccess1 = false;
            bool bSuccess2 = false;
            //string fileName = finename + ".jpg";
            string fileName = FindImage(finename, folder);
            Log.Info("Delete Image name: {0}", fileName);
            try
            {
                if (!string.IsNullOrEmpty(fileName))
                {
                    var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"\images\" + folder, fileName);
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);
                        bSuccess1 = true;
                        Log.Info("Deleting Image Success Server1");
                        Log.Info("===================================");
                    }

                    Log.Info("Deleting Image Success Server2");
                    Log.Info("===================================");
                    using (new NetworkConnection(ConfigurationManager.AppSettings["NetworkPath"]+folder + @"\", new NetworkCredential(ConfigurationManager.AppSettings["NetworkUsername"], ConfigurationManager.AppSettings["NetworkPassword"], ConfigurationManager.AppSettings["NetworkDomain"])))
                    {
                        var filepath2 = ConfigurationManager.AppSettings["NetworkPath"] + folder + @"\" + fileName;
                        if (File.Exists(filepath2))
                        {
                            File.Delete(filepath2);
                            bSuccess2 = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Info("Exception Delete Image", ex.Message);
                Log.Error(ex.Message);
            }
            if (bSuccess1 && bSuccess2)
                return Path.Combine(@"Images", fileName);
            else
                throw new Exception("Unable to delete image");
        }

        public static string FindImage(string finename, string folder)
        {
            string filePath = "";
            bool bSuccess = false;
            string fileName = finename + ".jpg";

            try
            {
           
                string[] sFiles;


                sFiles = SearchForFiles(AppDomain.CurrentDomain.BaseDirectory + @"\images\" + folder, fileName);

                foreach (string sFile in sFiles)
                {
                    filePath = sFile;
                }

                if (File.Exists(filePath))
                {
                    fileName = Path.GetFileName(filePath);
                    
                    bSuccess = true;
                }
                else
                {
                    //Log.Info("Deleting Image Failed");
                    //Log.Info("===================================");
                    using (new NetworkConnection(ConfigurationManager.AppSettings["NetworkPath"] + folder + @"\"
                    , new NetworkCredential(ConfigurationManager.AppSettings["NetworkUsername"], ConfigurationManager.AppSettings["NetworkPassword"], ConfigurationManager.AppSettings["NetworkDomain"])))
                    {
                        var filepath2 = ConfigurationManager.AppSettings["NetworkPath"] + folder + @"\" + fileName;
                        sFiles = SearchForFiles(ConfigurationManager.AppSettings["NetworkPath"] + folder + @"\", fileName);

                        foreach (string sFile in sFiles)
                        {
                            filePath = sFile;
                        }

                        if (File.Exists(filePath))
                        {
                            fileName = Path.GetFileName(filePath);
                            bSuccess = true;
                        }
                    } 
                }
              
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            if (bSuccess)
                return fileName;
            else
                return null;
        }

        private static string[] SearchForFiles(string DirectoryPath, string Pattern)
        {
            return Directory.GetFiles(DirectoryPath, Pattern, SearchOption.AllDirectories);
        }
    }
}