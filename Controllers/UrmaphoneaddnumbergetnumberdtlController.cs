using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Unifiedringmyaccountwebapi.Helper;
using Newtonsoft.Json;

namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaphoneaddnumbergetnumberdtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaphoneaddnumbergetnumberdtlInput
        {
			public int company_id { get; set; }
			public string number_type { get; set; }
			public string area_code { get; set; }
			public string country { get; set; }
            public int no_user  { get; set; }
           // public string  location { get; set; }  // Added For LCR
        }
        public class UrmaphoneaddnumbergetnumberdtlOutput
        {
			public string number { get; set; }
			public string country { get; set; }
			public Double? price { get; set; }
			public string currency { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }

        public class UrmaphoneaddnumbergetnumberdtlOutput1
        {
            public string country { get; set; }
            public string location { get; set; }
            public string area_code { get; set; }
            public string number_Type { get; set; }
            public double? installation_fee { get; set; }
            public double? monthly_fee { get; set; }
            public double? usage_fee { get; set; }
            public double? per_minute_usage_fee { get; set; }
            public double? price { get; set; }
            public int? channels { get; set; }
            public string currency { get; set; }
            public DateTime? price_updatedate { get; set; }
            public string source_name { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaphoneaddnumbergetnumberdtlInput req)
        {
            Log.Info("Urmaphoneaddnumbergetnumberdtlinput : " + JsonConvert.SerializeObject(req));
            List<UrmaphoneaddnumbergetnumberdtlOutput> OutputList = new List<UrmaphoneaddnumbergetnumberdtlOutput>();
            List<UrmaphoneaddnumbergetnumberdtlOutput1> OutputListt = new List<UrmaphoneaddnumbergetnumberdtlOutput1>();

            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_phone_add_number_get_number_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@number_type = req.number_type,
								@area_code = req.area_code,
								@country = req.country,
                                @no_user = req.no_user                                     
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmaphoneaddnumbergetnumberdtlOutput()
                        {
                            number = r.number,
                            country = r.country,
                            price = r.price == null ? 0.0D : r.price,
                            currency = r.currency,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                }
                //if (OutputList.Count == 0 )
                //    {
                //        using (var connn = new SqlConnection(ConfigurationManager.ConnectionStrings["Didportalapi"].ConnectionString))
                //        {
                //            connn.Open(); 

                //            var spp = "did_number_get_lcr_price_master_dtl_info";
                //            var resultt = connn.Query<dynamic>(
                //                    spp, new
                //                    {
                //                        @country = req.country,
                //                        @location = req.location,
                //                        @area_code = req.area_code
                //                    },
                //                    commandType: CommandType.StoredProcedure);
                //            if (resultt != null && resultt.Count() > 0)
                //            {
                //                OutputListt.AddRange(resultt.Select(r => new UrmaphoneaddnumbergetnumberdtlOutput1()
                //                {
                //                    country = r.country,
                //                    location = r.location,
                //                    area_code = r.area_code,
                //                    installation_fee = r.installation_fee == null ? 0.0D : r.installation_fee,
                //                    monthly_fee = r.monthly_fee == null ? 0.0D : r.monthly_fee,
                //                    errcode = r.errcode == null ? 0 : r.errcode,
                //                    errmsg = r.errmsg == null ? "Success" : r.errmsg                                  
                //                }));
                //            }
                //            return Request.CreateResponse(HttpStatusCode.OK, OutputListt);
                //        }
                //    }
            }
            catch (Exception ex)
            {
                UrmaphoneaddnumbergetnumberdtlOutput outputobj = new UrmaphoneaddnumbergetnumberdtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            Log.Info("UrmaphoneaddnumbergetnumberdtlController Output : " + JsonConvert.SerializeObject(OutputList));
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
            
        }
    }
}
