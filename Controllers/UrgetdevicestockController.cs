using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrgetdevicestockController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrgetdevicestockInput
        {			
            public string prod_code { get; set; }  	                   	    
        }
        public class UrgetdevicestockOutput
        {
			public int ds_id { get; set; }
			public string prod_code { get; set; }
            public int available_qty { get; set; }
            public int reserved_qty { get; set; }
            public int returned_qty { get; set; }
            public int inventory_id { get; set; }
            public int model_id { get; set; }
			public int qty_threshold { get; set; }
            public double? buy_price { get; set; }
            public double? ws_price { get; set; }
            public double? ur_price { get; set; }
			public string brand { get; set; }
			public double? prod_weight { get; set; }
			public string prod_short_desc { get; set; }
			public string prod_desc { get; set; }
			public string image_path { get; set; }
			public string data_sheet_path { get; set; }
			public string user_guide_path { get; set; }
            public string  prod_model { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrgetdevicestockInput req)
        {
            List<UrgetdevicestockOutput> OutputList = new List<UrgetdevicestockOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_get_device_stock";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {								
                                @prod_code = req.prod_code 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrgetdevicestockOutput()
                        {
							ds_id = r.ds_id == null ? 0 : r.ds_id,
							prod_code = r.prod_code,
							//qty = r.qty == null ? 0.0F : r.qty,
							qty_threshold = r.qty_threshold == null ? 0 : r.qty_threshold,
							buy_price = r.buy_price == null ? 0.0F : r.buy_price,
							ws_price = r.ws_price == null ? 0.0F : r.ws_price,
							ur_price = r.ur_price == null ? 0.0F : r.ur_price,
							brand = r.brand,
							prod_weight = r.prod_weight == null ? 0.0F : r.prod_weight,
							prod_short_desc = r.prod_short_desc,
							prod_desc = r.prod_desc,
							image_path = r.image_path,
							data_sheet_path = r.data_sheet_path,
							user_guide_path = r.user_guide_path,
                            available_qty = r.available_qty == null ? 0:r.available_qty,
                            reserved_qty = r.reserved_qty == null ? 0:r.reserved_qty,
                            returned_qty = r.returned_qty  == null ?0 :r.returned_qty,
                            inventory_id  = r.inventory_id  == null ?0 :r.inventory_id,
                            model_id  = r.model_id  == null?0:r.model_id,
                            prod_model = r.prod_model,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrgetdevicestockOutput outputobj = new UrgetdevicestockOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrgetdevicestockOutput outputobj = new UrgetdevicestockOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
