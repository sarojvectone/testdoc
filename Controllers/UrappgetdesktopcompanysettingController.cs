using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappgetdesktopcompanysettingController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappgetdesktopcompanysettingInput
        {
			public int company_id { get; set; }  	                   	    
        }
        public class UrappgetdesktopcompanysettingOutput
        {
			public int allow_invite { get; set; }
			public int allow_without_signup { get; set; }
			public int giphy_sharing { get; set; }
			public string giphy_sharing_desc { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public int compliance_export { get; set; }
            public int data_retention_days { get; set; }
            public int upload_mobile_computer { get; set; }
            public int google_drive { get; set; }
            public int dropbox { get; set; }
            public int box { get; set; }
            public int onedrive { get; set; }
            public int evernote { get; set; }
            public int data_retention_type { get; set; }
            public  DateTime data_retention_date { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappgetdesktopcompanysettingInput req)
        {
            List<UrappgetdesktopcompanysettingOutput> OutputList = new List<UrappgetdesktopcompanysettingOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_get_desktop_company_setting";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrappgetdesktopcompanysettingOutput()
                        {
							allow_invite = r.allow_invite == null ? 0 : r.allow_invite,
							allow_without_signup = r.allow_without_signup == null ? 0 : r.allow_without_signup,
							giphy_sharing = r.giphy_sharing == null ? 0 : r.giphy_sharing,
							giphy_sharing_desc = r.giphy_sharing_desc,
                            compliance_export = r.compliance_export == null ? 0 : r.compliance_export,
                            data_retention_days = r.data_retention_days == null ? 0 : r.data_retention_days,
                            upload_mobile_computer = r.upload_mobile_computer == null ? 0 : r.upload_mobile_computer,
                            google_drive = r.google_drive == null ? 0 : r.google_drive,
                            dropbox = r.dropbox == null ? 0 : r.dropbox,
                            box = r.box == null ? 0 : r.box,
                            onedrive = r.onedrive == null ? 0 : r.onedrive,
                            evernote = r.evernote == null ? 0 : r.evernote,
                            data_retention_date = r.data_retention_date == null ? null : r.data_retention_date,
                            data_retention_type = r.data_retention_type == null ? 0 : r.data_retention_type,
                            errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrappgetdesktopcompanysettingOutput outputobj = new UrappgetdesktopcompanysettingOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrappgetdesktopcompanysettingOutput outputobj = new UrappgetdesktopcompanysettingOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
