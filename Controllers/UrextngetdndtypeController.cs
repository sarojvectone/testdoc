﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Dapper;
using System.Data;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class UrextngetdndtypeController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public class UrextngetdndtypeOutput
        {
            public int id { get; set; }
            public string dnd_type { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }	     
        }
        [Authorize]
        public async Task<HttpResponseMessage> Get(string id)
        {
            List<UrextngetdndtypeOutput> OutputList = new List<UrextngetdndtypeOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_extn_get_dnd_type";
                    var result = conn.Query<dynamic>(
                            sp,  
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrextngetdndtypeOutput()
                        {
                            id = r.id,
                            dnd_type = r.dnd_type 
                        }));
                    }
                    else
                    {
                        UrextngetdndtypeOutput outputobj = new UrextngetdndtypeOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrextngetdndtypeOutput outputobj = new UrextngetdndtypeOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
