using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaparklocationinsertgroupController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaparklocationinsertgroupInput
        {
            public string domain_id { get; set; }
            public int park_groupid { get; set; }
            public string group_name { get; set; }
            public int extension { get; set; }
            public int status { get; set; }
            public int type { get; set; }
        }
        public class UrmaparklocationinsertgroupOutput
        { 
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaparklocationinsertgroupInput req)
        {
            List<UrmaparklocationinsertgroupOutput> OutputList = new List<UrmaparklocationinsertgroupOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_park_location_insert_group";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @domain_id = req.domain_id,
                                @park_groupid = req.park_groupid,
                                @group_name = req.group_name,
                                @extension = req.extension,
                                @status = req.status,
                                @type = req.type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmaparklocationinsertgroupOutput()
                        {						
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmaparklocationinsertgroupOutput outputobj = new UrmaparklocationinsertgroupOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmaparklocationinsertgroupOutput outputobj = new UrmaparklocationinsertgroupOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
