using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
using System.IO;
//using System.Web.Http.Cors;

namespace unifiedringmyaccountwebapi.Controllers
{
   //[EnableCors(origins: "https://app.unifiedring.co.uk", headers: "*", methods: "*")]
    public class UrUploadFilesController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public class UUrUploadFilesInput
        {
            //public string company_Id { get; set; }
            //public string extn { get; set; }
            //public string photo_info { get; set; }
            //public string Delete_image { get; set; }
            //public string get_image { get; set; }
            StreamContent filedata { get; set; }
        }
        public class UrUploadFilesOutput : Output
        {
            public string FileName { get; set; }
            public string FileURL { get; set; }
        }

        public class Output
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        [Authorize]
        public async Task<HttpResponseMessage> Post()
        {
            Log.Info("UrUploadFiles");
            List<UrUploadFilesOutput> OutputList = new List<UrUploadFilesOutput>();
            try
            {

                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {
                    Log.Info("UrUploadFiles : Files Count : " + httpRequest.Files.Count);
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        UrUploadFilesOutput result1 = new UrUploadFilesOutput();

                        var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"\images\Upload\", postedFile.FileName.Replace(" ", string.Empty));
                        Log.Info("filePath : " + filePath);
                        postedFile.SaveAs(filePath);
                        result1.FileName = postedFile.FileName;
                        result1.FileURL = ConfigurationManager.AppSettings["UploadPath"] + HttpUtility.UrlEncode(postedFile.FileName.Replace(" ", string.Empty));
                        Log.Info("FileURL : " + result1.FileURL);
                        try
                        {
                            using (new NetworkConnection(ConfigurationManager.AppSettings["NetworkPath2"], new NetworkCredential(ConfigurationManager.AppSettings["NetworkUsername"], ConfigurationManager.AppSettings["NetworkPassword"], ConfigurationManager.AppSettings["NetworkDomain"])))
                            {
                                Log.Info("NetworkPath : " + Path.Combine(ConfigurationManager.AppSettings["NetworkPath2"], postedFile.FileName.Replace(" ", string.Empty)));
                                postedFile.SaveAs(Path.Combine(ConfigurationManager.AppSettings["NetworkPath2"], postedFile.FileName.Replace(" ", string.Empty)));
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex.Message);
                        }
                        result1.errcode = 0;
                        result1.errmsg = "Success";
                        OutputList.Add(result1);

                    }
                }
                //// Check if the request contains multipart/form-data.
                //if (!Request.Content.IsMimeMultipartContent())
                //{
                //    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                //}

                //string root = HttpContext.Current.Server.MapPath("~/App_Data");
                //var provider = new MultipartFormDataStreamProvider(root);

                //try
                //{
                //    // Read the form data.
                //    await Request.Content.ReadAsMultipartAsync(provider);

                //    // This illustrates how to get the file names.
                //    foreach (MultipartFileData file in provider.FileData)
                //    {
                //        Log.Info(file.Headers.ContentDisposition.FileName);
                //        Log.Info("Server file path: " + file.LocalFileName);
                //    }
                //    return Request.CreateResponse(HttpStatusCode.OK);
                //}
                //catch (System.Exception e)
                //{
                //    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
                //}
            }
            catch (Exception ex)
            {
                UrUploadFilesOutput outputobj = new UrUploadFilesOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
