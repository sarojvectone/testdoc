using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using System.Security.Cryptography;
using System.Text;
using System.IO;
namespace unifiedringmyaccountwebapi.Controllers
{
    public class unifiedringmyaccountwebapiinserttokenmasterController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UspapiinserttokenmasterInput
        {
            public int? Proj_ID { get; set; }
            public string AToken_ID { get; set; }
            public int? AToken_Min { get; set; }
            public string RToken_ID { get; set; }
            public int? RToken_Min { get; set; }
            
        }
        public class UspapiinserttokenmasterOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
            public token tokenlist { get; set; }
            public string play_store_version { get; set; }
        }


        public class token
        {
            public string AccessTokenID { get; set; }
            public string RefreshTokenID { get; set; }
        }
      
        [Authorize]
       
        public async Task<HttpResponseMessage> Post(HttpRequestMessage request, UspapiinserttokenmasterInput req)
        {
            long timeCheck = Convert.ToInt64(DateTime.Now.ToString("ddmmyyhhmmss"));
            string AccessToken = req.Proj_ID + timeCheck + "Ac";
            string RefershToken = req.Proj_ID + timeCheck + "Rf";
            //string AccessToken = req.Proj_ID + DateTime.Now.ToString("hh/mm") + "Access";
            //string RefershToken = req.Proj_ID + DateTime.Now.ToString("HH/mm") + "Refresh";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["API_logger"].ConnectionString))
                {
                    conn.Open();


                    //Getting Project Key from authkey
                    string projectID = string.Empty;
                    projectID = Convert.ToString(req.Proj_ID);

                    //Getting Project Key from authkey
                    int firauthkey = Convert.ToInt16(projectID.Substring(0, 1));
                    if (firauthkey == 1)
                    {
                        //projectID = projectID.Substring(Math.Max(0, projectID.Length - 1));
                        projectID = projectID.Substring(4);
                    }
                    else if (firauthkey == 2)
                    {
                        projectID = projectID.Substring(5);
                    }
                    req.Proj_ID = Convert.ToInt32(projectID);
                    req.AToken_ID = aes_Convert(Base64Encode(AccessToken));
                    req.RToken_ID = aes_Convert(Base64Encode(RefershToken));
                    req.AToken_Min = Convert.ToInt32(ConfigurationManager.AppSettings["AccessTokenMinutes"]);
                    req.RToken_Min = Convert.ToInt32(ConfigurationManager.AppSettings["RefreshTokenMinutes"]);

                    var sp = "Usp_API_Insert_Token_Master";

                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @Proj_ID = req.Proj_ID,
                                @AToken_ID = req.AToken_ID,
                                @AToken_Min = req.AToken_Min,
                                @RToken_ID = req.RToken_ID,
                                @RToken_Min = req.RToken_Min
                            },
                            commandType: CommandType.StoredProcedure);
                    List<UspapiinserttokenmasterOutput> OutputList = new List<UspapiinserttokenmasterOutput>();
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UspapiinserttokenmasterOutput()
                        {
                            errcode = r.errcode,
                            errmsg = r.errmsg                           
                        }));
                      
                        UspapiinserttokenmasterOutput test = new UspapiinserttokenmasterOutput();
                        token token = new token();
                        token.AccessTokenID = Base64Encode(AccessToken);
                        token.RefreshTokenID = Base64Encode(RefershToken);
                        //Hard coded : 13-Oct-2020:As per request by Android team
                        string play_store_version = ConfigurationManager.AppSettings["play_store_version"];
                        test.tokenlist = token;
                        if (OutputList[0].errmsg == "Success")
                        {
                            test.errmsg = OutputList[0].errmsg;
                            test.errcode = OutputList[0].errcode;
                            test.play_store_version = play_store_version;
                        }                    
                        OutputList.Add(test);                       
                        return Request.CreateResponse(HttpStatusCode.OK, OutputList[1]);
                    }
                    else
                    {
                        UspapiinserttokenmasterOutput outputobj = new UspapiinserttokenmasterOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                        return Request.CreateResponse(HttpStatusCode.OK, OutputList);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            //return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }

        #region To encrypt from Base64
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        #endregion

        #region aes convertion
        private string aes_Convert(string aes_Convert)
        {
            string original = aes_Convert;

            // Create a new instance of the Aes
            // class.  This generates a new key and initialization 
            // vector (IV).
            //string roundtrip = "";
            string encryptStr = string.Empty;

            using (Aes myAes = Aes.Create())
            {
                byte[] encrypted;

                byte[] bkey = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
                byte[] biv = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };

                // Encrypt the string to an array of bytes.
                //encrypted = encryptStringToBytes_AES(original, myAes.Key, myAes.IV);
                encrypted = encryptStringToBytes_AES(original, bkey, biv);

                // Decrypt the bytes to a string.
                //roundtrip = decryptStringFromBytes_AES(encrypted, myAes.Key, myAes.IV);

                StringBuilder print = new StringBuilder();


                for (int i = 0; i < encrypted.Length; i++)
                {
                    print.Append(encrypted[i].ToString());
                }


                //Store encrypted string
                //StringBuilder to string
                encryptStr = print.ToString();
            }

            return encryptStr;
        }
        #endregion

        #region AES Encryption
        static byte[] encryptStringToBytes_AES(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // Declare the streams used
            // to encrypt to an in memory
            // array of bytes.
            MemoryStream msEncrypt = null;
            CryptoStream csEncrypt = null;
            StreamWriter swEncrypt = null;

            // Declare the Aes object
            // used to encrypt the data.
            Aes aesAlg = null;

            // Declare the bytes used to hold the
            // encrypted data.
            //byte[] encrypted = null;

            try
            {
                // Create an Aes object
                // with the specified key and IV.
                aesAlg = Aes.Create();
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                msEncrypt = new MemoryStream();
                csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
                swEncrypt = new StreamWriter(csEncrypt);

                //Write all data to the stream.
                swEncrypt.Write(plainText);

            }
            finally
            {
                // Clean things up.

                // Close the streams.
                if (swEncrypt != null)
                    swEncrypt.Close();
                //if (csEncrypt != null)
                //    csEncrypt.Close();
                //if (msEncrypt != null)
                //    msEncrypt.Close();

                // Clear the Aes object.
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            // Return the encrypted bytes from the memory stream.
            return msEncrypt.ToArray();

        }
        #endregion
     
    }
}
