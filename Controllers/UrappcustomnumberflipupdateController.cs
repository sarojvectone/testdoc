using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappcustomnumberflipupdateController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappcustomnumberflipupdateInput
        {
			public double dir_user_id { get; set; }
			public int id { get; set; }
			public string number { get; set; }
			//public string name { get; set; }
			//public int ring_type { get; set; }
			public int number_status { get; set; }
			public int type { get; set; }
            public int Ring_type { get; set; }
            public string dev_product_name { get; set; }    
        }
        public class UrappcustomnumberflipupdateOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappcustomnumberflipupdateInput req)
        {
            List<UrappcustomnumberflipupdateOutput> OutputList = new List<UrappcustomnumberflipupdateOutput>();
			Log.Info("Input : UrappcustomnumberflipupdateController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_custom_number_flip_update";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@dir_user_id = req.dir_user_id,
								@id = req.id,
								@number = req.number,
								//@name = req.name,
								//@ring_type = req.ring_type,
                                @name = req.dev_product_name,
                                @ring_type = req.Ring_type,
								@number_status = req.number_status,
								@type = req.type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrappcustomnumberflipupdateOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrappcustomnumberflipupdateOutput outputobj = new UrappcustomnumberflipupdateOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrappcustomnumberflipupdateOutput outputobj = new UrappcustomnumberflipupdateOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
