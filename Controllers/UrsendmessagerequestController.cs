using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using System.Text;
using System.IO;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrsendmessagerequestController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrsendmessagerequestInput
        {
			public string mobileno { get; set; }  	                   	    
        }
        public class UrsendmessagerequestOutput
        {
			public string pin { get; set; }
			public string smsurl { get; set; }
			public string org_address { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrsendmessagerequestInput req)
        {
            List<UrsendmessagerequestOutput> OutputList = new List<UrsendmessagerequestOutput>();
			Log.Info("Input : UrsendmessagerequestController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_send_message_request";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@mobileno = req.mobileno 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrsendmessagerequestOutput()
                        {
							pin = r.pin,
							smsurl = r.smsurl,
							org_address = r.org_address,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrsendmessagerequestOutput outputobj = new UrsendmessagerequestOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }

                    if (OutputList.Count > 0)
                    {
                      
                        if (OutputList[0].smsurl != null && OutputList[0].errcode != -1)
                        {
                            Log.Info("Send SMS SMSURL Start: " + OutputList[0].smsurl);
                            string sText = "Your verification code is "+OutputList[0].pin+".Thank you!";

                            ASCIIEncoding encoding = new ASCIIEncoding();
                            string postData = "delivery-receipt=No" +
                                   "&destination-addr=" + req.mobileno.Trim() +
                                   "&originator-addr-type=" + 5 +
                                   "&originator-addr=" + OutputList[0].org_address +
                                   "&payload-type=text" +
                                   "&message=" + System.Web.HttpUtility.UrlEncode(sText, Encoding.Default);

                            Console.WriteLine(postData);

                            HttpWebRequest smsReq = (HttpWebRequest)WebRequest.Create(OutputList[0].smsurl);
                            smsReq.Method = "POST";
                            smsReq.ContentType = "application/x-www-form-urlencoded";
                            smsReq.ServicePoint.Expect100Continue = false;
                            ASCIIEncoding enc = new ASCIIEncoding();
                            byte[] data = enc.GetBytes(postData);
                            smsReq.ContentLength = data.Length;

                            Stream newStream = smsReq.GetRequestStream();
                            newStream.Write(data, 0, data.Length);
                            newStream.Close();

                            HttpWebResponse smsRes = (HttpWebResponse)smsReq.GetResponse();
                            Stream resStream = smsRes.GetResponseStream();

                            Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                            StreamReader readStream = new StreamReader(resStream, encode);
                            string sResponse = readStream.ReadToEnd().Trim();

                            Log.Info("Send SMS SMSURL end: " + OutputList[0].smsurl);
                        
                        
                        }
                    
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrsendmessagerequestOutput outputobj = new UrsendmessagerequestOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
