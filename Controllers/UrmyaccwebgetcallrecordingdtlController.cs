using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    
    public class UrmyaccwebgetcallrecordingdtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class URMyaccWebgetCallRecordingdtlInput
        {
			public int? Company_id { get; set; }
            public int? Type { get; set; }  	 
        }
        public class URMyaccWebgetCallRecordingdtlOutput
        { 
            public string Announcement_start { get; set; }
            public string Announcement_stop { get; set; }
            public int? Greeting_by { get; set; }
            public int? Greeting_type { get; set; }
            public int Language { get; set; }
            public string Rec_Status { get; set; }
            public int errcode { get; set; }
            public int? All_inbound_Calls{ get; set; }
            public int? All_outbound_Calls { get; set; }
            public string errmsg { get; set; }
            public string greeting_content { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, URMyaccWebgetCallRecordingdtlInput req)
        {
            List<URMyaccWebgetCallRecordingdtlOutput> OutputList = new List<URMyaccWebgetCallRecordingdtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_get_Call_Recording_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @Company_id = req.Company_id,
                                @Type  = req.Type 
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new URMyaccWebgetCallRecordingdtlOutput()
                        {
                            Announcement_start = r.Announcement_start,
                            Announcement_stop = r.Announcement_stop,
                            Greeting_by = r.Greeting_by == null ? 0 : r.Greeting_by,
                            Greeting_type = r.Greeting_type == null ? 0 : r.Greeting_type,
                            Language = r.Language == null ? 0 : r.Language,
                            Rec_Status = r.Rec_Status, 
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg,
                            All_inbound_Calls = r.All_inbound_Calls == null ? 0 : r.All_inbound_Calls,
                            All_outbound_Calls = r.All_outbound_Calls == null ? 0 : r.All_outbound_Calls,
                            greeting_content = r.greeting_content
                        }));
                    }
                    else
                    {
                        URMyaccWebgetCallRecordingdtlOutput outputobj = new URMyaccWebgetCallRecordingdtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                URMyaccWebgetCallRecordingdtlOutput outputobj = new URMyaccWebgetCallRecordingdtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
