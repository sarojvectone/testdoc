using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountdotopupcdconlineController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountdotopupcdconlineInput
        {
			public int? company_id { get; set; }
			public double? topup_amount { get; set; }
			public int? productid { get; set; }
			public string paymentref { get; set; }
			public int? last4digit_ccno { get; set; }
			public string card_type { get; set; }
			public string cardexpdate { get; set; }
			public string client_ip { get; set; }
			public int? auto_topup_flag { get; set; }
			public double? auto_topup_amount { get; set; }  	                   	    
        }
        public class UrmyaccountdotopupcdconlineOutput
        {
            public double? prev_balance { get; set; }
            public double? after_balance { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountdotopupcdconlineInput req)
        {
            List<UrmyaccountdotopupcdconlineOutput> OutputList = new List<UrmyaccountdotopupcdconlineOutput>();
			Log.Info("Input : UrmyaccountdotopupcdconlineController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_do_topup_cdc_online";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@topup_amount = req.topup_amount,
								@productid = req.productid,
								@paymentref = req.paymentref,
								@last4digit_ccno = req.last4digit_ccno,
								@card_type = req.card_type,
								@cardexpdate = req.cardexpdate,
								@client_ip = req.client_ip,
								@auto_topup_flag = req.auto_topup_flag,
								@auto_topup_amount = req.auto_topup_amount 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountdotopupcdconlineOutput()
                        {
                            prev_balance = r.prev_balance == null ? 0.0D : r.prev_balance,
                            after_balance = r.after_balance == null ? 0.0D : r.after_balance,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountdotopupcdconlineOutput outputobj = new UrmyaccountdotopupcdconlineOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountdotopupcdconlineOutput outputobj = new UrmyaccountdotopupcdconlineOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
