using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using System.Xml.Serialization;
using System.Xml;
using Newtonsoft.Json;
using System.IO;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UruserextninsertblocknumberAppController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UruserextninsertblocknumberAppInput
        {
			public int domain_id { get; set; }
			public int extension { get; set; }
			public int block_type { get; set; }
			public string ivr_file { get; set; }
            public List<block> block_number { get; set; }  	                   	    
        }
        public class UruserextninsertblocknumberAppOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }

        public class block
        {
            public string number { get; set; }
            public string name { get; set; }         	    
        }
      
       
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UruserextninsertblocknumberAppInput req)
        {
            List<UruserextninsertblocknumberAppOutput> OutputList = new List<UruserextninsertblocknumberAppOutput>();

            Log.Info("Input : " + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_user_extn_insert_block_number";
                    // To convert JSON text contained in string json into an XML node
                    string strblocknumber = "";
                    if (req.block_number.Count() > 0)
                    {
                        strblocknumber = CreateXML(req.block_number);

                        strblocknumber = strblocknumber.Substring(134, strblocknumber.Length - 149);

                        strblocknumber = "<bulk>" + strblocknumber + "</bulk>";
                    }
                    Log.Info("strblocknumber : " + JsonConvert.SerializeObject(strblocknumber));
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id,
								@extension = req.extension,
								@block_type = req.block_type,
								@ivr_file = req.ivr_file,
                                @block_number = String.IsNullOrEmpty(strblocknumber) ? null : strblocknumber
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UruserextninsertblocknumberAppOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UruserextninsertblocknumberAppOutput outputobj = new UruserextninsertblocknumberAppOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UruserextninsertblocknumberAppOutput outputobj = new UruserextninsertblocknumberAppOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            Log.Info("Output : " + JsonConvert.SerializeObject(OutputList));
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
        public string CreateXML(Object YourClassObject)
        {
            XmlDocument xmlDoc = new XmlDocument();   //Represents an XML document, 
            // Initializes a new instance of the XmlDocument class.          
            XmlSerializer xmlSerializer = new XmlSerializer(YourClassObject.GetType());
            // Creates a stream whose backing store is memory. 
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, YourClassObject);
                xmlStream.Position = 0;
                //Loads the XML document from the specified string.
                xmlDoc.Load(xmlStream);
                return xmlDoc.InnerXml;
            }
        }
        
    }
}
