using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmacallflipreorderController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmacallflipreorderInput
        {
			public int dir_user_id { get; set; }
			public string callflip { get; set; }  	                   	    
        }
        public class UrmacallflipreorderOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmacallflipreorderInput req)
        {
            List<UrmacallflipreorderOutput> OutputList = new List<UrmacallflipreorderOutput>();
            try
            {
                if (!String.IsNullOrEmpty(req.callflip))
                {
                    using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                    {
                        conn.Open();
                        var sp = "ur_ma_call_flip_reorder";
                        var result = conn.Query<dynamic>(
                                sp, new
                                {
                                    @dir_user_id = req.dir_user_id,
                                    @callflip = req.callflip
                                },
                                commandType: CommandType.StoredProcedure);
                        if (result != null && result.Count() > 0)
                        {
                            OutputList.AddRange(result.Select(r => new UrmacallflipreorderOutput()
                            {
                                errcode = r.errcode == null ? 0 : r.errcode,
                                errmsg = r.errmsg == null ? "Success" : r.errmsg
                            }));
                        }
                        else
                        {
                            UrmacallflipreorderOutput outputobj = new UrmacallflipreorderOutput();
                            outputobj.errcode = -1;
                            outputobj.errmsg = "No Rec found";
                            OutputList.Add(outputobj);
                        }
                    }
                }
                else
                {
                    UrmacallflipreorderOutput outputobj = new UrmacallflipreorderOutput();
                    outputobj.errcode = -1;
                    outputobj.errmsg = "Input Details Not Found";
                    OutputList.Add(outputobj);
                }
            }
            catch (Exception ex)
            {
                UrmacallflipreorderOutput outputobj = new UrmacallflipreorderOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
