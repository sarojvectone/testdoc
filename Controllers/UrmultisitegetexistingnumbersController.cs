using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmultisitegetexistingnumbersController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmultisitegetexistingnumbersInput
        {
			public int company_id { get; set; }
			public string external_number { get; set; }  	                   	    
        }
        public class UrmultisitegetexistingnumbersOutput
        {
			public string Number { get; set; }
			public string Location { get; set; }
			public string Assigned_to { get; set; }
			public string Ext { get; set; }
			public string Type { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmultisitegetexistingnumbersInput req)
        {
            List<UrmultisitegetexistingnumbersOutput> OutputList = new List<UrmultisitegetexistingnumbersOutput>();
			Log.Info("Input : UrmultisitegetexistingnumbersController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Multisite_Get_existing_numbers";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@external_number = req.external_number 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmultisitegetexistingnumbersOutput()
                        {
							Number = r.Number,
							Location = r.Location,
							Assigned_to = r.Assigned_to,
							Ext = r.Ext,
							Type = r.Type,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmultisitegetexistingnumbersOutput outputobj = new UrmultisitegetexistingnumbersOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmultisitegetexistingnumbersOutput outputobj = new UrmultisitegetexistingnumbersOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
