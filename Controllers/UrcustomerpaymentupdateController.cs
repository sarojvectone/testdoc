using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrcustomerpaymentupdateController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrcustomerpaymentupdateInput
        {
			public int company_id { get; set; }
			public string period { get; set; }
			public string payment_ref { get; set; }
			public string pay_type { get; set; }
			public double? amount { get; set; }
			public int payment_status { get; set; }  	                   	    
        }
        public class UrcustomerpaymentupdateOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrcustomerpaymentupdateInput req)
        {
            List<UrcustomerpaymentupdateOutput> OutputList = new List<UrcustomerpaymentupdateOutput>();
			Log.Info("Input : UrcustomerpaymentupdateController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_customer_payment_update";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@period = req.period,
								@payment_ref = req.payment_ref,
								@pay_type = req.pay_type,
								@amount = req.amount,
								@payment_status = req.payment_status 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcustomerpaymentupdateOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrcustomerpaymentupdateOutput outputobj = new UrcustomerpaymentupdateOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrcustomerpaymentupdateOutput outputobj = new UrcustomerpaymentupdateOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
