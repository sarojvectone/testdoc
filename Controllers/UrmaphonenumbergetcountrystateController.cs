using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaphonenumbergetcountrystateController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaphonenumbergetcountrystateInput
        {
			public string country { get; set; }  	                   	    
        }
        public class UrmaphonenumbergetcountrystateOutput
        {
			public int? state_id { get; set; }
			public string state { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaphonenumbergetcountrystateInput req)
        {
            List<UrmaphonenumbergetcountrystateOutput> OutputList = new List<UrmaphonenumbergetcountrystateOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_phone_number_get_country_state";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@country = req.country 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmaphonenumbergetcountrystateOutput()
                        {
							state_id = r.state_id == null ? 0 : r.state_id,
							state = r.state,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmaphonenumbergetcountrystateOutput outputobj = new UrmaphonenumbergetcountrystateOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmaphonenumbergetcountrystateOutput outputobj = new UrmaphonenumbergetcountrystateOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
