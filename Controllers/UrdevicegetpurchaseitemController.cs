using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdevicegetpurchaseitemController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdevicegetpurchaseitemInput
        {
			public int company_id { get; set; }  	                   	    
        }
        public class UrdevicegetpurchaseitemOutput
        {
			public string category { get; set; }
			public string phone_id { get; set; }
			public string phone_desc { get; set; }
			public double? price { get; set; }
            public string phone_model { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdevicegetpurchaseitemInput req)
        {
            List<UrdevicegetpurchaseitemOutput> OutputList = new List<UrdevicegetpurchaseitemOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_get_purchase_item";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrdevicegetpurchaseitemOutput()
                        {
							category = r.category,
							phone_id = r.phone_id,
							phone_desc = r.phone_desc,
							price = r.price == null ? 0.0F : r.price,
                            phone_model = r.phone_model,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrdevicegetpurchaseitemOutput outputobj = new UrdevicegetpurchaseitemOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrdevicegetpurchaseitemOutput outputobj = new UrdevicegetpurchaseitemOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
