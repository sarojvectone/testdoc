using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebinsertaudiofileController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebinsertaudiofileInput
        {
            public int Company_Id { get; set; }
            public int Audio_Type { get; set; }
            public int Greeting_Play_Type { get; set; }
            public int Greeting { get; set; }
            public int Language { get; set; }
            public string Voice { get; set; }
            public string Content { get; set; }
            public string Audio_file { get; set; }
            public int Action { get; set; }
            public string Greeting_Custom_Type { get; set; }
            public string Extension { get; set; }
            public string Extension_Name { get; set; }
            public int site_id { get; set; }
            public int ivr_id { get; set; }
        }
        public class UrmyaccwebinsertaudiofileOutput
        {
            
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebinsertaudiofileInput req)
        {
            Log.Info("Urmyaccwebinsertaudiofile: Input : " + JsonConvert.SerializeObject(req));
            List<UrmyaccwebinsertaudiofileOutput> OutputList = new List<UrmyaccwebinsertaudiofileOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_Insert_audio_file";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @Company_Id = req.Company_Id,
                                @Audio_Type = req.Audio_Type,
                                @Greeting_Play_Type = req.Greeting_Play_Type,
                                @Greeting = req.Greeting,
                                @Language = req.Language,
                                @Voice = req.Voice,
                                @Content = req.Content,
                                @Audio_file = req.Audio_file,
                                @Action = req.Action,
                                @Greeting_Custom_Type = req.Greeting_Custom_Type,
                                @Extension = req.Extension,
                                @Extension_Name = req.Extension_Name,
                                @site_id = req.site_id,
                                @ivr_id = req.ivr_id
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebinsertaudiofileOutput()
                        {
                            //site_id = r.site_id == null ? 0 : r.site_id,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebinsertaudiofileOutput outputobj = new UrmyaccwebinsertaudiofileOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebinsertaudiofileOutput outputobj = new UrmyaccwebinsertaudiofileOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            Log.Info("Urmyaccwebinsertaudiofile: Input : : Output : " + JsonConvert.SerializeObject(OutputList));
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
