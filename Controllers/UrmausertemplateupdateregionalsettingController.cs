using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmausertemplateupdateregionalsettingController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmausertemplateupdateregionalsettingInput
        {
			public int temp_id { get; set; }
			public string time_zone { get; set; }
			public string home_country_code { get; set; }
			public string greetings_language { get; set; }
			public int time_format { get; set; }
			public string user_language { get; set; }
			public string regional_format { get; set; }  	                   	    
        }
        public class UrmausertemplateupdateregionalsettingOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmausertemplateupdateregionalsettingInput req)
        {
            List<UrmausertemplateupdateregionalsettingOutput> OutputList = new List<UrmausertemplateupdateregionalsettingOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_user_template_update_regional_setting";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@temp_id = req.temp_id,
								@time_zone = req.time_zone,
								@home_country_code = req.home_country_code,
								@greetings_language = req.greetings_language,
								@time_format = req.time_format,
								@user_language = req.user_language,
								@regional_format = req.regional_format 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmausertemplateupdateregionalsettingOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmausertemplateupdateregionalsettingOutput outputobj = new UrmausertemplateupdateregionalsettingOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmausertemplateupdateregionalsettingOutput outputobj = new UrmausertemplateupdateregionalsettingOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
