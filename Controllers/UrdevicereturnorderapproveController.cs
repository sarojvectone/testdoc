using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdevicereturnorderapproveController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        //@delivery_date-datetime, @delivery_comment-varchar
        public class UrdevicereturnorderapproveInput
        {
			public int id { get; set; }
            public int tracking_no { get; set; }
            public  string comments { get; set; }
			public int approval_flag { get; set; }
            public DateTime? delivery_date { get; set; }
            public string delivery_comment { get; set; }
			public int type { get; set; }  	                   	    
        }
        public class UrdevicereturnorderapproveOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdevicereturnorderapproveInput req)
        {
            List<UrdevicereturnorderapproveOutput> OutputList = new List<UrdevicereturnorderapproveOutput>();
			Log.Info("Input : UrdevicereturnorderapproveController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_return_order_approve";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@id = req.id,
                                @tracking_no = req.tracking_no,
                                @comments = req.comments,
								@approval_flag = req.approval_flag,
								@type = req.type, 
			                    @delivery_date = req.delivery_date,
                                @delivery_comment = req.delivery_comment
                                    
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrdevicereturnorderapproveOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrdevicereturnorderapproveOutput outputobj = new UrdevicereturnorderapproveOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrdevicereturnorderapproveOutput outputobj = new UrdevicereturnorderapproveOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
