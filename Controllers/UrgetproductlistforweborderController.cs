using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrgetproductlistforweborderController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrgetproductlistforweborderInput
        {
			public string category_id { get; set; }
			public string brand { get; set; }
			public double? range_price_min { get; set; }
			public double? range_price_max { get; set; }
            public string prod_code { get; set; }
            public int price_sort_type { get; set; }
        }
        public class UrgetproductlistforweborderOutput
        {
			public string prod_code { get; set; }
			public string prod_name { get; set; }
			public int category_id { get; set; }
			public int inventory_id { get; set; }
			public string prod_model { get; set; }
			public string brand { get; set; }
			public double? prod_weight { get; set; }
			public int qty { get; set; }
			public double? ur_price { get; set; }
			public string prod_short_desc { get; set; }
			public string prod_desc { get; set; }
			public string image_path { get; set; }
			public string data_sheet_path { get; set; }
			public string user_guide_path { get; set; }
            public int? product_id { get; set; }
            public string image_path_2 { get; set; }
            public string image_path_3 { get; set; }
            public string image_path_4 { get; set; }
            public string prod_source { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }

        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrgetproductlistforweborderInput req)
        {
            List<UrgetproductlistforweborderOutput> OutputList = new List<UrgetproductlistforweborderOutput>();
			Log.Info("Input : UrgetproductlistforweborderController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_get_product_list_for_web_order";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@category_id = req.category_id,
								@brand = req.brand,
								@range_price_min = req.range_price_min,
								@range_price_max = req.range_price_max,
                                @prod_code = req.prod_code,
                                @price_sort_type = req.price_sort_type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrgetproductlistforweborderOutput()
                        {
							prod_code = r.prod_code,
							prod_name = r.prod_name,
							category_id = r.category_id == null ? 0 : r.category_id,
							inventory_id = r.inventory_id == null ? 0 : r.inventory_id,
							prod_model = r.prod_model,
							brand = r.brand,
							prod_weight = r.prod_weight == null ? 0.0D : r.prod_weight,
							qty = r.qty == null ? 0 : r.qty,
							ur_price = r.ur_price == null ? 0.0D : r.ur_price,
							prod_short_desc = r.prod_short_desc,
							prod_desc = r.prod_desc,
							image_path = r.image_path,
							data_sheet_path = r.data_sheet_path,
							user_guide_path = r.user_guide_path,
                            product_id = r.product_id == null ? 0 : r.product_id,
                            image_path_2 = r.image_path_2,
                            image_path_3 = r.image_path_3,
                            image_path_4 = r.image_path_4,
                            prod_source = r.prod_source,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrgetproductlistforweborderOutput outputobj = new UrgetproductlistforweborderOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrgetproductlistforweborderOutput outputobj = new UrgetproductlistforweborderOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
