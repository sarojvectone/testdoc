using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebuserorderextensionController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebuserorderextensionInput
        {
			public int? OrderId { get; set; }
			public int? Order_Type { get; set; }
            public string user_role { get; set; }
            public string user_dept { get; set; }
            public string user_status { get; set; }
            public string usertype { get; set; }
        }
        public class UrmyaccwebuserorderextensionOutput
        {
			public string name { get; set; }
			public int? orderId { get; set; }
            public int? domain_id { get; set; }
            public string email { get; set; }
			public string ext_number { get; set; }
			public string user_role { get; set; }
			public string department { get; set; }
            public int? order_item_status_idx { get; set; }
            public string order_item_status { get; set; }
            public string mac_address { get; set; }
            public string provision_file { get; set; }
            public int? status { get; set; }
            public long? account_plan_id { get; set; }
            public string mobileno{ get; set; }
            public string direct_number { get; set; }
            public string emp_id { get; set; }
            public int? is_mapped { get; set; }
            public string dp_password { get; set; }
            public string did_number { get; set; }
            public string phone_name  { get; set; }
            public int  user_type { get; set; }
            public int site_id { get; set; }
            public string site_name { get; set; }
            public long? Hubspot_contact_id { get; set; }
            public int? onboarding_flag { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebuserorderextensionInput req)
        {
            List<UrmyaccwebuserorderextensionOutput> OutputList = new List<UrmyaccwebuserorderextensionOutput>();
           // Log.Info("Urmyaccwebuserorderextension Input : " + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_user_order_extension";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@OrderId = req.OrderId,
								@Order_Type = req.Order_Type,
                                @user_status = req.user_status,
                                @user_role = req.user_role,
                                @user_dept = req.user_dept,
                                @usertype = req.usertype
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebuserorderextensionOutput()
                        {
							name = r.name,
							orderId = r.orderId == null ? 0 : r.orderId,
							ext_number = r.ext_number,
							user_role = r.user_role,
							department = r.department,
                            domain_id = r.domain_id == null ? 0 : r.orderId,
                            email = r.email,
                            order_item_status_idx = r.order_item_status_idx == null ? 0 : r.order_item_status_idx,
                            order_item_status = r.order_item_status,
                            mac_address = r.mac_address,
                            provision_file = r.provision_file,
                            status = r.status == null ? 0 : r.status,
                            account_plan_id = r.account_plan_id == null ? 0 : r.account_plan_id,
                            mobileno = r.mobileno,
                            direct_number = r.direct_number,
                            emp_id = r.emp_id,
                            is_mapped = r.is_mapped == null ? 0 : r.is_mapped,
                            dp_password = r.dp_password,
                            did_number = r.did_number,
                            phone_name = r.phone_name,
                            user_type = r.user_type == null ? 0 : r.user_type,
                            site_id = r.site_id == null ? 0 : r.site_id,
                            site_name = r.site_name,
                            Hubspot_contact_id = r.Hubspot_contactid == null ? 0 : r.Hubspot_contactid,
                            onboarding_flag = r.onboarding_flag == null ? 0 : r.onboarding_flag,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebuserorderextensionOutput outputobj = new UrmyaccwebuserorderextensionOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebuserorderextensionOutput outputobj = new UrmyaccwebuserorderextensionOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            //Log.Info("Urmyaccwebuserorderextension Output : " + JsonConvert.SerializeObject(OutputList));
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
