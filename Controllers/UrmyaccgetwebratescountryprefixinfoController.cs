using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccgetwebratescountryprefixinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccgetwebratescountryprefixinfoInput
        {
			public string callingtype { get; set; }
			public string type { get; set; }
			public string country { get; set; }  	                   	    
        }
        public class UrmyaccgetwebratescountryprefixinfoOutput
        {
			public int? id { get; set; }
			public string calling_type { get; set; }
			public string country { get; set; }
			public string prefix { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccgetwebratescountryprefixinfoInput req)
        {
            List<UrmyaccgetwebratescountryprefixinfoOutput> OutputList = new List<UrmyaccgetwebratescountryprefixinfoOutput>();
			Log.Info("Input : UrmyaccgetwebratescountryprefixinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myacc_get_web_rates_country_prefix_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@callingtype = req.callingtype,
								@type = req.type,
								@country = req.country 			                                                       
                            },
                            commandTimeout: 5000,
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccgetwebratescountryprefixinfoOutput()
                        {
							id = r.id == null ? 0 : r.id,
							calling_type = r.calling_type,
							country = r.country,
							prefix = r.prefix,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccgetwebratescountryprefixinfoOutput outputobj = new UrmyaccgetwebratescountryprefixinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccgetwebratescountryprefixinfoOutput outputobj = new UrmyaccgetwebratescountryprefixinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
