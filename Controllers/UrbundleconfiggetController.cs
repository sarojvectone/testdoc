using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrbundleconfiggetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrbundleconfiggetInput
        {
            public int Bundle_type { get; set; }
        }
        public class UrbundleconfiggetOutput
        {
			public int? bundle_id { get; set; }
			public double? bundle_minute { get; set; }
			public double? bundle_price { get; set; }
           public string bundle_name { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage>Post(string id, HttpRequestMessage request, UrbundleconfiggetInput req)
        {
            List<UrbundleconfiggetOutput> OutputList = new List<UrbundleconfiggetOutput>();
			Log.Info("Input : UrbundleconfiggetController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_bundle_config_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @Bundle_type = req.Bundle_type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrbundleconfiggetOutput()
                        {
							bundle_id = r.bundle_id == null ? 0 : r.bundle_id,
                            bundle_name = r.bundle_name,
							bundle_minute = r.bundle_minute == null ? 0.0D : r.bundle_minute,
							bundle_price = r.bundle_price == null ? 0.0D : r.bundle_price,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrbundleconfiggetOutput outputobj = new UrbundleconfiggetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrbundleconfiggetOutput outputobj = new UrbundleconfiggetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
