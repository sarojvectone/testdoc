﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class URportalgetnewplaninfoController : ApiController
    {

        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class urportalgetnewplaninfoInput
        {
            public int company_id { get; set; }
            public int plan_duration_input { get; set; }
            public int no_of_user { get; set; }
        }
 
        public class urportalgetnewplaninfoOutput
        {
            public int? Pln_Idx { get; set; }
            public string New_plan { get; set; }
            public string Pln_Name { get; set; }
            //public double? pc_price { get; set; }
            public string pc_price { get; set; }
            public int? pc_duration { get; set; }
            public string yearly_monthly_Duration { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, urportalgetnewplaninfoInput req)
        {
            List<urportalgetnewplaninfoOutput> OutputList = new List<urportalgetnewplaninfoOutput>();
            Log.Info("Input : urportalgetnewplaninfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_crm_portal_get_new_plan_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id,
                                @plan_duration_input = req.plan_duration_input,
                                @no_of_user = req.no_of_user
                            },
                    commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new urportalgetnewplaninfoOutput()
                        {
                            Pln_Idx = r.Pln_Idx == null ? 0 : r.Pln_Idx,
                            New_plan = r.New_plan,
                            Pln_Name = r.Pln_Name,
                            pc_price = r.pc_price ,//== null ? 0.0F : r.pc_price,
                            pc_duration = r.pc_duration == null ? 0 : r.pc_duration,
                            yearly_monthly_Duration = r.yearly_monthly_Duration,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        Log.Info("Output DB : " + "Empty result");
                        urportalgetnewplaninfoOutput outputobj = new urportalgetnewplaninfoOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                urportalgetnewplaninfoOutput outputobj = new urportalgetnewplaninfoOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }

    }
}