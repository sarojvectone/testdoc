using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountcallqueuegetcustomhoursController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcallqueuegetcustomhoursInput
        {
			public int Company_id { get; set; }
			public int call_queue_id { get; set; }  	                   	    
        }
        public class UrmyaccountcallqueuegetcustomhoursOutput
        {
			public int? dayid { get; set; }
			public string day_name { get; set; }
            //public DateTime? From_Time { get; set; }
            //public DateTime? To_Time { get; set; }
            public TimeSpan? From_Time { get; set; }
            public TimeSpan? To_Time { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcallqueuegetcustomhoursInput req)
        {
            List<UrmyaccountcallqueuegetcustomhoursOutput> OutputList = new List<UrmyaccountcallqueuegetcustomhoursOutput>();
			Log.Info("Input : UrmyaccountcallqueuegetcustomhoursController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_call_queue_get_custom_hours";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_id = req.Company_id,
								@call_queue_id = req.call_queue_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcallqueuegetcustomhoursOutput()
                        {
							dayid = r.dayid == null ? 0 : r.dayid,
							day_name = r.day_name,
							From_Time = r.From_Time == null ? null : r.From_Time,
							To_Time = r.To_Time == null ? null : r.To_Time,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcallqueuegetcustomhoursOutput outputobj = new UrmyaccountcallqueuegetcustomhoursOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcallqueuegetcustomhoursOutput outputobj = new UrmyaccountcallqueuegetcustomhoursOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
