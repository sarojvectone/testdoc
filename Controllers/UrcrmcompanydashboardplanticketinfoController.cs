using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrcrmcompanydashboardplanticketinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrcrmcompanydashboardplanticketinfoInput
        {
			public int type { get; set; }  	                   	    
        }
        public class UrcrmcompanydashboardplanticketinfoOutput
        {
			public string report_desc { get; set; }
			public int tot_count { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrcrmcompanydashboardplanticketinfoInput req)
        {
            List<UrcrmcompanydashboardplanticketinfoOutput> OutputList = new List<UrcrmcompanydashboardplanticketinfoOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_crm_company_dashboard_plan_ticket_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@type = req.type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrcrmcompanydashboardplanticketinfoOutput()
                        {
							report_desc = r.report_desc,
							tot_count = r.tot_count == null ? 0 : r.tot_count,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrcrmcompanydashboardplanticketinfoOutput outputobj = new UrcrmcompanydashboardplanticketinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrcrmcompanydashboardplanticketinfoOutput outputobj = new UrcrmcompanydashboardplanticketinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
