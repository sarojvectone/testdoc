using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrecomgetcompanyfeebackinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrecomgetcompanyfeebackinfoInput
        {

            public int company_id { get; set; }
        }
        public class UrecomgetcompanyfeebackinfoOutput
        {
			public int? company_id { get; set; }
			public int? company_rating { get; set; }
			public string feedback_title { get; set; }
			public string feedback_review { get; set; }
			public DateTime? createddate { get; set; }
			public string called_by { get; set; }
			public string submitted_by { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrecomgetcompanyfeebackinfoInput req)
        {
            List<UrecomgetcompanyfeebackinfoOutput> OutputList = new List<UrecomgetcompanyfeebackinfoOutput>();
			Log.Info("Input : UrecomgetcompanyfeebackinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ecom_get_company_feeback_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrecomgetcompanyfeebackinfoOutput()
                        {
							company_id = r.company_id == null ? 0 : r.company_id,
							company_rating = r.company_rating == null ? 0 : r.company_rating,
							feedback_title = r.feedback_title,
							feedback_review = r.feedback_review,
							createddate = r.createddate == null ? null : r.createddate,
							called_by = r.called_by,
							submitted_by = r.submitted_by,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrecomgetcompanyfeebackinfoOutput outputobj = new UrecomgetcompanyfeebackinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrecomgetcompanyfeebackinfoOutput outputobj = new UrecomgetcompanyfeebackinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
