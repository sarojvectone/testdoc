using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccountgetpersonalinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountgetpersonalinfoInput
        {
			public string extension { get; set; }
			public int domain_id { get; set; }
            public int comp_flag { get; set; }           	    
        }
        public class UrmyaccountgetpersonalinfoOutput
        {
			public string Firstname { get; set; }
			public string lastname { get; set; }
			public string Mobileno { get; set; }
			public string Email { get; set; }
            public string  Descript { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountgetpersonalinfoInput req)
        {
            List<UrmyaccountgetpersonalinfoOutput> OutputList = new List<UrmyaccountgetpersonalinfoOutput>();
			Log.Info("Input : UrmyaccountgetpersonalinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_get_personal_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@extension = req.extension,
								@domain_id = req.domain_id,
                                @comp_flag = req.comp_flag                               
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountgetpersonalinfoOutput()
                        {
							Firstname = r.Firstname,
							lastname = r.lastname,
							Mobileno = r.Mobileno,
							Email = r.Email,
                            Descript = r.Descript,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountgetpersonalinfoOutput outputobj = new UrmyaccountgetpersonalinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountgetpersonalinfoOutput outputobj = new UrmyaccountgetpersonalinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
