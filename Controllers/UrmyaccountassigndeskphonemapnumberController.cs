using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccountassigndeskphonemapnumberController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountassigndeskphonemapnumberInput
        {
			public string extension { get; set; }
			public int? domain_id { get; set; }
			public string display_name { get; set; }
			public string map_number { get; set; }
			public string create_by { get; set; }  	                   	    
        }
        public class UrmyaccountassigndeskphonemapnumberOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountassigndeskphonemapnumberInput req)
        {
            List<UrmyaccountassigndeskphonemapnumberOutput> OutputList = new List<UrmyaccountassigndeskphonemapnumberOutput>();
			Log.Info("Input : UrmyaccountassigndeskphonemapnumberController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_assign_deskphone_map_number";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@extension = req.extension,
								@domain_id = req.domain_id,
								@display_name = req.display_name,
								@map_number = req.map_number,
								@create_by = req.create_by 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountassigndeskphonemapnumberOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountassigndeskphonemapnumberOutput outputobj = new UrmyaccountassigndeskphonemapnumberOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountassigndeskphonemapnumberOutput outputobj = new UrmyaccountassigndeskphonemapnumberOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
