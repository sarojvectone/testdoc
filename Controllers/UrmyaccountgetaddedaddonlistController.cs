using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountgetaddedaddonlistController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountgetaddedaddonlistInput
        {
			public int company_id { get; set; }
			public int plan_id { get; set; }
			public int addon_type { get; set; }  	                   	    
        }
        public class UrmyaccountgetaddedaddonlistOutput
        {
			public int? addon_id { get; set; }
			public string addon_name { get; set; }
			public int? unassigned_extension { get; set; }
			public int? assigned_extnsion { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountgetaddedaddonlistInput req)
        {
            List<UrmyaccountgetaddedaddonlistOutput> OutputList = new List<UrmyaccountgetaddedaddonlistOutput>();
			Log.Info("Input : UrmyaccountgetaddedaddonlistController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_get_added_addon_list";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@plan_id = req.plan_id,
								@addon_type = req.addon_type 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountgetaddedaddonlistOutput()
                        {
							addon_id = r.addon_id == null ? 0 : r.addon_id,
							addon_name = r.addon_name,
							unassigned_extension = r.unassigned_extension == null ? 0 : r.unassigned_extension,
							assigned_extnsion = r.assigned_extnsion == null ? 0 : r.assigned_extnsion,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountgetaddedaddonlistOutput outputobj = new UrmyaccountgetaddedaddonlistOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountgetaddedaddonlistOutput outputobj = new UrmyaccountgetaddedaddonlistOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
