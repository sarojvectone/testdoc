using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UradditionalbundlesubscribegetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UradditionalbundlesubscribegetInput
        {
			public int company_id { get; set; }  	                   	    
        }
        public class UradditionalbundlesubscribegetOutput
        {
			public int? bundle_id { get; set; }
			public double? bundle_minute { get; set; }
			public double? bundle_price { get; set; }
            public int bundle_type { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UradditionalbundlesubscribegetInput req)
        {
            List<UradditionalbundlesubscribegetOutput> OutputList = new List<UradditionalbundlesubscribegetOutput>();
			Log.Info("Input : UradditionalbundlesubscribegetController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_additional_bundle_subscribe_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UradditionalbundlesubscribegetOutput()
                        {
							bundle_id = r.bundle_id == null ? 0 : r.bundle_id,
							bundle_minute = r.bundle_minute == null ? 0.0D : r.bundle_minute,
							bundle_price = r.bundle_price == null ? 0.0D : r.bundle_price,
                            bundle_type = r.bundle_type == null ? 0 : r.bundle_type,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UradditionalbundlesubscribegetOutput outputobj = new UradditionalbundlesubscribegetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UradditionalbundlesubscribegetOutput outputobj = new UradditionalbundlesubscribegetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
