using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmultisiteivrsavepromptController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmultisiteivrsavepromptInput
        {
			public int ivr_id { get; set; }
			public int promot_mode { get; set; }
			public string prompt_name { get; set; }  	                   	    
        }
        public class UrmultisiteivrsavepromptOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmultisiteivrsavepromptInput req)
        {
            List<UrmultisiteivrsavepromptOutput> OutputList = new List<UrmultisiteivrsavepromptOutput>();
			Log.Info("Input : UrmultisiteivrsavepromptController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_multisite_ivr_save_prompt";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@ivr_id = req.ivr_id,
								@promot_mode = req.promot_mode,
								@prompt_name = req.prompt_name 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmultisiteivrsavepromptOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmultisiteivrsavepromptOutput outputobj = new UrmultisiteivrsavepromptOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmultisiteivrsavepromptOutput outputobj = new UrmultisiteivrsavepromptOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
