using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class VbgetissuetypesController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class VbgetissuetypesInput
        {
        }
        public class VbgetissuetypesOutput
        {
			public int IssueTypeId { get; set; }
			public string IssueTypeName { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Get(string id, HttpRequestMessage request, VbgetissuetypesInput req)
        {
            List<VbgetissuetypesOutput> OutputList = new List<VbgetissuetypesOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "Vb_get_issuetypes";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new VbgetissuetypesOutput()
                        {
							IssueTypeId = r.IssueTypeId == null ? 0 : r.IssueTypeId,
							IssueTypeName = r.IssueTypeName,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        VbgetissuetypesOutput outputobj = new VbgetissuetypesOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                VbgetissuetypesOutput outputobj = new VbgetissuetypesOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
