using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountusermgtgetmoduleController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountusermgtgetmoduleInput
        {
			public int? Module_id { get; set; }  	                   	    
        }
        public class UrmyaccountusermgtgetmoduleOutput
        {
			public int? Module_Id { get; set; }
			public string Module_name { get; set; }
			public int? Status { get; set; }
			public string Url { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountusermgtgetmoduleInput req)
        {
            List<UrmyaccountusermgtgetmoduleOutput> OutputList = new List<UrmyaccountusermgtgetmoduleOutput>();
			Log.Info("Input : UrmyaccountusermgtgetmoduleController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_usermgt_get_Module";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Module_id = req.Module_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountusermgtgetmoduleOutput()
                        {
							Module_Id = r.Module_Id == null ? 0 : r.Module_Id,
							Module_name = r.Module_name,
							Status = r.Status == null ? 0 : r.Status,
							Url = r.Url,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountusermgtgetmoduleOutput outputobj = new UrmyaccountusermgtgetmoduleOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountusermgtgetmoduleOutput outputobj = new UrmyaccountusermgtgetmoduleOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
