using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrbillgetserviceplanController : ApiController
    {

       private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrbillgetserviceplanInput
        {
            public int  company_id { get; set; }           
        }
        public class UrbillgetserviceplanOutput
        {
            public string prd_name { get; set; }
            public string pln_name { get; set; }
            public string pc_price { get; set; }
            public string price_per_user { get; set; }
            public string ord_user_range { get; set; }
           // public string account_credit { get; set; }
            public double? account_credit { get; set; }
            public string bill_cycle { get; set; }
            public string tot_localno { get; set; }
            public string localno_price { get; set; }
            public string contract_period { get; set; }
            public string bill_date { get; set; }
            public string bill_due_date { get; set; }
            public string bundle_plan { get; set; }
            public double?  bundle_mins_usage { get; set; }
            public int    tot_inter_nat_no { get; set; }
            public double? inter_nat_no_price { get; set; }
            public int    tot_non_geo_no { get; set; }
            public double? non_geo_no_price { get; set; }
            public double? calling_credit { get; set; }
            public double? threshold_limit { get; set; }          
            public string bill_cycle_type { get; set; }
            public int? Is_free_trail { get; set; }
            public string new_price_combined { get; set; }
            public int? device_phone_count { get; set; }
            public double? Calling_UK_mins { get; set; }
            public int? Unlimited_inbound_conf  { get; set; }
            public double? Unlimited_inbound_conf_value { get; set; }
            public DateTime? next_billing_date { get; set; }
            public string payment_method { get; set; }
            public string cc_no { get; set; }
            public double? next_month_bill { get; set; }
            public int? auto_topup_flag { get; set; }
            public int? plan_id { get; set; }
            public double? auto_topup_amount { get; set; }
            public int? freetrial_days { get; set; }
            public double? prev_paid_bill { get; set; }
		    public int    errcode { get; set; }
		    public string     errmsg { get; set; }	            	   
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrbillgetserviceplanInput req)
        {
            List<UrbillgetserviceplanOutput> OutputList = new List<UrbillgetserviceplanOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_bill_get_service_plan";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id,                               
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrbillgetserviceplanOutput()
                        {
                            prd_name = r.prd_name,
                            pln_name = r.pln_name,
                            pc_price = Convert.ToString(r.pc_price),
                           // pc_price  = r.pc_price ,
                            price_per_user = Convert.ToString(r.price_per_user),
                            ord_user_range = r.ord_user_range,
                            //account_credit = Convert.ToString(r.account_credit),
                            bill_cycle = r.bill_cycle,
                            tot_localno = Convert.ToString(r.tot_localno),
                            localno_price = Convert.ToString(r.localno_price), 
                            contract_period = Convert.ToString(r.contract_period),
                            bill_date = Convert.ToString(r.bill_date),
                            bill_due_date = Convert.ToString(r.bill_due_date),
                            bundle_plan = Convert.ToString(r.bundle_plan),
                            bundle_mins_usage= r.bundle_mins_usage,
                            tot_inter_nat_no = r.tot_inter_nat_no == null ? 0 : r.tot_inter_nat_no,
                            inter_nat_no_price = r.inter_nat_no_price,
                            tot_non_geo_no = r.tot_non_geo_no == null ? 0 : r.tot_non_geo_no,
                            calling_credit = r.calling_credit == null ? 0.0F : r.calling_credit,
                            threshold_limit = r.threshold_limit == null ? 0.0F : r.threshold_limit,
                            non_geo_no_price = r.non_geo_no_price,
                            bill_cycle_type = r.bill_cycle_type,
                            Is_free_trail = r.Is_free_trail == null ? 0 : r.Is_free_trail,
                            new_price_combined = r.new_price_combined,
                            device_phone_count = r.device_phone_count == null ? 0 : r.device_phone_count,
                            Calling_UK_mins = r.Calling_UK_mins == null ? 0.0F : r.Calling_UK_mins,
                            Unlimited_inbound_conf_value = r.Unlimited_inbound_conf_value == null ? 0.0F : r.Unlimited_inbound_conf_value,
                            Unlimited_inbound_conf = r.Unlimited_inbound_conf == null ? 0 : r.Unlimited_inbound_conf,
                            next_billing_date = r.next_billing_date == null ? null : r.next_billing_date,
                            payment_method = r.payment_method,
                            cc_no = r.cc_no,
                            next_month_bill = r.next_month_bill == null ? 0.0F : r.next_month_bill,
                            auto_topup_flag = r.auto_topup_flag == null ? 0 : r.auto_topup_flag,
                            account_credit = r.account_credit == null ? 0 : r.account_credit,
                            plan_id = r.plan_id == null ? 0 :r.plan_id,
                            auto_topup_amount = r.auto_topup_amount == null ? 0.0F : r.auto_topup_amount,
                            prev_paid_bill = r.prev_paid_bill == null ? 0.0F : r.prev_paid_bill,
                            freetrial_days = r.freetrial_days == null ? 0 : r.freetrial_days,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg

                        }));
                    }
                    else
                    {
                        UrbillgetserviceplanOutput outputobj = new UrbillgetserviceplanOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrbillgetserviceplanOutput outputobj = new UrbillgetserviceplanOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
