using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdeviceordercrmController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdeviceordercrmInput
        {
			public int company_id { get; set; }
			public string referenceid { get; set; }
			public string phone_charge { get; set; }
			public double discount { get; set; }
			public double tax_fee { get; set; }
			public string tot_charge { get; set; }
			public DateTime? order_date { get; set; }
			public string ship_info { get; set; }
			public int billing_type { get; set; }
			public string delivery_address { get; set; }
			public string billing_address { get; set; }
			public string order_item { get; set; }  	                   	    
        }
        public class UrdeviceordercrmOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdeviceordercrmInput req)
        {
            List<UrdeviceordercrmOutput> OutputList = new List<UrdeviceordercrmOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_order_crm";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@referenceid = req.referenceid,
								@phone_charge = req.phone_charge,
								@discount = req.discount,
								@tax_fee = req.tax_fee,
								@tot_charge = req.tot_charge,
								@order_date = req.order_date == null ? null : req.order_date,
								@ship_info = req.ship_info,
								@billing_type = req.billing_type,
								@delivery_address = req.delivery_address,
								@billing_address = req.billing_address,
								@order_item = req.order_item 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrdeviceordercrmOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrdeviceordercrmOutput outputobj = new UrdeviceordercrmOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrdeviceordercrmOutput outputobj = new UrdeviceordercrmOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
