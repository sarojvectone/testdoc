using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrcompanycallingrategetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrcompanycallingrategetInput
        {
			public int company_id { get; set; }
			public string calling_type { get; set; }  	                   	    
        }
        public class UrcompanycallingrategetOutput
        {
			public int? rate_id { get; set; }
			public string destination { get; set; }
			public string type { get; set; }
			public string prefix { get; set; }
			public string call_rate { get; set; }
			public int? status { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrcompanycallingrategetInput req)
        {
            List<UrcompanycallingrategetOutput> OutputList = new List<UrcompanycallingrategetOutput>();
			Log.Info("Input : UrcompanycallingrategetController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_company_calling_rate_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@calling_type = req.calling_type 			                                                       
                            },
                            commandTimeout: 0,
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcompanycallingrategetOutput()
                        {
							rate_id = r.rate_id == null ? 0 : r.rate_id,
							destination = r.destination,
							type = r.type,
							prefix = r.prefix,
							call_rate = r.call_rate,
							status = r.status == null ? 0 : r.status,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrcompanycallingrategetOutput outputobj = new UrcompanycallingrategetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrcompanycallingrategetOutput outputobj = new UrcompanycallingrategetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
