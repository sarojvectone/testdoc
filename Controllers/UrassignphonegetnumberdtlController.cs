using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrassignphonegetnumberdtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrassignphonegetnumberdtlInput
        {
			public int company_id { get; set; }
			public int domain_id { get; set; }
			public int number_type { get; set; }
            public string external_number { get; set; }
            public int country_id	 { get; set; }
            public string area_code { get; set; }
        }
        public class UrassignphonegetnumberdtlOutput
        {
			public string external_number { get; set; }
			public string country { get; set; }
			public string assigned_to { get; set; }
			public DateTime? assign_date { get; set; }
			public string number_Type { get; set; }
			public string name { get; set; }
            public int? status { get; set; }
            public string extension { get; set; }
            public string Location { get; set; }
            public string number_Type_id { get; set; }
            public string assign_type { get; set; }
            public int? assign_type_id { get; set; }
            public int? site_id  { get; set; }
            public string site_name { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }

        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrassignphonegetnumberdtlInput req)
        {
            List<UrassignphonegetnumberdtlOutput> OutputList = new List<UrassignphonegetnumberdtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_assign_phone_get_number_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @external_number = req.external_number,
								@company_id = req.company_id,
								@domain_id = req.domain_id,
								@number_type = req.number_type,
                                @country_id = req.country_id,
                                @area_code = req.area_code
                                   
                            },
                            commandTimeout: 0,
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrassignphonegetnumberdtlOutput()
                        {
							external_number = r.external_number,
							country = r.country,
							assigned_to = r.assigned_to,
							assign_date = r.assign_date == null ? null : r.assign_date,
							number_Type = r.number_Type,
							name = r.name,
                            status = r.status == null ? 0 : r.status,
                            extension = r.extension,
                            Location = r.Location,
                            number_Type_id = r.number_Type_id,
                            assign_type = r.assign_type,
                            assign_type_id = r.assign_type_id == null ? 0 : r.assign_type_id,
                            site_id = r.site_id == null ? 0 : r.site_id, 
                            site_name = r.site_name,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrassignphonegetnumberdtlOutput outputobj = new UrassignphonegetnumberdtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrassignphonegetnumberdtlOutput outputobj = new UrassignphonegetnumberdtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
