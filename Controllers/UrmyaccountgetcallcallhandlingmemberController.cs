using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountgetcallcallhandlingmemberController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountgetcallcallhandlingmemberInput
        {
			public int company_id { get; set; }
			public int Queue_id { get; set; }  	                   	    
        }
        public class UrmyaccountgetcallcallhandlingmemberOutput
        {
			public int? Queue_id { get; set; }
			public string extension { get; set; }
			public int? role { get; set; }
			public int? Work_Hour { get; set; }
			public int? Work_Hour_order { get; set; }
			public int? After_Hour { get; set; }
			public int? After_Hour_order { get; set; }
			public int? Q_Handling_Type { get; set; }
			public int? max_duration { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountgetcallcallhandlingmemberInput req)
        {
            List<UrmyaccountgetcallcallhandlingmemberOutput> OutputList = new List<UrmyaccountgetcallcallhandlingmemberOutput>();
			Log.Info("Input : UrmyaccountgetcallcallhandlingmemberController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_get_call_call_handling_member";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@Queue_id = req.Queue_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountgetcallcallhandlingmemberOutput()
                        {
							Queue_id = r.Queue_id == null ? 0 : r.Queue_id,
							extension = r.extension,
							role = r.role == null ? 0 : r.role,
							Work_Hour = r.Work_Hour == null ? 0 : r.Work_Hour,
							Work_Hour_order = r.Work_Hour_order == null ? 0 : r.Work_Hour_order,
							After_Hour = r.After_Hour == null ? 0 : r.After_Hour,
							After_Hour_order = r.After_Hour_order == null ? 0 : r.After_Hour_order,
							Q_Handling_Type = r.Q_Handling_Type == null ? 0 : r.Q_Handling_Type,
							max_duration = r.max_duration == null ? 0 : r.max_duration,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountgetcallcallhandlingmemberOutput outputobj = new UrmyaccountgetcallcallhandlingmemberOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountgetcallcallhandlingmemberOutput outputobj = new UrmyaccountgetcallcallhandlingmemberOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
