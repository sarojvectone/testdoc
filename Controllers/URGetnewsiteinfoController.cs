﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace unifiedringmyaccountwebapi.Controllers
{
    public class URGetnewsiteinfoController:ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class URGetnewsiteinfoInput
        {
            public int site_id { get; set; }
            public int comapany_id { get; set; }
        }
        public class URGetnewsiteinfoOutput
        {
            public int Ext { get; set; }
            public string Name { get; set; }
           //public string Address { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string address3 { get; set; }
            public string postcode { get; set; }
            public string country { get; set; }
            public string state { get; set; }
            public int site_main_number { get; set; }
            public int site_fax_number { get; set; }
            public string email { get; set; }
            public string city { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, URGetnewsiteinfoInput req)
        {
            List<URGetnewsiteinfoOutput> OutputList = new List<URGetnewsiteinfoOutput>();
            Log.Info("Input : URGetnewsiteinfo:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Get_new_site_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @site_id = req.site_id,
                                @comapany_id = req.comapany_id
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new URGetnewsiteinfoOutput()
                        {
                            Ext = r.Ext == null ? 0 : r.Ext,
                            Name = r.Name,
                            address1 = r.address1,
                            address2 = r.address2,
                            address3 = r.address3,
                            postcode = r.postcode,
                            country = r.country,
                            state = r.state,
                            site_main_number = r.site_main_number == null ? 0 : r.site_main_number,
                            site_fax_number = r.site_fax_number == null ? 0 : r.site_fax_number,
                            email = r.email,
                            city = r.city,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));

                    }
                    else
                    {
                        Log.Info("Output DB : " + "Empty result");
                        URGetnewsiteinfoOutput outputobj = new URGetnewsiteinfoOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                URGetnewsiteinfoOutput outputobj = new URGetnewsiteinfoOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}