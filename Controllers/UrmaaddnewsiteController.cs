using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaaddnewsiteController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaaddnewsiteInput
        {
			public int company_id { get; set; }
			public int domain_id { get; set; }
			public int site_id { get; set; }
			public int ivr_id { get; set; }
			public int ivr_extn_num { get; set; }
			public string ivr_extn_name { get; set; }
			public string external_number { get; set; }
			public string tax_per { get; set; }
			public double? tax_fee { get; set; }
			public double? total_charge { get; set; }
			public string reference_no { get; set; }
			public double adjustment_charge { get; set; }
			public string called_by { get; set; }  	                   	    
        }
        public class UrmaaddnewsiteOutput
        {
			//public int? site_id { get; set; }
            public int? ivr_id { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaaddnewsiteInput req)
        {
            List<UrmaaddnewsiteOutput> OutputList = new List<UrmaaddnewsiteOutput>();
			Log.Info("Input : UrmaaddnewsiteController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_add_new_site";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@domain_id = req.domain_id,
								@site_id = req.site_id,
								@ivr_id = req.ivr_id,
								@ivr_extn_num = req.ivr_extn_num,
								@ivr_extn_name = req.ivr_extn_name,
								@external_number = req.external_number,
								@tax_per = req.tax_per,
								@tax_fee = req.tax_fee,
								@total_charge = req.total_charge,
								@reference_no = req.reference_no,
								@adjustment_charge = req.adjustment_charge,
								@called_by = req.called_by			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmaaddnewsiteOutput()
                        {
                            ivr_id  = r.ivr_id == null ? 0 : r.ivr_id,
							//site_id = r.site_id == null ? 0 : r.site_id,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmaaddnewsiteOutput outputobj = new UrmaaddnewsiteOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmaaddnewsiteOutput outputobj = new UrmaaddnewsiteOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
