using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebgetuserpersonaldtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebgetuserpersonaldtlInput
        {
			public int Company_id { get; set; }
			public string extension { get; set; }  	                   	    
        }
        public class UrmyaccwebgetuserpersonaldtlOutput
        {
			public string Firstname { get; set; }
			public string Lastname { get; set; }
			public string personal_number { get; set; }
			public int? country_code { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebgetuserpersonaldtlInput req)
        {
            List<UrmyaccwebgetuserpersonaldtlOutput> OutputList = new List<UrmyaccwebgetuserpersonaldtlOutput>();
			Log.Info("Input : UrmyaccwebgetuserpersonaldtlController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_get_User_personal_Dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_id = req.Company_id,
								@extension = req.extension 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccwebgetuserpersonaldtlOutput()
                        {
							Firstname = r.Firstname,
							Lastname = r.Lastname,
							personal_number = r.personal_number,
                            country_code = r.country_code == null ? 0 : r.country_code,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccwebgetuserpersonaldtlOutput outputobj = new UrmyaccwebgetuserpersonaldtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccwebgetuserpersonaldtlOutput outputobj = new UrmyaccwebgetuserpersonaldtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
