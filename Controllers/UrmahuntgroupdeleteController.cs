using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmahuntgroupdeleteController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmahuntgroupdeleteInput
        {
			public int? domain_id { get; set; }
			public int? group_id { get; set; }
			public string did_number { get; set; }  	                   	    
        }
        public class UrmahuntgroupdeleteOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmahuntgroupdeleteInput req)
        {
            List<UrmahuntgroupdeleteOutput> OutputList = new List<UrmahuntgroupdeleteOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_hunt_group_delete";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id,
								@group_id = req.group_id,
								@did_number = req.did_number 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmahuntgroupdeleteOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmahuntgroupdeleteOutput outputobj = new UrmahuntgroupdeleteOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmahuntgroupdeleteOutput outputobj = new UrmahuntgroupdeleteOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
