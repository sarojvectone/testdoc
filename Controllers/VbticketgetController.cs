using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class VbticketgetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class VbticketgetInput
        {
			public int? ticketid { get; set; }  	                   	    
        }
        public class VbticketgetOutput
        {
			public int ticketid { get; set; }
			public string ticketcode { get; set; }
			public int ticketno { get; set; }
			public string reportername { get; set; }
			public string reporteremail { get; set; }
			public string issuesummary { get; set; }
			public int issuetypeid { get; set; }
			public string issuecode { get; set; }
			public string issuetypename { get; set; }
			public int issuesubtypeid { get; set; }
			public string issuesubtypecode { get; set; }
			public string issuesubtypename { get; set; }
			public string issuedescription { get; set; }
			public string priority { get; set; }
			public string prioritydesc { get; set; }
			public int assigneeuserid { get; set; }
			public string username { get; set; }
			public string emailid { get; set; }
			public  int issueimpact { get; set; }
			public string issueimpactdesc { get; set; }
			public int specificcustomerid { get; set; }
			public string orginalfilename { get; set; }
			public string renamedfilename { get; set; }
			public string filepath { get; set; }
			public string createdby { get; set; }
			public DateTime? createddate { get; set; }
            public string createddate_str { get { return createddate != null ? Convert.ToDateTime(createddate).ToString("dd-MM-yyyy") : ""; } }
			public string ticketstatus { get; set; }
			public int? note_id { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, VbticketgetInput req)
        {
            List<VbticketgetOutput> OutputList = new List<VbticketgetOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "vb_ticket_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@ticketid = req.ticketid 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new VbticketgetOutput()
                        {
							ticketid = r.ticketid == null ? 0 : r.ticketid,
							ticketcode = r.ticketcode,
                            ticketno = r.ticketno == null ? 0 : r.ticketno,
							reportername = r.reportername,
							reporteremail = r.reporteremail,
							issuesummary = r.issuesummary,
							issuetypeid = r.issuetypeid == null ? 0 : r.issuetypeid,
							issuecode = r.issuecode,
							issuetypename = r.issuetypename,
							issuesubtypeid = r.issuesubtypeid == null ? 0 : r.issuesubtypeid,
							issuesubtypecode = r.issuesubtypecode,
							issuesubtypename = r.issuesubtypename,
							issuedescription = r.issuedescription,
							priority = r.priority,
							prioritydesc = r.prioritydesc,
							assigneeuserid = r.assigneeuserid == null ? 0 : r.assigneeuserid,
							username = r.username,
							emailid = r.emailid,
							issueimpact = r.issueimpact,
							issueimpactdesc = r.issueimpactdesc,
							specificcustomerid = r.specificcustomerid == null ? 0 : r.specificcustomerid,
							orginalfilename = r.orginalfilename,
							renamedfilename = r.renamedfilename,
							filepath = r.filepath,
							createdby = r.createdby,
							createddate = r.createddate,
							ticketstatus = r.ticketstatus,
							note_id = r.note_id == null ? 0 : r.note_id,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        VbticketgetOutput outputobj = new VbticketgetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                VbticketgetOutput outputobj = new VbticketgetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
