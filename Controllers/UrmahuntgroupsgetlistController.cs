using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmahuntgroupsgetlistController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmahuntgroupsgetlistInput
        {
			public int domain_id { get; set; }
            public int group_id { get; set; }
        }
        public class UrmahuntgroupsgetlistOutput
        {
			public long? group_id { get; set; }
			public string Group_Name { get; set; }
            public int? Extension { get; set; }
			public string did_number { get; set; }
            public long? number { get; set; }
            public long? id_service { get; set; }
			public string ring_type { get; set; }
			public int? ring_id { get; set; }
			public string note { get; set; }
			public string status { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmahuntgroupsgetlistInput req)
        {
            List<UrmahuntgroupsgetlistOutput> OutputList = new List<UrmahuntgroupsgetlistOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_hunt_groups_get_list";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @domain_id = req.domain_id,
                                @group_id = req.group_id
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmahuntgroupsgetlistOutput()
                        {
                            group_id = r.group_id == null ? 0 : r.group_id,
							Group_Name = r.Group_Name,
                            Extension = r.Extension == null ? 0 : r.Extension,
							did_number = r.did_number,
							number = r.number == null ? 0 : r.number,
							id_service = r.id_service == null ? 0 : r.id_service,
							ring_type = r.ring_type,
							ring_id = r.ring_id == null ? 0 : r.ring_id,
							note = r.note,
							status = r.status,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmahuntgroupsgetlistOutput outputobj = new UrmahuntgroupsgetlistOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmahuntgroupsgetlistOutput outputobj = new UrmahuntgroupsgetlistOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
