using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    
    public class UrmyaccwebgetcompanyregionalsettingdtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebgetcompanyregionalsettingdtlInput
        {
			public int? Company_id { get; set; }
            public int? site_id { get; set; }
        
        }
        public class UrmyaccwebgetcompanyregionalsettingdtlOutput
        {
            public int? Company_Id { get; set; }
            public string Time_zone { get; set; }
            public string Home_country_code { get; set; }
            public int? Greetings_language { get; set; }
            public int? User_language { get; set; }
            public string Regional_format { get; set; }
            public int Time_format { get; set; }

            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebgetcompanyregionalsettingdtlInput req)
        {
            List<UrmyaccwebgetcompanyregionalsettingdtlOutput> OutputList = new List<UrmyaccwebgetcompanyregionalsettingdtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_get_Company_Regional_Setting_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @Company_id = req.Company_id, 
                                @site_id = req.site_id 
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebgetcompanyregionalsettingdtlOutput()
                        {
                            Company_Id = r.Company_id == null ? 0 : r.Company_id,
                            Time_zone = r.Time_zone,
                            Home_country_code = r.Home_country_code,
                            Greetings_language = r.Greetings_language == null ? 0 : r.Greetings_language,
                            User_language = r.User_language == null ? 0 : r.User_language,
                            Regional_format = r.Regional_format,
                            Time_format = r.Time_format == null ? 0 : r.Time_format, 
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmyaccwebgetcompanyregionalsettingdtlOutput outputobj = new UrmyaccwebgetcompanyregionalsettingdtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebgetcompanyregionalsettingdtlOutput outputobj = new UrmyaccwebgetcompanyregionalsettingdtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
