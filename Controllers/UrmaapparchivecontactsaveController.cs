using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaapparchivecontactsaveController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaapparchivecontactsaveInput
        {
			public int dir_user_id { get; set; }
			public int company_id { get; set; }
			public string mobileno { get; set; }
			public int status { get; set; }  	                   	    
        }
        public class UrmaapparchivecontactsaveOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaapparchivecontactsaveInput req)
        {
            List<UrmaapparchivecontactsaveOutput> OutputList = new List<UrmaapparchivecontactsaveOutput>();
			Log.Info("Input : UrmaapparchivecontactsaveController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_app_archive_contact_save";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@dir_user_id = req.dir_user_id,
								@company_id = req.company_id,
								@mobileno = req.mobileno,
								@status = req.status 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmaapparchivecontactsaveOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmaapparchivecontactsaveOutput outputobj = new UrmaapparchivecontactsaveOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmaapparchivecontactsaveOutput outputobj = new UrmaapparchivecontactsaveOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
