using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrshippingcarrierinsertController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrshippingcarrierinsertInput
        {
			public string shipp_name { get; set; }
			public float shipp_weight { get; set; }
			public string shipp_region { get; set; }
			public float cost { get; set; }
			public int type { get; set; }
			public int sc_id { get; set; }  	                   	    
        }
        public class UrshippingcarrierinsertOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrshippingcarrierinsertInput req)
        {
            List<UrshippingcarrierinsertOutput> OutputList = new List<UrshippingcarrierinsertOutput>();
			Log.Info("Input : UrshippingcarrierinsertController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_shipping_carrier_insert";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@shipp_name = req.shipp_name,
								@shipp_weight = req.shipp_weight,
								@shipp_region = req.shipp_region,
								@cost = req.cost,
								@type = req.type,
								@sc_id = req.sc_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrshippingcarrierinsertOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrshippingcarrierinsertOutput outputobj = new UrshippingcarrierinsertOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrshippingcarrierinsertOutput outputobj = new UrshippingcarrierinsertOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
