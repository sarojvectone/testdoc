using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountcallqueuedosettingsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcallqueuedosettingsInput
        {
			public int company_id { get; set; }
			public int Queue_id { get; set; }
			public int q_max_wait_time_next_call { get; set; }
			public int q_max_callers { get; set; }
			public int q_max_hold_time { get; set; }
			public string q_hold_time_sendcallers_to { get; set; }
			public string q_reach_max_callers_send_to { get; set; }
			public string q_reach_max_callers_greeting_type { get; set; }
			public string q_audio_file_name { get; set; }
			public string q_audio_content { get; set; }
			public int q_last_callend_agent_availablity { get; set; }  	                   	    
        }
        public class UrmyaccountcallqueuedosettingsOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcallqueuedosettingsInput req)
        {
            List<UrmyaccountcallqueuedosettingsOutput> OutputList = new List<UrmyaccountcallqueuedosettingsOutput>();
			Log.Info("Input : UrmyaccountcallqueuedosettingsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_call_queue_do_settings";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@Queue_id = req.Queue_id,
								@q_max_wait_time_next_call = req.q_max_wait_time_next_call,
								@q_max_callers = req.q_max_callers,
								@q_max_hold_time = req.q_max_hold_time,
								@q_hold_time_sendcallers_to = req.q_hold_time_sendcallers_to,
								@q_reach_max_callers_send_to = req.q_reach_max_callers_send_to,
								@q_reach_max_callers_greeting_type = req.q_reach_max_callers_greeting_type,
								@q_audio_file_name = req.q_audio_file_name,
								@q_audio_content = req.q_audio_content,
								@q_last_callend_agent_availablity = req.q_last_callend_agent_availablity 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcallqueuedosettingsOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcallqueuedosettingsOutput outputobj = new UrmyaccountcallqueuedosettingsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcallqueuedosettingsOutput outputobj = new UrmyaccountcallqueuedosettingsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
