using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappupdatenotificationController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappupdatenotificationInput
        {
            //public int app_log_id { get; set; }
            //public int company_id { get; set; }
            //public int notification_type { get; set; }
            //public int type { get; set; }           	    
            public int app_log_id { get; set; }
            public int company_id { get; set; }
            public int receive_type { get; set; }
            public int receive_notif_type { get; set; }
            public int sound_type { get; set; }
            public int sound_notif_type { get; set; }
            public int join_now_type { get; set; }
            public int join_now_notif_type { get; set; }
            public int team_type { get; set; }
            public int team_notif_type { get; set; }
            public int direct_message_type { get; set; }
            public int mention_type { get; set; }
            public int call_voice_text_type { get; set; }
            public int email_direct_msg { get; set; }
            public int email_direct_notif_msg { get; set; }
            public int email_team_msg { get; set; }
            public int email_team_notif_msg { get; set; }
            public int email_mention_type { get; set; }
            public int email_daily_digest_type { get; set; }
            public int email_unread_badge_count_type { get; set; }
            public int email_unread_badge_count_notif_type { get; set; }
        }
        public class UrappupdatenotificationOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappupdatenotificationInput req)
        {
            List<UrappupdatenotificationOutput> OutputList = new List<UrappupdatenotificationOutput>();
			Log.Info("Input : UrappupdatenotificationController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_update_notification";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                //@app_log_id = req.app_log_id,
                                //@company_id = req.company_id,
                                //@notification_type = req.notification_type,
                                //@type = req.type                                   
                                @app_log_id = req.app_log_id,
                                @company_id = req.company_id,
                                @receive_type = req.receive_type,
                                @receive_notif_type = req.receive_notif_type,
                                @sound_type = req.sound_type,
                                @sound_notif_type = req.sound_notif_type,
                                @join_now_type = req.join_now_type,
                                @join_now_notif_type = req.join_now_notif_type,
                                @team_type = req.team_type,
                                @team_notif_type = req.team_notif_type,
                                @direct_message_type = req.direct_message_type,
                                @mention_type = req.mention_type,
                                @call_voice_text_type = req.call_voice_text_type,
                                @email_direct_msg = req.email_direct_msg,
                                @email_direct_notif_msg = req.email_direct_notif_msg,
                                @email_team_msg = req.email_team_msg,
                                @email_team_notif_msg = req.email_team_notif_msg,
                                @email_mention_type = req.email_mention_type,
                                @email_daily_digest_type = req.email_daily_digest_type,
                                @email_unread_badge_count_type = req.email_unread_badge_count_type,
                                @email_unread_badge_count_notif_type = req.email_unread_badge_count_notif_type 
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrappupdatenotificationOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrappupdatenotificationOutput outputobj = new UrappupdatenotificationOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrappupdatenotificationOutput outputobj = new UrappupdatenotificationOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
