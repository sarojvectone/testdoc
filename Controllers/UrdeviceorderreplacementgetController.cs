using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdeviceorderreplacementgetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdeviceorderreplacementgetInput
        {
			public int company_id { get; set; }  	                   	    
        }
        public class UrdeviceorderreplacementgetOutput
        {
			public int order_id { get; set; }
			public string prod_code { get; set; }
			public string prod_name { get; set; }
			public string prod_model { get; set; }
			public DateTime? rpl_date { get; set; }
			public int rpl_quantity { get; set; }
			public string rpl_reason { get; set; }
			public int rpl_status { get; set; }
            public int id { get; set; }
            public string tracking_no { get; set; }
            public string comments { get; set; }
            public int company_id { get; set; }
            public string company_name { get; set; }
            public DateTime? delivery_date { get; set; }
            public string delivery_comment { get; set; }
            public DateTime? order_date { get; set; }
            public DateTime? last_update { get; set; }
            public string referenceid { get; set; }
            public string status_desc { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdeviceorderreplacementgetInput req)
        {
            List<UrdeviceorderreplacementgetOutput> OutputList = new List<UrdeviceorderreplacementgetOutput>();
			Log.Info("Input : UrdeviceorderreplacementgetController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_order_replacement_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrdeviceorderreplacementgetOutput()
                        {
							order_id = r.order_id == null ? 0 : r.order_id,
							prod_code = r.prod_code,
							prod_name = r.prod_name,
							prod_model = r.prod_model,
							rpl_date = r.rpl_date == null ? null : r.rpl_date,
							rpl_quantity = r.rpl_quantity == null ? 0 : r.rpl_quantity,
							rpl_reason = r.rpl_reason,
							rpl_status = r.rpl_status == null ? 0 : r.rpl_status,
                            id = r.id == null ? 0 : r.id,
                            tracking_no = r.tracking_no,
                            comments = r.comments,
                            company_name = r.company_name,
                            company_id = r.company_id == null ? 0 : r.company_id,
                            delivery_date = r.delivery_date,
                            order_date = r.order_date == null ? null : r.order_date,
                            last_update = r.last_update == null ? null : r.last_update,
                            delivery_comment = r.delivery_comment,
                            referenceid = r.referenceid,
                            status_desc = r.status_desc,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrdeviceorderreplacementgetOutput outputobj = new UrdeviceorderreplacementgetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrdeviceorderreplacementgetOutput outputobj = new UrdeviceorderreplacementgetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
