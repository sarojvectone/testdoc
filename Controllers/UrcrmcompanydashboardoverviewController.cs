using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrcrmcompanydashboardoverviewController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrcrmcompanydashboardoverviewInput
        {
        }
        public class UrcrmcompanydashboardoverviewOutput
        {
			public int tot_company { get; set; }
			public int tot_ticket { get; set; }
			public double tot_payment_collect { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Get(string id, HttpRequestMessage request, UrcrmcompanydashboardoverviewInput req)
        {
            List<UrcrmcompanydashboardoverviewOutput> OutputList = new List<UrcrmcompanydashboardoverviewOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_crm_company_dashboard_overview";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrcrmcompanydashboardoverviewOutput()
                        {
							tot_company = r.tot_company == null ? 0 : r.tot_company,
							tot_ticket = r.tot_ticket == null ? 0 : r.tot_ticket,
							tot_payment_collect = r.tot_payment_collect == null ? 0.0F : r.tot_payment_collect,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrcrmcompanydashboardoverviewOutput outputobj = new UrcrmcompanydashboardoverviewOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrcrmcompanydashboardoverviewOutput outputobj = new UrcrmcompanydashboardoverviewOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
