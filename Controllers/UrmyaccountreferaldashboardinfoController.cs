using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountreferaldashboardinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountreferaldashboardinfoInput
        {
			public int company_id { get; set; }  	                   	    
        }
        public class UrmyaccountreferaldashboardinfoOutput
        {
			public int? Total_Referal { get; set; }
			public int? pending_Referal { get; set; }
			public double? Total_earned { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountreferaldashboardinfoInput req)
        {
            List<UrmyaccountreferaldashboardinfoOutput> OutputList = new List<UrmyaccountreferaldashboardinfoOutput>();
			Log.Info("Input : UrmyaccountreferaldashboardinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_referal_dashboard_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountreferaldashboardinfoOutput()
                        {
							Total_Referal = r.Total_Referal == null ? 0 : r.Total_Referal,
                            pending_Referal = r.pending_Referal == null ? 0 : r.pending_Referal,
							Total_earned = r.Total_earned == null ? 0.0D : r.Total_earned,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountreferaldashboardinfoOutput outputobj = new UrmyaccountreferaldashboardinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountreferaldashboardinfoOutput outputobj = new UrmyaccountreferaldashboardinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
