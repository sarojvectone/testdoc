using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrcompanycallingratestatusupdateController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrcompanycallingratestatusupdateInput
        {
			public int company_id { get; set; }
			public int rate_id { get; set; }
			public int status { get; set; }  	                   	    
        }
        public class UrcompanycallingratestatusupdateOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrcompanycallingratestatusupdateInput req)
        {
            List<UrcompanycallingratestatusupdateOutput> OutputList = new List<UrcompanycallingratestatusupdateOutput>();
			Log.Info("Input : UrcompanycallingratestatusupdateController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_company_calling_rate_status_update";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@rate_id = req.rate_id,
								@status = req.status 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcompanycallingratestatusupdateOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrcompanycallingratestatusupdateOutput outputobj = new UrcompanycallingratestatusupdateOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrcompanycallingratestatusupdateOutput outputobj = new UrcompanycallingratestatusupdateOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
