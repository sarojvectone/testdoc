﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using PushSharp.Core;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class UrappleaveteamController:ApiController
    {
        public class UrappleaveteamInput
        {
            public int company_id { get; set; }
            public int SIPID { get; set; }
            //public int extension { get; set; }
            public string team { get; set; }
        }

        public class UrappleaveteamOutput 
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrappleaveteamInput req)
        {
            List<UrappleaveteamOutput> OutputList = new List<UrappleaveteamOutput>();
            Log.Info("Input : UrappleaveteamController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_leave_team";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id,
                                @SIPID  = req.SIPID,
                               // @extension = req.extension,
                                @team = req.team
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        Log.Info("UrappleaveteamController Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrappleaveteamOutput()
                        {
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        Log.Info(" UrappleaveteamController Output DB : " + "Empty result");
                        UrappleaveteamOutput outputobj = new UrappleaveteamOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                UrappleaveteamOutput outputobj = new UrappleaveteamOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}