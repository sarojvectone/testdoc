using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmultisitecreatemyaccountController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmultisitecreatemyaccountInput
        {
            //public int company_id { get; set; }
            //public string site_name { get; set; }
            //public int site_extn { get; set; }
            //public string site_address { get; set; }
            //public string Time_zone { get; set; }
            //public string Home_country_code { get; set; }
            //public int Greetings_language { get; set; }
            //public int User_language { get; set; }
            //public string Regional_format { get; set; }
            //public int Time_format { get; set; }
            //public string Working_Business_Type { get; set; }
            //public string Working_Hours { get; set; }
            //public int Type { get; set; }
            //public string did_number { get; set; }
            //public string IVR_type { get; set; }
            //public int Hour_type { get; set; }
            //public int Greeting_by { get; set; }
            //public int Greeting_type { get; set; }
            //public int Language { get; set; }
            //public int Audio_type { get; set; }
            //public int Music_type { get; set; }
            //public int Call_screening_type { get; set; }
            //public string Audio_file { get; set; }  	                   	    
            public int company_id { get; set; }
            public string site_name { get; set; }
            public int site_extn { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string address3 { get; set; }
            public string postcode { get; set; }
            public string city { get; set; }
            public string country { get; set; } 
        }
        public class UrmultisitecreatemyaccountOutput
        {
			public int? site_id { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmultisitecreatemyaccountInput req)
        {
            List<UrmultisitecreatemyaccountOutput> OutputList = new List<UrmultisitecreatemyaccountOutput>();
			Log.Info("Input : UrmultisitecreatemyaccountController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Multisite_create_myaccount";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                //@company_id = req.company_id,
                                //@site_name = req.site_name,
                                //@site_extn = req.site_extn,
                                //@site_address = req.site_address,
                                //@Time_zone = req.Time_zone,
                                //@Home_country_code = req.Home_country_code,
                                //@Greetings_language = req.Greetings_language,
                                //@User_language = req.User_language,
                                //@Regional_format = req.Regional_format,
                                //@Time_format = req.Time_format,
                                //@Working_Business_Type = req.Working_Business_Type,
                                //@Working_Hours = req.Working_Hours,
                                //@Type = req.Type,
                                //@did_number = req.did_number,
                                //@IVR_type = req.IVR_type,
                                //@Hour_type = req.Hour_type,
                                //@Greeting_by = req.Greeting_by,
                                //@Greeting_type = req.Greeting_type,
                                //@Language = req.Language,
                                //@Audio_type = req.Audio_type,
                                //@Music_type = req.Music_type,
                                //@Call_screening_type = req.Call_screening_type,
                                //@Audio_file = req.Audio_file 	
                                @company_id = req.company_id,
                                @site_name = req.site_name,
                                @site_extn = req.site_extn,
                                @address1 = req.address1,
                                @address2 = req.address2,
                                @address3 = req.address3,
                                @postcode = req.postcode,
                                @city = req.city,
                                @country = req.country                                
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmultisitecreatemyaccountOutput()
                        {
							site_id = r.site_id == null ? 0 : r.site_id,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmultisitecreatemyaccountOutput outputobj = new UrmultisitecreatemyaccountOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmultisitecreatemyaccountOutput outputobj = new UrmultisitecreatemyaccountOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
