using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class UrdisplaypictureController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdisplaypictureInput
        {
            public string company_Id { get; set; }
            public string extn { get; set; }
            public string photo_info { get; set; }
            public string Delete_image { get; set; }
            public string get_image { get; set; }
            public int domain_id{ get; set; }
        }
        public class UrdisplaypictureOutput : Output
        {
            public string ImageName { get; set; }
            public string ImageURL { get; set; }
        }
        public class UrmaextnDeviceTokenOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
            public int DeviceType { get; set; }
            public string message_token { get; set; }
            public string voip_token { get; set; }
        }
        public class Output
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdisplaypictureInput req)
        {
            List<UrdisplaypictureOutput> OutputList = new List<UrdisplaypictureOutput>();
            List<UrmaextnDeviceTokenOutput> OutputDeviceTokenList = new List<UrmaextnDeviceTokenOutput>();
            try
            {
                Log.Info("UrdisplaypictureController");
                //Log.Info("Input : {0}", Newtonsoft.Json.JsonConvert.SerializeObject(req));
                string path = "";
                String Datetime = DateTime.Now.ToString("yyyyMMddHHmmss");

                string oldimg = req.company_Id + "_" + req.extn + "_*";
                string img = req.company_Id + "_" + req.extn + "_" + Datetime;

                if (!String.IsNullOrEmpty(req.extn))
                {
                    if (!String.IsNullOrEmpty(req.photo_info) && req.Delete_image == "false")
                    {
                        path = LoadImageHelper.ReplaceImage(oldimg, img, req.photo_info, "Profile-Picture");
                        path = path + ".jpg";
                        //path = LoadImageHelper.LoadImage(req.photo_info, req.account_id + "_" + Datetime,"ProfilePic");
                    }
                    else if (req.Delete_image == "true")
                    {
                        path = LoadImageHelper.DeleteImage(oldimg, "Profile-Picture");
                    }
                    else if (req.get_image == "true")
                    {
                        path = LoadImageHelper.FindImage(oldimg + "*", "Profile-Picture");
                       // path = String.IsNullOrEmpty(path) ? "default_trainer.jpg" : path;
                        Log.Info("filepath : {0}, fileName : {1}", path, oldimg);
                    }
                }
                else
                {
                    UrdisplaypictureOutput outputobj = new UrdisplaypictureOutput();
                    outputobj.errcode = -1;
                    outputobj.errmsg = "Extention ID null/Mismatch";
                    OutputList.Add(outputobj);
                }
                //Log.Info("Image_path : {0}", path);

                #region Commented DB
                if (req != null && !string.IsNullOrEmpty(req.photo_info))
                {

                            OutputList = new List<UrdisplaypictureOutput>();
                            UrdisplaypictureOutput result1 = new UrdisplaypictureOutput();
                            result1.ImageName = img;
                            result1.ImageURL = ConfigurationManager.AppSettings["ProfilePic"] + "/"+img.Replace("\\", "/")+".jpg";
                            result1.errcode =  0;
                            result1.errmsg = "Success";

                            //Save to DB
                            //Company ID:
                            //extn:
                            //ImageURL:result1.ImageURL
                            //isdelete

                            List<UrdisplaypictureOutput> OutputListSaveDB = SaveToDB(req, result1.ImageURL,1);
                            Log.Debug("OutputListSaveDB : {0}", JsonConvert.SerializeObject(OutputListSaveDB));

                            OutputList.Add(result1);
                            //OutputList.AddRange(result.Select(r => new UrdisplaypictureOutput()
                            //{
                            //    ImageName = req.account_id + "_" + Datetime,
                            //    ImageURL = ConfigurationManager.AppSettings["ProfilePic"] + req.account_id + "_" + Datetime + ".jpg",
                            //    errcode = r.errcode == null ? 0 : r.errcode,
                            //    errmsg = r.errmsg == null ? "Success" : r.errmsg
                            //})); 
                }
                else if (string.IsNullOrEmpty(req.photo_info) && req.Delete_image == "true")
                {
                    UrdisplaypictureOutput outputobj = new UrdisplaypictureOutput();
                    outputobj.errcode = 0;
                    outputobj.errmsg = "Image Deleted Successfully";

                    //Save to DB
                    //Company ID:
                    //extn:
                    //ImageURL:result1.ImageURL
                    //isdelete
                    List<UrdisplaypictureOutput> OutputListDelete = SaveToDB(req, "", 2);
                    
                    Log.Debug("OutputListDelete : {0}", JsonConvert.SerializeObject(OutputListDelete));
                    OutputList.Add(outputobj);
                }
                else if (req.get_image == "true")
                {
                    UrdisplaypictureOutput result1 = new UrdisplaypictureOutput();
                    result1.ImageName = img;
                    if (!string.IsNullOrEmpty(path))
                    {
                        //if (path.ToLower().Contains("wwwroot"))
                        //{
                        //    result1.ImageURL = ConfigurationManager.AppSettings["ProfilePicLocal"] + "/" + path.Replace("\\", "/");
                        //}
                        //else
                        //{
                            result1.ImageURL = ConfigurationManager.AppSettings["ProfilePic"] + "/" + path.Replace("\\", "/");
                        //}
                        result1.errcode = 0;
                        result1.errmsg = "Success";
                    }
                    else
                    {
                        result1.ImageURL = "";
                        result1.errcode = -1;
                        //result1.errmsg = "Failed"; // 11/06/2019 -- Error MESSAGE
                        result1.errmsg = "Please update/upload you profile picture";
                    }
                    OutputList.Add(result1);
                }
                //if (!string.IsNullOrEmpty(req.extn) && req.domain_id != 0 && OutputList[0].errcode != -1)
                //{
                //    Log.Info("Output ur_getdevicetoken_info SP: " + JsonConvert.SerializeObject(OutputList));
                //    using (var conn1 = new SqlConnection(ConfigurationManager.ConnectionStrings["apns_provider"].ConnectionString))
                //    {
                //        conn1.Open();
                //        try
                //        {
                //            #region GetDeviceToken
                //            var spDeviceToken = "ur_getdevicetoken_info";
                //            var resultDeviceToken = conn1.Query<dynamic>(
                //                    spDeviceToken, new
                //                    {
                //                        @domain_id = req.domain_id,
                //                        @extension = req.extn
                //                    },
                //                    commandType: CommandType.StoredProcedure);
                //            if (resultDeviceToken != null && resultDeviceToken.Count() > 0)
                //            {
                //                OutputDeviceTokenList.AddRange(resultDeviceToken.Select(result1 => new UrmaextnDeviceTokenOutput()
                //                {
                //                    errcode = 0,
                //                    errmsg = "Success",
                //                    DeviceType = result1.DeviceType == null ? 0 : result1.DeviceType,
                //                    voip_token = result1.voip_token,
                //                    message_token = result1.message_token
                //                }));
                //                Log.Info("Output ur_getdevicetoken_info SP: " + JsonConvert.SerializeObject(resultDeviceToken));
                //            }
                //            #endregion
                //        }
                //        catch (Exception ex)
                //        {
                //            Log.Info("Output ur_getdevicetoken_info error: " + ex.Message.ToString());

                //        }
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                UrdisplaypictureOutput outputobj = new UrdisplaypictureOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }

        private static List<UrdisplaypictureOutput> SaveToDB(UrdisplaypictureInput req,string imageURL,int type)
        {
            List<UrdisplaypictureOutput> OutputList = new List<UrdisplaypictureOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_update_profile_image_ulr";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_Id,
                                @extension = req.extn,
                                @profile_image_url = imageURL,
                                @type = type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrdisplaypictureOutput()
                        {
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg

                        }));
                    }
                    else
                    {
                        UrdisplaypictureOutput outputobj = new UrdisplaypictureOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrdisplaypictureOutput outputobj = new UrdisplaypictureOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return OutputList;
        }
    }
}
