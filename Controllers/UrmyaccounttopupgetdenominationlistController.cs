using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccounttopupgetdenominationlistController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccounttopupgetdenominationlistInput
        {
			public int? company_id { get; set; }  	                   	    
        }
        public class UrmyaccounttopupgetdenominationlistOutput
        {
			public int? product_id { get; set; }
			public double? Topup_amount { get; set; }
			public string Topup_desc { get; set; }
			public double? extra_percentage { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccounttopupgetdenominationlistInput req)
        {
            List<UrmyaccounttopupgetdenominationlistOutput> OutputList = new List<UrmyaccounttopupgetdenominationlistOutput>();
			Log.Info("Input : UrmyaccounttopupgetdenominationlistController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_topup_get_denomination_list";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccounttopupgetdenominationlistOutput()
                        {
							product_id = r.product_id == null ? 0 : r.product_id,
							Topup_amount = r.Topup_amount == null ? 0.0D : r.Topup_amount,
							Topup_desc = r.Topup_desc,
							extra_percentage = r.extra_percentage == null ? 0.0D : r.extra_percentage,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccounttopupgetdenominationlistOutput outputobj = new UrmyaccounttopupgetdenominationlistOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccounttopupgetdenominationlistOutput outputobj = new UrmyaccounttopupgetdenominationlistOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
