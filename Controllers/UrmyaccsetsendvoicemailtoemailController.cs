﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccsetsendvoicemailtoemailController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccsetsendvoicemailtoemailInput
        {
            public int company_id { get; set; }
            public string Extension { get; set; }
            public int Status { get; set; }
            public int processtype { get; set; }
            public int id { get; set; }
        }
        public class UrmyaccsetsendvoicemailtoemailOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccsetsendvoicemailtoemailInput req)
        {
            List<UrmyaccsetsendvoicemailtoemailOutput> OutputList = new List<UrmyaccsetsendvoicemailtoemailOutput>();
            Log.Info("Input : UrmyaccsetsendvoicemailtoemailController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myacc_set_send_voicemail_to_email";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @company_id = req.company_id,
                                @Extension = req.Extension,
                                @Status = req.Status,
                                @processtype = req.processtype,
                                @id = req.id
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccsetsendvoicemailtoemailOutput()
                        {
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        Log.Info("Output DB : " + "Empty result");
                        UrmyaccsetsendvoicemailtoemailOutput outputobj = new UrmyaccsetsendvoicemailtoemailOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                UrmyaccsetsendvoicemailtoemailOutput outputobj = new UrmyaccsetsendvoicemailtoemailOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}