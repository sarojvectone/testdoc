using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappgetcallsringtypeController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappgetcallsringtypeInput
        {
        }
        public class UrappgetcallsringtypeOutput
        {
			public int? ICR { get; set; }
			public string ICR_Name { get; set; }
			public int? ring_count { get; set; }
			public int? sec_count { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Get(string id, HttpRequestMessage request, UrappgetcallsringtypeInput req)
        {
            List<UrappgetcallsringtypeOutput> OutputList = new List<UrappgetcallsringtypeOutput>();
			Log.Info("Input : UrappgetcallsringtypeController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_app_get_calls_ring_type";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrappgetcallsringtypeOutput()
                        {
							ICR = r.ICR == null ? 0 : r.ICR,
							ICR_Name = r.ICR_Name,
							ring_count = r.ring_count == null ? 0 : r.ring_count,
							sec_count = r.sec_count == null ? 0 : r.sec_count,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrappgetcallsringtypeOutput outputobj = new UrappgetcallsringtypeOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrappgetcallsringtypeOutput outputobj = new UrappgetcallsringtypeOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
