﻿#region Assemblies
using Dapper;
using unifiedringmyaccountwebapi.Helpers;
using Newtonsoft.Json;
using NLog;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
#endregion

#region NameSpace

namespace unifiedringmyaccountwebapi.Controllers
{
    #region Class
    public class UrappRxSavedCardPaymentController : ApiController
    {
        #region Declarations
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        #endregion

        public class Output
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
            public string ReferenceID { get; set; }
        }

        public class RxSavedCardPaymentInput
        {
            public string account_id { get; set; }
            public string mobileno { get; set; }
            public string Last4digitsCC { get; set; }
            public string expirydate { get; set; }
            public string card_type { get; set; }
            public decimal amount { get; set; }
            public string currency { get; set; }
            public int productid { get; set; }
            public string calledby { get; set; }
            //13-Jul-2018 : added for cvn(CVV)
            public string cvn { get; set; }
            public string appcode { get; set; }
         
            public string sitecode { get; set; }
            public string productcode { get; set; }
            public string  paymentagent { get; set; }
            public string paymentmode { get; set; }
            public string servicetype { get; set; }
            public string country { get; set; }

        }

        public class RxSavedCardPaymentOutput2 : Output
        {
            public string payment_agent { get; set; }
            
            public string topup_country { get; set; }
          
            public string service_type { get; set; }
            public string payment_mode { get; set; }
            
            public string subscribtion_id { get; set; }
        }

        public class RxSavedCardPaymentOutput : Output
        {
            public string sms_sender { get; set; }
            public string sms_text { get; set; }
            public string sms_url { get; set; }
        }

        #region PaymentRestrictTopup
        public static void PaymentRestrictTopup(string accountId, string paymentagent, string Currency, decimal amount, string appcode)
        {
            Log.Info("PaymentRestrictTopup : {0} , {1} , {2} , {3} ", accountId, paymentagent, Currency, amount);
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Payment"].ConnectionString))
            {
                conn.Open();
                var sp = "payment_restrict_topup";
                var result = conn.Query<dynamic>(
                        sp, new
                        {
                            @account_id = accountId,
                            @payment_agent = paymentagent,
                            @currency = Currency,
                            @topup_amount = amount,
                            @appcode = appcode
                        },
                        commandType: CommandType.StoredProcedure);
                Log.Debug("PaymentRestrictTopup : Output : " + Newtonsoft.Json.JsonConvert.SerializeObject(result));
                if (result != null && result.Count() > 0)
                {
                    if (result.ElementAt(0).errcode == -1)
                        throw new Exception(result.ElementAt(0).errmsg);
                }
            }
        }
        #endregion

        #region Post
          [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, RxSavedCardPaymentInput req)
        {
         
            Log.Info("RxSavedCardPayment : Input: " + JsonConvert.SerializeObject(req));

            //17-Jan-2019 : Moorthy : Added for SMS A2P Automation request
            //SMSInsertRequestLogInput smsinsertrequestlogInput = new SMSInsertRequestLogInput();
            //smsinsertrequestlogInput.logid = 0;
           
            var output = new Output();
            try
            {
                //17-Jan-2019 : Moorthy : Added for SMS A2P Automation request
                //smsinsertrequestlogInput.mobileno = req.mobileno;
                //smsinsertrequestlogInput.sms_url = "";
                //smsinsertrequestlogInput.sms_id = "-1:-1";
                //smsinsertrequestlogInput.sms_sender = "";
                //smsinsertrequestlogInput.request_type = 1;
                //smsinsertrequestlogInput.called_by = !String.IsNullOrEmpty(req.calledby) ? req.calledby : "GoCheap App";
                //smsinsertrequestlogInput.sitecode = "";
                //smsinsertrequestlogInput.logid = SMSHelper.SMSInsertRequestLog(smsinsertrequestlogInput);

                #region Fruad
                //if (req.account_id.Trim() != "1701098") //For Testing
               // PaymentRestrictTopup(req.account_id, "DC", req.currency, req.amount, req.appcode);
                #endregion

                if (req != null)
                {
                    RxSavedCardPaymentOutput2 output2 = new RxSavedCardPaymentOutput2();

                    output2 = Getpaymenttopupdetail(req.account_id, req.Last4digitsCC);

                    output2.payment_agent = req.paymentagent;
                    output2.payment_mode = req.paymentmode;
                    output2.service_type = req.servicetype;
                    output2.topup_country = req.country;
                     


                    Log.Info(" getpaymenttopupdetail : output2 : " + JsonConvert.SerializeObject(output2));

                    if (output2.errcode == 0 && !String.IsNullOrEmpty(output2.subscribtion_id))
                    {
                        //UFGBP-WEB-DSKTP-UR-20GBP-RX-5619

                        string paymentRef = getPaymentRef(req.appcode, req.sitecode, "SC-" + req.productcode + "-" + (int)req.amount);

                        Log.Info(" getpaymenttopupdetail : paymentRef : {0}", paymentRef);

                        #region Payment Gateway
                        Payment myPayment = new Payment();
                        myPayment = Payment.Receiptin(req.sitecode, req.appcode, paymentRef,
                                    req.productcode, Convert.ToString(output2.subscribtion_id).Trim(),
                                    Convert.ToString(req.amount).Trim(), req.account_id, output2.payment_mode, output2.service_type, req.currency,
                                   "", output2.topup_country, req.cvn);
                        Log.Info(" getpaymenttopupdetail : Payment Gateway Responce : " + JsonConvert.SerializeObject(myPayment));
                        #endregion
                        string result;
                        if (myPayment.Decision == "APPROVAL" && (myPayment.ReasonCode == "0" || myPayment.ReasonCode == "100"))
                        {
                               result = rxpaym_PaymentInsert_v1(
                                 myPayment.MerchantReferenceCode,
                                 req.sitecode,
                                 req.productcode,
                                 output2.payment_agent,
                                 output2.service_type,
                                 output2.payment_mode,
                                 3,
                                 3,
                                 req.account_id,
                                 req.Last4digitsCC,
                                 Convert.ToString(req.amount).Trim(),
                                 req.currency,
                                 //myPayment.SubscriptionId,
                                 Convert.ToString(output2.subscribtion_id).Trim(),
                                 req.productcode,
                                 "CS",
                                 "0",
                                 "0",
                                 "",
                                 "",
                                 "Transaction Succeeded.",
                                //24-Aug-2019 : Moorthy modified for storing SRD value
                                 myPayment.SRD);

                            //RxSavedCardPaymentOutput output3 = tpdotopupresbyccdc(req, paymentRef);

                         //   Log.Info(" RxSavedCardPayment : output3 : " + JsonConvert.SerializeObject(output3));

                          //  if (output3.errcode == 0 && !String.IsNullOrEmpty(output3.sms_url))
                           // {
                                //17-Jan-2019 : Moorthy : Added for SMS A2P Automation request
                                //smsinsertrequestlogInput.sms_url = output3.sms_url;
                                //smsinsertrequestlogInput.sms_sender = output3.sms_sender;
                                //smsinsertrequestlogInput.sms_text = output3.sms_text;

                                //string response = string.Empty;
                                //string SMSStatus = SMSHelper. SMSSend(output3.sms_url, req.account_id, output3.sms_text, output3.sms_sender, ref response);
                                //Log.Info(" RxSavedCardPayment : SMSStatus : " + SMSStatus);

                                ////17-Jan-2019 : Moorthy : Added for SMS A2P Automation request
                                //smsinsertrequestlogInput.sms_id = SMSStatus;

                           // }
                               if (!string.IsNullOrEmpty(result))
                               {
                                   output.errcode = 0;
                                   output.errmsg = "Success";
                                   output.ReferenceID = result;
                               }
                               else
                               {
                                   output.errcode = -1;
                                   output.errmsg = "Failed";
                                   output.ReferenceID = result;

                               }
                        }
                        else
                        {
                           result =  rxpaym_PaymentInsert_v1(
                                 myPayment.MerchantReferenceCode,
                                 req.sitecode,
                                 req.productcode,
                                 output2.payment_agent,
                                 output2.service_type,
                                 output2.payment_mode,
                                 3,
                                 3,
                                 req.account_id,
                                 req.Last4digitsCC,
                                 Convert.ToString(req.amount).Trim(),
                                 req.currency,
                                Convert.ToString(output2.subscribtion_id).Trim(),
                                 req.productcode,
                                 "CS",
                                 myPayment.ReasonCode, 
                                 myPayment.ReasonCode,
                                 "",
                                 "",
                                 String.IsNullOrEmpty(myPayment.Decision) ? "Transaction Failed." : myPayment.Decision,
                                //24-Aug-2019 : Moorthy modified for storing SRD value
                                 myPayment.SRD);
                            output = new Output();
                            output.errcode = -1;
                            output.errmsg = ConfigurationManager.AppSettings["ERRORMSG"]; //myPayment.Decision ?? "Payment Failed";
                        }
                    }
                    else
                    {
                        output = new Output();
                        output.errcode = -1;
                        output.errmsg = "Subscription details not found";
                    }
                    //}
                }
                else
                {
                    output = new Output();
                    output.errcode = -1;
                    output.errmsg = "input details not found";
                }
            }
            catch (Exception ex)
            {
                Log.Error(" RxSavedCardPayment : Error: " + ex.Message);
                output = new Output();
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            finally
            {
                ////17-Jan-2019 : Moorthy : Added for SMS A2P Automation request
                //if (smsinsertrequestlogInput.logid > 0)
                //{
                //    smsinsertrequestlogInput.request_type = 2;
                //    SMSHelper.SMSInsertRequestLog(smsinsertrequestlogInput);
                //}
            }
            Log.Info("RxSavedCardPayment : Output: " + JsonConvert.SerializeObject(output));
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
        #endregion

        //24-Aug-2019 : Moorthy modified for storing SRD value
        public string rxpaym_PaymentInsert_v1(string reference_id, string sitecode, string productcode, string paymentagent, string servicetype, string paymentmode,
             int paymentstep, int paymentstatus, string accountid, string last6digitccno, string totalamount, string currency, string subscriptionid, string merchantid,
             string providercode, string csreasoncode, string generalerrorcode, string email, string IPpaymentagent, string ReplyMessage, string SRD)
        {
            string paymentRef = reference_id;
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Payment"].ConnectionString))
                {
                    var sp = "Ur_savedcard_insert_payment";

                    conn.Open();

                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @reference_id = reference_id,
                         @sitecode = sitecode,
                         @productcode = productcode,
                         @paymentagent = paymentagent,
                         @servicetype = servicetype,
                         @paymentmode = paymentmode,
                         @paymentstep = paymentstep,
                         @paymentstatus = paymentstatus,
                         @accountid = accountid,
                         @last6digitccno = last6digitccno,
                         @totalamount = totalamount,
                         @currency = currency,
                         @subscriptionid = subscriptionid,
                         @merchantid = merchantid,
                         @providercode = providercode,
                         @csreasoncode = csreasoncode,
                         @generalerrorcode = generalerrorcode,
                         @email = email,
                         @IPpaymentagent = IPpaymentagent,
                         @ReplyMessage = ReplyMessage,
                         //24-Aug-2019 : Moorthy added for storing SRD value
                         @srd = SRD
                     },
                     commandType: CommandType.StoredProcedure).SingleOrDefault();
                    if (result != null)
                    {
                        paymentRef = result.ref_id;
                    }
                    else
                    {
                        paymentRef = null;
                    }
                }

                return paymentRef;
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public string getPaymentRef(string applicationCode, string siteCode, string productCode)
        {
            string paymentRef = "";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Payment"].ConnectionString))
                {
                    var sp = "Rxpaym_generate_payment_reference";

                    conn.Open();

                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @sitecode = siteCode,
                         @applicationcode = applicationCode,
                         @productcode = productCode
                     },
                     commandType: CommandType.StoredProcedure).SingleOrDefault();
                    if (result != null)
                    {
                        paymentRef = result.ref_id;
                    }
                    else
                    {
                        paymentRef = null;
                    }
                }

                return paymentRef;
            }
            catch (SqlException ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public RxSavedCardPaymentOutput2 Getpaymenttopupdetail(string account_id, string last6digitcard)
        {
            RxSavedCardPaymentOutput2 output = new RxSavedCardPaymentOutput2();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["payment"].ConnectionString))
                {
                    Log.Info(conn.ConnectionString.ToString());
                    var sp = "ur_saved_card_get_subscribtion_info";

                    conn.Open();

                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @account_id = account_id,
                         @cc_no = last6digitcard
                     },
                     commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        output.errcode = 0;
                        output.errmsg = "Success";
                       // output.payment_agent = result.ElementAt(0).payment_agent;
                      //  output.app_code = result.ElementAt(0).app_code;
                       // output.topup_country = result.ElementAt(0).topup_country;
                       // output.topup_sitecode = result.ElementAt(0).topup_sitecode;
                       // output.service_type = result.ElementAt(0).service_type;
                       // output.payment_mode = result.ElementAt(0).payment_mode;
                        //output.productcode = result.ElementAt(0).productcode;
                        output.subscribtion_id = result.ElementAt(0).SubscriptionID;
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = "No Rec found";
                    }
                }
            }
            catch (Exception ex)
            {
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return output;
        }

        public RxSavedCardPaymentOutput tpdotopupresbyccdc(RxSavedCardPaymentInput input, string paymentRef)
        {
            Log.Info("tpdotopupresbyccdc Input : " + JsonConvert.SerializeObject(input));
            var output = new RxSavedCardPaymentOutput();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Gocheapappapi"].ConnectionString))
                {
                    var sp = "Gocheap_ccdc_topup_process";

                    conn.Open();
                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @account_id = input.account_id,
                         @cli = input.mobileno,
                         @paymentid = 2,
                         @process_by = input.calledby,
                         @productid = input.productid,
                         @payment_ref = paymentRef,
                         @amount = input.amount,
                         @currency = input.currency,
                     },
                     commandType: CommandType.StoredProcedure).SingleOrDefault();
                    if (result != null)
                    {
                        output.errcode = result.errcode;
                        output.errmsg = result.errmsg;
                        output.sms_sender = result.sms_sender;
                        output.sms_text = result.sms_text;
                        output.sms_url = result.sms_url;
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = "no records found";
                    }
                }
            }
            catch (SqlException ex)
            {
                Log.Error(ex.Message);
                throw new Exception(ex.Message);
            }
            return output;
        }
    }
    #endregion
}
#endregion
