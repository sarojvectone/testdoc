using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace unifiedringappapi.Controllers
{ 
    public class SaveGoogleUserController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class SaveGoogleUserInput
        {
            public string login_user_name { get; set; }
            public string login_password { get; set; }
            public string login_source { get; set; }
            public string login_device_id { get; set; }
            public string login_ipaddress { get; set; }
        }
        public class SaveGoogleUserOutput
        {
            public int? error_code { get; set; }
            public string error_msg { get; set; }
            public int? dir_user_id { get; set; }
            public int? domain_id { get; set; }
            public int? ext { get; set; }
            public string cli { get; set; }
            public long? sip_login_id { get; set; }
            public string sip_password { get; set; }
            public string caller_id { get; set; }
            public int? company_id { get; set; }
            public int? enetepriseid { get; set; }
            public int? customer_id { get; set; }
            public string local_number { get; set; }
            public string country_code { get; set; }
            public int? app_log_id { get; set; }
            public int? role_id { get; set; }
      
        }
        [HttpGet]
        public async Task<HttpResponseMessage> SaveGoogleUser(string code, string state, string session_state)
        {
            List<SaveGoogleUserOutput> OutputList = new List<SaveGoogleUserOutput>();
            if (string.IsNullOrEmpty(code))
            {
                SaveGoogleUserOutput outputobj = new SaveGoogleUserOutput();
                outputobj.error_code = -1;
                outputobj.error_msg = "Error";
                OutputList.Add(outputobj);
            }

            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://www.googleapis.com")
            };
            string client_id = ConfigurationManager.AppSettings["Google.ClientID"];
            string redirect_uri = ConfigurationManager.AppSettings["Google.RedirectUrl"];
            string SecretKey = ConfigurationManager.AppSettings["Google.SecretKey"];
            var requestUrl = "oauth2/v4/token?code=" + code + "&client_id=" + client_id + "&client_secret=" + SecretKey + "&redirect_uri="+redirect_uri+"&grant_type=authorization_code";  

            var dict = new Dictionary<string, string>
            {
                { "Content-Type", "application/x-www-form-urlencoded" }
            };
            var req = new HttpRequestMessage(HttpMethod.Post, requestUrl) { Content = new FormUrlEncodedContent(dict) };
            var response = await httpClient.SendAsync(req);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                SaveGoogleUserOutput outputobj = new SaveGoogleUserOutput();
                outputobj.error_code = 0;
                outputobj.error_msg = "Success";
                OutputList.Add(outputobj);
            
            }
            else
            {
                SaveGoogleUserOutput outputobj = new SaveGoogleUserOutput();
                outputobj.error_code = -1;
                outputobj.error_msg = "Not valid";
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }

}
