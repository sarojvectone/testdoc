﻿using Newtonsoft.Json;
using PushSharp.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class TelservordernumberController:ApiController
    {

        public class TelservordernumberInput
        {

            public int stockDnisId  { get; set; }

        }
        public class resultTelserv : Products
        {
            public termination termination { get; set; }
            public pricing pricing { get; set; }
            public List<sessionPricing> sessionPricing { get; set; }
            public List<userTariffs> userTariffs { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        public class Products
        {
            public int id { get; set; }
            public string productName { get; set; }
            public string country { get; set; }
            public string numberType { get; set; }
            public string remarks { get; set; }
            public int availableInStock { get; set; }
            public string canBeBackOrdered { get; set; }
            public string backOrderLeadTime { get; set; }
            public string reachableFromNationalFixed { get; set; }
            public string reachableFromNationalFixedExceptions { get; set; }
            public string reachableFromNationalMobile { get; set; }
            public string reachableFromNationalMobileExceptions { get; set; }
            public string reachableFromPayPhone { get; set; }
            public string reachableFromPayPhoneExceptions { get; set; }
            public string reachableFromInternationalFixed { get; set; }
            public string reachableFromInternationalFixedExceptions { get; set; }
            public string reachableFromInternationalMobile { get; set; }
            public string reachableFromInternationalMobileExceptions { get; set; }
            public int defaultNumberOfChannels { get; set; }
            public string portingAvailable { get; set; }
            public string portingCost { get; set; }
            public string portingLeadTime { get; set; }
            public string cliDelivery { get; set; }
            public string restrictedCliDelivery { get; set; }
            public string conditions { get; set; }
            public string verificationRequired { get; set; }
            public string initialContractDuration { get; set; }
            public string initialNoticePeriod { get; set; }
            public string renewalContractDuration { get; set; }
            public string renewalNoticePeriod { get; set; }
        }
        public class sessionPricing
        {
            public string origin { get; set; }
            public string originDescription { get; set; }
            public float onPeakSetupPrice { get; set; }
            public float offPeakSetupPrice { get; set; }
            public float nssSetupPrice { get; set; }
            public float onPeakMinutePrice { get; set; }
            public float offPeakMinutePrice { get; set; }
            public float nssMinutePrice { get; set; }
            public float ivrSetupPrice { get; set; }
            public float ivrMinutePrice { get; set; }
            public float handlingSetupPrice { get; set; }
            public float handlingMinutePrice { get; set; }
        }
        public class pricing
        {
            public string currency { get; set; }
            public int? maxDurationCharged { get; set; }
            public int? minDurationBeforeCharge { get; set; }
            public int? unbilledSeconds { get; set; }
            public string billingIncrement { get; set; }
            public staticPricing staticPricing { get; set; }
        }
        public class staticPricing
        {
            public float nrc { get; set; }
            public float mrc { get; set; }
        }
        public class termination
        {
            public string destinationDistributionType { get; set; }
            public List<destinations> destinations { get; set; }
        }
        public class destinations
        {
            public int id { get; set; }
            public string source { get; set; }
            public string origin { get; set; }
            public string originDescription { get; set; }
            public int sortOrder { get; set; }
            public string destination { get; set; }
            public int ringTime { get; set; }
            public DateTime fromUTC { get; set; }
            public DateTime tillUTC { get; set; }
        }
        public class userTariffs
        {
            public string origin { get; set; }
            public string originDescription { get; set; }
            public string currency { get; set; }
            public float perCall { get; set; }
            public float perMinute { get; set; }
        }
        public class CommonOutput 
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public class TelservordernumberOutput 
        {

        }

        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, TelservordernumberInput TelservSingleNumberInput)
        {

            WebClient client = new WebClient();
            client.Headers["Content-type"] = "application/vnd.api+json";
            client.Encoding = Encoding.UTF8;
            string param = "stockDnisId=" + TelservSingleNumberInput.stockDnisId;
            string EndpointTeslerv = (ConfigurationManager.AppSettings["TELESEVAPIENDPOINT"]);
            string KeyTelserv = (ConfigurationManager.AppSettings["TELESEVAPIKEY"]);
            string URL = EndpointTeslerv +"shop/"+ TelservSingleNumberInput.stockDnisId + "?"+ KeyTelserv;
            string resTelserv = client.UploadString(URL, param);

            //string resTelserv = client.UploadString("https://partnerapidev.sipserv.nl:443/shop/" + TelservSingleNumberInput.stockDnisId + "?apiKey=1B7F4B7B-652A-4D47-93EE-9B0172A13812", param);
            resultTelserv Response = new resultTelserv();
            if (resTelserv != null && resTelserv != string.Empty)
            {
                Response = JsonConvert.DeserializeObject<resultTelserv>(resTelserv);
                Response.errcode = 0;
                Response.errmsg = "Sucess";
               // objOrderSingleNumberTelservOutput.errcode = 0;
                //objOrderSingleNumberTelservOutput.errmsg = "Sucess";
            }
            else
            {
               // CommonOutput objOrderSingleNumberTelservOutput = new CommonOutput();
                Response.errcode = -1;
                Response.errmsg = "Number does not Exists!";
            }
            return Request.CreateResponse(HttpStatusCode.OK, Response);
        }
    }
}