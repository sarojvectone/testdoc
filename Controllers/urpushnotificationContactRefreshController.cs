using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using System.Security.Cryptography.X509Certificates;

namespace Unifiedringmyaccountwebapi.Controllers
{
    public class urpushnotificationContactRefreshController  : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger(); 

        public class PushNotificationInput
        {
            public string sender { get; set; }
            public string receiver { get; set; }
            public string message { get; set; }
            public bool isvoip { get; set; }
            public bool isLive { get; set; }
            public string iuid { get; set; }
        }

        public class PushNotificationOutput : Output
        {

        }

        public class Output
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public HttpResponseMessage Get(string SIPID)
        {
            Log.Info("urpushnotification -Get");
            PushNotificationOutput output = new PushNotificationOutput();
            output.errcode = 0;
            output.errmsg = "Success";
            NewPushNotification(SIPID);
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
        private static void NewPushNotification(string SIPID)
        {
            String deletemessage = ConfigurationManager.AppSettings["deletemessage"];
            String updatemessage = ConfigurationManager.AppSettings["updatemessage"]; 
            String createmessage = ConfigurationManager.AppSettings["createmessage"]; 
            #region NewPushNotification
            Log.Info("Sending Push Notification after delete : start "); 
           
           
            string Type = "";
            string[] updatedusername;
            string[] createdusername;
            string[] deletedusername;
            List<string> tokens = new List<string>();
            string tokenvalue = string.Empty;
            List<DbUpdate.GetDeviceTokenOutput> resultToken = null;
            //ResultDetailsFCM data = new ResultDetailsFCM();
            //Result has to fetch from DB
            resultToken = new List<DbUpdate.GetDeviceTokenOutput>();
            resultToken = DbUpdate.Newpushnotificationexegetcustomer(SIPID);
            //End Result
            if (resultToken != null)
            {
                for (int iCount = 0; iCount < resultToken.Count(); iCount++)
                {

                    if (resultToken[iCount].errcode == 0)
                    {
                        //message = message.Replace("{0}", resultToken[iCount].Extn.ToString());
                        SIPID = resultToken[iCount].sip_id;
                        Type = resultToken[iCount].type;
                        updatedusername = resultToken[iCount].update_user_name.Split(',');
                        createdusername = resultToken[iCount].create_user_name.Split(',');
                        deletedusername = resultToken[iCount].del_user_name.Split(',');
                        // message = message.Replace("{0}", resultToken[iCount].Extn.ToString());

                        #region UpdatedUser

                        for (int i = 0; i < updatedusername.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty(updatedusername[i].ToString()))
                            {
                                // if (resultToken[iCount].Extn == "472")//IOS Test
                                // if (resultToken[iCount].Extn == "312")//Android Test
                                // {
                                updatemessage = updatemessage.Replace("{0}", updatedusername[i].ToString());
                                SendNotification(updatemessage, SIPID, Type, resultToken[iCount], iCount);
                                updatemessage = ConfigurationManager.AppSettings["updatemessage"];
                                // }
                            }
                        }
                        #endregion

                        #region CreatedUser

                        for (int i = 0; i < createdusername.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty(createdusername[i].ToString()))
                            {
                                createmessage = createmessage.Replace("{0}", createdusername[i].ToString());
                                SendNotification(createmessage, SIPID, Type, resultToken[iCount], iCount);
                                createmessage = ConfigurationManager.AppSettings["createmessage"];
                            }
                        }
                        #endregion

                        #region DeletedUser
                        for (int i = 0; i < deletedusername.Count(); i++)
                        {
                            if (!string.IsNullOrEmpty(deletedusername[i].ToString()))
                            {
                                deletemessage = deletemessage.Replace("{0}", deletedusername[i].ToString());
                                SendNotification(deletemessage, SIPID, Type, resultToken[iCount], iCount);
                                deletemessage = ConfigurationManager.AppSettings["createmessage"];
                            }
                        }
                        #endregion
                    }
                    // message = WebConfigurationManager.AppSettings["Message"];
                }
            }
            # endregion
        }
        # region SendNotification
        private static void SendNotification(String message, string SIPID, string Type, DbUpdate.GetDeviceTokenOutput resultToken, int iCount)
        {
            // tokenvalue = resultToken[iCount].message_token;
            List<DbUpdate.GetDeviceTokenOutput> listTokenValue = new List<DbUpdate.GetDeviceTokenOutput>();
            //calling to check the list values for token count 
            listTokenValue = DbUpdate.GetActiveTokens(resultToken.device_id);

            List<string> AndroidMessageTokens = new List<string>();
            List<DbUpdate.GetDeviceTokenOutput> IOSMessageTokens = new List<DbUpdate.GetDeviceTokenOutput>();
            foreach (DbUpdate.GetDeviceTokenOutput strMessageToken in listTokenValue)
            {
                if (strMessageToken.DeviceType == 2)
                    AndroidMessageTokens.Add(strMessageToken.message_token);
                else
                    if (strMessageToken.DeviceType == 1)
                        IOSMessageTokens.Add(strMessageToken);
            }
            //End Message token

            if (AndroidMessageTokens.Count > 0)
            {
                #region Android
                try
                {
                    Log.Info("Sending Push Notification Android Start..: " + SIPID);
                    Type = "Contact_Refresh";
                    unifiedringmyaccountwebapi.Controllers.FirebaseHelper.SendSilentNotification("Notification", message, AndroidMessageTokens, SIPID, Type);
                    Log.Info("Sending Push Notification Android End..: " + SIPID);
                }
                catch (Exception ex)
                {


                }

                #endregion
            }

            if (IOSMessageTokens.Count > 0)
            {
                try
                {
                    Log.Info("Sending Push Notification IOS Start..: " + SIPID);
                    #region IOS
                    //Call push notification for IOS
                    string p12File = "";
                    string p12FilePwd = "";
                    string iuid = "";
                    string media = "";
                    string FromNumber = "";
                    int iscancel;
                    bool isLive;
                    bool isvoip = false;
                    p12File = APNSHelper.message_key_file;
                    p12FilePwd = APNSHelper.message_key_pwd;
                    ApnsConfiguration config = null;
                    isLive = Convert.ToBoolean(ConfigurationManager.AppSettings["isLive"]);
                    isvoip = Convert.ToBoolean(ConfigurationManager.AppSettings["isvoip"]);

                    foreach (DbUpdate.GetDeviceTokenOutput objIOSTOken in IOSMessageTokens)
                    {

                        if (objIOSTOken.dev_identy == "Sandbox")
                        {
                            isLive = false;
                        }

                        if (isLive)
                        {
                            APNSHelper.push_hostname = APNSHelper.push_hostname.ToLower();
                        }
                        else
                        {
                            APNSHelper.push_hostname = APNSHelper.push_staging_hostname.ToLower();
                        }

                        if (!isLive)
                        {
                            config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, p12File, p12FilePwd, !isvoip);
                        }
                        else
                        {
                            config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, p12File, p12FilePwd, !isvoip);
                        }

                        //Unifiedringmyaccountwebapi.Controllers.urpushnotificationFCMController.PushNotificationOutput output = new Unifiedringmyaccountwebapi.Controllers.urpushnotificationFCMController.PushNotificationOutput();
                        //config = new PushSharp.Apple.ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, p12File, p12FilePwd, !isvoip);
                        ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                        // Create a new broker
                        var apnsBroker = new PushSharp.Apple.ApnsServiceBroker(config);
                        string strtoken = null;
                        // List<DbUpdate.GetDeviceTokenOutput> resultIOSToken = new List<DbUpdate.GetDeviceTokenOutput>();
                        // resultIOSToken.Add(resultToken[iCount]);
                        // urpushnotificationController objurpushnotificationController = new urpushnotificationController();
                        //We have to get the result from DB
                        //foreach (DbUpdate.GetDeviceTokenOutput token in resultIOSToken)

                        // token.message_token = "90EB35B30DA30993EE204404E57BE2EE60D67A72E4AC65BFEF8709BBBE462D4B";


                        // Wire up events
                        apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                        {
                            aggregateEx.Handle(ex =>
                            {
                                //output.errcode = -1;
                                // See what kind of exception it was to further diagnose
                                if (ex is ApnsNotificationException)
                                {
                                    //DeActivate Subscription
                                    string result = string.Empty;

                                    //if (isvoip)
                                    //    result = objurpushnotificationController.DeactivateSubscription("", token.voip_token);
                                    //else
                                    //    result = objurpushnotificationController.DeactivateSubscription(token.message_token, "");
                                    //DeaActivate End

                                    var notificationException = (ApnsNotificationException)ex;
                                    var apnsNotification = notificationException.Notification;
                                    var statusCode = notificationException.ErrorStatusCode;

                                    //output.errmsg = String.Format("Apple Notification Failed: ID={0}, Code={1}", apnsNotification.Identifier, statusCode,apnsNotification.Payload);
                                    Log.Info(String.Format("Apple Notification Failed: ID={0}, Code={1},DeviceToken{2}", apnsNotification.Identifier, statusCode, apnsNotification.DeviceToken));

                                }
                                else
                                {
                                    //DeActivate Subscription
                                    string result = string.Empty;
                                    //if (isvoip)
                                    //    result = objurpushnotificationController.DeactivateSubscription("", token.voip_token);
                                    //else
                                    //    result = objurpushnotificationController.DeactivateSubscription(token.message_token, "");
                                    //DeaActivate End
                                    //output.errmsg = String.Format("Apple Notification Failed for some unknown reason : {0}", ex.InnerException);
                                    Log.Info(String.Format("Apple Notification Failed for some unknown reason : {0}", ex.InnerException));

                                }
                                return true;
                            });
                        };
                        // apnsBroker.OnNotificationSucceeded += (notification) =>
                        // {


                        // output.errcode = 0;
                        //output.errmsg = "Notification Sent!"; 
                        //Log.Info("Notification Sent!");

                        // };

                        apnsBroker.Start();
                        // foreach (DbUpdate.GetDeviceTokenOutput token in listTokenValue)
                        // {

                        if (isvoip)
                        {
                            strtoken = objIOSTOken.voip_token;
                        }
                        else
                        {
                            strtoken = objIOSTOken.message_token;
                        }

                        string jsonString1 = "{\"aps\": {\"alert\": {\"title\": \"Hey!we refreshed Contact\",\"subtitle\": \"Deleted contact\",\"body\":\"" + message + "\"},\"sound\": \"" + APNSHelper.push_sound + "\",\"category\": \"contact_refresh\",\"badge\":\"" + APNSHelper.push_badge + "\",\"content-available\": 1,\"sip_login\":\"" + SIPID + "\"}}";

                        string jsonString2 = "{\"aps\": {\"content-available\":\"" + "1" + "\",\"sound\": \"\",\"category\": \"contact_refresh\",\"sip_login\":\"" + SIPID + "\"}}";

                        //Log.Info("payload : " + jsonString1);

                        Log.Info("payload : " + jsonString2);
                        Log.Info("DeviceToken : " + strtoken);


                        //{"aps": {"alert": {"title": "Hey!we refreshed Contact","subtitle": "Deleted contact","body":"User 225 deleted,Kindly refresh"},"sound": "default","category": "contact_refresh","badge":"1","content-available": 1, "sip_login":"1655"}}
                        ApnsNotification objApnsNotification = new ApnsNotification();
                        objApnsNotification.DeviceToken = strtoken;
                        objApnsNotification.Expiration = DateTime.UtcNow;
                        objApnsNotification.Payload = JObject.Parse(jsonString2);

                        apnsBroker.QueueNotification(objApnsNotification);


                        //apnsBroker.QueueNotification(new ApnsNotification
                        //{
                        //    DeviceToken = strtoken,
                        //    Expiration = DateTime.UtcNow,
                        //    Payload = JObject.Parse(jsonString2)
                        //    //Payload = JObject.Parse("{\"aps\":{\"content-available\":\"" + "1" + "\",\"alert\":\"" + message + "\",\"badge\":\"" + APNSHelper.push_badge + "\",\"sound\":\"" + APNSHelper.push_sound + "\"}}")
                        //});  

                        //  }
                        apnsBroker.Stop();

                    }

                    #endregion
                    Log.Info("Sending Push Notification IOS End..: " + SIPID);
                }
                catch (Exception ex)
                {


                }

            }
        }
        # endregion

    }
   
    class DbUpdate
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger(); 
        public class RatepushnotificationexegetcustomerOutput
        {
            //public string VOIPToken { get; set; }
            public string MessageToken { get; set; }
            public int DeviceType { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        public class GetDeviceTokenOutput
        {
            public string message_token { get; set; }
            public string voip_token { get; set; }
            public int? DeviceType { get; set; }
            public string Extn { get; set; }
            public string sip_id { get; set; }
            public string type { get; set; }
            public int? errcode { get; set; }
            public string errmsg { get; set; }
            public string del_user_name { get; set; }
            public string update_user_name { get; set; }
            public string create_user_name { get; set; }
            public string dev_identy { get; set; }
            public string device_id { get; set; }

        }
        public static List<GetDeviceTokenOutput> pushnotificationexegetcustomer()
        {
            Log.Info("Ratepushnotificationexegetcustomer");
            string Responce = string.Empty;
            List<GetDeviceTokenOutput> OutputList = new List<GetDeviceTokenOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ToString()))
                {
                    conn.Open();
                    var sp = "ur_push_notifcation_exe_get_customer";

                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure);
                    
                    Log.Info("result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new GetDeviceTokenOutput()
                        {
                            message_token = r.MessageToken,
                            DeviceType = r.device_type,
                            voip_token = r.VOIPToken,
                            Extn = r.ext_no,
                            sip_id = r.sip_id,
                            type = r.type == null ? 0 : r.type,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                        Responce = result.ElementAt(0).errmsg;
                    }
                    else
                    {
                        GetDeviceTokenOutput output = new GetDeviceTokenOutput();
                        output.errcode = -1;
                        output.errmsg = "No Records Found";
                        OutputList.Add(output);
                    }
                }
            }
            catch (Exception ex)
            {
                GetDeviceTokenOutput output = new GetDeviceTokenOutput();
                output.errcode = -1;
                output.errmsg = ex.Message;
                OutputList.Add(output);
                Log.Info("Catch in RATE_PUSH_NOTIFICATION_EXE_GET_CUSTOMER : " + ex.Message);
            }
            return OutputList;
        }



        public static List<GetDeviceTokenOutput> Newpushnotificationexegetcustomer(string SIPID)
        {
            Log.Info("Newpushnotificationexegetcustomer");
            string Responce = string.Empty;
            List<GetDeviceTokenOutput> OutputList = new List<GetDeviceTokenOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ToString()))
                {
                    conn.Open();
                    var sp = "ur_push_notifcation_exe_get_customer_v3";

                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @sip_id = SIPID
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Info("result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new GetDeviceTokenOutput()
                        {
                            message_token = r.MessageToken,
                            DeviceType = r.device_type,
                            voip_token = r.VOIPToken,
                            Extn = r.ext_no,
                            sip_id = r.sip_id,
                            create_user_name = r.create_user_name,
                            del_user_name = r.del_user_name,
                            update_user_name = r.update_user_name,
                            dev_identy = r.dev_identy,
                            device_id = r.device_id,
                            type = r.type == null ? 0 : r.type,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                        Responce = result.ElementAt(0).errmsg;
                    }
                    else
                    {
                        GetDeviceTokenOutput output = new GetDeviceTokenOutput();
                        output.errcode = -1;
                        output.errmsg = "No Records Found";
                        OutputList.Add(output);
                    }
                }
            }
            catch (Exception ex)
            {
                GetDeviceTokenOutput output = new GetDeviceTokenOutput();
                output.errcode = -1;
                output.errmsg = ex.Message;
                OutputList.Add(output);
                Log.Info("Catch in RATE_PUSH_NOTIFICATION_EXE_GET_CUSTOMER : " + ex.Message);
            }
            return OutputList;
        }

        public static List<GetDeviceTokenOutput> GetActiveTokens(string DeviceID)
        {
            Log.Info("GetActiveTokens");
            string Responce = string.Empty;
            List<GetDeviceTokenOutput> OutputList = new List<GetDeviceTokenOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["apns_provider"].ToString()))
                {
                    conn.Open();
                    var sp = "ur_getdevicetoken_by_deviceid";

                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @DeviceID = DeviceID
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Info("result : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new GetDeviceTokenOutput()
                        {
                            message_token = r.message_token,
                            DeviceType = r.DeviceType,
                            voip_token = r.voip_token,
                            dev_identy = r.dev_identy,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                        Responce = result.ElementAt(0).errmsg;
                    }
                    else
                    {
                        GetDeviceTokenOutput output = new GetDeviceTokenOutput();
                        output.errcode = -1;
                        output.errmsg = "No Records Found";
                        OutputList.Add(output);
                    }
                }
            }
            catch (Exception ex)
            {
                GetDeviceTokenOutput output = new GetDeviceTokenOutput();
                output.errcode = -1;
                output.errmsg = ex.Message;
                OutputList.Add(output);
                Log.Info("Catch in RATE_PUSH_NOTIFICATION_EXE_GET_CUSTOMER : " + ex.Message);
            }
            return OutputList;
        }
    }
}
