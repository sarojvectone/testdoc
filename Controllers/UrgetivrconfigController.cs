using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrgetivrconfigController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrgetivrconfigInput
        {
			public int company_id { get; set; }
            public int site_id	 { get; set; }           	    
        }
        public class UrgetivrconfigOutput
        {
			public int? ivr_id { get; set; }
			public string ivr_extension { get; set; }
			public string ivr_name { get; set; }
            public int? site_id { get; set; }
            public string Extn_number { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrgetivrconfigInput req)
        {
            List<UrgetivrconfigOutput> OutputList = new List<UrgetivrconfigOutput>();
			Log.Info("Input : UrgetivrconfigController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_get_IVR_Config";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
		                        @site_id = req.site_id	                               
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrgetivrconfigOutput()
                        {
							ivr_id = r.ivr_id == null ? 0 : r.ivr_id,
							ivr_extension = r.ivr_extension,
							ivr_name = r.ivr_name,
                            site_id = r.site_id == null ? 0 : r.site_id,
                            Extn_number  = r.Extn_number,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrgetivrconfigOutput outputobj = new UrgetivrconfigOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrgetivrconfigOutput outputobj = new UrgetivrconfigOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
