using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{   
    public class UrcrmgetassigntouserController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public class UrcrmgetassigntouserInput
        {

        }

        public class UrcrmgetassigntouserOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
            public int id { get; set; }
            public string name { get; set; }
        }

        [Authorize]
        public async Task<HttpResponseMessage> Get(string id, HttpRequestMessage request, UrcrmgetassigntouserInput req)
        {
            List<UrcrmgetassigntouserOutput> OutputList = new List<UrcrmgetassigntouserOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_crm_get_assignto_user";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {

                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrcrmgetassigntouserOutput()
                        {
                            errcode = 0,
                            errmsg= "Success",
                            id = r.id,
                            name = r.name 
                        }));
                    }
                    else
                    {
                        UrcrmgetassigntouserOutput outputobj = new UrcrmgetassigntouserOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrcrmgetassigntouserOutput outputobj = new UrcrmgetassigntouserOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
