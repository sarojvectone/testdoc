using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmahuntgroupsremovecallmemberController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmahuntgroupsremovecallmemberInput
        {
			public int domain_id { get; set; }
			public int group_id { get; set; }
			public string extension { get; set; }  	                   	    
        }
        public class UrmahuntgroupsremovecallmemberOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmahuntgroupsremovecallmemberInput req)
        {
            List<UrmahuntgroupsremovecallmemberOutput> OutputList = new List<UrmahuntgroupsremovecallmemberOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_ma_hunt_groups_remove_call_member";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id,
								@group_id = req.group_id,
								@extension = req.extension 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmahuntgroupsremovecallmemberOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmahuntgroupsremovecallmemberOutput outputobj = new UrmahuntgroupsremovecallmemberOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmahuntgroupsremovecallmemberOutput outputobj = new UrmahuntgroupsremovecallmemberOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
