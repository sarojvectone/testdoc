using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaannouncementupdateextensionController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaannouncementupdateextensionInput
        {
			public int anno_id { get; set; }
			public int domain_id { get; set; }
			public string extn_name { get; set; }
			public int extn_number { get; set; }
			public string email { get; set; }
			public string company_name { get; set; }
			public string contact_no { get; set; }
			public int status { get; set; }
			public string greeting_filename { get; set; }
			public string time_zone { get; set; }
			public int time_format { get; set; }
			public string home_country_code { get; set; }
			public string greeting_language { get; set; }
			public string user_language { get; set; }
			public string regional_format { get; set; }
			public int message_enable { get; set; }
			public string message_enable_duration { get; set; }
			public int type { get; set; } 
            public int  greeting_by {get;set;}
            public int greeting_custom_type { get; set; }
    
        }
        public class UrmaannouncementupdateextensionOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaannouncementupdateextensionInput req)
        {
            List<UrmaannouncementupdateextensionOutput> OutputList = new List<UrmaannouncementupdateextensionOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_announcement_update_extension";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@anno_id = req.anno_id,
								@domain_id = req.domain_id,
								@extn_name = req.extn_name,
								@extn_number = req.extn_number,
								@email = req.email,
								@company_name = req.company_name,
								@contact_no = req.contact_no,
								@status = req.status,
								@greeting_filename = req.greeting_filename,
								@time_zone = req.time_zone,
								@time_format = req.time_format,
								@home_country_code = req.home_country_code,
								@greeting_language = req.greeting_language,
								@user_language = req.user_language,
								@regional_format = req.regional_format,
								@message_enable = req.message_enable,
								@message_enable_duration = req.message_enable_duration,
								@type = req.type,
                                @greeting_by = req.greeting_by,
                                @greeting_custom_type = req.greeting_custom_type 
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmaannouncementupdateextensionOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmaannouncementupdateextensionOutput outputobj = new UrmaannouncementupdateextensionOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmaannouncementupdateextensionOutput outputobj = new UrmaannouncementupdateextensionOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
