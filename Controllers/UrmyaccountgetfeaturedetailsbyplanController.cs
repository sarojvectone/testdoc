using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountgetfeaturedetailsbyplanController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountgetfeaturedetailsbyplanInput
        {
			public int plan_id { get; set; }
			public int duration { get; set; }
			public int Feature_hdr { get; set; }  	                   	    
        }
        public class UrmyaccountgetfeaturedetailsbyplanOutput
        {
			public int PF_Idx { get; set; }
			public string PF_Name { get; set; }
			public string PF_category { get; set; }
			public int Category_id { get; set; }
			public string Feature_Status { get; set; }
			public string Feature_keyword { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountgetfeaturedetailsbyplanInput req)
        {
            List<UrmyaccountgetfeaturedetailsbyplanOutput> OutputList = new List<UrmyaccountgetfeaturedetailsbyplanOutput>();
			Log.Info("Input : UrmyaccountgetfeaturedetailsbyplanController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_get_feature_details_by_plan";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@plan_id = req.plan_id,
								@duration = req.duration,
								@Feature_hdr = req.Feature_hdr 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountgetfeaturedetailsbyplanOutput()
                        {
							PF_Idx = r.PF_Idx == null ? 0 : r.PF_Idx,
							PF_Name = r.PF_Name,
							PF_category = r.PF_category,
							Category_id = r.Category_id == null ? 0 : r.Category_id,
							Feature_Status = r.Feature_Status,
							Feature_keyword = r.Feature_keyword,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountgetfeaturedetailsbyplanOutput outputobj = new UrmyaccountgetfeaturedetailsbyplanOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountgetfeaturedetailsbyplanOutput outputobj = new UrmyaccountgetfeaturedetailsbyplanOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
