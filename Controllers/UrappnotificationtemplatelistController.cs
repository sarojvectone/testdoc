using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrappnotificationtemplatelistController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrappnotificationtemplatelistInput
        {
        }
        public class UrappnotificationtemplatelistOutput
        {
            public List<notification> notification { get; set; }
            public List<soundnotification> soundnotification { get; set; }
            public List<joinnotification> joinnotification { get; set; }
            public List<msgnotification> msgnotification { get; set; }
            public List<emaildirectnotification> emaildirectnotification { get; set; }
            public List<emailteamnotification> emailteamnotification { get; set; }
            public List<badgenotification> badgenotification { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }	      	    
        }

        public class badgenotification
        {
            public int? badge_id { get; set; }
			public string badge_display_desc { get; set; }
        }

        public class emailteamnotification 
        {
            public int? email_team_msg_id { get; set; }
            public string email_team_msg_desc { get; set; }
        }
        public class emaildirectnotification 
        {
            public int? email_direct_msg_id { get; set; }
            public string email_direct_msg_desc { get; set; }
        }
        public class msgnotification 
        {
            public int? msg_id { get; set; }
            public string msg_desc { get; set; }
        }

        public class joinnotification 
        {
            public int? join_id { get; set; }
            public string join_now_desc { get; set; }
        }
        public class notification
        {
            public int? typeid { get; set; }
            public string notification_desc { get; set; }
        }
        public class soundnotification
        {
            public int? sound_id { get; set; }
            public string sound_notification_desc { get; set; }
        }

        [Authorize]
        public async Task<HttpResponseMessage> Get(string id, HttpRequestMessage request, UrappnotificationtemplatelistInput req)
        {
            UrappnotificationtemplatelistOutput output = new UrappnotificationtemplatelistOutput();
           
			Log.Info("Input : UrappnotificationtemplatelistController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_app_notification_template_list";
                    var result = conn.QueryMultiple(
                            sp, new
                            {
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                      
                      output.errcode = 0;
                      output.errmsg = "Success";
                      output.notification = (List<notification>)result.Read<notification>();
                      output.soundnotification = (List<soundnotification>)result.Read<soundnotification>();
                      output.joinnotification = (List<joinnotification>)result.Read<joinnotification>();
                      output.msgnotification = (List<msgnotification>)result.Read<msgnotification>();
                      output.emaildirectnotification = (List<emaildirectnotification>)result.Read<emaildirectnotification>();
                      output.emailteamnotification = (List<emailteamnotification>)result.Read<emailteamnotification>();
                      output.badgenotification = (List<badgenotification>)result.Read<badgenotification>();
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                       UrappnotificationtemplatelistOutput outputobj = new UrappnotificationtemplatelistOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                       // OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrappnotificationtemplatelistOutput outputobj = new UrappnotificationtemplatelistOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
               /// OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
    }
}
