﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using WinSCP;
using System.IO;
using unifiedringmyaccountwebapi.Controllers;
using Newtonsoft.Json;
using System.Text;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccwebivrinsertextentionaudiofileController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccwebivrinsertextentionaudiofileInput
        {
            public int Company_id { get; set; }
            public int Domain_id { get; set; }
            public int Extension { get; set; }
            public string IVR_type { get; set; }
            public int? Hour_type { get; set; }
            public int Greeting_by { get; set; }
            public int Greeting_type { get; set; }
            public int Language { get; set; }
            public int Audio_type { get; set; }
            public int Music_type { get; set; }
            public int Call_screening_type { get; set; }
            public string Audio_file { get; set; }
            public string Audio_file_info { get; set; }
            public int Active { get; set; }
            public int update_type { get; set; }
            public int? greeting_custom_type { get; set; }
            public string greeting_content { get; set; }
            public string calledby { get; set; }
        }
        public class UrmyaccwebivrinsertextentionaudiofileOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        public class Result
        {
            public string msg { get; set; }
            public string data { get; set; }
        }

        public class TexttoSpeechResult
        {
            public int statusCode { get; set; }
            public Result result { get; set; }
        }


        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccwebivrinsertextentionaudiofileInput req)
        {
            List<UrmyaccwebivrinsertextentionaudiofileOutput> OutputList = new List<UrmyaccwebivrinsertextentionaudiofileOutput>();
            Log.Info("Urmyaccwebivrinsertextentionaudiofile: Input : " + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "UR_Myacc_Web_IVR_Insert_extention_audio_file";
                    string filename = "";
                    string paramFilename = "";
                    if (req.calledby == "APP")
                    {
                        filename = req.Extension + "_" + req.Domain_id + "_" + "UG" + "_" + DateTime.Now.ToString("ddmmyyhhmmss") + ".wav";
                        paramFilename = "audio/" + filename;
                    }
                    else
                    {
                        filename = req.Audio_file.Replace("audio/","");
                        paramFilename = req.Audio_file;
                    }
                 
                   

                    //"audio/"
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @Company_id = req.Company_id,
                               // @Domain_id = req.Domain_id,
                                @Extension = req.Extension,
                                @IVR_type = req.IVR_type,
                                @Hour_type = req.Hour_type,
                                @Greeting_by = req.Greeting_by,
                                @Greeting_type = req.Greeting_type,
                                @Language = req.Language,
                                @Audio_type = req.Audio_type,
                                @Music_type = req.Music_type,
                                @Call_screening_type = req.Call_screening_type,
                               // @Audio_file_info = req.Audio_file_info,
                                @Audio_file = paramFilename,
                                @Active = req.Active,
                                @update_type = req.update_type,
                                @greeting_custom_type = req.greeting_custom_type,
                                @greeting_content = req.greeting_content
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmyaccwebivrinsertextentionaudiofileOutput()
                        {
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                        Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        //Send file to UMN and PHP server
                        if (OutputList[0].errcode == 0)
                        {
                            Log.Info("SFTP Transfer start : " + JsonConvert.SerializeObject(OutputList));
                            Log.Info("SFTP Transfer start : " + req.IVR_type);
                            byte[] bytesString = null;
                            if (req.IVR_type == "USERGREETING" || req.IVR_type == "CONNECTINGMESSAGE")
                            { 
                                if (req.Greeting_by == 1)
                                {
                                    //Text to speech
                                    var webAddr1 = ConfigurationManager.AppSettings["texttospeech"];

                                    HttpWebResponse httpResponse1;
                                    string responseText;
                                    TextToSpeech(req, webAddr1, out httpResponse1, out responseText);

                                    TexttoSpeechResult response = JsonConvert.DeserializeObject<TexttoSpeechResult>(responseText);

                                    bytesString = Convert.FromBase64String(response.result.data);
                                    Log.Info("Output texttospeech: " + JsonConvert.SerializeObject(httpResponse1));
                                }
                                else
                                {
                                    bytesString = Convert.FromBase64String(req.Audio_file_info);
                                }
                                MoveToSFTPServers(req, filename, bytesString);
                            }
                            else if (req.IVR_type == "HOLDMUSIC" || req.IVR_type == "AUDIOCONNECTING")
                            {
                                if (req.Greeting_by != 1)
                                {
                                    bytesString = Convert.FromBase64String(req.Audio_file_info);
                                    MoveToSFTPServers(req, filename, bytesString);
                                }
                            }
                        }
                    }
                    else
                    {
                        UrmyaccwebivrinsertextentionaudiofileOutput outputobj = new UrmyaccwebivrinsertextentionaudiofileOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmyaccwebivrinsertextentionaudiofileOutput outputobj = new UrmyaccwebivrinsertextentionaudiofileOutput();
                outputobj.errcode = -1;
                outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }

        private static void MoveToSFTPServers(UrmyaccwebivrinsertextentionaudiofileInput req, string filename, byte[] bytesString)
        {
            Log.Info("MoveToSFTPServers");
            //  byte[] bytesString = Convert.FromBase64String(req.Audio_file_info);
            //Writing in local server
            string filePath = @Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"\WavFiles\" + req.Domain_id, filename);
            Log.Info("Creating file in local server start:" + filePath);
            if (!Directory.Exists(filePath))
            {
                DirectoryInfo di = Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"\WavFiles\" + req.Domain_id);
            }
            File.WriteAllBytes(filePath, bytesString);
            Log.Info("Creating file in local server end:" + filePath);

            //Adding in cluster server

            using (new NetworkConnection(ConfigurationManager.AppSettings["WAVNetworkPath"], new NetworkCredential(ConfigurationManager.AppSettings["WAVNetworkUsername"], ConfigurationManager.AppSettings["WAVNetworkPassword"], ConfigurationManager.AppSettings["WAVNetworkDomain"])))
            {
                //Save to cluster network
                string ClusterfilePath = @Path.Combine(ConfigurationManager.AppSettings["WAVNetworkPath"] + req.Domain_id, filename);
                Log.Info("Creating file in cluster server start:" + ClusterfilePath);
                if (!Directory.Exists(filePath))
                {
                    DirectoryInfo di = Directory.CreateDirectory(ConfigurationManager.AppSettings["WAVNetworkPath"] + req.Domain_id);
                }
                File.WriteAllBytes(ClusterfilePath, bytesString);
                Log.Info("Creating file in cluster server end:" + ClusterfilePath);
            }


            try
            {

                UMNFTPServer(filename, req.Domain_id.ToString(), "UG", filePath);
            }
            catch (Exception ex)
            {

                Log.Info("Creating file in UMN FTP server catch exception:" + ex.Message.ToString());
            }
            try
            {
                UMNFTPServer1(filename, req.Domain_id.ToString(), "UG", filePath);
            }
            catch (Exception ex)
            {

                Log.Info("Creating file in UMN FTP cluster server catch exception:" + ex.Message.ToString());
            }
            try
            {
                PHPFTPServer(filename, req.Domain_id.ToString(), "UG", filePath);
            }
            catch (Exception ex)
            {

                Log.Info("Creating file in PHP server catch exception:" + ex.Message.ToString());
            }
        }

        private static void TextToSpeech(UrmyaccwebivrinsertextentionaudiofileInput req, string webAddr1, out HttpWebResponse httpResponse1, out string responseText)
        {
            var httpWebRequest1 = (HttpWebRequest)WebRequest.Create(webAddr1);
            httpWebRequest1.ContentType = "application/json";
            httpWebRequest1.Method = "POST";

            string input = @"<speak><break strength=""weak""/>" + req.greeting_content + "</speak>";

            TexttoSpeech objTexttoSpeech = new TexttoSpeech();
            objTexttoSpeech.text = "<speak><break strength='weak'/>" + req.greeting_content + "</speak>";
            objTexttoSpeech.lang = "en-US";
            objTexttoSpeech.name = "en-US-Wavenet-C";
            objTexttoSpeech.speaking_rate = "0.25";
            objTexttoSpeech.pitch = "-1.00";
            objTexttoSpeech.volume_gain_db = "0.45";

            string jsonresult = JsonConvert.SerializeObject(objTexttoSpeech);

            Log.Info("Input texttospeech: " + JsonConvert.SerializeObject(jsonresult));

            using (var streamWriter = new StreamWriter(httpWebRequest1.GetRequestStream()))
            {
                streamWriter.Write(jsonresult);
                streamWriter.Flush();
            }
            httpResponse1 = (HttpWebResponse)httpWebRequest1.GetResponse();
            WebHeaderCollection header = httpResponse1.Headers;

            var encoding = ASCIIEncoding.ASCII;
            responseText = "";
            using (var reader = new System.IO.StreamReader(httpResponse1.GetResponseStream(), encoding))
            {
                responseText = reader.ReadToEnd();
            }
        }

        public class TexttoSpeech
        {
            public string text { get; set; }
            public string lang { get; set; }
            public string name { get; set; }
            public string speaking_rate { get; set; }
            public string pitch { get; set; }
            public string volume_gain_db { get; set; }
        }
        private static void UMNFTPServer(string filename, string domain_ID, string details, string filePath)
        {
            Log.Info("UMNFTPServer");
            string ftpServerName = ConfigurationManager.AppSettings["ftpServerName"];
            string ftpUser = ConfigurationManager.AppSettings["ftpUser"]; 
            string RemotePath = ConfigurationManager.AppSettings["RemotePath"];
            int PortNo = Convert.ToInt32(ConfigurationManager.AppSettings["PortNo"]);
            string ftpUserpassword = ConfigurationManager.AppSettings["ftpUserpassword"];
            string SshHostKeyFingerprint = ConfigurationManager.AppSettings["SshHostKeyFingerprint"];

            Log.Info("Creating file in UMN FTP server start:" + ftpServerName);
            SessionOptions sessionOptions = new SessionOptions
            {
                Protocol = Protocol.Sftp,
                HostName = ftpServerName, //hostname e.g. IP: 192.54.23.32, or mysftpsite.com
                UserName = ftpUser,
                PortNumber = PortNo,
                Password = ftpUserpassword,
                SshHostKeyFingerprint = SshHostKeyFingerprint
            };
            using (Session session = new Session())
            {


                Session obj = new Session();
                obj.Open(sessionOptions); //Attempts to connect to your sFtp site
                TransferOptions transferOptions = new TransferOptions();
                transferOptions.TransferMode = TransferMode.Binary; //The Transfer Mode - 
                try
                {
                    string domainID = "/" + domain_ID;
                    string purpose = "/" + details + "/";
                    string domainPath = @RemotePath + domainID + purpose;
                    bool fileexists = obj.FileExists(domainPath);
                    if (fileexists)
                    {
                        //RemoteDirectoryInfo directory = obj.ListDirectory(domainPath);
                        TransferOperationResult transferResult = null;
                        transferResult = obj.PutFiles(filePath , domainPath + filename, false, transferOptions);
                        transferResult.Check();
                    }
                    else
                    {

                        obj.CreateDirectory(@RemotePath + domainID);
                        obj.CreateDirectory(domainPath);
                        TransferOperationResult transferResult = null;
                        transferResult = obj.PutFiles(filePath , domainPath + filename, false, transferOptions);
                        transferResult.Check();
                    }

                }
                catch (Exception ex1)
                {
                    Log.Info("Creating file in UMN FTP server Exception:" + ex1.Message.ToString());
                    //Console.WriteLine(ex1.Message.ToString());
                    //Console.ReadLine();
                }
                Log.Info("Creating file in UMN FTP server End:" + ftpServerName);
            }
        }

        private static void UMNFTPServer1(string filename, string domain_ID, string details, string filePath)
        {

            string ftpServerName = ConfigurationManager.AppSettings["ftpServerName1"];
            string ftpUser = ConfigurationManager.AppSettings["ftpUser1"];
            string RemotePath = ConfigurationManager.AppSettings["RemotePath"];
            int PortNo = Convert.ToInt32(ConfigurationManager.AppSettings["PortNo"]);
            string ftpUserpassword = ConfigurationManager.AppSettings["ftpUserpassword1"];
            string SshHostKeyFingerprint = ConfigurationManager.AppSettings["SshHostKeyFingerprint1"];
            Log.Info("Creating file in UMN FTP cluster server start:" + ftpServerName);

            SessionOptions sessionOptions = new SessionOptions
            {
                Protocol = Protocol.Sftp,
                HostName = ftpServerName, //hostname e.g. IP: 192.54.23.32, or mysftpsite.com
                UserName = ftpUser,
                PortNumber = PortNo,
                Password = ftpUserpassword,
                SshHostKeyFingerprint = SshHostKeyFingerprint
            };
            using (Session session = new Session())
            {


                Session obj = new Session();
                obj.Open(sessionOptions); //Attempts to connect to your sFtp site
                TransferOptions transferOptions = new TransferOptions();
                transferOptions.TransferMode = TransferMode.Binary; //The Transfer Mode - 
                try
                {
                    string domainID = "/" + domain_ID;
                    string purpose = "/" + details + "/";
                    string domainPath = @RemotePath + domainID + purpose;
                    bool fileexists = obj.FileExists(domainPath);
                    if (fileexists)
                    {
                        //RemoteDirectoryInfo directory = obj.ListDirectory(domainPath);
                        TransferOperationResult transferResult = null;
                        transferResult = obj.PutFiles(filePath, domainPath + filename, false, transferOptions);
                        transferResult.Check();
                    }
                    else
                    {

                        obj.CreateDirectory(@RemotePath + domainID);
                        obj.CreateDirectory(domainPath);
                        TransferOperationResult transferResult = null;
                        transferResult = obj.PutFiles(filePath, domainPath + filename, false, transferOptions);
                        transferResult.Check();
                    }

                }
                catch (Exception ex1)
                {
                    Log.Info("Creating file in UMN FTP cluster server exception:" + ex1.Message.ToString());
                    //Console.WriteLine(ex1.Message.ToString());
                    //Console.ReadLine();
                }
                Log.Info("Creating file in UMN FTP cluster server end:" + ftpServerName);
            }
        }

        private static void PHPFTPServer(string filename, string domain_ID, string details, string filePath)
        {

            string ftpServerName = ConfigurationManager.AppSettings["ftpPHPServerName"];
            string ftpUser = ConfigurationManager.AppSettings["ftpPHPUser"]; 
            string RemotePath = ConfigurationManager.AppSettings["PHPRemotePath"];
            int PortNo = Convert.ToInt32(ConfigurationManager.AppSettings["PortNo"]);
            string ftpUserpassword = ConfigurationManager.AppSettings["ftpPHPUserpassword"];
            string SshHostKeyFingerprint = ConfigurationManager.AppSettings["PHPSshHostKeyFingerprint"];

            Log.Info("Creating file in PHP FTP cluster server start:" + ftpServerName);

            SessionOptions sessionOptions = new SessionOptions
            {
                Protocol = Protocol.Sftp,
                HostName = ftpServerName, //hostname e.g. IP: 192.54.23.32, or mysftpsite.com
                UserName = ftpUser,
                PortNumber = PortNo,
                Password = ftpUserpassword,
                SshHostKeyFingerprint = SshHostKeyFingerprint
            };
            using (Session session = new Session())
            {


                Session obj = new Session();
                obj.Open(sessionOptions); //Attempts to connect to your sFtp site
                TransferOptions transferOptions = new TransferOptions();
                transferOptions.TransferMode = TransferMode.Binary; //The Transfer Mode - 
                try
                {

                    string domainPath = @RemotePath;
                    //RemoteDirectoryInfo directory = obj.ListDirectory(domainPath);
                    TransferOperationResult transferResult = null;
                    transferResult = obj.PutFiles(filePath, domainPath + filename, false, transferOptions);
                    transferResult.Check();
                }
                catch (Exception ex1)
                {
                    Log.Info("Creating file in PHP FTP cluster server exception:" + ex1.Message.ToString());
                    //Console.WriteLine(ex1.Message.ToString());
                    //Console.ReadLine();
                }
                Log.Info("Creating file in PHP FTP cluster server end:" + ftpServerName);
            }
        }
    }
}
