using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountassignlicenceusertoexistingextnController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountassignlicenceusertoexistingextnInput
        {
			public int? company_id { get; set; }
			public string ext { get; set; }
			public string did { get; set; }
			public string ex_user_extn { get; set; }
			public string updated_by { get; set; }  	                   	    
        }
        public class UrmyaccountassignlicenceusertoexistingextnOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountassignlicenceusertoexistingextnInput req)
        {
            List<UrmyaccountassignlicenceusertoexistingextnOutput> OutputList = new List<UrmyaccountassignlicenceusertoexistingextnOutput>();
			Log.Info("Input : UrmyaccountassignlicenceusertoexistingextnController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_assign_licence_user_to_existing_extn";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@ext = req.ext,
								@did = req.did,
								@ex_user_extn = req.ex_user_extn,
								@updated_by = req.updated_by 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountassignlicenceusertoexistingextnOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountassignlicenceusertoexistingextnOutput outputobj = new UrmyaccountassignlicenceusertoexistingextnOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountassignlicenceusertoexistingextnOutput outputobj = new UrmyaccountassignlicenceusertoexistingextnOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
