using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountcallqueuegetsettingsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcallqueuegetsettingsInput
        {
			public int company_id { get; set; }
			public int Queue_id { get; set; }  	                   	    
        }
        public class UrmyaccountcallqueuegetsettingsOutput
        {
			public int? q_max_wait_time_next_call { get; set; }
			public int? q_max_callers { get; set; }
			public int? q_max_hold_time { get; set; }
			public string q_hold_time_sendcallers_to { get; set; }
			public string q_reach_max_callers_send_to { get; set; }
			public string q_reach_max_callers_greeting_type { get; set; }
			public string q_audio_file_name { get; set; }
			public string q_audio_content { get; set; }
			public int? q_last_callend_agent_availablity { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcallqueuegetsettingsInput req)
        {
            List<UrmyaccountcallqueuegetsettingsOutput> OutputList = new List<UrmyaccountcallqueuegetsettingsOutput>();
			Log.Info("Input : UrmyaccountcallqueuegetsettingsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_call_queue_get_settings";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@Queue_id = req.Queue_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcallqueuegetsettingsOutput()
                        {
							q_max_wait_time_next_call = r.q_max_wait_time_next_call == null ? 0 : r.q_max_wait_time_next_call,
							q_max_callers = r.q_max_callers == null ? 0 : r.q_max_callers,
							q_max_hold_time = r.q_max_hold_time == null ? 0 : r.q_max_hold_time,
							q_hold_time_sendcallers_to = r.q_hold_time_sendcallers_to ,
							q_reach_max_callers_send_to = r.q_reach_max_callers_send_to,
							q_reach_max_callers_greeting_type = r.q_reach_max_callers_greeting_type,
							q_audio_file_name = r.q_audio_file_name,
							q_audio_content = r.q_audio_content,
							q_last_callend_agent_availablity = r.q_last_callend_agent_availablity == null ? 0 : r.q_last_callend_agent_availablity,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcallqueuegetsettingsOutput outputobj = new UrmyaccountcallqueuegetsettingsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcallqueuegetsettingsOutput outputobj = new UrmyaccountcallqueuegetsettingsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
