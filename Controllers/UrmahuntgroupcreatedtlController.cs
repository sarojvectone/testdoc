using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmahuntgroupcreatedtlController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmahuntgroupcreatedtlInput
        {
			public int company_id { get; set; }
			public int domain_id { get; set; }
			public int order_id { get; set; }
			public string group_name { get; set; }
			public string landline_number { get; set; }
			public float price { get; set; }
            public int group_ext { get; set; }
            public int type { get; set; }
            public int group_id { get; set; }
        }
        public class UrmahuntgroupcreatedtlOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmahuntgroupcreatedtlInput req)
        {
            List<UrmahuntgroupcreatedtlOutput> OutputList = new List<UrmahuntgroupcreatedtlOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_hunt_group_create_dtl";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@domain_id = req.domain_id,
								@order_id = req.order_id,
								@group_name = req.group_name,
								@landline_number = req.landline_number,
								@price = req.price,
                                @group_ext = req.group_ext,
                                @type = req.type,
                                @group_id = req.group_id
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmahuntgroupcreatedtlOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmahuntgroupcreatedtlOutput outputobj = new UrmahuntgroupcreatedtlOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmahuntgroupcreatedtlOutput outputobj = new UrmahuntgroupcreatedtlOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
