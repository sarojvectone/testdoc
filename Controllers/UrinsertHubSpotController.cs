using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;

using System.Text;
using Newtonsoft.Json;
using System.IO;


namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrinsertHubSpotController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger(); 
      
        public class UrcrmusergetdetailsOutput
        {
            public string username { get; set; }
            public string password { get; set; }
            public int user_role { get; set; }
            public string email { get; set; }
            public string firstname { get; set; }
            public string lastname { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string mobileno { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        public class Contact
        {
            public string property { get; set; }
            public string value { get; set; }
        }

        public class Properties
        {
            public List<Contact> properties { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrcrmusergetdetailsOutput req)
        {
            string Url = ConfigurationManager.AppSettings["HUBSPOTCREATECONTACTURL"].ToString();
            List<UrcrmusergetdetailsOutput> OutputList = new List<UrcrmusergetdetailsOutput>();
            try
            {
              //  List<UrcrmusergetdetailsOutput> objResult = GetActiveCustomers();

                Properties objProperties = new Properties();
                List<Contact> objList = new List<Contact>();

                //if (objResult.Count > 0 && objResult[0].errcode != -1)
                //{
                    //foreach (UrcrmusergetdetailsOutput objUrcrmusergetdetailsOutput in objResult)
                    //{
                        CreateProperties(req, out objList);
                        Encoding encoding = new UTF8Encoding();
                        objProperties.properties = objList;
                        string postData = JsonConvert.SerializeObject(objProperties);
                        byte[] data = encoding.GetBytes(postData);
                        HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(Url);
                        httpWReq.ProtocolVersion = HttpVersion.Version11;
                        httpWReq.Method = "POST";
                        httpWReq.ContentType = "application/json";

                        Stream stream = httpWReq.GetRequestStream();
                        stream.Write(data, 0, data.Length);
                        stream.Close();

                        HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                        string s = response.ToString();
                        StreamReader reader = new StreamReader(response.GetResponseStream());
                        String jsonresponse = "";
                        String temp = null;
                        while ((temp = reader.ReadLine()) != null)
                        {
                            jsonresponse += temp;
                        }
                    //}

                //} 
                //using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                //{
                //    conn.Open();
                //    var sp = "ur_app_insert_user_chat_log";
                //    var result = conn.Query<dynamic>(
                //            sp, new
                //            {
                //                @company_id = req.company_id,
                //                @user_from = req.user_from,
                //                @user_to = req.user_to,
                //                @file_type = req.file_type,
                //                @file_desc = req.file_desc 			                                                       
                //            },
                //            commandType: CommandType.StoredProcedure);
                //    if (result != null && result.Count() > 0)
                //    {
                //        OutputList.AddRange(result.Select(r => new UrinsertHubSpotOutput()
                //        {
                //            errcode = r.errcode == null ? 0 : r.errcode,
                //            errmsg = r.errmsg == null ? "Success" : r.errmsg
                //        }));
                //    }
                //    else
                //    {
                //        UrinsertHubSpotOutput outputobj = new UrinsertHubSpotOutput();
                //        outputobj.errcode = -1;
                //        outputobj.errmsg = "No Rec found";                     
                //        OutputList.Add(outputobj);
                //    }
                //}
            }
            catch (Exception ex)
            {
                UrcrmusergetdetailsOutput outputobj = new UrcrmusergetdetailsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
        private static void CreateProperties(UrcrmusergetdetailsOutput objResult, out List<Contact> objList)
        {
            Properties objProperties = new Properties();
            objList = new List<Contact>();

            if (!string.IsNullOrEmpty(objResult.email))
            {
                Contact objemail = new Contact();
                objemail.property = "email";
                objemail.value = objResult.email;
                objProperties.properties = new List<Contact>();
                objList.Add(objemail);


            }
            if (!string.IsNullOrEmpty(objResult.firstname))
            {
                Contact objfirstname = new Contact();
                objfirstname.property = "firstname";
                objfirstname.value = objResult.firstname;
                objList.Add(objfirstname);
            }
            if (!string.IsNullOrEmpty(objResult.lastname))
            {
                Contact objlastname = new Contact();
                objlastname.property = "lastname";
                objlastname.value = objResult.lastname;
                objList.Add(objlastname);
            }
            if (!string.IsNullOrEmpty(objResult.mobileno))
            {
                Contact objphone = new Contact();
                objphone.property = "phone";
                objphone.value = objResult.mobileno;
                objList.Add(objphone);
            }
            if (!string.IsNullOrEmpty(objResult.address1))
            {
                Contact objaddress = new Contact();
                objaddress.property = "address";
                objaddress.value = objResult.address1 + " " + objResult.address2;
                objList.Add(objaddress);
            }
            objProperties.properties = objList;
        }
    }
}
