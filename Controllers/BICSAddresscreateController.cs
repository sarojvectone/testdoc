﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PushSharp.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using Dapper;
using System.Data;

namespace unifiedringmyaccountwebapi.Controllers
{
    public class BICSAddresscreateController : ApiController
    {      
        public class BICSAddressInput
        {
            public string alias { get; set; }
            public string street { get; set; }
            public string streetNumber { get; set; }
            public string box { get; set; }
            public string residence { get; set; }
            public string postalCode { get; set; }
            public string floor { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string country { get; set; }
            public string serviceUsage { get; set; }
            public UserIdentity userIdentity { get; set; }
            public string did_number { get; set; }
        }
        public class BICSAddressOutput : CommonOutput
        {
            public string alias { get; set; }
            public string street { get; set; }
            public string streetNumber { get; set; }
            public string box { get; set; }
            public string residence { get; set; }
            public string floor { get; set; }
            public string postalCode { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string country { get; set; }
            public string reference { get; set; }
            public string status { get; set; }
            public object comment { get; set; }
            public string serviceUsage { get; set; }
        }
        public class BICSAddressOutput1
        {
            public string alias { get; set; }
            public string street { get; set; }
            public string streetNumber { get; set; }
            public string box { get; set; }
            public string residence { get; set; }
            public string floor { get; set; }
            public string postalCode { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string country { get; set; }
            public string reference { get; set; }
            public string status { get; set; }
            public object comment { get; set; }
            public string serviceUsage { get; set; }
            public object addressDocuments { get; set; }
            public UserIdentity userIdentity { get; set; }
            public List<string> allowedLocations { get; set; }
            public List<AllowedLocationsAreaCode> allowedLocationsAreaCodes { get; set; }

        }

        public class AllowedLocationsAreaCode
        {
            public string location { get; set; }
            public string areaCode { get; set; }
            public bool nonGeographic { get; set; }
        }
        public class UserIdentity
        {
            public string emailAddress { get; set; }
            public string companyName { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string vatNumber { get; set; }
            public string phoneNumber { get; set; }
        }
        public class CommonOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        public class BICSErrorResponse
        {
            public string code { get; set; }
            public string description { get; set; }
            public string timestamp { get; set; }
        }
        public class DidnumbergetlcrpricemasterdtlinfoOutput
        {
            public string country { get; set; }
            public string location { get; set; }
            public string area_code { get; set; }
            public double? monthly_fee { get; set; }
            public double? installation_fee { get; set; }
            public string source_name { get; set; }
            public string currency { get; set; }
            public string number_Type { get; set; }
            public string status { get; set; }
            public string iso3 { get; set; }
            public string address_reference { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public class DidnumbergetlcrpricemasterdtlinfoInput
        {
            public string country { get; set; }
            public string location { get; set; }
            public string area_code { get; set; }
        }


        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, BICSAddressInput AddAddressInput)
        {
            BICSAddressOutput1 result1 = new BICSAddressOutput1();
            List<BICSAddressOutput> output = new List<BICSAddressOutput>();                      
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var httpRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["BICSAPIENDPOINT"] + "/addresses");
                string authenticationtoken = ConfigurationManager.AppSettings["BICSAPIKEY"];
                httpRequest.UserAgent = "Fiddler";
                httpRequest.Headers.Add("Authorization", "Bearer " + authenticationtoken);
                httpRequest.Accept = "application/json";
                httpRequest.ContentType = "application/json";
                httpRequest.Timeout = 180000;
                httpRequest.Method = "POST";
                string Bicsaddressresult = "Error";

                using (var streamBicsWriters = new StreamWriter(httpRequest.GetRequestStream()))
                {
                    string jsonBics = new JavaScriptSerializer().Serialize(AddAddressInput);
                    streamBicsWriters.Write(jsonBics);
                    streamBicsWriters.Flush();
                    streamBicsWriters.Dispose();
                    HttpWebResponse httpBicsResponce = (HttpWebResponse)httpRequest.GetResponse();
                    Stream BicsaddressStream = httpBicsResponce.GetResponseStream();
                    StreamReader Bicsaddressreader = new StreamReader(BicsaddressStream);
                    Bicsaddressresult = Bicsaddressreader.ReadToEnd();
                    result1 = JsonConvert.DeserializeObject<BICSAddressOutput1>(Bicsaddressresult);
                    BicsaddressStream.Close();
                    Bicsaddressreader.Close();
                    // Call To DB
                    if (result1.reference != null  )
                    {                        
                        string inputreference = result1.reference;
                        string inputdid_number = "";
                        string inpputfirst_name = result1.userIdentity.firstName;
                        string inputlast_name = result1.userIdentity.lastName;
                        string inputvat_number = result1.userIdentity.vatNumber;
                        string inputcompany_name = result1.userIdentity.companyName;
                        string inputalias_name = result1.alias;
                        string inputstreet = result1.street;
                        string inputstreetNumber = result1.streetNumber;
                        string inputbox = result1.box;
                        string inputresidence = result1.residence;
                        string inputfloor_info = result1.floor;
                        string inputpostal_code = result1.postalCode;
                        string inputcity = result1.city;
                        string inputstate = result1.state;
                        string inputcountry = result1.country;
                        string inputstatus = result1.status;
                        object inputaddressDocuments = result1.addressDocuments;
                        string inputemail_id = result1.userIdentity.emailAddress;
                        using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Didportalapi"].ConnectionString))
                        {
                            conn.Open();
                            var sp = "did_number_insert_address_info";
                            var result = conn.Query<dynamic>(
                                    sp, new
                                    {
                                        reference = inputreference,
                                        did_number = inputdid_number,
                                        first_name = inpputfirst_name,
                                        last_name = inputlast_name,
                                        vat_number =inputvat_number,
                                        company_name = inputcompany_name,
                                        alias_name = inputalias_name,
                                        street = inputstreet,
                                        streetNumber = inputstreetNumber,
                                        box = inputbox,
                                        residence = inputresidence,
                                        floor_info = inputfloor_info,
                                        postal_code = inputpostal_code,
                                        city = inputcity,
                                        state = inputstate,
                                        country = inputcountry,
                                        status = inputstatus,
                                        comment = "Created for BICS address",
                                        //comment = inputcomment,
                                        serviceUsage = "BICS Service",
                                        addressDocuments = "",
                                        email_id = inputemail_id

                                    },
                                    commandType: CommandType.StoredProcedure);
                            if (result != null && result.Count() > 0)
                            {

                            }
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                Log.Error(ex.Message);
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    try
                    {
                        HttpWebResponse err = ex.Response as HttpWebResponse;
                        if (err != null)
                        {
                            string errResponse = new StreamReader(err.GetResponseStream()).ReadToEnd();
                            Log.Info("Exception : Response : " + errResponse);
                            List<BICSErrorResponse> response = JsonConvert.DeserializeObject<List<BICSErrorResponse>>(errResponse);
                            if (response != null && response.Count > 0)
                            {
                                BICSAddressOutput objAddAddressOutput = new BICSAddressOutput();
                                objAddAddressOutput.errcode = -1;
                                objAddAddressOutput.errmsg = response.ElementAt(0).code + ":" + response.ElementAt(0).description;
                                output.Add(objAddAddressOutput);
                                return Request.CreateResponse(HttpStatusCode.OK, output);
                            }
                        }
                    }
                    catch (Exception innerEx)
                    {
                        Log.Error(innerEx.Message);
                        BICSAddressOutput objAddAddressOutput = new BICSAddressOutput();
                        objAddAddressOutput.errcode = -1;
                        objAddAddressOutput.errmsg = innerEx.Message;
                        output.Add(objAddAddressOutput);
                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }

                }
                else
                {
                    BICSAddressOutput objAddAddressOutput = new BICSAddressOutput();
                    objAddAddressOutput.errcode = -1;
                    objAddAddressOutput.errmsg = ex.Message;
                    output.Add(objAddAddressOutput);
                    return Request.CreateResponse(HttpStatusCode.OK, output);
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, result1);
        }

    }
}