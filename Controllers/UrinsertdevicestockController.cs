using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrinsertdevicestockController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrinsertdevicestockInput
        {
            //public int company_id { get; set; }
			public string prod_code { get; set; }
			public int qty { get; set; }
			public int qty_threshold { get; set; }
			public double? buy_price { get; set; }
            public double? ws_price { get; set; }
            public double? ur_price { get; set; }
			//public string brand { get; set; }
            //public double? prod_weight { get; set; }
			public string prod_short_desc { get; set; }
			public string prod_desc { get; set; }
			public string image_path { get; set; }
			public string data_sheet_path { get; set; }
			public string user_guide_path { get; set; }
			public int type { get; set; }
			//public int ds_id { get; set; }  	                   	    
        }
        public class UrinsertdevicestockOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrinsertdevicestockInput req)
        {
            List<UrinsertdevicestockOutput> OutputList = new List<UrinsertdevicestockOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_insert_device_stock";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {							
								@prod_code = req.prod_code,
								@qty = req.qty,
								@qty_threshold = req.qty_threshold,
								@buy_price = req.buy_price,
								@ws_price = req.ws_price,
								@ur_price = req.ur_price,								
								@prod_short_desc = req.prod_short_desc,
								@prod_desc = req.prod_desc,
								@image_path = req.image_path,
								@data_sheet_path = req.data_sheet_path,
								@user_guide_path = req.user_guide_path,
								@type = req.type
								//@ds_id = req.ds_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrinsertdevicestockOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrinsertdevicestockOutput outputobj = new UrinsertdevicestockOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrinsertdevicestockOutput outputobj = new UrinsertdevicestockOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
