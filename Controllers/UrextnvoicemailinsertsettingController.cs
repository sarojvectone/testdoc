using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrextnvoicemailinsertsettingController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrextnvoicemailinsertsettingInput
        {
			public int domain_id { get; set; }
			public int extension { get; set; }
			public int pwd_status { get; set; }
			public string voicemail_pwd { get; set; }
			public int voicemail_status { get; set; }  	                   	    
        }
        public class UrextnvoicemailinsertsettingOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrextnvoicemailinsertsettingInput req)
        {
            List<UrextnvoicemailinsertsettingOutput> OutputList = new List<UrextnvoicemailinsertsettingOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_extn_voicemail_insert_setting";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id,
								@extension = req.extension,
								@pwd_status = req.pwd_status,
								@voicemail_pwd = req.voicemail_pwd,
								@voicemail_status = req.voicemail_status 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrextnvoicemailinsertsettingOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrextnvoicemailinsertsettingOutput outputobj = new UrextnvoicemailinsertsettingOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrextnvoicemailinsertsettingOutput outputobj = new UrextnvoicemailinsertsettingOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
