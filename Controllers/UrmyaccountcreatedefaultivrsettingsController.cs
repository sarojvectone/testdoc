using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountcreatedefaultivrsettingsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcreatedefaultivrsettingsInput
        {
			public int company_id { get; set; }
			public int ivr_type { get; set; }
			public string extension { get; set; }
			public string uu_id { get; set; }
			public string create_by { get; set; }  	                   	    
        }
        public class UrmyaccountcreatedefaultivrsettingsOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcreatedefaultivrsettingsInput req)
        {
            List<UrmyaccountcreatedefaultivrsettingsOutput> OutputList = new List<UrmyaccountcreatedefaultivrsettingsOutput>();
			Log.Info("Input : UrmyaccountcreatedefaultivrsettingsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_create_default_ivr_settings";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@ivr_type = req.ivr_type,
								@extension = req.extension,
								@uu_id = req.uu_id,
								@create_by = req.create_by 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcreatedefaultivrsettingsOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcreatedefaultivrsettingsOutput outputobj = new UrmyaccountcreatedefaultivrsettingsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcreatedefaultivrsettingsOutput outputobj = new UrmyaccountcreatedefaultivrsettingsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
