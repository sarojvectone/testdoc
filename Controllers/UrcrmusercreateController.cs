using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrcrmusercreateController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrcrmusercreateInput
        {
            public string username { get; set; }
            public string password { get; set; }
            public string email { get; set; }
            public int user_role { get; set; }
            public int type { get; set; }
            public int status { get; set; }
            public string  firstname { get; set; }
            public  string lastname { get; set; }
            public  string address1 { get; set; }
            public  string address2 { get; set; }
            public  string mobileno { get; set; }
        }
        public class UrcrmusercreateOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrcrmusercreateInput req)
        {
            Log.Info("Input :  Urcrmusercreate:" + JsonConvert.SerializeObject(req));
            List<UrcrmusercreateOutput> OutputList = new List<UrcrmusercreateOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_crm_user_create";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @username = req.username,
                                @password = req.password,
                                @email = req.email,
                                @user_role = req.user_role,
                                @type = req.type,
                                @status = req.status,
                                @firstname = req.firstname,
                                @lastname  = req.lastname,
                                @address1  = req.address1,
                                @address2  = req.address2,
                                @mobileno = req.mobileno
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrcrmusercreateOutput()
                        {
							errcode = r.errcode == null ? -1 : r.errcode,
							errmsg = r.errmsg == null ? "Failure" : r.errmsg,
                        }));
                    }
                    else
                    {
                        UrcrmusercreateOutput outputobj = new UrcrmusercreateOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrcrmusercreateOutput outputobj = new UrcrmusercreateOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
