using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdesktopprefemailmobilenotificationupdateController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdesktopprefemailmobilenotificationupdateInput
        {
			public int company_id { get; set; }
			public int extn { get; set; }
			public string notification_info { get; set; }  	                   	    
        }
        public class UrdesktopprefemailmobilenotificationupdateOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdesktopprefemailmobilenotificationupdateInput req)
        {
            List<UrdesktopprefemailmobilenotificationupdateOutput> OutputList = new List<UrdesktopprefemailmobilenotificationupdateOutput>();
			Log.Info("Input : UrdesktopprefemailmobilenotificationupdateController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_desktop_pref_email_mobile_notification_update";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@extn = req.extn,
								@notification_info = req.notification_info 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrdesktopprefemailmobilenotificationupdateOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrdesktopprefemailmobilenotificationupdateOutput outputobj = new UrdesktopprefemailmobilenotificationupdateOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrdesktopprefemailmobilenotificationupdateOutput outputobj = new UrdesktopprefemailmobilenotificationupdateOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
