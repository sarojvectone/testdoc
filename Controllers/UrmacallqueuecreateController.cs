using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmacallqueuecreateController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmacallqueuecreateInput
        {
            public int site_id  { get; set; }
			public int order_id { get; set; }
			public int company_id { get; set; }
			public int domain_id { get; set; }
			public int status { get; set; }
			public int callqueue_extension { get; set; }
			public string callqueue_directno { get; set; }
			public string callqueue_name { get; set; }
			public string call_queue_play_file { get; set; }
			public string call_queue_message_file { get; set; }
			public int cq_id { get; set; }
			public int type { get; set; }
            public int? audio_file_type { get; set; }
            public string file_type_content { get; set; }    
        }
        public class UrmacallqueuecreateOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmacallqueuecreateInput req)
        {
            List<UrmacallqueuecreateOutput> OutputList = new List<UrmacallqueuecreateOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_call_queue_create";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@order_id = req.order_id,
								@company_id = req.company_id,
								@domain_id = req.domain_id,
								@status = req.status,
								@callqueue_extension = req.callqueue_extension,
								@callqueue_directno = req.callqueue_directno,
								@callqueue_name = req.callqueue_name,
								@call_queue_play_file = req.call_queue_play_file,
								@call_queue_message_file = req.call_queue_message_file,
								@cq_id = req.cq_id,
								@type = req.type,
                                @audio_file_type = req.audio_file_type,
                                @file_type_content = req.file_type_content,
                                @site_id = req.site_id
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmacallqueuecreateOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrmacallqueuecreateOutput outputobj = new UrmacallqueuecreateOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmacallqueuecreateOutput outputobj = new UrmacallqueuecreateOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
