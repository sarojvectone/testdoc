using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaappgetmutenotificationsinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaappgetmutenotificationsinfoInput
        {
			                    	    
        }
        public class UrmaappgetmutenotificationsinfoOutput
        {
            public int? mute_notif_id { get; set; }
            public string mute_notif_info { get; set; }  
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaappgetmutenotificationsinfoInput req)
        {
            List<UrmaappgetmutenotificationsinfoOutput> OutputList = new List<UrmaappgetmutenotificationsinfoOutput>();
			Log.Info("Input : UrmaappgetmutenotificationsinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_app_get_mute_notifications_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								 		                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmaappgetmutenotificationsinfoOutput()
                        {
                            mute_notif_id = r.mute_notif_id == null ? 0 : r.mute_notif_id,
                            mute_notif_info = r.mute_notif_info,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmaappgetmutenotificationsinfoOutput outputobj = new UrmaappgetmutenotificationsinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmaappgetmutenotificationsinfoOutput outputobj = new UrmaappgetmutenotificationsinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
