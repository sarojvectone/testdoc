using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrextnfindflowmegetController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrextnfindflowmegetInput
        {
			public int domain_id { get; set; }
			public int extension { get; set; }
			public int id { get; set; }  	                   	    
        }
        public class UrextnfindflowmegetOutput
        {
			public int id { get; set; }
			public string name { get; set; }
			public string mobileno { get; set; }
			public DateTime? schedule_from { get; set; }
			public DateTime? schedule_to { get; set; }
			public int status { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrextnfindflowmegetInput req)
        {
            List<UrextnfindflowmegetOutput> OutputList = new List<UrextnfindflowmegetOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_extn_find_flow_me_get";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id,
								@extension = req.extension,
								@id = req.id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrextnfindflowmegetOutput()
                        {
							id = r.id == null ? 0 : r.id,
							name = r.name,
							mobileno = r.mobileno,
							schedule_from = r.schedule_from == null ? null : r.schedule_from,
							schedule_to = r.schedule_to == null ? null : r.schedule_to,
							status = r.status == null ? 0 : r.status,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrextnfindflowmegetOutput outputobj = new UrextnfindflowmegetOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrextnfindflowmegetOutput outputobj = new UrextnfindflowmegetOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
