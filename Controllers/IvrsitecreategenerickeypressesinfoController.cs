using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class IvrsitecreategenerickeypressesinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class IvrsitecreategenerickeypressesinfoInput
        {
			public int company_id { get; set; }
			public int site_id { get; set; }
			public int ivr_id { get; set; }
			public int press_type { get; set; }
			public int press_hash { get; set; }
			public int press_star { get; set; }
			public int caller_noaction { get; set; }
			public int caller_connect_ext { get; set; }
			public int processtype { get; set; }  	                   	    
        }
        public class IvrsitecreategenerickeypressesinfoOutput
        {
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, IvrsitecreategenerickeypressesinfoInput req)
        {
            List<IvrsitecreategenerickeypressesinfoOutput> OutputList = new List<IvrsitecreategenerickeypressesinfoOutput>();
			Log.Info("Input : IvrsitecreategenerickeypressesinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ivr_site_create_generic_key_presses_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id,
								@site_id = req.site_id,
								@ivr_id = req.ivr_id,
								@press_type = req.press_type,
								@press_hash = req.press_hash,
								@press_star = req.press_star,
								@caller_noaction = req.caller_noaction,
								@caller_connect_ext = req.caller_connect_ext,
								@processtype = req.processtype 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new IvrsitecreategenerickeypressesinfoOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        IvrsitecreategenerickeypressesinfoOutput outputobj = new IvrsitecreategenerickeypressesinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                IvrsitecreategenerickeypressesinfoOutput outputobj = new IvrsitecreategenerickeypressesinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
