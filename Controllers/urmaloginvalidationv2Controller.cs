using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
using System.IO;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class urmaloginvalidationv2Controller : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class urmaloginvalidation
        {
            public string login_user_name { get; set; }
            public string login_password { get; set; }
            public string login_source { get; set; }
            public string login_device_id { get; set; }
            public string login_ipaddress { get; set; }
        }
        public class VtmyaccountloginOutput
        {
            public int? error_code { get; set; }
            public string error_msg { get; set; }
            public int? dir_user_id { get; set; }
            public int? domain_id { get; set; }
            public int? ext { get; set; }
            public string cli { get; set; }
            public long? sip_login_id { get; set; }
            public string sip_password { get; set; }
            public string caller_id { get; set; }
            public int? company_id { get; set; }
            public int? enetepriseid { get; set; }
            public int? customer_id { get; set; }
            public string local_number { get; set; }
            public string country_code { get; set; }
            public int? app_log_id { get; set; }
            public int? log_id { get; set; }
            public int? role_id { get; set; }
            public string ext_password { get; set; }
            public string Email { get; set; }
            public string user_status { get; set; }
            // Added for APP
            public string profile_url { get; set; }
            public int? site_id { get; set; }
            public int? order_id { get; set; }
            public string dp_password { get; set; }
            public int? platform { get; set; }
            public string host_address { get; set; }
      
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, urmaloginvalidation req)
        {
            Log.Info("Request : urmaloginvalidationController start..");

            List<VtmyaccountloginOutput> OutputList = new List<VtmyaccountloginOutput>();
            try
            {
                Log.Info("urmaloginvalidationController");
                Log.Info("Input : " + JsonConvert.SerializeObject(req));
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_login_validation";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @login_user_name = req.login_user_name,
                                @login_password = req.login_password,
                                @login_source = req.login_source,
                                @login_device_id = req.login_device_id,
                                @login_ipaddress = req.login_ipaddress,
                               
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Info("Output : " + JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                       

                        OutputList.AddRange(result.Select(r => new VtmyaccountloginOutput()
                        {
                            error_code = r.error_code == null ? 0 : r.error_code,
                            error_msg = r.error_msg == null ? "Success" : r.error_msg,
                            dir_user_id = r.dir_user_id,
                            domain_id = r.domain_id,
                            ext = r.ext,
                            cli = r.cli,
                            sip_login_id = r.sip_login_id,
                            sip_password = r.sip_password,
                            caller_id = r.caller_id,
                            company_id = r.company_id,
                            enetepriseid = r.enetepriseid,
                            customer_id = r.customer_id,
                            local_number = r.local_number,
                            country_code = r.country_code,
                            //app_log_id = r.app_log_id,
                            //Added for APP
                            app_log_id = null,//Based on request from IOS we passed NULL
                            log_id = r.app_log_id == null ? 0 : r.app_log_id,
                            role_id = r.role_id,
                            Email = r.Email,
                            user_status = r.user_status,
                            profile_url = r.profile_url,
                            order_id = r.order_id,
                            site_id  = r.site_id == null ? 0 : r.site_id, // Added on APP Side get Site Id 
                            dp_password = r.dp_password,
                            platform = r.platform == null ? 0 : r.platform,
                            host_address = r.host_address
                        }));

                        if (OutputList[0].error_code == 0)
                        {
                            try
                            {
                                if (ConfigurationManager.AppSettings["ejabberdchat_cluster"] != "1")
                                {
                                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                    var webAddr1 = ConfigurationManager.AppSettings["ejabberdchat_mobileregister_host"];
                                    var httpWebRequest1 = (HttpWebRequest)WebRequest.Create(webAddr1);
                                    httpWebRequest1.ContentType = "application/json";
                                    httpWebRequest1.Method = "POST";
                                    // string json = "{\"userid\": \"" + OutputList[0].sip_id + "\",\"password\": \"" + OutputList[0].sip_password + "\"}";

                                    string json = "{\"userid\": \"" + OutputList[0].app_log_id + "\",\"company_id\": \"" + OutputList[0].company_id + "\",\"host\": \"" + OutputList[0].host_address + "\",\"password\": \"" + OutputList[0].sip_password + "\"}";
                                    Log.Info("Output ejabberdchat_mobileregister_host: Input" + json);
                                    using (var streamWriter = new StreamWriter(httpWebRequest1.GetRequestStream()))
                                    {
                                        streamWriter.Write(json);
                                        streamWriter.Flush();
                                    }
                                    var httpResponse1 = (HttpWebResponse)httpWebRequest1.GetResponse();
                                    Log.Info("Output ejabberdchat_mobileregister_host: " + JsonConvert.SerializeObject(httpResponse1));
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Info("Error ejabberdchat_mobileregister_host: " + ex.Message.ToString());
                            }
                        }

                        //Call ejabberd server end

                    }
                    else
                    {
                        VtmyaccountloginOutput outputobj = new VtmyaccountloginOutput();
                        outputobj.error_code = -1;
                        outputobj.error_msg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                VtmyaccountloginOutput outputobj = new VtmyaccountloginOutput();
                outputobj.error_code = -1;
                outputobj.error_msg = ex.Message;
                OutputList.Add(outputobj);
            }
            Log.Info("Response : urmaloginvalidationController");
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
