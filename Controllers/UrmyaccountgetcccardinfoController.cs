using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmyaccountgetcccardinfoController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountgetcccardinfoInput
        {
			public int company_id { get; set; }  	                   	    
        }
        public class UrmyaccountgetcccardinfoOutput
        {
			public string payment_method { get; set; }
			public string card_holder_name { get; set; }
			public string cc_no { get; set; }
			public string expiry_date { get; set; }
			public string card_type { get; set; }
			public string card_status { get; set; }
            public string bank_name { get; set; }
            public string sort_code { get; set; }
            public int? id { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }

        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountgetcccardinfoInput req)
        {
            List<UrmyaccountgetcccardinfoOutput> OutputList = new List<UrmyaccountgetcccardinfoOutput>();
			Log.Info("Input : UrmyaccountgetcccardinfoController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_myaccount_get_cc_card_info";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountgetcccardinfoOutput()
                        {
							payment_method = r.payment_method,
							card_holder_name = r.card_holder_name,
							cc_no = r.cc_no,
							expiry_date = r.expiry_date,
							card_type = r.card_type,
							card_status = r.card_status,
                            bank_name = r.bank_name,
                            sort_code = r.sort_code,
                            id = r.id == null ? 0 : r.id,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountgetcccardinfoOutput outputobj = new UrmyaccountgetcccardinfoOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountgetcccardinfoOutput outputobj = new UrmyaccountgetcccardinfoOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
