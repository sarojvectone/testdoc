using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrgetextnloginpwdController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrgetextnloginpwdInput
        {
			public int company_id { get; set; }  	                   	    
        }
        public class UrgetextnloginpwdOutput
        {
			public string firstname { get; set; }
			public string Extension_Number { get; set; }
			public string email { get; set; }
			public string MyAcc_Password { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrgetextnloginpwdInput req)
        {
            List<UrgetextnloginpwdOutput> OutputList = new List<UrgetextnloginpwdOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_get_extn_login_pwd";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@company_id = req.company_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrgetextnloginpwdOutput()
                        {
							firstname = r.firstname,
							Extension_Number = r.Extension_Number,
							email = r.email,
							MyAcc_Password = r.MyAcc_Password,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrgetextnloginpwdOutput outputobj = new UrgetextnloginpwdOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrgetextnloginpwdOutput outputobj = new UrgetextnloginpwdOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
