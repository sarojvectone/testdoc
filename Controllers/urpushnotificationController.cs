using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using System.Security.Cryptography.X509Certificates;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class urpushnotificationController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public class PushNotificationInput
        {
            public string sender { get; set; }
            public string receiver { get; set; }
            public string message { get; set; }
            public bool isvoip { get; set; }
            public bool isLive { get; set; }
            public string iuid { get; set; }
        }

        public class PushNotificationOutput : Output
        {

        }

        public class Output
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }

        public HttpResponseMessage Get(string origin, string target, bool isvoip, string media, int iscancel, bool isLive, string iuid)
        {
            Log.Info("urpushnotification -Get");
            PushNotificationOutput output = new PushNotificationOutput();
            List<GetDeviceTokenOutput> resultToken = null;
            try
            {
                Log.Info("Input : origin=" + origin + "&target=" + target + "&isvoip=" + isvoip + "&media=" + media + "&iscancel=" + iscancel + "&iuid=" + iuid);
                //Log.Info("sender : " + origin + " & receiver : " + target);


                bool iscancelled = false;
                if (iscancel != 0)
                    iscancelled = true;

                string ToNumber = target; //GetMobileNo(target);
                if (ToNumber != "")
                {
                    Log.Info("To mobile no : " + ToNumber);
                    string FromNumber = origin.IndexOf("_") > -1 ? origin.Split(new string[] { "_" }, StringSplitOptions.None)[1] : origin;//GetName(origin);origin; //GetMobileNo(origin);

                    Log.Info("From mobile no : " + FromNumber);
                    //Device Token
                    resultToken = GetDeviceToken(ToNumber, isvoip, 1);
                    if (resultToken[0].errcode == -1)
                    {
                        output.errcode = -1;
                        output.errmsg = "device not found";
                        Log.Info("device not found");
                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }
                    if (resultToken.Count < 0)
                    {
                        output.errcode = -1;
                        output.errmsg = "device not found";
                        Log.Info("device not found");
                        return Request.CreateResponse(HttpStatusCode.OK, output);
                    }
                    else
                    {
                        Log.Info("deviceToken : " + JsonConvert.SerializeObject(resultToken));

                        string name;
                        if (origin.IndexOf("_") > -1)
                            name = GetName(origin);//target.Split(new string[] { "_" }, StringSplitOptions.None)[1];//GetName(origin);
                        else
                            name = origin;
                        //name = origin;
                        //if (name.Length == 0)
                        //    name = origin;
                        Log.Info(string.Format("Extension : {0}", name));

                        //Todo : Anees added to get Contact name
                        //FromNumber = GetName(FromNumber);
                        FromNumber = String.IsNullOrEmpty(FromNumber) ? name : FromNumber;

                        String message = (!iscancelled) ? string.Format(APNSHelper.push_message, name) : "Cancelled";
                        //String message = string.Format(APNSHelper.push_message, origin);

                        Log.Info("message : " + message);

                        string p12File = "";
                        string p12FilePwd = "";

                        if (isvoip)
                        {
                            p12File = APNSHelper.voip_key_file;
                            p12FilePwd = APNSHelper.voip_key_pwd;
                        }
                        else
                        {
                            p12File = APNSHelper.message_key_file;
                            p12FilePwd = APNSHelper.message_key_pwd;
                        }

                        ApnsConfiguration config = null;
                        //TODO : Temp need to remove || target.Contains("472") || target.Contains("354")
                        //if (APNSHelper.push_hostname.ToLower() == "gateway.sandbox.push.apple.com" || target.Contains("472") || target.Contains("354") || origin.Contains("472") || origin.Contains("354"))
                        
                        //Temp assigned for testing
                        //if (target.Contains("727"))
                        //{
                        //    isLive = false;

                        //}
                        //Assigned for Sandbox
                        if (resultToken.Count > 0)
                        {
                            try
                            {
                                if (resultToken[0].DeviceIdenty.Contains("Sandbox"))
                                    isLive = false;
                            }
                            catch (Exception ex)
                            {
                                Log.Info("Error push notification message Sandbox: " + ex.Message.ToString());
                                 
                            }
                         
                        }


                        if (isLive)
                        {
                            APNSHelper.push_hostname = APNSHelper.push_hostname.ToLower();
                        }
                        else
                        {
                            APNSHelper.push_hostname = APNSHelper.push_staging_hostname.ToLower();
                        }

                        if (!isLive)
                        {
                            config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, p12File, p12FilePwd, !isvoip);
                        }
                        else
                        {
                            config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, p12File, p12FilePwd, !isvoip);
                        }

                        //ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                        // Create a new broker
                        var apnsBroker = new ApnsServiceBroker(config);
                        string strtoken = null;
                        foreach (GetDeviceTokenOutput token in resultToken)
                        {
                            if (isvoip)
                            {
                                strtoken = token.voip_token;
                            }
                            else
                            {
                                strtoken = token.message_token;
                            }
                            // Wire up events
                            apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                            {
                                aggregateEx.Handle(ex =>
                                {
                                    output.errcode = -1;
                                    // See what kind of exception it was to further diagnose
                                    if (ex is ApnsNotificationException)
                                    {
                                        var notificationException = (ApnsNotificationException)ex;
                                        var apnsNotification = notificationException.Notification;
                                        var statusCode = notificationException.ErrorStatusCode;
                                        Log.Error(ex.InnerException);

                                        //DeActivate Subscription
                                        string result = string.Empty;
                                        if (isvoip)
                                            result = DeactivateSubscription("", strtoken);
                                        else
                                            result = DeactivateSubscription(strtoken, "");

                                        output.errmsg = String.Format("Apple Notification Failed: ID={0}, Code={1}", apnsNotification.Identifier, statusCode);
                                        Log.Error(output.errmsg);
                                        Log.Error(apnsNotification);
                                    }
                                    else
                                    {
                                        output.errmsg = String.Format("Apple Notification Failed for some unknown reason : {0}", ex.InnerException);
                                        Log.Error(output.errmsg);
                                    }
                                    return true;
                                });
                            };


                            apnsBroker.OnNotificationSucceeded += (notification) =>
                            {
                                output.errcode = 0;
                                output.errmsg = "Notification Sent!";
                                Log.Info(output.errmsg);
                            };

                            apnsBroker.Start();

                            string jsonString = "{\"aps\":{\"content-available\":\"" + "1" + "\",\"name\":\"" + name + "\",\"iuid\":\"" + iuid + "\",\"media\":\"" + media + "\",\"UUID\":\"" + strtoken + "\",\"alert\":\"" + message + "\",\"badge\":\"" + APNSHelper.push_badge + "\",\"phoneno\":\"" + FromNumber + "\",\"sound\":\"" + APNSHelper.push_sound + "\"}}";

                            apnsBroker.QueueNotification(new ApnsNotification
                            {
                                DeviceToken = strtoken,
                                Expiration = DateTime.UtcNow,
                                Payload = JObject.Parse(jsonString)
                                //Payload = JObject.Parse("{\"aps\":{\"content-available\":\"" + "1" + "\",\"alert\":\"" + message + "\",\"badge\":\"" + APNSHelper.push_badge + "\",\"sound\":\"" + APNSHelper.push_sound + "\"}}")
                            });
                            apnsBroker.Stop();
                        }
                    }
                }
                else
                {
                    output.errcode = -1;
                    output.errmsg = "mobile no not found";
                    Log.Info(output.errmsg);
                }
            }
            catch (Exception ex)
            {
                //We have to deactivate
                Log.Error(ex.Message);
                output.errcode = -1;
                output.errmsg = ex.Message;
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }

        //[Authorize]
        public HttpResponseMessage Post(PushNotificationInput input)
        {
            Log.Info("urpushnotification -Post");
            PushNotificationOutput output = new PushNotificationOutput();
            List<GetDeviceTokenOutput> resultToken = null;
            if (input == null)
            {
                output.errcode = -1;
                output.errmsg = "input details not found";
            }
            else
            {
                try
                {
                    Log.Info("sender : " + input.sender + " & receiver : " + input.receiver + " & message : " + input.message);
                    string ToNumber = GetMobileNo(input.receiver);
                    if (ToNumber != "")
                    {
                        Log.Info("To mobile no : " + ToNumber);
                        //Device Token
                        resultToken = GetDeviceToken(ToNumber, input.isvoip, 1);
                        if (resultToken[0].errcode == -1)
                        {
                            output.errcode = -1;
                            output.errmsg = "device not found";
                            Log.Info("device not found");
                        }
                        if (resultToken.Count < 0)
                        {
                            output.errcode = -1;
                            output.errmsg = "device not found";
                            Log.Info("device not found");
                        }
                        else
                        {
                            Log.Info("deviceToken : " + resultToken);
                            String message = input.message.Trim().Length == 0 ? string.Format(APNSHelper.push_message, input.sender) : input.message;
                            Log.Info("message : " + message);

                            string[] Names = input.sender.Split('_');//GetName(input.sender);
                            string Name = Names[1];//GetName(Names[1]);
                            Log.Info("Name : " + Name);
                            string FromNumber = GetMobileNo(input.sender);
                            Log.Info("MobileNo : " + FromNumber);

                            string p12File = "";
                            string p12FilePwd = "";
                            if (input.isvoip)
                            {
                                p12File = APNSHelper.voip_key_file;
                                p12FilePwd = APNSHelper.voip_key_pwd;
                            }
                            else
                            {
                                p12File = APNSHelper.message_key_file;
                                p12FilePwd = APNSHelper.message_key_pwd;
                            }

                            ApnsConfiguration config = null;
                            if (APNSHelper.push_hostname.ToLower() == "gateway.sandbox.push.apple.com")
                                config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, p12File, p12FilePwd, !input.isvoip);
                            else
                                config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, p12File, p12FilePwd, !input.isvoip);

                            // Create a new broker
                            var apnsBroker = new ApnsServiceBroker(config);

                            if (output.errcode != -1)
                            {
                                //List<string> tokens = new List<string>();
                                foreach (GetDeviceTokenOutput token in resultToken)
                                {

                                    // Wire up events
                                    apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
                                    {
                                        aggregateEx.Handle(ex =>
                                        {
                                            output.errcode = -1;
                                            // See what kind of exception it was to further diagnose
                                            if (ex is ApnsNotificationException)
                                            {
                                                //DeActivate Subscription
                                                string result = string.Empty;
                                                if (input.isvoip)
                                                    result = DeactivateSubscription("", token.voip_token);
                                                else
                                                    result = DeactivateSubscription(token.message_token, "");
                                                //DeaActivate End

                                                var notificationException = (ApnsNotificationException)ex;
                                                var apnsNotification = notificationException.Notification;
                                                var statusCode = notificationException.ErrorStatusCode;
                                                output.errmsg = String.Format("Apple Notification Failed: ID={0}, Code={1}", apnsNotification.Identifier, statusCode);
                                                Log.Info(output.errmsg);
                                            }
                                            else
                                            {
                                                //DeActivate Subscription
                                                string result = string.Empty;
                                                if (input.isvoip)
                                                    result = DeactivateSubscription("", token.voip_token);
                                                else
                                                    result = DeactivateSubscription(token.message_token, "");
                                                //DeaActivate End
                                                output.errmsg = String.Format("Apple Notification Failed for some unknown reason : {0}", ex.InnerException);
                                                Log.Info(output.errmsg);
                                            }
                                            return true;
                                        });
                                    };

                                    apnsBroker.OnNotificationSucceeded += (notification) =>
                                    {
                                        output.errcode = 0;
                                        output.errmsg = "Notification Sent!";
                                        Log.Info(output.errmsg);
                                    };
                                    apnsBroker.Start();
                                    apnsBroker.QueueNotification(new ApnsNotification
                                    {
                                        DeviceToken = token.message_token,
                                        // Expiration = DateTime.UtcNow, 
                                        Payload = JObject.Parse("{\"aps\":{\"alert\":\"" + message + " @" + Name + "(" + FromNumber + ")" + "\",\"badge\":\"" + APNSHelper.push_badge + "\",\"name\":\"" + Name + "\",\"mobileno\":\"" + FromNumber + "\",\"sound\":\"" + APNSHelper.push_sound + "\"}}")
                                    });

                                    Log.Info("Queue Input : " + JsonConvert.SerializeObject(apnsBroker));
                                    apnsBroker.Stop();
                                }
                            }
                        }
                    }
                    else
                    {
                        output.errcode = -1;
                        output.errmsg = "mobile no not found";
                        Log.Info(output.errmsg);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    output.errcode = -1;
                    output.errmsg = ex.Message;
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, output);
        }
        public class GetDeviceTokenOutput
        {
            public string message_token { get; set; }
            public string voip_token { get; set; }
            public int errcode { get; set; }
            public string errmsg { get; set; }
            public string DeviceIdenty { get; set; }
            
        }
        private List<GetDeviceTokenOutput> GetDeviceToken(string deviceId, bool isvoip, int device_type)
        {
            List<GetDeviceTokenOutput> OutputList = new List<GetDeviceTokenOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["apns_provider"].ConnectionString))
                {
                    conn.Open();
                    var sp = "usp_getdevicetoken";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @DeviceID = deviceId,
                                @device_type = device_type
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        //if (isvoip)
                        //    return result.ElementAt(0).voip_token;
                        //else
                        //    return result.ElementAt(0).message_token;

                        Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new GetDeviceTokenOutput()
                        {
                            message_token = r.message_token,
                            voip_token = r.voip_token,
                            DeviceIdenty = r.DeviceIdenty == null ? "0" : r.DeviceIdenty,
                            errcode = r.errcode == null ? 0 : r.errcode,
                            errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        Log.Info("Output DB : " + "Empty result");
                        GetDeviceTokenOutput outputobj = new GetDeviceTokenOutput();
                        outputobj.errcode = -1;
                        outputobj.errmsg = "No Rec found";
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return OutputList;
        }

        public string DeactivateSubscription(string MessageToken, string VOIPToken)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["apns_provider"].ConnectionString))
                {
                    conn.Open();
                    var sp = "usp_deactivate_devicetoken";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @MessageToken = MessageToken,
                                @VOIPToken = VOIPToken
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        return "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return "Success";
        }

        private string GetName(string accountid)
        {
            string custName = "";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringappapi"].ConnectionString))
                {
                    var sp = "ur_ma_push_notification_get_username";

                    conn.Open();

                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @mobileno = accountid.Split(new string[] { "_" }, StringSplitOptions.None)[1],
                         @domain_id = accountid.Split(new string[] { "_" }, StringSplitOptions.None)[0]
                     },
                     commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0 && result.ElementAt(0).errcode != null && result.ElementAt(0).errcode == 0)
                    {
                        custName = result.ElementAt(0).name;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return custName;
        }

        private string GetMobileNo(string accountid)
        {
            string custName = "";
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MVNO.CTP"].ConnectionString))
                {
                    var sp = "ctp_get_mobileno";

                    conn.Open();

                    var result = conn.Query<dynamic>(
                     sp, new
                     {
                         @accountid = accountid
                     },
                     commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
                        custName = result.ElementAt(0).mobileno;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.InnerException);
                Log.Error(ex.StackTrace);
            }
            return custName;
        }
    }
}
