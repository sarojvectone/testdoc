using System;
using Newtonsoft.Json; 
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi_without_Host.Controllers
{
    public class UrmyaccountcallqueuegetvoicemailsettingsController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmyaccountcallqueuegetvoicemailsettingsInput
        {
			public int? Company_id { get; set; }
			public int? call_queue_id { get; set; }  	                   	    
        }
        public class UrmyaccountcallqueuegetvoicemailsettingsOutput
        {
			public int Vm_call_Transfer_Type { get; set; }
			public string Transfer_ext { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmyaccountcallqueuegetvoicemailsettingsInput req)
        {
            List<UrmyaccountcallqueuegetvoicemailsettingsOutput> OutputList = new List<UrmyaccountcallqueuegetvoicemailsettingsOutput>();
			Log.Info("Input : UrmyaccountcallqueuegetvoicemailsettingsController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "urmyaccount_call_queue_get_voicemail_settings";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@Company_id = req.Company_id,
								@call_queue_id = req.call_queue_id 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmyaccountcallqueuegetvoicemailsettingsOutput()
                        {
							Vm_call_Transfer_Type = r.Vm_call_Transfer_Type == null ? 0 : r.Vm_call_Transfer_Type,
							Transfer_ext = r.Transfer_ext,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmyaccountcallqueuegetvoicemailsettingsOutput outputobj = new UrmyaccountcallqueuegetvoicemailsettingsOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmyaccountcallqueuegetvoicemailsettingsOutput outputobj = new UrmyaccountcallqueuegetvoicemailsettingsOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
