using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaappfavouriteteamsaveController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaappfavouriteteamsaveInput
        {
            public int team_id { get; set; }
            public int input { get; set; }
            public int status { get; set; }
            public int notif_id{ get; set; }
            public int mute_notif_id { get; set; }
            //public int fav_status { get; set; }
            //public int Mute_Notification { get; set; }
            //public int Phone_Notification { get; set; }
            //public int allow_members_to { get; set; }
            //public int post_messages { get; set; }
            //public int add_integrations { get; set; }
            //public int pin_posts { get; set; }
            //public int use_team_mentions { get; set; }

        }
        public class UrmaappfavouriteteamsaveOutput
        {
            public int errcode { get; set; }
            public string errmsg { get; set; }
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaappfavouriteteamsaveInput req)
        {
            List<UrmaappfavouriteteamsaveOutput> OutputList = new List<UrmaappfavouriteteamsaveOutput>();
			Log.Info("Input : UrmaappfavouriteteamsaveController:" + JsonConvert.SerializeObject(req));
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_app_favourite_team_save";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
                                @team_id = req.team_id,
                                @input = req.input,
                                @status = req.status,
                                @mute_notif_id = req.mute_notif_id,
                                @notif_id = req.notif_id
                               // @Phone_Notification = req.Phone_Notification,
                               // @allow_members_to = req.allow_members_to,
                               // @post_messages = req.post_messages,
                               // @add_integrations = req.add_integrations,
                               // @pin_posts = req.pin_posts,
                               // @use_team_mentions = req.use_team_mentions
                            },
                            commandType: CommandType.StoredProcedure);

            

                    if (result != null && result.Count() > 0)
                    {
					  Log.Info("Output DB : " + JsonConvert.SerializeObject(result));
                        OutputList.AddRange(result.Select(r => new UrmaappfavouriteteamsaveOutput()
                        {
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
					  Log.Info("Output DB : " + "Empty result");
                        UrmaappfavouriteteamsaveOutput outputobj = new UrmaappfavouriteteamsaveOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
			    Log.Error(ex.Message);
                UrmaappfavouriteteamsaveOutput outputobj = new UrmaappfavouriteteamsaveOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
