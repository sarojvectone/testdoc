using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
using Newtonsoft.Json;
using System.IO;
using unifiedringmyaccountwebapi.Helpers;
using unifiedringmyaccountwebapi.Models;
//using unifiedringmyaccountwebapi.Helpers;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrmaphoneupdatemacaddressController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrmaphoneupdatemacaddressInput
        {
			public int domain_id { get; set; }
			public int extension { get; set; }
			public string deskphone_mac { get; set; }
			public string deskphone_model { get; set; }

            //public int customer_id { get; set; }
            public string order_number { get; set; }
            //public string order_item { get; set; }
            public string user_login { get; set; }
        }
        public class UrmaphoneupdatemacaddressOutput
        {
            public string Pass { get; set; }
            public string domain_name { get; set; }
            public string admin_password { get; set; }
            public int customer_id { get; set; }
            public int dir_user_id { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }
            public string Prov_URL { get; set; }   
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrmaphoneupdatemacaddressInput req)
        {
            Log.Debug("Urmaphoneupdatemacaddress");
            Log.Debug("Input : {0}", JsonConvert.SerializeObject(req));
            List<UrmaphoneupdatemacaddressOutput> OutputList = new List<UrmaphoneupdatemacaddressOutput>();
            try
            {
                DeskPhoneModel DeskPhoneModelresult = new DeskPhoneModel();
                //DirectoryInfo DirInfo;
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_ma_phone_update_mac_address";
                    Log.Debug("sp : {0}", sp);
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@domain_id = req.domain_id,
								@extension = req.extension,
								@deskphone_mac = req.deskphone_mac,
								@deskphone_model = req.deskphone_model 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    Log.Debug("SP_result : {0}", JsonConvert.SerializeObject(result));
                    if (result != null && result.Count() > 0)
                    {
                        OutputList.AddRange(result.Select(r => new UrmaphoneupdatemacaddressOutput()
                        {
                            customer_id = r.customer_id == null ? 0 : r.customer_id,
                            dir_user_id = r.dir_user_id == null ? 0 : r.dir_user_id,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));

                        if (OutputList.Count != 0 && OutputList[0].errcode == 0)
                        {

                            DeskPhoneModelresult = Deskphone_Configuration.Create_DeskphoneConfig_File(OutputList[0].customer_id, req.order_number, Convert.ToString(req.extension), Convert.ToString(req.extension), req.deskphone_mac, req.user_login, req.deskphone_model, "Update");

                            if (!String.IsNullOrEmpty(DeskPhoneModelresult.vm_password))
                            {
                                //OutputList[0].Pass = DeskphoneConfig_Out.Split(':')[0];
                                //OutputList[0].domain_id = DeskphoneConfig_Out.Split(':')[1];


                                OutputList[0].Pass = DeskPhoneModelresult.vm_password;
                                OutputList[0].domain_name = DeskPhoneModelresult.domain_name;
                                OutputList[0].admin_password = DeskPhoneModelresult.admin_password;
                                OutputList[0].Prov_URL = ConfigurationManager.AppSettings["ProvURL"] + DeskPhoneModelresult.SecurityID + "/";
                                //Update Mac Deskphone
                                //var sp2 = "UR_Myacc_Web_update_mac_deskphone";
                                //Log.Debug("sp2 : {0}", sp2);
                                //var result2 = conn.Query<dynamic>(
                                //        sp2, new
                                //        {
                                //            @dir_user_id = OutputList[0].dir_user_id,
                                //            @deskphone_mac = req.mac
                                //        },
                                //        commandType: CommandType.StoredProcedure);
                                //Log.Debug("SP2_result : {0}", JsonConvert.SerializeObject(result2));
                            }
                            else
                            {
                                ////Revert MAC Address Update
                                //Deskphone_FTP_Info resx = new Deskphone_FTP_Info();
                                //if (req.deskphone_model.ToLower() == "aastra")
                                //{
                                //    resx = OrderBL.Check_RemoteLocation(OutputList[0].customer_id, Convert.ToString(req.extension), req.deskphone_mac, req.user_login, req.deskphone_mac.ToUpper().Trim() + ".cfg");//db hit
                                //}
                                //if (req.deskphone_model.ToLower() == "grandstream")
                                //{
                                //    resx = OrderBL.Check_RemoteLocation(OutputList[0].customer_id, Convert.ToString(req.extension), req.deskphone_mac, req.user_login, req.deskphone_mac.ToUpper().Trim() + ".xml");//db hit
                                //}
                                // if (req.deskphone_model.ToLower() == "cisco")
                                //{
                                //   resx = OrderBL.Check_RemoteLocation(OutputList[0].customer_id, Convert.ToString(req.extension), req.deskphone_mac, req.user_login, req.deskphone_mac.ToUpper().Trim() + ".txt");//db hit
                                // }
                                // if (req.deskphone_model.ToLower() == "polycom")
                                // {
                                //     resx = OrderBL.Check_RemoteLocation(OutputList[0].customer_id, Convert.ToString(req.extension), req.deskphone_mac, req.user_login, req.deskphone_mac.ToUpper().Trim() + ".cfg");//db hit
                                // } 

                                //var sp1 = "ur_ma_phone_update_mac_address";
                                //Log.Debug("sp : {0}", sp);
                                //var result1 = conn.Query<dynamic>(
                                //        sp1, new
                                //        {
                                //            @domain_id = resx.Customer_Id,
                                //            @extension = resx.Ext,
                                //            @deskphone_mac = req.deskphone_mac,
                                //            @deskphone_model = req.deskphone_model
                                //        },
                                //        commandType: CommandType.StoredProcedure);
                                //Log.Debug("SP_result : {0}", JsonConvert.SerializeObject(result1));
                                //if (result1 != null && result1.Count() > 0)
                                //{
                                //    Log.Debug("ur_ma_phone_update_mac_address: {0}", "");
                                //    OutputList = new List<UrmaphoneupdatemacaddressOutput>();
                                //    UrmaphoneupdatemacaddressOutput Reverted = new UrmaphoneupdatemacaddressOutput();
                                //    Reverted.errcode = -1;
                                //    Reverted.errmsg = "Failed to provision: File not found";
                                //    OutputList.Add(Reverted);

                                //}
                                OutputList = new List<UrmaphoneupdatemacaddressOutput>();
                                UrmaphoneupdatemacaddressOutput Reverted = new UrmaphoneupdatemacaddressOutput();
                                Reverted.errcode = -1;
                                Reverted.errmsg = "Failed to provision: File not found";
                                OutputList.Add(Reverted);
                            }
                        }

                        
                    }
                    else
                    {
                        UrmaphoneupdatemacaddressOutput outputobj = new UrmaphoneupdatemacaddressOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrmaphoneupdatemacaddressOutput outputobj = new UrmaphoneupdatemacaddressOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
