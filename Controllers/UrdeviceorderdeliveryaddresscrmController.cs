using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Dapper;
using NLog;
using System.Data.SqlTypes;
namespace Unifiedringmyaccountwebapi.Controllers
{
    public class UrdeviceorderdeliveryaddresscrmController : ApiController
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public class UrdeviceorderdeliveryaddresscrmInput
        {
			public int orderid { get; set; }  	                   	    
        }
        public class UrdeviceorderdeliveryaddresscrmOutput
        {
			public string d_customer_name { get; set; }
			public string d_address1 { get; set; }
			public string d_address2 { get; set; }
			public string d_city { get; set; }
			public string d_postcode { get; set; }
			public string d_country { get; set; }
			public string d_email { get; set; }
			public string d_mobileno { get; set; }
			public string d_company_name { get; set; }
			public string d_state { get; set; }
			public double? d_shipping_charge { get; set; }
			public double? d_shipping_tax_fee { get; set; }
            public double?  tot_order_amt { get; set; }
            public double?  tot_tax_amt { get; set; }
			public string b_customer_name { get; set; }
			public string b_address1 { get; set; }
			public string b_address2 { get; set; }
			public string b_city { get; set; }
			public string b_postcode { get; set; }
			public string b_country { get; set; }
			public string b_email { get; set; }
			public string b_mobileno { get; set; }
			public string b_company_name { get; set; }
			public string b_state { get; set; }
            public DateTime? orderdate { get; set; }
            public DateTime? delivery_date { get; set; }
            public string  delivery_comment { get; set; }
            public string comment { get; set; }
			public int errcode { get; set; }
			public string errmsg { get; set; }	            	    
        }
        [Authorize]
        public async Task<HttpResponseMessage> Post(string id, HttpRequestMessage request, UrdeviceorderdeliveryaddresscrmInput req)
        {
            List<UrdeviceorderdeliveryaddresscrmOutput> OutputList = new List<UrdeviceorderdeliveryaddresscrmOutput>();
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["unifiedringmyaccountwebapi"].ConnectionString))
                {
                    conn.Open();
                    var sp = "ur_device_order_delivery_address_crm";
                    var result = conn.Query<dynamic>(
                            sp, new
                            {
								@orderid = req.orderid 			                                                       
                            },
                            commandType: CommandType.StoredProcedure);
                    if (result != null && result.Count() > 0)
                    {

                        OutputList.AddRange(result.Select(r => new UrdeviceorderdeliveryaddresscrmOutput()
                        {
							d_customer_name = r.d_customer_name,
							d_address1 = r.d_address1,
							d_address2 = r.d_address2,
							d_city = r.d_city,
							d_postcode = r.d_postcode,
							d_country = r.d_country,
							d_email = r.d_email,
							d_mobileno = r.d_mobileno,
							d_company_name = r.d_company_name,
							d_state = r.d_state,
                            tot_order_amt = r.tot_order_amt == null ? 0.0D : r.tot_order_amt,
                            tot_tax_amt = r.tot_tax_amt == null ? 0.0D : r.tot_tax_amt,
                            d_shipping_charge = r.d_shipping_charge == null ? 0.0D : r.d_shipping_charge,
							d_shipping_tax_fee = r.d_shipping_tax_fee == null ? 0.0D : r.d_shipping_tax_fee,
							b_customer_name = r.b_customer_name,
							b_address1 = r.b_address1,
							b_address2 = r.b_address2,
							b_city = r.b_city,
							b_postcode = r.b_postcode,
							b_country = r.b_country,
							b_email = r.b_email,
							b_mobileno = r.b_mobileno,
							b_company_name = r.b_company_name,
							b_state = r.b_state,
                            orderdate = r.orderdate == null ? null : r.orderdate,
                            delivery_date = r.delivery_date == null ? null : r.delivery_date,
                            delivery_comment = r.delivery_comment,
                            comment = r.comment,
							errcode = r.errcode == null ? 0 : r.errcode,
							errmsg = r.errmsg == null ? "Success" : r.errmsg
                        }));
                    }
                    else
                    {
                        UrdeviceorderdeliveryaddresscrmOutput outputobj = new UrdeviceorderdeliveryaddresscrmOutput();
						outputobj.errcode = -1;
						outputobj.errmsg = "No Rec found";                     
                        OutputList.Add(outputobj);
                    }
                }
            }
            catch (Exception ex)
            {
                UrdeviceorderdeliveryaddresscrmOutput outputobj = new UrdeviceorderdeliveryaddresscrmOutput();
				outputobj.errcode = -1;
				outputobj.errmsg = ex.Message;
                OutputList.Add(outputobj);
            }
            return Request.CreateResponse(HttpStatusCode.OK, OutputList);
        }
    }
}
