using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
namespace unifiedringmyaccountwebapi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
         
            config.Routes.MapHttpRoute(
            "URINSERTLINKACCOUNTTOGA",
            routeTemplate: "v1/user/{id}/Urinsertlinkaccounttoga",
            defaults: new { controller = "Urinsertlinkaccounttoga" });
            
              config.Routes.MapHttpRoute(
            "URAPPTEAMCLOSECONVERSATION",
            routeTemplate: "v1/user/{id}/Urappteamcloseconversation",
            defaults: new { controller = "Urappteamcloseconversation" });
           
            config.Routes.MapHttpRoute(
            "URAPPLEAVETEAM",
            routeTemplate: "v1/user/{id}/Urappleaveteam",
            defaults: new { controller = "Urappleaveteam" });
            	
             config.Routes.MapHttpRoute(
            "URAPPDELETETEAM",
            routeTemplate: "v1/user/{id}/Urappdeleteteam",
            defaults: new { controller = "Urappdeleteteam" });

            config.Routes.MapHttpRoute(
            "URBILLGETSERVICEPLANAPP",
            routeTemplate: "v1/user/{id}/UrbillgetserviceplanApp",
            defaults: new { controller = "UrbillgetserviceplanApp" });

            config.Routes.MapHttpRoute(
            "URCRMGETASSIGNTOUSER",
            routeTemplate: "v1/user/{id}/Urcrmgetassigntouser",
            defaults: new { controller = "Urcrmgetassigntouser" });

            config.Routes.MapHttpRoute(
            "URCRMUSERCREATE",
            routeTemplate: "v1/user/{id}/Urcrmusercreate",
            defaults: new { controller = "Urcrmusercreate" });              
          
            config.Routes.MapHttpRoute(
            "TELSERVORDERNUMBER",
            routeTemplate: "v1/user/{id}/Telservordernumber",
            defaults: new { controller = "Telservordernumber" });  
        
            config.Routes.MapHttpRoute(
            "BICSORDERNUMBERS",
            routeTemplate: "v1/user/{id}/BICSOrderNumbers",
            defaults: new { controller = "BICSOrderNumbers" });
                         
            config.Routes.MapHttpRoute(
            "DIDWWORDERNUMBER",
            routeTemplate: "v1/user/{id}/Didwwordernumber",
            defaults: new { controller = "Didwwordernumber" });

            config.Routes.MapHttpRoute(
            "BICSADDRESSCREATE",
            routeTemplate: "v1/user/{id}/BICSAddresscreate",
            defaults: new { controller = "BICSAddresscreate" });
            
            config.Routes.MapHttpRoute(
            "BICSUPDATEADDRESS",
            routeTemplate: "v1/user/{id}/BicsupdateAddress",
            defaults: new { controller = "BicsupdateAddress" });

            config.Routes.MapHttpRoute(
            "URCOMPANYSITEDELETE",
            routeTemplate: "v1/user/{id}/Urcompanysitedelete",
            defaults: new { controller = "Urcompanysitedelete" });

            config.Routes.MapHttpRoute(
            "URCRMGETALLUSER",
            routeTemplate: "v1/user/{id}/Urcrmgetalluser",
            defaults: new { controller = "Urcrmgetalluser" }); 
           
            config.Routes.MapHttpRoute(
            "DIDNUMBERGETLCRPRICEMASTERDTLINFO",
            routeTemplate: "v1/user/{id}/Didnumbergetlcrpricemasterdtlinfo",
            defaults: new { controller = "Didnumbergetlcrpricemasterdtlinfo" });


            config.Routes.MapHttpRoute(
            "URCRMGETALLCOMPANYINFO",
            routeTemplate: "v1/user/{id}/Urcrmgetallcompanyinfo",
            defaults: new { controller = "Urcrmgetallcompanyinfo" });

            config.Routes.MapHttpRoute(
            "URCRMUSERLOGIN",
            routeTemplate: "v1/user/{id}/Urcrmuserlogin",
            defaults: new { controller = "Urcrmuserlogin" });

            config.Routes.MapHttpRoute(
            "URBILLGETSERVICEPLAN",
            routeTemplate: "v1/user/{id}/Urbillgetserviceplan",
            defaults: new { controller = "Urbillgetserviceplan" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBINSERTCALLHANDLINGFORWARDING",
            routeTemplate: "v1/user/{id}/Urmyaccwebinsertcallhandlingforwarding",
            defaults: new { controller = "Urmyaccwebinsertcallhandlingforwarding" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBGETCALLHANDLINGFORWARDING",
            routeTemplate: "v1/user/{id}/Urmyaccwebgetcallhandlingforwarding",
            defaults: new { controller = "Urmyaccwebgetcallhandlingforwarding" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBGETCALLSRINGTYPE",
            routeTemplate: "v1/user/{id}/Urmyaccwebgetcallsringtype",
            defaults: new { controller = "Urmyaccwebgetcallsringtype" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBGETCALLSFORWARDTYPE",
            routeTemplate: "v1/user/{id}/Urmyaccwebgetcallsforwardtype",
            defaults: new { controller = "Urmyaccwebgetcallsforwardtype" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBINSERTAMENDMENTORDER",
            routeTemplate: "v1/user/{id}/Urmyaccwebinsertamendmentorder",
            defaults: new { controller = "Urmyaccwebinsertamendmentorder" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBGETCOMPANYADDRESS",
            routeTemplate: "v1/user/{id}/Urmyaccwebgetcompanyaddress",
            defaults: new { controller = "Urmyaccwebgetcompanyaddress" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBADDUSERACCOUNTSTATUS",
            routeTemplate: "v1/user/{id}/Urmyaccwebadduseraccountstatus",
            defaults: new { controller = "Urmyaccwebadduseraccountstatus" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBLOGIN",
            routeTemplate: "v1/user/{id}/Urmyaccweblogin",
            defaults: new { controller = "Urmyaccweblogin" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBUSERROLE",
            routeTemplate: "v1/user/{id}/Urmyaccwebuserrole",
            defaults: new { controller = "Urmyaccwebuserrole" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBUSERORDEREXTENSION",
            routeTemplate: "v1/user/{id}/Urmyaccwebuserorderextension",
            defaults: new { controller = "Urmyaccwebuserorderextension" });                   
             config.Routes.MapHttpRoute(
            "URGETUSERHISTORYLOG",
            routeTemplate: "v1/user/{id}/URgetuserhistorylog",
            defaults: new { controller = "URgetuserhistorylog" });
            config.Routes.MapHttpRoute(
            "URPHONEUPDATENUMBERTYPE",
            routeTemplate: "v1/user/{id}/Urphoneupdatenumbertype",
            defaults: new { controller = "Urphoneupdatenumbertype" });        
            config.Routes.MapHttpRoute(
            "URNOTIFICATIONGETCUSTOMERBILLINFO",
            routeTemplate: "v1/user/{id}/Urnotificationgetcustomerbillinfo",
            defaults: new { controller = "Urnotificationgetcustomerbillinfo" });  
            config.Routes.MapHttpRoute(
            "URECOMINSERTUSEREXTENSIONDTL",
            routeTemplate: "v1/user/{id}/Urecominsertuserextensiondtl",
            defaults: new { controller = "Urecominsertuserextensiondtl" });  
        
            config.Routes.MapHttpRoute(
            "URPHONEGETNUMBERTYPE",
            routeTemplate: "v1/user/{id}/Urphonegetnumbertype",
            defaults: new { controller = "Urphonegetnumbertype" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBINSERTAUDIOFILE",
            routeTemplate: "v1/user/{id}/Urmyaccwebinsertaudiofile",
            defaults: new { controller = "Urmyaccwebinsertaudiofile" });

             config.Routes.MapHttpRoute(
            "URGETNEWSITEINFO",
            routeTemplate: "v1/user/{id}/URGetnewsiteinfo",
            defaults: new { controller = "URGetnewsiteinfo" });
           
             config.Routes.MapHttpRoute(
            "URADDNEWSITEINFO",
            routeTemplate: "v1/user/{id}/URaddnewsiteinfo",
            defaults: new { controller = "URaddnewsiteinfo" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBGETAUDIOFILE",
            routeTemplate: "v1/user/{id}/Urmyaccwebgetaudiofile",
            defaults: new { controller = "Urmyaccwebgetaudiofile" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBGETIVRSETTING",
            routeTemplate: "v1/user/{id}/Urmyaccwebgetivrsetting",
            defaults: new { controller = "Urmyaccwebgetivrsetting" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBINSERTCOMPANYWORKINGHOURS",
            routeTemplate: "v1/user/{id}/Urmyaccwebinsertcompanyworkinghours",
            defaults: new { controller = "Urmyaccwebinsertcompanyworkinghours" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBGETCOMPANYWORKINGHOURS",
            routeTemplate: "v1/user/{id}/Urmyaccwebgetcompanyworkinghours",
            defaults: new { controller = "Urmyaccwebgetcompanyworkinghours" });            
            
            config.Routes.MapHttpRoute(
            "URHUBSPOTSENDSMS",
            routeTemplate: "v1/user/urhubspotsendsms/{id}",
            defaults: new { controller = "urhubspotsendsms" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBGETUSEREXTENSIONINFO",
            routeTemplate: "v1/user/{id}/Urmyaccwebgetuserextensioninfo",
            defaults: new { controller = "Urmyaccwebgetuserextensioninfo" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBUPDATEUSEREXTENSIONDTL",
            routeTemplate: "v1/user/{id}/Urmyaccwebupdateuserextensiondtl",
            defaults: new { controller = "Urmyaccwebupdateuserextensiondtl" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBPHONEGETNUMBER",
            routeTemplate: "v1/user/{id}/Urmyaccwebphonegetnumber",
            defaults: new { controller = "Urmyaccwebphonegetnumber" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBIVRGETMUSICTYPE",
            routeTemplate: "v1/user/{id}/Urmyaccwebivrgetmusictype",
            defaults: new { controller = "Urmyaccwebivrgetmusictype" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBIVRINSERTEXTENTIONAUDIOFILE",
            routeTemplate: "v1/user/{id}/Urmyaccwebivrinsertextentionaudiofile",
            defaults: new { controller = "Urmyaccwebivrinsertextentionaudiofile" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBIVRGETEXTENTIONAUDIOFILE",
            routeTemplate: "v1/user/{id}/Urmyaccwebivrgetextentionaudiofile",
            defaults: new { controller = "Urmyaccwebivrgetextentionaudiofile" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBVOICEMSGNGETEXTENTIONAUDIOFILE",
            routeTemplate: "v1/user/{id}/Urmyaccwebvoicemsgngetextentionaudiofile",
            defaults: new { controller = "Urmyaccwebvoicemsgngetextentionaudiofile" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBVOICEMSGNINSERTEXTENTIONAUDIOFILE",
            routeTemplate: "v1/user/{id}/Urmyaccwebvoicemsgninsertextentionaudiofile",
            defaults: new { controller = "Urmyaccwebvoicemsgninsertextentionaudiofile" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBGETCOMPANYDTL",
            routeTemplate: "v1/user/{id}/Urmyaccwebgetcompanydtl",
            defaults: new { controller = "Urmyaccwebgetcompanydtl" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBUPDATECOMPANYDTL",
            routeTemplate: "v1/user/{id}/Urmyaccwebupdatecompanydtl",
            defaults: new { controller = "Urmyaccwebupdatecompanydtl" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBUSEREXTESIONDELETE",
            routeTemplate: "v1/user/{id}/Urmyaccwebuserextesiondelete",
            defaults: new { controller = "Urmyaccwebuserextesiondelete" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBGETCALLERIDNUMBERS",
            routeTemplate: "v1/user/{id}/Urmyaccwebgetcalleridnumbers",
            defaults: new { controller = "Urmyaccwebgetcalleridnumbers" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBGETUPDATECALLERIDNUMBER",
            routeTemplate: "v1/user/{id}/Urmyaccwebgetupdatecalleridnumber",
            defaults: new { controller = "Urmyaccwebgetupdatecalleridnumber" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBGETGETCALLERIDDTL",
            routeTemplate: "v1/user/{id}/Urmyaccwebgetgetcalleriddtl",
            defaults: new { controller = "Urmyaccwebgetgetcalleriddtl" });


            config.Routes.MapHttpRoute(
            "URMYACCWEBPROVISIONORDERDESKPHONE",
            routeTemplate: "v1/user/{id}/Urmyaccwebprovisionorderdeskphone",
            defaults: new { controller = "Urmyaccwebprovisionorderdeskphone" });

            config.Routes.MapHttpRoute(
            "URMAPHONEADDNUMBERGETNUMBERDTL",
            routeTemplate: "v1/user/{id}/Urmaphoneaddnumbergetnumberdtl",
            defaults: new { controller = "Urmaphoneaddnumbergetnumberdtl" });

            config.Routes.MapHttpRoute(
            "URMAINSERTPHONEADDNUMBERDTL",
            routeTemplate: "v1/user/{id}/Urmainsertphoneaddnumberdtl",
            defaults: new { controller = "Urmainsertphoneaddnumberdtl" });

            config.Routes.MapHttpRoute(
            "URMAASSIGNUNASSIGNPHONEADDNUMBERDTL",
            routeTemplate: "v1/user/{id}/Urmaassignunassignphoneaddnumberdtl",
            defaults: new { controller = "Urmaassignunassignphoneaddnumberdtl" });

            config.Routes.MapHttpRoute(
            "URMAGETCOMPANYEXTENSIONDTL",
            routeTemplate: "v1/user/{id}/Urmagetcompanyextensiondtl",
            defaults: new { controller = "Urmagetcompanyextensiondtl" });

            config.Routes.MapHttpRoute(
            "URASSIGNPHONEGETNUMBERDTL",
            routeTemplate: "v1/user/{id}/Urassignphonegetnumberdtl",
            defaults: new { controller = "Urassignphonegetnumberdtl" });
            config.Routes.MapHttpRoute(
            "URMAPHONENUMBERGETCOUNTRY",
            routeTemplate: "v1/user/{id}/Urmaphonenumbergetcountry",
            defaults: new { controller = "Urmaphonenumbergetcountry" });

            config.Routes.MapHttpRoute(
            "URMAPHONENUMBERGETCOUNTRYSTATE",
            routeTemplate: "v1/user/{id}/Urmaphonenumbergetcountrystate",
            defaults: new { controller = "Urmaphonenumbergetcountrystate" });

            config.Routes.MapHttpRoute(
            "URMAPARKLOCATIONGETGROUPMEMBERS",
            routeTemplate: "v1/user/{id}/Urmaparklocationgetgroupmembers",
            defaults: new { controller = "Urmaparklocationgetgroupmembers" });

            config.Routes.MapHttpRoute(
            "URMAPARKLOCATIONUPDATEGROUPMEMBERS",
            routeTemplate: "v1/user/{id}/Urmaparklocationupdategroupmembers",
            defaults: new { controller = "Urmaparklocationupdategroupmembers" });

            config.Routes.MapHttpRoute(
            "URMAINSERTPHONEADDNUMBER",
            routeTemplate: "v1/user/{id}/Urmainsertphoneaddnumber",
            defaults: new { controller = "Urmainsertphoneaddnumber" });

            config.Routes.MapHttpRoute(
            "URPHONECOMPANYGETFREENUMBER",
            routeTemplate: "v1/user/{id}/Urphonecompanygetfreenumber",
            defaults: new { controller = "Urphonecompanygetfreenumber" });

            config.Routes.MapHttpRoute(
            "URMAANNOUNCEMENTINSERTEXTENSION",
            routeTemplate: "v1/user/{id}/Urmaannouncementinsertextension",
            defaults: new { controller = "Urmaannouncementinsertextension" });

            config.Routes.MapHttpRoute(
            "URMAANNOUNCEMENTUPDATEEXTENSION",
            routeTemplate: "v1/user/{id}/Urmaannouncementupdateextension",
            defaults: new { controller = "Urmaannouncementupdateextension" });

            config.Routes.MapHttpRoute(
            "URMAANNOUNCEMENTGETEXTENSION",
            routeTemplate: "v1/user/{id}/Urmaannouncementgetextension",
            defaults: new { controller = "Urmaannouncementgetextension" });

            config.Routes.MapHttpRoute(
            "URMACALLBLASTDETAILSSAVE",
            routeTemplate: "v1/user/{id}/Urmacallblastdetailssave",
            defaults: new { controller = "Urmacallblastdetailssave" });

            config.Routes.MapHttpRoute(
            "URMACALLBLASTGETDTL",
            routeTemplate: "v1/user/{id}/Urmacallblastgetdtl",
            defaults: new { controller = "Urmacallblastgetdtl" });

            config.Routes.MapHttpRoute(
            "URMACALLBLASTDELETEDTL",
            routeTemplate: "v1/user/{id}/Urmacallblastdeletedtl",
            defaults: new { controller = "Urmacallblastdeletedtl" });

            config.Routes.MapHttpRoute(
            "URMAHUNTGROUPCREATEDTL",
            routeTemplate: "v1/user/{id}/Urmahuntgroupcreatedtl",
            defaults: new { controller = "Urmahuntgroupcreatedtl" });

            config.Routes.MapHttpRoute(
            "URMAHUNTGROUPSGETLIST",
            routeTemplate: "v1/user/{id}/Urmahuntgroupsgetlist",
            defaults: new { controller = "Urmahuntgroupsgetlist" });

            config.Routes.MapHttpRoute(
            "URMAHUNTGROUPSGETRINGSTYPE",
            routeTemplate: "v1/user/{id}/Urmahuntgroupsgetringstype",
            defaults: new { controller = "Urmahuntgroupsgetringstype" });

            config.Routes.MapHttpRoute(
            "URMAHUNTGROUPSADDCALLMEMBER",
            routeTemplate: "v1/user/{id}/Urmahuntgroupsaddcallmember",
            defaults: new { controller = "Urmahuntgroupsaddcallmember" });

            config.Routes.MapHttpRoute(
            "URMAHUNTGROUPSREMOVECALLMEMBER",
            routeTemplate: "v1/user/{id}/Urmahuntgroupsremovecallmember",
            defaults: new { controller = "Urmahuntgroupsremovecallmember" });

            config.Routes.MapHttpRoute(
            "URMAHUNTGROUPSUPDATEGROUPTYPE",
            routeTemplate: "v1/user/{id}/Urmahuntgroupsupdategrouptype",
            defaults: new { controller = "Urmahuntgroupsupdategrouptype" });

            config.Routes.MapHttpRoute(
            "URMAHUNTGROUPGETCALLMEMBERS",
            routeTemplate: "v1/user/{id}/Urmahuntgroupgetcallmembers",
            defaults: new { controller = "Urmahuntgroupgetcallmembers" });

            config.Routes.MapHttpRoute(
            "URMAIVROPERATOREXTENSIONUPDATE",
            routeTemplate: "v1/user/{id}/Urmaivroperatorextensionupdate",
            defaults: new { controller = "Urmaivroperatorextensionupdate" });

            config.Routes.MapHttpRoute(
            "URMAIVROPERATORGETEXTENSION",
            routeTemplate: "v1/user/{id}/Urmaivroperatorgetextension",
            defaults: new { controller = "Urmaivroperatorgetextension" });

            config.Routes.MapHttpRoute(
            "URADDPHONEGETDIRECTNUMBER",
            routeTemplate: "v1/user/{id}/Uraddphonegetdirectnumber",
            defaults: new { controller = "Uraddphonegetdirectnumber" });

            config.Routes.MapHttpRoute(
            "URMACALLQUEUEGETDIRECTNUMBER",
            routeTemplate: "v1/user/{id}/Urmacallqueuegetdirectnumber",
            defaults: new { controller = "Urmacallqueuegetdirectnumber" });

            config.Routes.MapHttpRoute(
            "URMACALLQUEUECREATE",
            routeTemplate: "v1/user/{id}/Urmacallqueuecreate",
            defaults: new { controller = "Urmacallqueuecreate" });

            config.Routes.MapHttpRoute(
            "URMACALLQUEUEGETDTL",
            routeTemplate: "v1/user/{id}/Urmacallqueuegetdtl",
            defaults: new { controller = "Urmacallqueuegetdtl" });

            config.Routes.MapHttpRoute(
            "URMACALLBLASTGETFILENAME",
            routeTemplate: "v1/user/{id}/Urmacallblastgetfilename",
            defaults: new { controller = "Urmacallblastgetfilename" });

            config.Routes.MapHttpRoute(
            "URMAGROUPOTHERCOUNT",
            routeTemplate: "v1/user/{id}/Urmagroupothercount",
            defaults: new { controller = "Urmagroupothercount" });

            config.Routes.MapHttpRoute(
            "URADDTEMPLATESETTING",
            routeTemplate: "v1/user/{id}/Uraddtemplatesetting",
            defaults: new { controller = "Uraddtemplatesetting" });

            config.Routes.MapHttpRoute(
            "URUPDATETEMPLATEINFO",
            routeTemplate: "v1/user/{id}/Urupdatetemplateinfo",
            defaults: new { controller = "Urupdatetemplateinfo" });

            config.Routes.MapHttpRoute(
            "URDELETETEMPLATE",
            routeTemplate: "v1/user/{id}/Urdeletetemplate",
            defaults: new { controller = "Urdeletetemplate" });

            config.Routes.MapHttpRoute(
            "URMAUSERTEMPLATEGETREGIONALSETTING",
            routeTemplate: "v1/user/{id}/Urmausertemplategetregionalsetting",
            defaults: new { controller = "Urmausertemplategetregionalsetting" });

            config.Routes.MapHttpRoute(
            "URMAUSERTEMPLATEUPDATEREGIONALSETTING",
            routeTemplate: "v1/user/{id}/Urmausertemplateupdateregionalsetting",
            defaults: new { controller = "Urmausertemplateupdateregionalsetting" });

            config.Routes.MapHttpRoute(
            "URMADIALBYDIRUPDATEDTL",
            routeTemplate: "v1/user/{id}/Urmadialbydirupdatedtl",
            defaults: new { controller = "Urmadialbydirupdatedtl" });

            config.Routes.MapHttpRoute(
            "URMADIALBYDIRGETDTL",
            routeTemplate: "v1/user/{id}/Urmadialbydirgetdtl",
            defaults: new { controller = "Urmadialbydirgetdtl" });

            config.Routes.MapHttpRoute(
            "URMAPHONEUPDATEMACADDRESS",
            routeTemplate: "v1/user/{id}/Urmaphoneupdatemacaddress",
            defaults: new { controller = "Urmaphoneupdatemacaddress" });

            config.Routes.MapHttpRoute(
            "URMAUPDATEPROVISIONFILEDTL",
            routeTemplate: "v1/user/{id}/Urmaupdateprovisionfiledtl",
            defaults: new { controller = "Urmaupdateprovisionfiledtl" });

            config.Routes.MapHttpRoute(
            "URMAGETDOMAINNAME",
            routeTemplate: "v1/user/{id}/Urmagetdomainname",
            defaults: new { controller = "Urmagetdomainname" });

            config.Routes.MapHttpRoute(
            "URMAUSERGETDEPARTMENT",
            routeTemplate: "v1/user/{id}/Urmausergetdepartment",
            defaults: new { controller = "Urmausergetdepartment" });

            config.Routes.MapHttpRoute(
            "URUSEREXTNINSERTBLOCKNUMBER",
            routeTemplate: "v1/user/{id}/Uruserextninsertblocknumber",
            defaults: new { controller = "Uruserextninsertblocknumber" });

            config.Routes.MapHttpRoute(
        "URUSEREXTNINSERTBLOCKNUMBERAPP",
        routeTemplate: "v1/user/{id}/UruserextninsertblocknumberApp",
        defaults: new { controller = "UruserextninsertblocknumberApp" });

            config.Routes.MapHttpRoute(
            "URUSEREXTNGETBLOCKNUMBER",
            routeTemplate: "v1/user/{id}/Uruserextngetblocknumber",
            defaults: new { controller = "Uruserextngetblocknumber" });

            config.Routes.MapHttpRoute(
            "UREXTNVOICEMAILINSERTSETTING",
            routeTemplate: "v1/user/{id}/Urextnvoicemailinsertsetting",
            defaults: new { controller = "Urextnvoicemailinsertsetting" });

            config.Routes.MapHttpRoute(
            "URMAEXTNUPDATEAPPLOGINPWD",
            routeTemplate: "v1/user/{id}/Urmaextnupdateapploginpwd",
            defaults: new { controller = "Urmaextnupdateapploginpwd" });

            config.Routes.MapHttpRoute(
            "URMAEXTNUPDATEAPPLOGINPWDV2",
            routeTemplate: "v1/user/{id}/Urmaextnupdateapploginpwdv2",
            defaults: new { controller = "Urmaextnupdateapploginpwdv2" });


            config.Routes.MapHttpRoute(
            "UREXTNVOICEMAILGETSETTING",
            routeTemplate: "v1/user/{id}/Urextnvoicemailgetsetting",
            defaults: new { controller = "Urextnvoicemailgetsetting" });

            config.Routes.MapHttpRoute(
            "UREXTNDIRECTCALLINSERTSETTING",
            routeTemplate: "v1/user/{id}/Urextndirectcallinsertsetting",
            defaults: new { controller = "Urextndirectcallinsertsetting" });

            config.Routes.MapHttpRoute(
            "UREXTNDIRECTCALLGETSETTING",
            routeTemplate: "v1/user/{id}/Urextndirectcallgetsetting",
            defaults: new { controller = "Urextndirectcallgetsetting" });

            config.Routes.MapHttpRoute(
            "UREXTNCALLRECORDINSERTSETTING",
            routeTemplate: "v1/user/{id}/Urextncallrecordinsertsetting",
            defaults: new { controller = "Urextncallrecordinsertsetting" });

            config.Routes.MapHttpRoute(
            "UREXTNCALLRECORDGETSETTING",
            routeTemplate: "v1/user/{id}/Urextncallrecordgetsetting",
            defaults: new { controller = "Urextncallrecordgetsetting" });

            config.Routes.MapHttpRoute(
            "URMACALLFLIPINFOGET",
            routeTemplate: "v1/user/{id}/Urmacallflipinfoget",
            defaults: new { controller = "Urmacallflipinfoget" });

            config.Routes.MapHttpRoute(
            "URMAINSERTCALLFLIPINFO",
            routeTemplate: "v1/user/{id}/Urmainsertcallflipinfo",
            defaults: new { controller = "Urmainsertcallflipinfo" });

            config.Routes.MapHttpRoute(
            "URMACALLFLIPREORDER",
            routeTemplate: "v1/user/{id}/Urmacallflipreorder",
            defaults: new { controller = "Urmacallflipreorder" });

            config.Routes.MapHttpRoute(
            "URMACALLFLIPUPDATE",
            routeTemplate: "v1/user/{id}/Urmacallflipupdate",
            defaults: new { controller = "Urmacallflipupdate" });

            config.Routes.MapHttpRoute(
            "UREXTNFINDFLOWMESAVE",
            routeTemplate: "v1/user/{id}/Urextnfindflowmesave",
            defaults: new { controller = "Urextnfindflowmesave" });

            config.Routes.MapHttpRoute(
            "UREXTNFINDFLOWMEGET",
            routeTemplate: "v1/user/{id}/Urextnfindflowmeget",
            defaults: new { controller = "Urextnfindflowmeget" });

            config.Routes.MapHttpRoute(
            "UREXTNFINDFLOWMEUPDATE",
            routeTemplate: "v1/user/{id}/Urextnfindflowmeupdate",
            defaults: new { controller = "Urextnfindflowmeupdate" });

            config.Routes.MapHttpRoute(
            "UREXTNUPDATEDNDTYPE",
            routeTemplate: "v1/user/{id}/Urextnupdatedndtype",
            defaults: new { controller = "Urextnupdatedndtype" });

            config.Routes.MapHttpRoute(
            "URMACONFERENCESESSIONSAVE",
            routeTemplate: "v1/user/{id}/Urmaconferencesessionsave",
            defaults: new { controller = "Urmaconferencesessionsave" });

            config.Routes.MapHttpRoute(
            "URMACONFERENCESESSIONGET",
            routeTemplate: "v1/user/{id}/Urmaconferencesessionget",
            defaults: new { controller = "Urmaconferencesessionget" });

            config.Routes.MapHttpRoute(
            "URMAGETEXTNCALLLOG",
            routeTemplate: "v1/user/{id}/Urmagetextncalllog",
            defaults: new { controller = "Urmagetextncalllog" });

            config.Routes.MapHttpRoute(
            "URAPPGETVOICEMSGDTL",
            routeTemplate: "v1/user/{id}/Urappgetvoicemsgdtl",
            defaults: new { controller = "Urappgetvoicemsgdtl" });

            config.Routes.MapHttpRoute(
            "URAPPVOICEMAILUPDATESTATUS",
            routeTemplate: "v1/user/{id}/Urappvoicemailupdatestatus",
            defaults: new { controller = "Urappvoicemailupdatestatus" });

            config.Routes.MapHttpRoute(
            "URAPPUPDATEUSERSTATUS",
            routeTemplate: "v1/user/{id}/Urappupdateuserstatus",
            defaults: new { controller = "Urappupdateuserstatus" });

            config.Routes.MapHttpRoute(
            "URAPPGETUSERSTATUS",
            routeTemplate: "v1/user/{id}/Urappgetuserstatus",
            defaults: new { controller = "Urappgetuserstatus" });

            config.Routes.MapHttpRoute(
            "URDISPLAYPICTURE",
            routeTemplate: "v1/user/{id}/Urdisplaypicture",
            defaults: new { controller = "Urdisplaypicture" });

            config.Routes.MapHttpRoute(
            "URGETEXTNLOGINPWD",
            routeTemplate: "v1/user/{id}/Urgetextnloginpwd",
            defaults: new { controller = "Urgetextnloginpwd" });

            config.Routes.MapHttpRoute(
            "VBTICKETGET",
            routeTemplate: "v1/user/{id}/Vbticketget",
            defaults: new { controller = "Vbticketget" });

            config.Routes.MapHttpRoute(
            "VBTICKETINSERT",
            routeTemplate: "v1/user/{id}/Vbticketinsert",
            defaults: new { controller = "Vbticketinsert" });

            config.Routes.MapHttpRoute(
            "VBGETISSUETYPES",
            routeTemplate: "v1/user/{id}/Vbgetissuetypes",
            defaults: new { controller = "Vbgetissuetypes" });

            config.Routes.MapHttpRoute(
            "VBGETISSUESUBTYPES",
            routeTemplate: "v1/user/{id}/Vbgetissuesubtypes",
            defaults: new { controller = "Vbgetissuesubtypes" });

            config.Routes.MapHttpRoute(
            "URBILLGETCUSTOMERINVOICESUMMARY",
            routeTemplate: "v1/user/{id}/Urbillgetcustomerinvoicesummary",
            defaults: new { controller = "Urbillgetcustomerinvoicesummary" });

            config.Routes.MapHttpRoute(
            "URBILLGETCUSTOMERINVOICESUMMARYDTL",
            routeTemplate: "v1/user/{id}/Urbillgetcustomerinvoicesummarydtl",
            defaults: new { controller = "Urbillgetcustomerinvoicesummarydtl" });

            config.Routes.MapHttpRoute(
            "URBILLINSERTDIRECTDEBITACCOUNT",
            routeTemplate: "v1/user/{id}/Urbillinsertdirectdebitaccount",
            defaults: new { controller = "Urbillinsertdirectdebitaccount" });

            config.Routes.MapHttpRoute(
            "URBILLGETDIRECTDEBITACCOUNT",
            routeTemplate: "v1/user/{id}/Urbillgetdirectdebitaccount",
            defaults: new { controller = "Urbillgetdirectdebitaccount" });

            config.Routes.MapHttpRoute(
            "URCRMCOMPANYDASHBOARDOVERVIEW",
            routeTemplate: "v1/user/{id}/Urcrmcompanydashboardoverview",
            defaults: new { controller = "Urcrmcompanydashboardoverview" });

            config.Routes.MapHttpRoute(
            "URCRMCOMPANYDASHBOARDPLANTICKETINFO",
            routeTemplate: "v1/user/{id}/Urcrmcompanydashboardplanticketinfo",
            defaults: new { controller = "Urcrmcompanydashboardplanticketinfo" });

            config.Routes.MapHttpRoute(
            "URMAHUNTGROUPDELETE",
            routeTemplate: "v1/user/{id}/Urmahuntgroupdelete",
            defaults: new { controller = "Urmahuntgroupdelete" });

            config.Routes.MapHttpRoute(
            "URMAPARKLOCATIONDELETE",
            routeTemplate: "v1/user/{id}/Urmaparklocationdelete",
            defaults: new { controller = "Urmaparklocationdelete" });

            config.Routes.MapHttpRoute(
            "URCRMUSERGET",
            routeTemplate: "v1/user/{id}/Urcrmuserget",
            defaults: new { controller = "Urcrmuserget" });

            config.Routes.MapHttpRoute(
            "URMABULKUPLOADUSEREXTENSION",
            routeTemplate: "v1/user/{id}/Urmabulkuploaduserextension",
            defaults: new { controller = "Urmabulkuploaduserextension" });

            config.Routes.MapHttpRoute(
            "URGETPRODUCTPLAN",
            routeTemplate: "v1/user/{id}/Urgetproductplan",
            defaults: new { controller = "Urgetproductplan" });

            config.Routes.MapHttpRoute(
            "URMAINSERTPLANCHANGEREQUEST",
            routeTemplate: "v1/user/{id}/Urmainsertplanchangerequest",
            defaults: new { controller = "Urmainsertplanchangerequest" });

            config.Routes.MapHttpRoute(
            "URAPPADDDESKTOPADMIN",
            routeTemplate: "v1/user/{id}/Urappadddesktopadmin",
            defaults: new { controller = "Urappadddesktopadmin" });

            config.Routes.MapHttpRoute(
            "URAPPGETDESKTOPADMIN",
            routeTemplate: "v1/user/{id}/Urappgetdesktopadmin",
            defaults: new { controller = "Urappgetdesktopadmin" });

            config.Routes.MapHttpRoute(
            "URAPPGETGIPHYSHARINGCONFIG",
            routeTemplate: "v1/user/{id}/Urappgetgiphysharingconfig",
            defaults: new { controller = "Urappgetgiphysharingconfig" });

            config.Routes.MapHttpRoute(
            "URAPPUPDATEDESKTOPCOMPANYSETTING",
            routeTemplate: "v1/user/{id}/Urappupdatedesktopcompanysetting",
            defaults: new { controller = "Urappupdatedesktopcompanysetting" });

            config.Routes.MapHttpRoute(
            "URAPPGETDESKTOPCOMPANYSETTING",
            routeTemplate: "v1/user/{id}/Urappgetdesktopcompanysetting",
            defaults: new { controller = "Urappgetdesktopcompanysetting" });

            config.Routes.MapHttpRoute(
            "URAPPINSERTUSERCHATLOG",
            routeTemplate: "v1/user/{id}/Urappinsertuserchatlog",
            defaults: new { controller = "Urappinsertuserchatlog" });

            config.Routes.MapHttpRoute(
            "URAPPUPDATEDESKTOPCOMPLIANCESETTING",
            routeTemplate: "v1/user/{id}/Urappupdatedesktopcompliancesetting",
            defaults: new { controller = "Urappupdatedesktopcompliancesetting" });

            config.Routes.MapHttpRoute(
            "URAPPDESKTOPGETUSERCHATLOG",
            routeTemplate: "v1/user/{id}/Urappdesktopgetuserchatlog",
            defaults: new { controller = "Urappdesktopgetuserchatlog" });

            config.Routes.MapHttpRoute(
            "URAPPUPDATEDESKTOPDATARETENTIONSETTING",
            routeTemplate: "v1/user/{id}/Urappupdatedesktopdataretentionsetting",
            defaults: new { controller = "Urappupdatedesktopdataretentionsetting" });

            config.Routes.MapHttpRoute(
            "URAPPUPDATEDESKTOPINTEGRATIONADMIN",
            routeTemplate: "v1/user/{id}/Urappupdatedesktopintegrationadmin",
            defaults: new { controller = "Urappupdatedesktopintegrationadmin" });

            config.Routes.MapHttpRoute(
            "WPGETCDRCALLTYPESUMMARY",
            routeTemplate: "v1/user/{id}/Wpgetcdrcalltypesummary",
            defaults: new { controller = "Wpgetcdrcalltypesummary" });

            config.Routes.MapHttpRoute(
            "URDEVICEINSERTDELIVERYADDRESS",
            routeTemplate: "v1/user/{id}/Urdeviceinsertdeliveryaddress",
            defaults: new { controller = "Urdeviceinsertdeliveryaddress" });

            config.Routes.MapHttpRoute(
            "URDEVICEGETDELIVERYADDRESS",
            routeTemplate: "v1/user/{id}/Urdevicegetdeliveryaddress",
            defaults: new { controller = "Urdevicegetdeliveryaddress" });

            config.Routes.MapHttpRoute(
            "URDEVICEUPDATEDELIVERYADDRESS",
            routeTemplate: "v1/user/{id}/Urdeviceupdatedeliveryaddress",
            defaults: new { controller = "Urdeviceupdatedeliveryaddress" });

            config.Routes.MapHttpRoute(
            "URDEVICEINSERTSTOCKORDER",
            routeTemplate: "v1/user/{id}/Urdeviceinsertstockorder",
            defaults: new { controller = "Urdeviceinsertstockorder" });

            config.Routes.MapHttpRoute(
            "URDEVICEGETSTOCKORDER",
            routeTemplate: "v1/user/{id}/Urdevicegetstockorder",
            defaults: new { controller = "Urdevicegetstockorder" });

            config.Routes.MapHttpRoute(
            "URDEVICEGETSTOCKORDERDTL",
            routeTemplate: "v1/user/{id}/Urdevicegetstockorderdtl",
            defaults: new { controller = "Urdevicegetstockorderdtl" });

            config.Routes.MapHttpRoute(
            "URDEVICEGETPURCHASEITEM",
            routeTemplate: "v1/user/{id}/Urdevicegetpurchaseitem",
            defaults: new { controller = "Urdevicegetpurchaseitem" });

            config.Routes.MapHttpRoute(
            "URAPPREGISTERINVITECONTACT",
            routeTemplate: "v1/user/{id}/Urappregisterinvitecontact",
            defaults: new { controller = "Urappregisterinvitecontact" });

            config.Routes.MapHttpRoute(
            "URAPPREGISTERINVITECONTACTV2",
            routeTemplate: "v1/user/{id}/Urappregisterinvitecontactv2",
            defaults: new { controller = "Urappregisterinvitecontactv2" });

            config.Routes.MapHttpRoute(
            "URINSERTPRODUCTDEVICE",
            routeTemplate: "v1/user/{id}/Urinsertproductdevice",
            defaults: new { controller = "Urinsertproductdevice" });

            config.Routes.MapHttpRoute(
            "URGETPRODUCTDEVICE",
            routeTemplate: "v1/user/{id}/Urgetproductdevice",
            defaults: new { controller = "Urgetproductdevice" });

            config.Routes.MapHttpRoute(
            "URINSERTDEVICESTOCK",
            routeTemplate: "v1/user/{id}/Urinsertdevicestock",
            defaults: new { controller = "Urinsertdevicestock" });

            config.Routes.MapHttpRoute(
            "URGETDEVICESTOCK",
            routeTemplate: "v1/user/{id}/Urgetdevicestock",
            defaults: new { controller = "Urgetdevicestock" });

            config.Routes.MapHttpRoute(
            "URDEVICEORDERCRM",
            routeTemplate: "v1/user/{id}/Urdeviceordercrm",
            defaults: new { controller = "Urdeviceordercrm" });

            config.Routes.MapHttpRoute(
            "URDEVICEORDERPRODUCTSEARCH",
            routeTemplate: "v1/user/{id}/Urdeviceorderproductsearch",
            defaults: new { controller = "Urdeviceorderproductsearch" });

            config.Routes.MapHttpRoute(
            "URDEVICEORDERCOMPANYSEARCH",
            routeTemplate: "v1/user/{id}/Urdeviceordercompanysearch",
            defaults: new { controller = "Urdeviceordercompanysearch" });

            config.Routes.MapHttpRoute(
            "URDEVICEORDERDELIVERYADDRESSCRM",
            routeTemplate: "v1/user/{id}/Urdeviceorderdeliveryaddresscrm",
            defaults: new { controller = "Urdeviceorderdeliveryaddresscrm" });

            config.Routes.MapHttpRoute(
            "URDEVICEGETORDERITEMCRM",
            routeTemplate: "v1/user/{id}/Urdevicegetorderitemcrm",
            defaults: new { controller = "Urdevicegetorderitemcrm" });

            config.Routes.MapHttpRoute(
            "URDEVICEORDERAPPROVECRM",
            routeTemplate: "v1/user/{id}/Urdeviceorderapprovecrm",
            defaults: new { controller = "Urdeviceorderapprovecrm" });

            config.Routes.MapHttpRoute(
            "URDEVICEGETAVAILABLESTOCK",
            routeTemplate: "v1/user/{id}/Urdevicegetavailablestock",
            defaults: new { controller = "Urdevicegetavailablestock" });

            config.Routes.MapHttpRoute(
            "URDEVICEORDERUPDATETRACKINGNO",
            routeTemplate: "v1/user/{id}/Urdeviceorderupdatetrackingno",
            defaults: new { controller = "Urdeviceorderupdatetrackingno" });



            config.Routes.MapHttpRoute(
            "URDEVICEORDERRETURNINSERT",
            routeTemplate: "v1/user/{id}/Urdeviceorderreturninsert",
            defaults: new { controller = "Urdeviceorderreturninsert" });

            config.Routes.MapHttpRoute(
            "URDEVICEORDERRETURNGET",
            routeTemplate: "v1/user/{id}/Urdeviceorderreturnget",
            defaults: new { controller = "Urdeviceorderreturnget" });

            config.Routes.MapHttpRoute(
            "URDEVICEORDERREPLACEMENTINSERT",
            routeTemplate: "v1/user/{id}/Urdeviceorderreplacementinsert",
            defaults: new { controller = "Urdeviceorderreplacementinsert" });

            config.Routes.MapHttpRoute(
            "URDEVICEORDERREPLACEMENTGET",
            routeTemplate: "v1/user/{id}/Urdeviceorderreplacementget",
            defaults: new { controller = "Urdeviceorderreplacementget" });

            config.Routes.MapHttpRoute(
            "URDESKTOPPREFCALLINGUPDATE",
            routeTemplate: "v1/user/{id}/Urdesktopprefcallingupdate",
            defaults: new { controller = "Urdesktopprefcallingupdate" });

            config.Routes.MapHttpRoute(
            "URDESKTOPPREFCALLINGGET",
            routeTemplate: "v1/user/{id}/Urdesktopprefcallingget",
            defaults: new { controller = "Urdesktopprefcallingget" });

            config.Routes.MapHttpRoute(
            "URDESKTOPPREFAUDIOUPDATE",
            routeTemplate: "v1/user/{id}/Urdesktopprefaudioupdate",
            defaults: new { controller = "Urdesktopprefaudioupdate" });

            config.Routes.MapHttpRoute(
            "URDESKTOPPREFAUDIOGET",
            routeTemplate: "v1/user/{id}/Urdesktopprefaudioget",
            defaults: new { controller = "Urdesktopprefaudioget" });

            config.Routes.MapHttpRoute(
            "URDEVICERETURNORDERAPPROVE",
            routeTemplate: "v1/user/{id}/Urdevicereturnorderapprove",
            defaults: new { controller = "Urdevicereturnorderapprove" });

            config.Routes.MapHttpRoute(
            "URSHIPPINGCARRIERINSERT",
            routeTemplate: "v1/user/{id}/Urshippingcarrierinsert",
            defaults: new { controller = "Urshippingcarrierinsert" });

            config.Routes.MapHttpRoute(
            "URSHIPPINGCARRIERGET",
            routeTemplate: "v1/user/{id}/Urshippingcarrierget",
            defaults: new { controller = "Urshippingcarrierget" });

            config.Routes.MapHttpRoute(
            "URDESKTOPPREFSOUNDUPDATE",
            routeTemplate: "v1/user/{id}/Urdesktopprefsoundupdate",
            defaults: new { controller = "Urdesktopprefsoundupdate" });

            config.Routes.MapHttpRoute(
            "URDESKTOPPREFSOUNDGET",
            routeTemplate: "v1/user/{id}/Urdesktopprefsoundget",
            defaults: new { controller = "Urdesktopprefsoundget" });

            config.Routes.MapHttpRoute(
            "URDESKTOPPREFEMAILMOBILENOTIFICATIONUPDATE",
            routeTemplate: "v1/user/{id}/Urdesktopprefemailmobilenotificationupdate",
            defaults: new { controller = "Urdesktopprefemailmobilenotificationupdate" });

            config.Routes.MapHttpRoute(
            "URDESKTOPPREFEMAILMOBILENOTIFICATIONGET",
            routeTemplate: "v1/user/{id}/Urdesktopprefemailmobilenotificationget",
            defaults: new { controller = "Urdesktopprefemailmobilenotificationget" });

            config.Routes.MapHttpRoute(
            "URDESKTOPPREFCONVERSATIONSUPDATE",
            routeTemplate: "v1/user/{id}/Urdesktopprefconversationsupdate",
            defaults: new { controller = "Urdesktopprefconversationsupdate" });

            config.Routes.MapHttpRoute(
            "URDESKTOPPREFCONVERSATIONSGET",
            routeTemplate: "v1/user/{id}/Urdesktopprefconversationsget",
            defaults: new { controller = "Urdesktopprefconversationsget" });

            config.Routes.MapHttpRoute(
            "URDESKTOPPREFOTHERUPDATE",
            routeTemplate: "v1/user/{id}/Urdesktopprefotherupdate",
            defaults: new { controller = "Urdesktopprefotherupdate" });

            config.Routes.MapHttpRoute(
            "URDESKTOPPREFOTHERGET",
            routeTemplate: "v1/user/{id}/Urdesktopprefotherget",
            defaults: new { controller = "Urdesktopprefotherget" });

            config.Routes.MapHttpRoute(
            "URSHIPPINGCARRIERDELETE",
            routeTemplate: "v1/user/{id}/Urshippingcarrierdelete",
            defaults: new { controller = "Urshippingcarrierdelete" });

            config.Routes.MapHttpRoute(
            "URUPDATEDEVICEPRODSTATUS",
            routeTemplate: "v1/user/{id}/Urupdatedeviceprodstatus",
            defaults: new { controller = "Urupdatedeviceprodstatus" });

            config.Routes.MapHttpRoute(
            "URGETPRODUCTDEVICEMODEL",
            routeTemplate: "v1/user/{id}/Urgetproductdevicemodel",
            defaults: new { controller = "Urgetproductdevicemodel" });

            config.Routes.MapHttpRoute(
            "URUPDATEPRODUCTDEVICEFROMAPI",
            routeTemplate: "v1/user/{id}/Urupdateproductdevicefromapi",
            defaults: new { controller = "Urupdateproductdevicefromapi" });

            config.Routes.MapHttpRoute(
            "URGETPRODUCTLISTFORWEBORDER",
            routeTemplate: "v1/user/{id}/Urgetproductlistforweborder",
            defaults: new { controller = "Urgetproductlistforweborder" });

            config.Routes.MapHttpRoute(
            "URGETPRODUCTBRANDNAME",
            routeTemplate: "v1/user/{id}/Urgetproductbrandname",
            defaults: new { controller = "Urgetproductbrandname" });

            config.Routes.MapHttpRoute(
            "URADDEMAILALERTSNOTIFICATIONS",
            routeTemplate: "v1/user/{id}/Uraddemailalertsnotifications",
            defaults: new { controller = "Uraddemailalertsnotifications" });

            config.Routes.MapHttpRoute(
            "URGETEMAILALERTSNOTIFICATIONS",
            routeTemplate: "v1/user/{id}/Urgetemailalertsnotifications",
            defaults: new { controller = "Urgetemailalertsnotifications" });

            config.Routes.MapHttpRoute(
            "URMYACCOUNTINSERTEMAILNOTIF",
            routeTemplate: "v1/user/{id}/Urmyaccountinsertemailnotif",
            defaults: new { controller = "Urmyaccountinsertemailnotif" });

            config.Routes.MapHttpRoute(
            "URMYACCOUNTGETEMAILNOTIF",
            routeTemplate: "v1/user/{id}/Urmyaccountgetemailnotif",
            defaults: new { controller = "Urmyaccountgetemailnotif" });
            
			config.Routes.MapHttpRoute(
			"URWEBAPPGETEXTENSIONLIST",
 			routeTemplate: "v1/user/{id}/Urwebappgetextensionlist",
			defaults: new { controller = "Urwebappgetextensionlist" });
			
			config.Routes.MapHttpRoute(
			"URMYACCEMERGCONTACTSAVE",
 			routeTemplate: "v1/user/{id}/Urmyaccemergcontactsave",
			defaults: new { controller = "Urmyaccemergcontactsave" });
			
			config.Routes.MapHttpRoute(
			"URBTDATFILECUSTOMERDTLGET",
 			routeTemplate: "v1/user/{id}/Urbtdatfilecustomerdtlget",
			defaults: new { controller = "Urbtdatfilecustomerdtlget" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTDOPORTINREQUEST",
 			routeTemplate: "v1/user/{id}/Urmyaccountdoportinrequest",
			defaults: new { controller = "Urmyaccountdoportinrequest" });
			
			config.Routes.MapHttpRoute(
			"URMYACCGETPORTINREQUEST",
 			routeTemplate: "v1/user/{id}/Urmyaccgetportinrequest",
			defaults: new { controller = "Urmyaccgetportinrequest" });
			
			config.Routes.MapHttpRoute(
			"URMYACCEMERGCONTACTGET",
 			routeTemplate: "v1/user/{id}/Urmyaccemergcontactget",
			defaults: new { controller = "Urmyaccemergcontactget" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTASSIGNDESKPHONEMAPNUMBER",
 			routeTemplate: "v1/user/{id}/Urmyaccountassigndeskphonemapnumber",
			defaults: new { controller = "Urmyaccountassigndeskphonemapnumber" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETPURCHASEDDEVICEORDER",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetpurchaseddeviceorder",
			defaults: new { controller = "Urmyaccountgetpurchaseddeviceorder" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTUPDATEMACADDRESS",
 			routeTemplate: "v1/user/{id}/Urmyaccountupdatemacaddress",
			defaults: new { controller = "Urmyaccountupdatemacaddress" });
			
			config.Routes.MapHttpRoute(
			"URINSERTLINKACCOUNTTOALEXA",
 			routeTemplate: "v1/user/{id}/Urinsertlinkaccounttoalexa",
			defaults: new { controller = "Urinsertlinkaccounttoalexa" });
			
			config.Routes.MapHttpRoute(
			"URGETMYACCUSERLOCATION",
 			routeTemplate: "v1/user/{id}/Urgetmyaccuserlocation",
			defaults: new { controller = "Urgetmyaccuserlocation" });
			
			config.Routes.MapHttpRoute(
			"GETURCOMPANYSITEBYCOMPANYID",
 			routeTemplate: "v1/user/{id}/Geturcompanysitebycompanyid",
			defaults: new { controller = "Geturcompanysitebycompanyid" });
			
			config.Routes.MapHttpRoute(
			"URMAPHONENUMBERGETCITY",
 			routeTemplate: "v1/user/{id}/Urmaphonenumbergetcity",
			defaults: new { controller = "Urmaphonenumbergetcity" });

			
			config.Routes.MapHttpRoute(
			"GETUREMERGENCYADDRESS",
 			routeTemplate: "v1/user/{id}/Geturemergencyaddress",
			defaults: new { controller = "Geturemergencyaddress" });
			
			config.Routes.MapHttpRoute(
			"ADDUREMERGENCYADDRESS",
 			routeTemplate: "v1/user/{id}/Adduremergencyaddress",
			defaults: new { controller = "Adduremergencyaddress" });
			
			config.Routes.MapHttpRoute(
			"MODIFYUREMERGENCYADDRESS",
 			routeTemplate: "v1/user/{id}/Modifyuremergencyaddress",
			defaults: new { controller = "Modifyuremergencyaddress" });
			
			config.Routes.MapHttpRoute(
			"URMACHECKDIDNUMBERAVAILABLE",
 			routeTemplate: "v1/user/{id}/Urmacheckdidnumberavailable",
			defaults: new { controller = "Urmacheckdidnumberavailable" });
			
			config.Routes.MapHttpRoute(
			"URPAYMENTGETCUSTOMERBILLINFO",
 			routeTemplate: "v1/user/{id}/Urpaymentgetcustomerbillinfo",
			defaults: new { controller = "Urpaymentgetcustomerbillinfo" });
			
			config.Routes.MapHttpRoute(
			"URCUSTOMERPAYMENTUPDATE",
 			routeTemplate: "v1/user/{id}/Urcustomerpaymentupdate",
			defaults: new { controller = "Urcustomerpaymentupdate" });
			
			config.Routes.MapHttpRoute(
			"URPHONENUMBERASSIGNTYPEGET",
 			routeTemplate: "v1/user/{id}/Urphonenumberassigntypeget",
			defaults: new { controller = "Urphonenumberassigntypeget" });
			
			config.Routes.MapHttpRoute(
			"URCOMPANYUPDATEAUTOPURCHASECREDIT",
 			routeTemplate: "v1/user/{id}/Urcompanyupdateautopurchasecredit",
			defaults: new { controller = "Urcompanyupdateautopurchasecredit" });
			
			config.Routes.MapHttpRoute(
			"URCOMPANYBILLINGCYCLEUPDATE",
 			routeTemplate: "v1/user/{id}/Urcompanybillingcycleupdate",
			defaults: new { controller = "Urcompanybillingcycleupdate" });
			
			config.Routes.MapHttpRoute(
			"URINTERNATIONALCALLINGRATESTATUSUPDATE",
 			routeTemplate: "v1/user/{id}/Urinternationalcallingratestatusupdate",
			defaults: new { controller = "Urinternationalcallingratestatusupdate" });
			
			config.Routes.MapHttpRoute(
			"URINTERNATIONALCALLINGRATESTATUSGET",
 			routeTemplate: "v1/user/{id}/Urinternationalcallingratestatusget",
			defaults: new { controller = "Urinternationalcallingratestatusget" });
			
			config.Routes.MapHttpRoute(
			"URCOMPANYCALLINGRATEGET",
 			routeTemplate: "v1/user/{id}/Urcompanycallingrateget",
			defaults: new { controller = "Urcompanycallingrateget" });
			
			config.Routes.MapHttpRoute(
			"URCOMPANYCALLINGRATESTATUSUPDATE",
 			routeTemplate: "v1/user/{id}/Urcompanycallingratestatusupdate",
			defaults: new { controller = "Urcompanycallingratestatusupdate" });
			
			config.Routes.MapHttpRoute(
			"URBUNDLECONFIGGET",
 			routeTemplate: "v1/user/{id}/Urbundleconfigget",
			defaults: new { controller = "Urbundleconfigget" });
			
			config.Routes.MapHttpRoute(
			"URADDITIONALBUNDLESUBSCRIBE",
 			routeTemplate: "v1/user/{id}/Uradditionalbundlesubscribe",
			defaults: new { controller = "Uradditionalbundlesubscribe" });
			
			config.Routes.MapHttpRoute(
			"URADDITIONALBUNDLESUBSCRIBEGET",
 			routeTemplate: "v1/user/{id}/Uradditionalbundlesubscribeget",
			defaults: new { controller = "Uradditionalbundlesubscribeget" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEINSERTCOMPANYWORKINGHOURS",
 			routeTemplate: "v1/user/{id}/Urmultisiteinsertcompanyworkinghours",
			defaults: new { controller = "Urmultisiteinsertcompanyworkinghours" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEGETCOMPANYWORKINGHOURS",
 			routeTemplate: "v1/user/{id}/Urmultisitegetcompanyworkinghours",
			defaults: new { controller = "Urmultisitegetcompanyworkinghours" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEGETCOMPANYREGIONALSETTINGDTL",
 			routeTemplate: "v1/user/{id}/Urmultisitegetcompanyregionalsettingdtl",
			defaults: new { controller = "Urmultisitegetcompanyregionalsettingdtl" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEUPDATECOMPANYREGIONALSETTING",
 			routeTemplate: "v1/user/{id}/Urmultisiteupdatecompanyregionalsetting",
			defaults: new { controller = "Urmultisiteupdatecompanyregionalsetting" });
			
			config.Routes.MapHttpRoute(
			"UREXTNPHONENAMEUPDATE",
 			routeTemplate: "v1/user/{id}/Urextnphonenameupdate",
			defaults: new { controller = "Urextnphonenameupdate" });
			
			config.Routes.MapHttpRoute(
			"URDEVICEGETUSERTYPE",
 			routeTemplate: "v1/user/{id}/Urdevicegetusertype",
			defaults: new { controller = "Urdevicegetusertype" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEGETEXISTINGNUMBERS",
 			routeTemplate: "v1/user/{id}/Urmultisitegetexistingnumbers",
			defaults: new { controller = "Urmultisitegetexistingnumbers" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEGETEXISTINGIVRMENU",
 			routeTemplate: "v1/user/{id}/Urmultisitegetexistingivrmenu",
			defaults: new { controller = "Urmultisitegetexistingivrmenu" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITECREATEMYACCOUNT",
 			routeTemplate: "v1/user/{id}/Urmultisitecreatemyaccount",
			defaults: new { controller = "Urmultisitecreatemyaccount" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCHECKMULTISITEINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccountcheckmultisiteinfo",
			defaults: new { controller = "Urmyaccountcheckmultisiteinfo" });
			
			config.Routes.MapHttpRoute(
			"EXECURMULTISITEUPDATECOMPANYREGIONALSETTINGBYSITEID",
 			routeTemplate: "v1/user/{id}/Execurmultisiteupdatecompanyregionalsettingbysiteid",
			defaults: new { controller = "Execurmultisiteupdatecompanyregionalsettingbysiteid" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEINSERTCOMPANYWORKINGHOURSBYSITEID",
 			routeTemplate: "v1/user/{id}/Urmultisiteinsertcompanyworkinghoursbysiteid",
			defaults: new { controller = "Urmultisiteinsertcompanyworkinghoursbysiteid" });
			
			config.Routes.MapHttpRoute(
			"URVALIDATEBUNDLESUBSCRIBE",
 			routeTemplate: "v1/user/{id}/Urvalidatebundlesubscribe",
			defaults: new { controller = "Urvalidatebundlesubscribe" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEGETSITEADDRESS",
 			routeTemplate: "v1/user/{id}/Urmultisitegetsiteaddress",
			defaults: new { controller = "Urmultisitegetsiteaddress" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEUPDATESITEADDRESS",
 			routeTemplate: "v1/user/{id}/Urmultisiteupdatesiteaddress",
			defaults: new { controller = "Urmultisiteupdatesiteaddress" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEUPDATESITEINFO",
 			routeTemplate: "v1/user/{id}/Urmultisiteupdatesiteinfo",
			defaults: new { controller = "Urmultisiteupdatesiteinfo" });
			
			config.Routes.MapHttpRoute(
			"URGETIVRCONFIG",
 			routeTemplate: "v1/user/{id}/Urgetivrconfig",
			defaults: new { controller = "Urgetivrconfig" });
			
			config.Routes.MapHttpRoute(
			"URVALIDATEIVRCONFIGINFO",
 			routeTemplate: "v1/user/{id}/Urvalidateivrconfiginfo",
			defaults: new { controller = "Urvalidateivrconfiginfo" });
			
			config.Routes.MapHttpRoute(
			"URMAADDNEWSITE",
 			routeTemplate: "v1/user/{id}/Urmaaddnewsite",
			defaults: new { controller = "Urmaaddnewsite" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEENABLE",
 			routeTemplate: "v1/user/{id}/Urmultisiteenable",
			defaults: new { controller = "Urmultisiteenable" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEENABLEGET",
 			routeTemplate: "v1/user/{id}/Urmultisiteenableget",
			defaults: new { controller = "Urmultisiteenableget" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEUPDATECHANGESITEFORUSER",
 			routeTemplate: "v1/user/{id}/Urmultisiteupdatechangesiteforuser",
			defaults: new { controller = "Urmultisiteupdatechangesiteforuser" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEUPDATECALLERIDNAME",
 			routeTemplate: "v1/user/{id}/Urmultisiteupdatecalleridname",
			defaults: new { controller = "Urmultisiteupdatecalleridname" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITECREATEIVRMENU",
 			routeTemplate: "v1/user/{id}/Urmultisitecreateivrmenu",
			defaults: new { controller = "Urmultisitecreateivrmenu" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEIVRSAVEPROMPT",
 			routeTemplate: "v1/user/{id}/Urmultisiteivrsaveprompt",
			defaults: new { controller = "Urmultisiteivrsaveprompt" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEIVRSAVEKEYPRESS",
 			routeTemplate: "v1/user/{id}/Urmultisiteivrsavekeypress",
			defaults: new { controller = "Urmultisiteivrsavekeypress" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEIVRGETKEYPRESS",
 			routeTemplate: "v1/user/{id}/Urmultisiteivrgetkeypress",
			defaults: new { controller = "Urmultisiteivrgetkeypress" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEIVRGETPROMPTINFO",
 			routeTemplate: "v1/user/{id}/Urmultisiteivrgetpromptinfo",
			defaults: new { controller = "Urmultisiteivrgetpromptinfo" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEIVRGETMENUINFO",
 			routeTemplate: "v1/user/{id}/Urmultisiteivrgetmenuinfo",
			defaults: new { controller = "Urmultisiteivrgetmenuinfo" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEUPDATEDIRECTORYASSISTANCE",
 			routeTemplate: "v1/user/{id}/Urmultisiteupdatedirectoryassistance",
			defaults: new { controller = "Urmultisiteupdatedirectoryassistance" });
			
			config.Routes.MapHttpRoute(
			"URAPPCREATETEAMINFO",
 			routeTemplate: "v1/user/{id}/Urappcreateteaminfo",
			defaults: new { controller = "Urappcreateteaminfo" });
			
			config.Routes.MapHttpRoute(
			"URAPPGETTEAMINFO",
 			routeTemplate: "v1/user/{id}/Urappgetteaminfo",
			defaults: new { controller = "Urappgetteaminfo" });
			
			config.Routes.MapHttpRoute(
			"URAPPCREATETEAMMEMBERINFO",
 			routeTemplate: "v1/user/{id}/Urappcreateteammemberinfo",
			defaults: new { controller = "Urappcreateteammemberinfo" });
			
			config.Routes.MapHttpRoute(
			"URAPPGETTEAMMEMBERINFO",
 			routeTemplate: "v1/user/{id}/Urappgetteammemberinfo",
			defaults: new { controller = "Urappgetteammemberinfo" });
			
			config.Routes.MapHttpRoute(
			"URMAAPPFAVOURITETEAMSAVE",
 			routeTemplate: "v1/user/{id}/Urmaappfavouriteteamsave",
			defaults: new { controller = "Urmaappfavouriteteamsave" });
			
			config.Routes.MapHttpRoute(
			"URAPPCREATETEAMMEMBERADMIN",
 			routeTemplate: "v1/user/{id}/Urappcreateteammemberadmin",
			defaults: new { controller = "Urappcreateteammemberadmin" });
			
			config.Routes.MapHttpRoute(
			"URECOMCREATECOMPANYFEEBACKINFO",
 			routeTemplate: "v1/user/{id}/Urecomcreatecompanyfeebackinfo",
			defaults: new { controller = "Urecomcreatecompanyfeebackinfo" });
			
			config.Routes.MapHttpRoute(
			"URGETMYACCOUNTTMSFEATURELIST",
 			routeTemplate: "v1/user/{id}/Urgetmyaccounttmsfeaturelist",
			defaults: new { controller = "Urgetmyaccounttmsfeaturelist" });
			
			config.Routes.MapHttpRoute(
			"URECOMGETPLANFEATURELIST",
 			routeTemplate: "v1/user/{id}/Urecomgetplanfeaturelist",
			defaults: new { controller = "Urecomgetplanfeaturelist" });
			
			config.Routes.MapHttpRoute(
			"IVRSITEGETGENERICKEYPRESSESINFO",
 			routeTemplate: "v1/user/{id}/Ivrsitegetgenerickeypressesinfo",
			defaults: new { controller = "Ivrsitegetgenerickeypressesinfo" });
			
			config.Routes.MapHttpRoute(
			"IVRSITECREATEGENERICKEYPRESSESINFO",
 			routeTemplate: "v1/user/{id}/Ivrsitecreategenerickeypressesinfo",
			defaults: new { controller = "Ivrsitecreategenerickeypressesinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCGETCONFERENCEMASTERINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccgetconferencemasterinfo",
			defaults: new { controller = "Urmyaccgetconferencemasterinfo" });
			
			config.Routes.MapHttpRoute(
			"URMAGETSITEINFO",
 			routeTemplate: "v1/user/{id}/Urmagetsiteinfo",
			defaults: new { controller = "Urmagetsiteinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTHUNTGROUPUPDATE",
 			routeTemplate: "v1/user/{id}/Urmyaccounthuntgroupupdate",
			defaults: new { controller = "Urmyaccounthuntgroupupdate" });
			
			config.Routes.MapHttpRoute(
			"URUSERGETPHONEINFO",
 			routeTemplate: "v1/user/{id}/Urusergetphoneinfo",
			defaults: new { controller = "Urusergetphoneinfo" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEIVRMENUUPDATE",
 			routeTemplate: "v1/user/{id}/Urmultisiteivrmenuupdate",
			defaults: new { controller = "Urmultisiteivrmenuupdate" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEGETPHONENUMBER",
 			routeTemplate: "v1/user/{id}/Urmultisitegetphonenumber",
			defaults: new { controller = "Urmultisitegetphonenumber" });
			
			config.Routes.MapHttpRoute(
			"URAPPUPDATENOTIFICATION",
 			routeTemplate: "v1/user/{id}/Urappupdatenotification",
			defaults: new { controller = "Urappupdatenotification" });
			
			config.Routes.MapHttpRoute(
			"URAPPNOTIFICATIONTYPEGET",
 			routeTemplate: "v1/user/{id}/Urappnotificationtypeget",
			defaults: new { controller = "Urappnotificationtypeget" });
			
			config.Routes.MapHttpRoute(
			"URAPPGETNOTIFICATIONSETTING",
 			routeTemplate: "v1/user/{id}/Urappgetnotificationsetting",
			defaults: new { controller = "Urappgetnotificationsetting" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEGETNEXTSITEEXTENSION",
 			routeTemplate: "v1/user/{id}/Urmultisitegetnextsiteextension",
			defaults: new { controller = "Urmultisitegetnextsiteextension" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEVALIDATESITEEXTENSION",
 			routeTemplate: "v1/user/{id}/Urmultisitevalidatesiteextension",
			defaults: new { controller = "Urmultisitevalidatesiteextension" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEIVRDELETEKEYPRESS",
 			routeTemplate: "v1/user/{id}/Urmultisiteivrdeletekeypress",
			defaults: new { controller = "Urmultisiteivrdeletekeypress" });
			
			config.Routes.MapHttpRoute(
			"URIVRCHECKVALIDATION",
 			routeTemplate: "v1/user/{id}/Urivrcheckvalidation",
			defaults: new { controller = "Urivrcheckvalidation" });
			
			config.Routes.MapHttpRoute(
			"URAPPNOTIFICATIONSOUNDGET",
 			routeTemplate: "v1/user/{id}/Urappnotificationsoundget",
			defaults: new { controller = "Urappnotificationsoundget" });
			
			config.Routes.MapHttpRoute(
			"URAPPNOTIFICATIONJOINNOWGET",
 			routeTemplate: "v1/user/{id}/Urappnotificationjoinnowget",
			defaults: new { controller = "Urappnotificationjoinnowget" });
			
			config.Routes.MapHttpRoute(
			"URAPPNOTIFICATIONTEAMMSGGET",
 			routeTemplate: "v1/user/{id}/Urappnotificationteammsgget",
			defaults: new { controller = "Urappnotificationteammsgget" });
			
			config.Routes.MapHttpRoute(
			"URAPPEMAILNOTIFICATIONDIRECTMSGGET",
 			routeTemplate: "v1/user/{id}/Urappemailnotificationdirectmsgget",
			defaults: new { controller = "Urappemailnotificationdirectmsgget" });
			
			config.Routes.MapHttpRoute(
			"URAPPEMAILNOTIFICATIONTEAMMSGGET",
 			routeTemplate: "v1/user/{id}/Urappemailnotificationteammsgget",
			defaults: new { controller = "Urappemailnotificationteammsgget" });
			
			config.Routes.MapHttpRoute(
			"URAPPNOTIFICATIONUNREADBADGECOUNTGET",
 			routeTemplate: "v1/user/{id}/Urappnotificationunreadbadgecountget",
			defaults: new { controller = "Urappnotificationunreadbadgecountget" });
			
			config.Routes.MapHttpRoute(
			"URAPPGETCALLSRINGTYPE",
 			routeTemplate: "v1/user/{id}/Urappgetcallsringtype",
			defaults: new { controller = "Urappgetcallsringtype" });
			
			config.Routes.MapHttpRoute(
			"URAPPGETCALLSFORWARDTYPE",
 			routeTemplate: "v1/user/{id}/Urappgetcallsforwardtype",
			defaults: new { controller = "Urappgetcallsforwardtype" });
			
			config.Routes.MapHttpRoute(
			"URAPPGETINCOMINGCALLSSETTINGTYPE",
 			routeTemplate: "v1/user/{id}/Urappgetincomingcallssettingtype",
			defaults: new { controller = "Urappgetincomingcallssettingtype" });
			
			config.Routes.MapHttpRoute(
			"URAPPGETINCOMINGCALLHANDLINGFORWARDING",
 			routeTemplate: "v1/user/{id}/Urappgetincomingcallhandlingforwarding",
			defaults: new { controller = "Urappgetincomingcallhandlingforwarding" });
			
			config.Routes.MapHttpRoute(
			"URAPPCREATEINCOMINGCALLCUSTOMNUMBER",
 			routeTemplate: "v1/user/{id}/Urappcreateincomingcallcustomnumber",
			defaults: new { controller = "Urappcreateincomingcallcustomnumber" });
			
			config.Routes.MapHttpRoute(
			"URAPPNOTIFICATIONTEMPLATELIST",
 			routeTemplate: "v1/user/{id}/Urappnotificationtemplatelist",
			defaults: new { controller = "Urappnotificationtemplatelist" });
			
			config.Routes.MapHttpRoute(
			"URMULTISITEGETEXTENSION",
 			routeTemplate: "v1/user/{id}/Urmultisitegetextension",
			defaults: new { controller = "Urmultisitegetextension" });
			
			config.Routes.MapHttpRoute(
			"URECOMGETCOMPANYFEEBACKINFO",
 			routeTemplate: "v1/user/{id}/Urecomgetcompanyfeebackinfo",
			defaults: new { controller = "Urecomgetcompanyfeebackinfo" });
			
			config.Routes.MapHttpRoute(
			"URAPPADDCUSTOMNUMBERFLIPINFO",
 			routeTemplate: "v1/user/{id}/Urappaddcustomnumberflipinfo",
			defaults: new { controller = "Urappaddcustomnumberflipinfo" });
			
			config.Routes.MapHttpRoute(
			"URAPPCUSTOMNUMBERFLIPINFOGET",
 			routeTemplate: "v1/user/{id}/Urappcustomnumberflipinfoget",
			defaults: new { controller = "Urappcustomnumberflipinfoget" });
			
			config.Routes.MapHttpRoute(
			"URAPPCUSTOMNUMBERFLIPUPDATE",
 			routeTemplate: "v1/user/{id}/Urappcustomnumberflipupdate",
			defaults: new { controller = "Urappcustomnumberflipupdate" });
			
			config.Routes.MapHttpRoute(
			"URAPPCUSTOMNUMBERFLIPREORDER",
 			routeTemplate: "v1/user/{id}/Urappcustomnumberflipreorder",
			defaults: new { controller = "Urappcustomnumberflipreorder" });
			
			config.Routes.MapHttpRoute(
			"URUPDATENOCALLERIDEXTNBLOCKNUMBER",
 			routeTemplate: "v1/user/{id}/Urupdatenocalleridextnblocknumber",
			defaults: new { controller = "Urupdatenocalleridextnblocknumber" });
			
			config.Routes.MapHttpRoute(
			"URGETNOCALLERIDEXTNBLOCKNUMBER",
 			routeTemplate: "v1/user/{id}/Urgetnocalleridextnblocknumber",
			defaults: new { controller = "Urgetnocalleridextnblocknumber" });
			
			config.Routes.MapHttpRoute(
			"URMYACCADDPHONEGETDIRECTNUMBER",
 			routeTemplate: "v1/user/{id}/Urmyaccaddphonegetdirectnumber",
			defaults: new { controller = "Urmyaccaddphonegetdirectnumber" });
			
			config.Routes.MapHttpRoute(
			"URMYACCUPDATEDEXTNCONFERENCEINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccupdatedextnconferenceinfo",
			defaults: new { controller = "Urmyaccupdatedextnconferenceinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETMAINNUMBER",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetmainnumber",
			defaults: new { controller = "Urmyaccountgetmainnumber" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETPERSONALINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetpersonalinfo",
			defaults: new { controller = "Urmyaccountgetpersonalinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTSAVEVOICEMAILGREETINGS",
 			routeTemplate: "v1/user/{id}/Urmyaccountsavevoicemailgreetings",
			defaults: new { controller = "Urmyaccountsavevoicemailgreetings" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETVOICEMAILGREETINGS",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetvoicemailgreetings",
			defaults: new { controller = "Urmyaccountgetvoicemailgreetings" });
			
			config.Routes.MapHttpRoute(
			"URECOMVALIDATEUSEREXTENSIONSTATUS",
 			routeTemplate: "v1/user/{id}/Urecomvalidateuserextensionstatus",
			defaults: new { controller = "Urecomvalidateuserextensionstatus" });
			
			config.Routes.MapHttpRoute(
			"URECOMONBOARDGETADMINDETAILSINFO",
 			routeTemplate: "v1/user/{id}/Urecomonboardgetadmindetailsinfo",
			defaults: new { controller = "Urecomonboardgetadmindetailsinfo" });
			
			config.Routes.MapHttpRoute(
			"URECOMONBOARDGETUSERDETAILS",
 			routeTemplate: "v1/user/{id}/Urecomonboardgetuserdetails",
			defaults: new { controller = "Urecomonboardgetuserdetails" });
			
			config.Routes.MapHttpRoute(
			"URECOMONBOARDSAVEEMERGDETAILS",
 			routeTemplate: "v1/user/{id}/Urecomonboardsaveemergdetails",
			defaults: new { controller = "Urecomonboardsaveemergdetails" });
			
			config.Routes.MapHttpRoute(
			"URMYACCMOBILENUMBERTYPEINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccmobilenumbertypeinfo",
			defaults: new { controller = "Urmyaccmobilenumbertypeinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCWEBINSERTMOBILENUMBERTYPEINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccwebinsertmobilenumbertypeinfo",
			defaults: new { controller = "Urmyaccwebinsertmobilenumbertypeinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCWEBGETMOBILENUMBERTYPEINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccwebgetmobilenumbertypeinfo",
			defaults: new { controller = "Urmyaccwebgetmobilenumbertypeinfo" });
			
			config.Routes.MapHttpRoute(
			"URONBOARDINSERTUSERDETAILS",
 			routeTemplate: "v1/user/{id}/Uronboardinsertuserdetails",
			defaults: new { controller = "Uronboardinsertuserdetails" });
			
			config.Routes.MapHttpRoute(
			"URONBOARDGETUSERDETAILS",
 			routeTemplate: "v1/user/{id}/Uronboardgetuserdetails",
			defaults: new { controller = "Uronboardgetuserdetails" });
			
			config.Routes.MapHttpRoute(
			"URUSERINSERTBIILINGADDRESS",
 			routeTemplate: "v1/user/{id}/Uruserinsertbiilingaddress",
			defaults: new { controller = "Uruserinsertbiilingaddress" });
			
			config.Routes.MapHttpRoute(
			"URSENDMESSAGEREQUEST",
 			routeTemplate: "v1/user/{id}/Ursendmessagerequest",
			defaults: new { controller = "Ursendmessagerequest" });
			
			config.Routes.MapHttpRoute(
			"URMAAPPARCHIVECONTACTSAVE",
 			routeTemplate: "v1/user/{id}/Urmaapparchivecontactsave",
			defaults: new { controller = "Urmaapparchivecontactsave" });
			
			config.Routes.MapHttpRoute(
			"URMAAPPGETNOTIFICATIONSINFO",
 			routeTemplate: "v1/user/{id}/Urmaappgetnotificationsinfo",
			defaults: new { controller = "Urmaappgetnotificationsinfo" });
			
			config.Routes.MapHttpRoute(
			"URMAAPPGETMUTENOTIFICATIONSINFO",
 			routeTemplate: "v1/user/{id}/Urmaappgetmutenotificationsinfo",
			defaults: new { controller = "Urmaappgetmutenotificationsinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCREATEUPDATECCCARDINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccountcreateupdatecccardinfo",
			defaults: new { controller = "Urmyaccountcreateupdatecccardinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCREATEUPDATEDIRECTDEBITINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccountcreateupdatedirectdebitinfo",
			defaults: new { controller = "Urmyaccountcreateupdatedirectdebitinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETCCCARDINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetcccardinfo",
			defaults: new { controller = "Urmyaccountgetcccardinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETDIRECTDEBITINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetdirectdebitinfo",
			defaults: new { controller = "Urmyaccountgetdirectdebitinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETSAVEDCCCARDINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetsavedcccardinfo",
			defaults: new { controller = "Urmyaccountgetsavedcccardinfo" });
			
			config.Routes.MapHttpRoute(
			"SMSCONTENTGETTEXT",
 			routeTemplate: "v1/user/{id}/Smscontentgettext",
			defaults: new { controller = "Smscontentgettext" });
			
			config.Routes.MapHttpRoute(
			"URMYACCWEBGETUSERPREFEXTENSIONINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccwebgetuserprefextensioninfo",
			defaults: new { controller = "Urmyaccwebgetuserprefextensioninfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCREATEREFERALDETAILS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcreatereferaldetails",
			defaults: new { controller = "Urmyaccountcreatereferaldetails" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETREFERALDETAILS",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetreferaldetails",
			defaults: new { controller = "Urmyaccountgetreferaldetails" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTREFERALDASHBOARDINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccountreferaldashboardinfo",
			defaults: new { controller = "Urmyaccountreferaldashboardinfo" });
			
			config.Routes.MapHttpRoute(
			"URECOMINSERTUSERRANGEMASTERDETAIL",
 			routeTemplate: "v1/user/{id}/Urecominsertuserrangemasterdetail",
			defaults: new { controller = "Urecominsertuserrangemasterdetail" });
			
			config.Routes.MapHttpRoute(
			"URECOMGETUSERRANGEMASTERDETAIL",
 			routeTemplate: "v1/user/{id}/Urecomgetuserrangemasterdetail",
			defaults: new { controller = "Urecomgetuserrangemasterdetail" });
			
			config.Routes.MapHttpRoute(
			"URECOMGETPLANPRICINGRANGEUSERINFO",
 			routeTemplate: "v1/user/{id}/Urecomgetplanpricingrangeuserinfo",
			defaults: new { controller = "Urecomgetplanpricingrangeuserinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETADDONFEATURESBYPLAN",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetaddonfeaturesbyplan",
			defaults: new { controller = "Urmyaccountgetaddonfeaturesbyplan" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETFEATUREDETAILSBYPLAN",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetfeaturedetailsbyplan",
			defaults: new { controller = "Urmyaccountgetfeaturedetailsbyplan" });
			
			config.Routes.MapHttpRoute(
			"URTARIFFGETTARIFFDETAILS",
 			routeTemplate: "v1/user/{id}/Urtariffgettariffdetails",
			defaults: new { controller = "Urtariffgettariffdetails" });
			
			config.Routes.MapHttpRoute(
			"URVMSAVEVOICEMAILMSGINFO",
 			routeTemplate: "v1/user/{id}/Urvmsavevoicemailmsginfo",
			defaults: new { controller = "Urvmsavevoicemailmsginfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTPURCHASEDDEVICEINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccountpurchaseddeviceinfo",
			defaults: new { controller = "Urmyaccountpurchaseddeviceinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTUPDATEONBOARDINGLINK",
 			routeTemplate: "v1/user/{id}/Urmyaccountupdateonboardinglink",
			defaults: new { controller = "Urmyaccountupdateonboardinglink" });
			
			config.Routes.MapHttpRoute(
			"URMYACCGETWEBRATESCOUNTRYSUMMARYINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccgetwebratescountrysummaryinfo",
			defaults: new { controller = "Urmyaccgetwebratescountrysummaryinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCGETWEBRATESCOUNTRYPREFIXINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccgetwebratescountryprefixinfo",
			defaults: new { controller = "Urmyaccgetwebratescountryprefixinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTTOPUPGETDENOMINATIONLIST",
 			routeTemplate: "v1/user/{id}/Urmyaccounttopupgetdenominationlist",
			defaults: new { controller = "Urmyaccounttopupgetdenominationlist" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTDOTOPUPCDCONLINE",
 			routeTemplate: "v1/user/{id}/Urmyaccountdotopupcdconline",
			defaults: new { controller = "Urmyaccountdotopupcdconline" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTAUTOTOPUPENABLEDISABLE",
 			routeTemplate: "v1/user/{id}/Urmyaccountautotopupenabledisable",
			defaults: new { controller = "Urmyaccountautotopupenabledisable" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETADDONLISTBYPLAN",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetaddonlistbyplan",
			defaults: new { controller = "Urmyaccountgetaddonlistbyplan" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCOMPANYSAVEADDONINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccountcompanysaveaddoninfo",
			defaults: new { controller = "Urmyaccountcompanysaveaddoninfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETADDEDADDONLIST",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetaddedaddonlist",
			defaults: new { controller = "Urmyaccountgetaddedaddonlist" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETUSEREXTENSION",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetuserextension",
			defaults: new { controller = "Urmyaccountgetuserextension" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTASSIGNADDONEXTENSION",
 			routeTemplate: "v1/user/{id}/Urmyaccountassignaddonextension",
			defaults: new { controller = "Urmyaccountassignaddonextension" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTRESETPASSWORDBYADMIN",
 			routeTemplate: "v1/user/{id}/Urmyaccountresetpasswordbyadmin",
			defaults: new { controller = "Urmyaccountresetpasswordbyadmin" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTUNIQUEEXTENSIONVALIDATION",
 			routeTemplate: "v1/user/{id}/Urmyaccountuniqueextensionvalidation",
			defaults: new { controller = "Urmyaccountuniqueextensionvalidation" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTPURCHASELISCENCEDUSER",
 			routeTemplate: "v1/user/{id}/Urmyaccountpurchaseliscenceduser",
			defaults: new { controller = "Urmyaccountpurchaseliscenceduser" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTASSIGNLICENCEUSERTOEXISTINGEXTN",
 			routeTemplate: "v1/user/{id}/Urmyaccountassignlicenceusertoexistingextn",
			defaults: new { controller = "Urmyaccountassignlicenceusertoexistingextn" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTUSERMGTCREATEROLE",
 			routeTemplate: "v1/user/{id}/Urmyaccountusermgtcreaterole",
			defaults: new { controller = "Urmyaccountusermgtcreaterole" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTUSERMGTGETROLE",
 			routeTemplate: "v1/user/{id}/Urmyaccountusermgtgetrole",
			defaults: new { controller = "Urmyaccountusermgtgetrole" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTUSERMGTGETMODULE",
 			routeTemplate: "v1/user/{id}/Urmyaccountusermgtgetmodule",
			defaults: new { controller = "Urmyaccountusermgtgetmodule" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETONBOARDINGLINK",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetonboardinglink",
			defaults: new { controller = "Urmyaccountgetonboardinglink" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTDELETECARDDIRECTDEBITACCOUNT",
 			routeTemplate: "v1/user/{id}/Urmyaccountdeletecarddirectdebitaccount",
			defaults: new { controller = "Urmyaccountdeletecarddirectdebitaccount" });
			
			config.Routes.MapHttpRoute(
			"URMYACCGETCOUNTRYLISTINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccgetcountrylistinfo",
			defaults: new { controller = "Urmyaccgetcountrylistinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCWEBUPDATEAUTOCALLRECORD",
 			routeTemplate: "v1/user/{id}/Urmyaccwebupdateautocallrecord",
			defaults: new { controller = "Urmyaccwebupdateautocallrecord" });
			
			config.Routes.MapHttpRoute(
			"URMYACCRULEENABLEAUTOCALLRECORD",
 			routeTemplate: "v1/user/{id}/Urmyaccruleenableautocallrecord",
			defaults: new { controller = "Urmyaccruleenableautocallrecord" });
			
			config.Routes.MapHttpRoute(
			"URMYACCRULEGETAUTOCALLRECORD",
 			routeTemplate: "v1/user/{id}/Urmyaccrulegetautocallrecord",
			defaults: new { controller = "Urmyaccrulegetautocallrecord" });
			
			config.Routes.MapHttpRoute(
			"URMAGETAVAILABLECONFERENCENUMBERDTL",
 			routeTemplate: "v1/user/{id}/Urmagetavailableconferencenumberdtl",
			defaults: new { controller = "Urmagetavailableconferencenumberdtl" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCREATECALLQUEUEINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccountcreatecallqueueinfo",
			defaults: new { controller = "Urmyaccountcreatecallqueueinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUECREATEMEMBERS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuecreatemembers",
			defaults: new { controller = "Urmyaccountcallqueuecreatemembers" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETCALLQUEUEINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetcallqueueinfo",
			defaults: new { controller = "Urmyaccountgetcallqueueinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUEGETMEMBERS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuegetmembers",
			defaults: new { controller = "Urmyaccountcallqueuegetmembers" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUESAVEGREETINGS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuesavegreetings",
			defaults: new { controller = "Urmyaccountcallqueuesavegreetings" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETCALLQUEUEGREETINGS",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetcallqueuegreetings",
			defaults: new { controller = "Urmyaccountgetcallqueuegreetings" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUEGETSETTINGS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuegetsettings",
			defaults: new { controller = "Urmyaccountcallqueuegetsettings" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUEDOSETTINGS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuedosettings",
			defaults: new { controller = "Urmyaccountcallqueuedosettings" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUECREATECALLHANDLING",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuecreatecallhandling",
			defaults: new { controller = "Urmyaccountcallqueuecreatecallhandling" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETCALLCALLHANDLINGMEMBER",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetcallcallhandlingmember",
			defaults: new { controller = "Urmyaccountgetcallcallhandlingmember" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETCALLQUEUEDRP",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetcallqueuedrp",
			defaults: new { controller = "Urmyaccountgetcallqueuedrp" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUECREATEOVERFLOWSETTINGS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuecreateoverflowsettings",
			defaults: new { controller = "Urmyaccountcallqueuecreateoverflowsettings" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUEGETOVERFLOWSETTINGS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuegetoverflowsettings",
			defaults: new { controller = "Urmyaccountcallqueuegetoverflowsettings" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUEGETVOICEMAILSETTINGS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuegetvoicemailsettings",
			defaults: new { controller = "Urmyaccountcallqueuegetvoicemailsettings" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUECREATEVOICEMAILSETTINGS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuecreatevoicemailsettings",
			defaults: new { controller = "Urmyaccountcallqueuecreatevoicemailsettings" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUESAVEVOICEMAILGREETINGS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuesavevoicemailgreetings",
			defaults: new { controller = "Urmyaccountcallqueuesavevoicemailgreetings" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUESAVECUSTOMHOURS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuesavecustomhours",
			defaults: new { controller = "Urmyaccountcallqueuesavecustomhours" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUEGETCUSTOMHOURS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuegetcustomhours",
			defaults: new { controller = "Urmyaccountcallqueuegetcustomhours" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUEGETVOICEMAILGREETINGS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuegetvoicemailgreetings",
			defaults: new { controller = "Urmyaccountcallqueuegetvoicemailgreetings" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUEGETDIDNUMBERS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuegetdidnumbers",
			defaults: new { controller = "Urmyaccountcallqueuegetdidnumbers" });
			
			config.Routes.MapHttpRoute(
			"URMYACCGETDIRECTNUMBERBYEXTN",
 			routeTemplate: "v1/user/{id}/Urmyaccgetdirectnumberbyextn",
			defaults: new { controller = "Urmyaccgetdirectnumberbyextn" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCALLQUEUEGETOVERFLOWSETTINGSINFO",
 			routeTemplate: "v1/user/{id}/Urmyaccountcallqueuegetoverflowsettingsinfo",
			defaults: new { controller = "Urmyaccountcallqueuegetoverflowsettingsinfo" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTDELETECALLQUEUEMEMBER",
 			routeTemplate: "v1/user/{id}/Urmyaccountdeletecallqueuemember",
			defaults: new { controller = "Urmyaccountdeletecallqueuemember" });
			
			config.Routes.MapHttpRoute(
			"URVALIDATEUSERBYMOBILENO",
 			routeTemplate: "v1/user/{id}/Urvalidateuserbymobileno",
			defaults: new { controller = "Urvalidateuserbymobileno" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTCREATEDEFAULTIVRSETTINGS",
 			routeTemplate: "v1/user/{id}/Urmyaccountcreatedefaultivrsettings",
			defaults: new { controller = "Urmyaccountcreatedefaultivrsettings" });
			
			config.Routes.MapHttpRoute(
			"URMYACCOUNTGETDEFAULTIVRSETTINGS",
 			routeTemplate: "v1/user/{id}/Urmyaccountgetdefaultivrsettings",
			defaults: new { controller = "Urmyaccountgetdefaultivrsettings" });
			//RegisterMapRoute
            config.Routes.MapHttpRoute(
            "UNIFIEDRINGMYACCOUNTWEBAPIINSERTTOKENMASTER",
            routeTemplate: "v1/user/unifiedringmyaccountwebapiinserttokenmaster",
            defaults: new { controller = "unifiedringmyaccountwebapiinserttokenmaster" });



            config.Routes.MapHttpRoute(
            "URMYACCWEBMOHDETAILSSAVE",
            routeTemplate: "v1/user/{id}/Urmyaccwebmohdetailssave",
            defaults: new { controller = "Urmyaccwebmohdetailssave" });

            config.Routes.MapHttpRoute(
          "URMYACCWEBUPDATECALLRECORDING",
          routeTemplate: "v1/user/{id}/Urmyaccwebupdatecallrecording",
          defaults: new { controller = "Urmyaccwebupdatecallrecording" });

            config.Routes.MapHttpRoute(
          "URMYACCWEBGETCALLRECORDINGDTL",
          routeTemplate: "v1/user/{id}/Urmyaccwebgetcallrecordingdtl",
          defaults: new { controller = "Urmyaccwebgetcallrecordingdtl" });


            config.Routes.MapHttpRoute(
           "URMYACCWEBUPDATECOMPANYREGIONALSETTING",
           routeTemplate: "v1/user/{id}/Urmyaccwebupdatecompanyregionalsetting",
           defaults: new { controller = "Urmyaccwebupdatecompanyregionalsetting" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBGETCOMPANYREGIONALSETTINGDTL",
            routeTemplate: "v1/user/{id}/Urmyaccwebgetcompanyregionalsettingdtl",
            defaults: new { controller = "Urmyaccwebgetcompanyregionalsettingdtl" });

            config.Routes.MapHttpRoute(
            "URMYACCWEBUPDATEEXTENSIONLOGINPASSWORD",
            routeTemplate: "v1/user/{id}/Urmyaccwebupdateextensionloginpassword",
            defaults: new { controller = "Urmyaccwebupdateextensionloginpassword" });

            config.Routes.MapHttpRoute(
            "URMAPARKLOCATIONINSERTGROUP",
            routeTemplate: "v1/user/{id}/Urmaparklocationinsertgroup",
            defaults: new { controller = "Urmaparklocationinsertgroup" });

            config.Routes.MapHttpRoute(
            "URMALOGINVALIDATION",
            routeTemplate: "v1/user/{id}/urmaloginvalidation",
            defaults: new { controller = "urmaloginvalidation" });

            config.Routes.MapHttpRoute(
           "URMALOGINVALIDATIONV2",
           routeTemplate: "v1/user/{id}/urmaloginvalidationv2",
           defaults: new { controller = "urmaloginvalidationv2" });

            config.Routes.MapHttpRoute(
          "UREXTNGETDNDTYPE",
          routeTemplate: "v1/user/{id}/urextngetdndtype",
          defaults: new { controller = "urextngetdndtype" });

            config.Routes.MapHttpRoute(
            "URAPPLOGINFORGOTPWD",
            routeTemplate: "v1/user/{id}/Urapploginforgotpwd",
            defaults: new { controller = "Urapploginforgotpwd" });

            config.Routes.MapHttpRoute(
            "URMAINSERTPHONEADDNUMBERAPP",
            routeTemplate: "v1/user/{id}/UrmainsertphoneaddnumberApp",
            defaults: new { controller = "UrmainsertphoneaddnumberApp" });

            config.Routes.MapHttpRoute(
            "URAPPLINKAPP",
            routeTemplate: "v1/user/{id}/Urapplinkapp",
            defaults: new { controller = "Urapplinkapp" });

            config.Routes.MapHttpRoute(
            "URUPLOADFILES",
            routeTemplate: "v1/user/{id}/UrUploadFiles",
            defaults: new { controller = "UrUploadFiles" });

            config.Routes.MapHttpRoute(
            "URDIRECTORYUPDATE",
            routeTemplate: "v1/user/{id}/UrDirectoryUpdate",
            defaults: new { controller = "UrDirectoryUpdate" });

            config.Routes.MapHttpRoute(
       "URGETDESKPHONEMODEL",
       routeTemplate: "v1/user/{id}/Urgetdeskphonemodel",
       defaults: new { controller = "Urgetdeskphonemodel" });

            //Added from unifiedringappapi
            config.Routes.MapHttpRoute(
        "urmalocalnumberupdate",
        routeTemplate: "v1/user/{id}/urmalocalnumberupdate",
        defaults: new { controller = "urmalocalnumberupdate" });

            config.Routes.MapHttpRoute(
           "urmaworkcontactget",
           routeTemplate: "v1/user/{id}/urmaworkcontactget",
           defaults: new { controller = "urmaworkcontactget" });

            config.Routes.MapHttpRoute(
           "URMAWORKCONTACTGETV2",
           routeTemplate: "v2/user/{id}/urmaworkcontactget",
           defaults: new { controller = "urmaworkcontactgetv2" });

            config.Routes.MapHttpRoute(
        "URMAAPPMUTEBLOCKCONTACTSAVE",
        routeTemplate: "v1/user/{id}/Urmaappmuteblockcontactsave",
        defaults: new { controller = "Urmaappmuteblockcontactsave" });

            config.Routes.MapHttpRoute(
          "urmacallparkidget",
          routeTemplate: "v1/user/{id}/Urmacallparkidget",
          defaults: new { controller = "Urmacallparkidget" });

            config.Routes.MapHttpRoute(
         "ursubscription",
         routeTemplate: "v1/user/{id}/ursubscription",
         defaults: new { controller = "ursubscription" });

            config.Routes.MapHttpRoute(
        "URMAAPPFAVOURITECONTACTSAVE",
        routeTemplate: "v1/user/{id}/Urmaappfavouritecontactsave",
        defaults: new { controller = "Urmaappfavouritecontactsave" });

        config.Routes.MapHttpRoute(
        "URMAAPPFAVOURITECONTACTSAVETEST",
        routeTemplate: "v1/user/{id}/Urmaappfavouritecontactsavetest",
        defaults: new { controller = "Urmaappfavouritecontactsavetest" });

        config.Routes.MapHttpRoute(
        "URPUSHNOTIFICATION",
        routeTemplate: "v1/user/urpushnotification",
        defaults: new { controller = "urpushnotification" });


        config.Routes.MapHttpRoute(
        "URPUSHNOTIFICATIONFCM",
        routeTemplate: "v1/user/urpushnotificationFCM",
        defaults: new { controller = "urpushnotificationFCM" });


        config.Routes.MapHttpRoute(
        "URPUSHNOTIFICATIONNEW",
        routeTemplate: "v1/user/urpushnotificationnew",
        defaults: new { controller = "urpushnotificationnew" });

        config.Routes.MapHttpRoute(
        "URVMPUSHNOTIFICATION",
        routeTemplate: "v1/user/urvmpushnotification",
        defaults: new { controller = "urvmpushnotification" });

        //urmagoogleloginvalidationController
        config.Routes.MapHttpRoute(
        "URMAGOOGLELOGINVALIDATION",
        routeTemplate: "v1/user/{id}/urmagoogleloginvalidation",
        defaults: new { controller = "urmagoogleloginvalidation" });

        config.Routes.MapHttpRoute(
        "SAVEGOOGLEUSER",
        routeTemplate: "v1/user/SaveGoogleUser",
        defaults: new { controller = "SaveGoogleUser" });
        //urmagoogleloginvalidationController
          
        config.Routes.MapHttpRoute(
        "GETGOOGLEUSER",
        routeTemplate: "v1/user/{id}/GetGoogleUser",
        defaults: new { controller = "GetGoogleUser" });

        config.Routes.MapHttpRoute(
       "URCRMUSERGETDETAILS",
       routeTemplate: "v1/user/{id}/Urcrmusergetdetails",
       defaults: new { controller = "Urcrmusergetdetails" }); 
           
       config.Routes.MapHttpRoute(
       "URPORTALGETNEWPLANINFO",
       routeTemplate: "v1/user/{id}/URportalgetnewplaninfo",
       defaults: new { controller = "URportalgetnewplaninfo" });
         
        config.Routes.MapHttpRoute(
        "URINSERTHUBSPOT",
        routeTemplate: "v1/user/{id}/UrinsertHubSpot",
        defaults: new { controller = "UrinsertHubSpot" });

        config.Routes.MapHttpRoute(
        "URUPDATEHUBSPOT",
        routeTemplate: "v1/user/{id}/UrUpdateHubSpot",
        defaults: new { controller = "UrUpdateHubSpot" });
            
        config.Routes.MapHttpRoute(
        "URCREATEANDUPDATEMANDATE",
        routeTemplate: "v1/user/{id}/Urcreateandupdatemandate",
        defaults: new { controller = "Urcreateandupdatemandate" });

        config.Routes.MapHttpRoute(
        "URMYACCWEBSAVEUSERPERSONALDTL",
        routeTemplate: "v1/user/{id}/Urmyaccwebsaveuserpersonaldtl",
        defaults: new { controller = "Urmyaccwebsaveuserpersonaldtl" });


        config.Routes.MapHttpRoute(
        "URWEBINSERTTMPPAYMENTINFO",
        routeTemplate: "v1/user/{id}/Urwebinserttmppaymentinfo",
        defaults: new { controller = "Urwebinserttmppaymentinfo" });

        config.Routes.MapHttpRoute(
        "URWEBGETTMPPAYMENTINFO",
        routeTemplate: "v1/user/{id}/Urwebgettmppaymentinfo",
        defaults: new { controller = "Urwebgettmppaymentinfo" });

        config.Routes.MapHttpRoute(
        "URWEBDELETETEMPTRANSACTIONINFO",
        routeTemplate: "v1/user/{id}/Urwebdeletetemptransactioninfo",
        defaults: new { controller = "Urwebdeletetemptransactioninfo" });


        config.Routes.MapHttpRoute(
        "URMYACCWEBGETUSERPERSONALDTL",
        routeTemplate: "v1/user/{id}/Urmyaccwebgetuserpersonaldtl",
        defaults: new { controller = "Urmyaccwebgetuserpersonaldtl" });
       
         config.Routes.MapHttpRoute(
        "URMYACCSETSENDVOICEMAILTOEMAIL",
        routeTemplate: "v1/user/{id}/Urmyaccsetsendvoicemailtoemail",
        defaults: new { controller = "Urmyaccsetsendvoicemailtoemail" });
                  
        config.Routes.MapHttpRoute(
        "URMYACCGETSENDVOICEMAILTOEMAIL",
        routeTemplate: "v1/user/{id}/Urmyaccgetsendvoicemailtoemail",
        defaults: new { controller = "Urmyaccgetsendvoicemailtoemail" });            
            
        config.Routes.MapHttpRoute(
        "URAPPPUBLISHHUBSPOT",
        routeTemplate: "v1/user/{id}/UrappPublishHubspot",
        defaults: new { controller = "UrappPublishHubspot" });

        config.Routes.MapHttpRoute(
         "URPUSHNOTIFICATIONCONTACTREFRESH",
         routeTemplate: "v1/user/urpushnotificationContactRefresh",
         defaults: new { controller = "urpushnotificationContactRefresh" });


        config.Routes.MapHttpRoute(
     "URAPPRXSAVEDCARDPAYMENT",
     routeTemplate: "v1/user/{id}/UrappRxSavedCardPayment",
     defaults: new { controller = "UrappRxSavedCardPayment" });

        }
    }
}































































































































































































